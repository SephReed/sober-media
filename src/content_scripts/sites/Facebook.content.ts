import { setupSingleContentScriptAssert, observeUrlChanges, addSettingEffect, ISettingLogic } from "../common/Utils";
import { ContentScript } from "../../middleware/ContentScriptEnum";
import { blurAllFound, blurTogether, hideAllFound } from "../common/SimpleMutations";
import { rebootCssQueryObserve, setDefaultQueryLogic } from "../common/CssQueryObserve";
import { renderTrustCoin } from "../common/TrustCoin";
import { FB_TRUST_SETTING_PREFIX } from "../../middleware/SettingsId";

if (setupSingleContentScriptAssert(ContentScript.FACEBOOK)) {  

  setDefaultQueryLogic({ not: "fb-disable" });

  observeUrlChanges(async (loc) => {
    if (loc.host.endsWith("facebook.com") === false) { return; }
    blurNotifications();
    removeLogo();
    // removeFbWatchTab();
    // removeFbGamingTab();
    blurGroupsTab();
    blurWatchTab();
    blurGamingTab();
    blurMarketTab();
    
    blurPopularityStats();
    blurEachFeedPost();
    blurPostComments();
  
    if (loc.pathname === "/") {
      blurHomeFeed();
      blurStories();
  
    } else if (loc.pathname.includes("/photo")) {
      blurPhotoComments();
  
    } else if (loc.pathname.includes("/marketplace")) {
      blurMarketplaceFeed();

    } else if(loc.pathname.includes("/watch")) {
      blurWatchFeed();

    } else if(loc.pathname.includes("/gaming")) {
      blurGamingFeed();
    }
    
  });
  
  
  function blurHomeFeed() {
    blurAllFound({
      queryArgs: { 
        cssQuery: `div[role="main"].rq0escxv.l9j0dhe7.du4w35lb.j83agx80.g5gj957u.pmt1y7k9.buofh1pr.hpfvmrgz.taijpn5t.gs1a9yip.owycx6da.btwxx1t3.f7vcsfb0.fjf4s8hc.b6rwyo50.oyrvap6t`,
        settingLogic: "fb-blur-home-feed",
      }
    })
    // blurAllFound(`div[role="main"].rq0escxv.l9j0dhe7.du4w35lb.j83agx80.g5gj957u.pmt1y7k9.buofh1pr.hpfvmrgz.taijpn5t.gs1a9yip.owycx6da.btwxx1t3.f7vcsfb0.fjf4s8hc.b6rwyo50.oyrvap6t`);
  }
  
  function blurStories() {
    blurAllFound({
      queryArgs: { 
        cssQuery: `div[data-pagelet="Stories"]`,
        settingLogic: "fb-blur-stories",
      }
    })
  }
  
  function blurEachFeedPost() {
    rebootCssQueryObserve({
      cssQuery: `div[role="feed"] > div`,
      settingLogic: null,
      cb: ({ domNode }) => {
        let blurredItems: HTMLElement[] = [];
        if (location.pathname.includes("/search")) { return; }

        const nameEl = domNode.querySelector(".pybr56ya.dati1w0a.hv4rvrfc.n851cfcs.btwxx1t3.j83agx80.ll8tlv6m");
        if (!nameEl) { 
          blurredItems.push(domNode);
          doBlur("fb-blur-untrusted");
          return; 
        }

        let trustSettingId: string | undefined;
        const personLink = nameEl.querySelector("h4 a");
        if (personLink) {
          let trustCoin: HTMLElement;

          addSettingEffect({
            settingLogic: { all: [
              { not: "fb-disable"},
              "fb-trust-tokens",
              "fb-blur-untrusted"
            ]},
            effect: () => {
              if (!trustCoin) {
                const name = personLink.textContent as string;
                trustSettingId = `${FB_TRUST_SETTING_PREFIX}${name}`
                trustCoin = renderTrustCoin({
                  settingId: trustSettingId,
                  name,
                });
              }
              (personLink.parentElement as HTMLElement).appendChild(trustCoin);
            },
            undoEffect: () => {
              trustCoin && trustCoin.remove();
            },
          });
        }
  
        let ptr: Element | null = nameEl.parentElement;
        while (ptr && (ptr = ptr.nextElementSibling) && ptr) {
          blurredItems.push(ptr as HTMLElement);
        }

        if (trustSettingId) {
          doBlur({ all: [
            { not: "fb-disable" },
            "fb-blur-untrusted",
            { all: [
              "fb-trust-tokens",
              { not: trustSettingId as any }
            ]}
          ]});
        } else {
          doBlur({ all:[ 
            { not: "fb-disable" },
            "fb-blur-untrusted"
          ]});
        }
  
        function doBlur(blurRules: ISettingLogic) {
          addSettingEffect({
            settingLogic: blurRules,
            effect: () => {
              blurredItems.length && blurTogether({
                domNodes: blurredItems,
                blurArgs: {
                  blur: true,
                  showText: true,
                  textSize: "SMALL",
                  onUnblur: () => {
                    blurredItems = [];
                  }
                }
              })
            },
            undoEffect: () => {
              blurTogether({
                domNodes: blurredItems,
                blurArgs: { blur: false },
              });
            }
          })
        }
      },
      undoCb: () => {}
    })
  }

  function blurPostComments() {
    blurAllFound({
      queryArgs: { 
        cssQuery: `div[role="feed"] > div  .cwj9ozl2.tvmbv18p > ul`,
        settingLogic: "fb-blur-post-comments",
      },
      blurArgs: {
        blur: true,
        textSize: "SMALL"
      }
    })
  }
  
  
  async function blurNotifications() {
    blurAllFound({
      queryArgs: {
        cssQuery: `.bp9cbjyn.j83agx80.datstx6m.taijpn5t.oi9244e8:nth-child(2)`,
        settingLogic: "fb-blur-notifications",
      },
      blurArgs: {
        blur: true,
        showText: false,
      }
    })
  }
  
  async function removeLogo() {
    hideAllFound({
      queryArgs: {
        cssQuery: `.bp9cbjyn.rq0escxv.j83agx80.buofh1pr.byvelhso.dhix69tm.poy2od1o.j9ispegn.kr520xx4.ehxjyohh`,
        settingLogic: "fb-remove-logo",
      }
    });
  }

  function blurMarketTab() {
    blurTab("Marketplace", `fb-blur-tab-market`);
  }

  function blurGroupsTab() {
    blurTab("Groups", `fb-blur-tab-groups`);
  }

  function blurWatchTab() {
    blurTab("Watch", `fb-blur-tab-watch`);
  }

  function blurGamingTab() {
    blurTab("Gaming", `fb-blur-tab-gaming`);
  }

  function blurTab(ariaLabel: string, settingLogic: ISettingLogic) {
    blurAllFound({
      queryArgs: {
        cssQuery: `a[aria-label^="${ariaLabel}"]`,
        settingLogic,
      },
      blurArgs: {
        blur: true,
        showText: false,
      }
    })
  }

  
  function blurPhotoComments() {
    blurAllFound({
      queryArgs: {
        cssQuery: `.q5bimw55.rpm2j7zs.k7i0oixp.gvuykj2m.j83agx80.cbu4d94t.ni8dbmo4.eg9m0zos.l9j0dhe7.du4w35lb.ofs802cu.pohlnb88.dkue75c7.mb9wzai9.d8ncny3e.buofh1pr.g5gj957u.tgvbjcpo.l56l04vs.r57mb794.kh7kg01d.c3g1iek1.k4xni2cv .cwj9ozl2>ul`,
        settingLogic: `fb-blur-photo-comments`
      }
    });
  }
  
  function blurPopularityStats() {
    blurAllFound({
      queryArgs: {
        cssQuery: `.bp9cbjyn.m9osqain.j83agx80.jq4qci2q.bkfpd7mw.a3bd9o3v.kvgmc6g5.wkznzc2l.oygrvhab.dhix69tm.jktsbyx5.rz4wbd8a.osnr6wyh.a8nywdso.s1tcr66n`,
        settingLogic: `fb-blur-popularity-stats`
      },
      blurArgs: { blur: true, showText: false }
    });
  }
  
  
  
  function blurMarketplaceFeed() {
    blurAllFound({
      queryArgs: {
        cssQuery: `div[aria-label="Collection of Marketplace items"]`,
        settingLogic: "fb-blur-marketplace-feed"
      }
    });
  }

  function blurWatchFeed() {
    blurAllFound({
      queryArgs: {
        cssQuery: `#watch_feed`,
        settingLogic: "fb-blur-watch-feed"
      }
    });
  }

  function blurGamingFeed() {
    blurAllFound({
      queryArgs: {
        cssQuery: `div[aria-label="Gaming"][role="main"]`,
        settingLogic: "fb-blur-gaming-feed"
      }
    });
  }
}

