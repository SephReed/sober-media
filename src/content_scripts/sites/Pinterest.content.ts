import { setupSingleContentScriptAssert, observeUrlChanges } from "../common/Utils";
import { ContentScript } from "../../middleware/ContentScriptEnum";
import { setDefaultQueryLogic } from "../common/CssQueryObserve";
import { blurAllFound, hideAllFound } from "../common/SimpleMutations";

if (setupSingleContentScriptAssert(ContentScript.PINTEREST)) {

  setDefaultQueryLogic({ not: "pin-disable" });

  observeUrlChanges(async (loc) => {
    if (loc.host.endsWith("www.pinterest.com") === false) { return; }

    hideLogo();
  
    if (loc.pathname === "/") {
      blurHomeFeed();
    } else if (loc.pathname.startsWith("/today")) {
      blurTodayFeed();
    } else if (loc.pathname.startsWith("/pin")) {
      blurMoreLikeThisFeed();
    }
  });
  

  function hideLogo() {
    hideAllFound({
      queryArgs: {
        cssQuery: `a[aria-label="Home"`,
        settingLogic: "pin-remove-logo"
      }
    })
  }
  
  
  function blurHomeFeed() {
    blurAllFound({
      queryArgs: {
        cssQuery: `div[data-test-id="homefeed-feed"]`,
        settingLogic: "pin-blur-home-feed"
      },
    })
  }
  
  
  function blurTodayFeed() {
    blurAllFound({
      queryArgs: {
        cssQuery: `div[data-test-id="today-tab-feed"]`,
        settingLogic: "pin-blur-today-feed"
      },
    });
  }
  
  
  function blurMoreLikeThisFeed() {
    blurAllFound({
      queryArgs: {
        cssQuery: `div[data-test-id="relatedPins"]`,
        settingLogic: "pin-blur-more-like-feed"
      },
    });
  }
}





