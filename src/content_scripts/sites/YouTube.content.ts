import { setupSingleContentScriptAssert, observeUrlChanges } from "../common/Utils";
import { ContentScript } from "../../middleware/ContentScriptEnum";
import { hideAllFound, blurAllFound } from "../common/SimpleMutations";
import { setDefaultQueryLogic } from "../common/CssQueryObserve";

if (setupSingleContentScriptAssert(ContentScript.YOUTUBE)) {
  setDefaultQueryLogic({not: "ytd-disable"});

  observeUrlChanges(async (loc) => {
    if (loc.host.endsWith("youtube.com") === false) { return; }
    removeLogo();
    removeTrendingTab();

    if (loc.pathname === "/") {
      blurHomeFeed();
    } else if (loc.pathname === "/watch") {
      blurRecommendations();
      blurComments();
    }
  })

  function removeLogo() {
    hideAllFound({
      queryArgs: {
        cssQuery: `ytd-topbar-logo-renderer#logo`,
        settingLogic: "ytd-remove-logo"
      }
    })
  }

  function removeTrendingTab() {
    hideAllFound({
      queryArgs: {
        cssQuery: `a#endpoint[title="Trending"]`,
        settingLogic: "ytd-no-trending"
      }
    })
  }
  
  
  function blurHomeFeed() {
    blurAllFound({
      queryArgs: {
        cssQuery: `ytd-browse[page-subtype="home"] #contents`,
        settingLogic: "ytd-blur-home-screen-feed"
      }
    });
  }
  
  
  function blurRecommendations() {
    blurAllFound({
      queryArgs: {
        cssQuery: `ytd-watch-flexy #items`,
        settingLogic: "ytd-blur-recommendations"
      }
    });
  }
  
  
  function blurComments() {
    blurAllFound({
      queryArgs: {
        cssQuery: `ytd-comments#comments`,
        settingLogic: "ytd-blur-comments"
      }
    });
  }
}
