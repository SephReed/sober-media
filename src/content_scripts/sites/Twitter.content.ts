import { setupSingleContentScriptAssert, observeUrlChanges, addSettingEffect, ISettingLogic } from "../common/Utils";
import { ContentScript } from "../../middleware/ContentScriptEnum";
import { blurChildrenAsGroup, blurAllFound, hideAllFound, setElementBlur } from "../common/SimpleMutations";
import { rebootCssQueryObserve, setDefaultQueryLogic } from "../common/CssQueryObserve";
import { TWIT_TRUST_SETTING_PREFIX } from "../../middleware/SettingsId";
import { renderTrustCoin } from "../common/TrustCoin";

if (setupSingleContentScriptAssert(ContentScript.TWITTER)) {

  setDefaultQueryLogic({not: `twit-disable`});

  const knownTweets = new Map<string, boolean>();

  observeUrlChanges(async (loc) => {
    if (loc.host.endsWith("twitter.com") === false) { return; }
  
    blurTrendingNow();
    blurRelevantPeople();
    blurWhoToFollow();
    removeLogo();
  
    if (loc.pathname === "/home") {
      blurFeed();
      blurTweets();
    }
  });
  
  
  function blurTrendingNow() {
    blurChildrenAsGroup({
      queryArgs: {
        cssQuery: `div[aria-label="Timeline: Trending now"] > div`,
        settingLogic: "twit-blur-trending-now"
      },
      checkBlurChild: (child) => child.textContent !== "What’s happening",
    })
  }
  
  function blurWhoToFollow() {
    blurChildrenAsGroup({
      queryArgs: {
        cssQuery: `aside[aria-label="Who to follow"]`,
        settingLogic: "twit-blur-who-to-follow",
      },
      checkBlurChild: (child) => child.textContent !== "Who to follow",
    })
  }
  
  function blurRelevantPeople() {
    blurAllFound({
      queryArgs: {
        cssQuery: `aside[aria-label="Relevant people"]`,
        settingLogic: "twit-blur-relevant-people"
      }, 
      blurArgs: {
        blur: true,
        showText: "ON_HOVER"
      }
    });
  }
  
  function blurFeed() {
    blurAllFound({
      permaUnblur: true,
      queryArgs: {
        cssQuery: `div[aria-label="Timeline: Your Home Timeline"]`,
        settingLogic: "twit-blur-home-screen-feed",
      }, 
    });
  }
  
  function blurTweets() {
    rebootCssQueryObserve({
      cssQuery: `div[aria-label="Timeline: Your Home Timeline"] div[data-testid="tweet"] > .css-1dbjc4n.r-1iusvr4.r-16y2uox.r-1777fci.r-1mi0q7o > div:nth-child(2)`,
      reboot: false,
      // settingLogic: "twit-blur-tweet",
      settingLogic: null,
      cb: ({domNode}) => {
        const textNode = domNode.querySelector(`.css-901oao`) as HTMLElement;
        const tweetText = textNode && textNode.textContent && textNode.textContent.slice(0, 60);
        if (tweetText && knownTweets.get(tweetText) === false) {
          return;
        }

        const prev = domNode.previousElementSibling as HTMLElement;
        const link = prev.querySelector("a");
        const handle = link && link.innerText.match(/@.+/);
        if (!link || !handle || !handle[0]) {
          addBlur({ all: [
            { not: "twit-disable" },
            "twit-blur-tweet"
          ]});
          return;
        }
        
        const trustSettingId = `${TWIT_TRUST_SETTING_PREFIX}${handle}`
        let trustCoin: HTMLElement;

        addSettingEffect({
          settingLogic: { all: [
            { not: "twit-disable" },
            "twit-blur-tweet",
            "twit-trust-tokens"
          ]},
          effect: () => {
            if (!trustCoin) {
              trustCoin = renderTrustCoin({
                settingId: trustSettingId,
                name: handle[0],
              });
              (link.parentElement as HTMLElement).style.flexDirection = "row";
            }
            (link.parentElement as HTMLElement).append(trustCoin)
          },
          undoEffect: () => trustCoin && trustCoin.remove()
        })
        


        addBlur({ all: [
          { not: "twit-disable" },
          "twit-blur-tweet",
          { all: [
            "twit-trust-tokens",
            { not: trustSettingId as any }
          ]}
        ]});

        function addBlur(settingLogic: ISettingLogic) {
          addSettingEffect({
            settingLogic,
            effect: () => setElementBlur(domNode, { 
              blur: true, 
              showText: false,
              onUnblur: (ev) => {
                ev.removeEventListeners();
                tweetText && knownTweets.set(tweetText, false);
              }
            }),
            undoEffect: () => setElementBlur(domNode, { blur: false })
          });
        }
      },
      undoCb: () => {}
    })
  }

  function removeLogo() {
    hideAllFound({
      queryArgs: {
        cssQuery: `a[href="/home"][aria-label="Twitter"]`,
        settingLogic: "twit-remove-logo",
      }
    })
  }
}




