import { setupSingleContentScriptAssert, observeUrlChanges, addSettingEffect } from "../common/Utils";
import { ContentScript } from "../../middleware/ContentScriptEnum";
import { rebootCssQueryObserve, setDefaultQueryLogic } from "../common/CssQueryObserve";
import { blurTogether, hideAllFound, blurAllFound } from "../common/SimpleMutations";

if (setupSingleContentScriptAssert(ContentScript.REDDIT)) {

  setDefaultQueryLogic({ not: "redd-disable" });
  
  observeUrlChanges(async (loc) => {
    if (loc.host.endsWith("www.reddit.com") === false) { return; }
  
    hideLogo();

    if (nonSubredditPage()) {
      blurFeed();
      blurPosts();
      blurTrendingCommunities();
      blurSuggestedSubreddits();
      removeSidebar();
    }
  });

  function hideLogo() {
    hideAllFound({
      queryArgs: {
        cssQuery: `a[aria-label="Home"]`,
        settingLogic: "redd-remove-logo"
      }
    });
  }
  
  function blurPosts() {
    rebootCssQueryObserve({
      cssQuery: `div._1poyrkZ7g36PawDueRza-J`,
      settingLogic: null,
      undoCb: () => null,
      cb: ({domNode}) => {
        if (nonSubredditPage() === false) { return; }
        let blurUs: HTMLElement[] =Array.from(domNode.childNodes).slice(1) as any;

        if ((domNode.firstChild as HTMLElement).tagName === "ARTICLE") {
          const article = domNode.firstChild as HTMLElement;
          blurUs = blurUs.concat(Array.from(article.childNodes).slice(1) as any);
          blurUs = blurUs.concat(Array.from((article.firstChild as HTMLElement).childNodes).slice(1) as any)
        } 

        addSettingEffect({
          settingLogic: { all: [
            { not: "redd-disable" },
            "redd-blur-home-posts"
          ]},
          effect: () => {
            blurTogether({
              domNodes: blurUs,
              blurArgs: {
                blur: true,
                blurStrength: "LOW"
              }
            });
          },
          undoEffect: () => {
            blurTogether({
              domNodes: blurUs,
              blurArgs: { blur: false },
            });
          }
        })
      },
    })
  }
  
  
  function blurTrendingCommunities() {
    blurAllFound({
      checkIgnore: () => nonSubredditPage() === false,
      queryArgs: {
        cssQuery: `div._1G4yU68P50vRZ4USXfaceV > .TmgZY6tDcdErbE5d7E0HJ`,
        settingLogic: "redd-blur-trending-communities"
      },
      blurArgs: {
        blur: true,
        showText: "ON_HOVER",
        textSize: "SMALL",
      }
    });
  }
  
  
  function blurSuggestedSubreddits() {
    rebootCssQueryObserve({
      cssQuery: `div._3RPJ8hHnfFohktLZca18J6`,
      settingLogic: null,
      undoCb: () => null,
      cb: ({domNode}) => {
        if (nonSubredditPage() === false) { return; }
        let blurUs: HTMLElement[] = Array.from(domNode.childNodes).slice(1) as any;

        addSettingEffect({
          settingLogic: { all: [
            { not: "redd-disable" },
            "redd-blur-sub-suggs"
          ]},
          effect: () => {
            blurTogether({
              domNodes: blurUs,
              blurArgs: {
                blur: true,
                blurStrength: "LOW",
                showText: "ON_HOVER",
                textSize: "SMALL"
              }
            });
          },
          undoEffect: () => {
            blurTogether({
              domNodes: blurUs,
              blurArgs: { blur: false },
            });
          }
        })
      }
    });
  }
  
  
  function blurFeed() {
    blurAllFound({
      queryArgs: {
        cssQuery: `div._3ozFtOe6WpJEMUtxDOIvtU`,
        settingLogic: "redd-blur-home-feed"
      },
      checkIgnore: () => location.pathname === "/subreddits/leaderboard/" || nonSubredditPage() === false,
    });
  }
  
  function removeSidebar() {
    hideAllFound({
      queryArgs: {
        cssQuery: `div._1BFbVxT49QnrAN3fqGZ1z8._1tvThPWQpORoc2taKebHxs`,
        settingLogic: "redd-remove-home-sidebar"
      }
    })
  }
  
  function nonSubredditPage() {
    switch (location.pathname) {
      case "/":
      case "/r/popular/":
      case "/r/all/":
      case "/subreddits/leaderboard/": return true;
      default: return false;
    }
  }
}