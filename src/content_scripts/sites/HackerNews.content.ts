import { setupSingleContentScriptAssert, addSettingEffect, ISettingEffect } from "../common/Utils";
import { ContentScript } from "../../middleware/ContentScriptEnum";
import { rebootCssQueryObserve } from "../common/CssQueryObserve";
import { SettingId } from "../../middleware/SettingsId";
import { blurTogether } from "../common/SimpleMutations";

if (setupSingleContentScriptAssert(ContentScript.HACKER_NEWS)) {  
  const customRegexSettId: SettingId = "hn-custom-regex";

  const defaultBlurSettings: Array<{
    settingId: SettingId,
    regex?: RegExp,
  }> = [
    { settingId: "hn-blur-apple", regex: /apple/i },
    { settingId: "hn-blur-fb", regex: /facebook/i },
    { settingId: "hn-blur-google", regex: /google/i },
    { settingId: "hn-blur-ms", regex: /microsoft/i },
    { settingId: "hn-blur-moz", regex: /mozilla/i },
    { settingId: "hn-blur-twit", regex: /twitter/i },
  ];

  let logoEl: HTMLElement;
  let customRegexBlurSetting: typeof defaultBlurSettings[number];

  const links: Array<{
    text: string;
    blurNodes: HTMLElement[];
    effect?: Required<ISettingEffect>;
  }> = [];

  const settingValToRegex = (value: string): RegExp | undefined => {
    const match = value.match(/^\/(.+)\/(\w*)$/);
    if (!match) { return; }
    const [, patternContent, flags] = match;
    if (patternContent) {
      return new RegExp(patternContent.replace("\\", "\\\\"), flags);
    }
  }

  chrome.storage.sync.get(customRegexSettId, (resp) => {
    const customRegex = resp[customRegexSettId];
    if (customRegex) {
      customRegexBlurSetting = {
        settingId: customRegexSettId,
        regex: settingValToRegex(customRegex)
      }
      defaultBlurSettings.push(customRegexBlurSetting)
    }

    blurRegexedPosts();
    removeLogo();
  })

  chrome.storage.onChanged.addListener((changes) => {
    const change = changes[customRegexSettId];
    if (!change) { return }
    let newRegex: undefined | RegExp;
    if (change.newValue) {
      newRegex = settingValToRegex(change.newValue);
    }

    if (customRegexBlurSetting) {
      customRegexBlurSetting.regex = newRegex;

    } else {
      customRegexBlurSetting = {
        settingId: customRegexSettId,
        regex: newRegex
      }
      defaultBlurSettings.push(customRegexBlurSetting);
    }

    links.forEach(updateLink);
  })
  

  function updateLink(link: typeof links[number]) {
    const blurReasons = defaultBlurSettings.filter((sett) => {
      return sett.regex && sett.regex.test(link.text);
    }).map((it) => it.settingId);
  
    if (!blurReasons.length) {
      link.effect && link.effect.updateLogic(false);
    
    } else {
      if (link.effect) {
        link.effect.updateLogic({ all: [
          { not: "hn-disable" },
          { any: blurReasons as any }
        ]});
        return;

      } else {
        link.effect = addSettingEffect({
          settingLogic: { all: [
            { not: "hn-disable" },
            { any: blurReasons as any }
          ]},
          effect: () => {
            blurTogether({
              domNodes: link.blurNodes, 
              blurArgs: {
                blur: true,
                blurStrength: "VERY_LOW"
              }
            });
          },
          undoEffect: () => {
            blurTogether({
              domNodes: link.blurNodes, 
              blurArgs: { blur: false }
            });
          }
        });
      }
    }   
  }


  function blurRegexedPosts() {
    rebootCssQueryObserve({
      cssQuery: "tr.athing",
      settingLogic: null,
      undoCb: () => null,
      cb: ({domNode}) => {
        const text = domNode.textContent;
        if (!text) { return; }
    
        const title = domNode.querySelector(".title > a") as HTMLElement;
        const stats = domNode.nextElementSibling as HTMLElement;

        const link = {
          text,
          blurNodes: [title, stats]
        };
        links.push(link)
        updateLink(link);
      }
    })
  }


  function removeLogo() {
    if (logoEl) { return; }

    addSettingEffect({
      settingLogic: { all: [
        { not: "hn-disable" },
        "hn-remove-logo"
      ]},
      effect: () => {
        if (!logoEl) {
          const img = document.querySelector(`img[src="y18.gif"]`) as any;
          logoEl = img.parentElement.parentElement;
        }
        logoEl.style.display = "none";
      },
      undoEffect: () => {
        logoEl && (logoEl.style.display = "");
      }
    })
  }
}
