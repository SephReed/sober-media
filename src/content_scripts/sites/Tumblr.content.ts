import { setupSingleContentScriptAssert, observeUrlChanges, addSettingEffect } from "../common/Utils";
import { ContentScript } from "../../middleware/ContentScriptEnum";
import { blurAllFound, blurTogether, hideAllFound } from "../common/SimpleMutations";
import { rebootCssQueryObserve, setDefaultQueryLogic } from "../common/CssQueryObserve";



if(setupSingleContentScriptAssert(ContentScript.TUMBLR)) {

  setDefaultQueryLogic({ not: "tum-disable" });

  observeUrlChanges(async (loc) => {
    console.log(loc.host);
    if (loc.host.endsWith("www.tumblr.com") === false) { return; }

    removeTrendingNavButton();
    hideLogo();
  
    if (loc.pathname.startsWith("/dashboard")) {
      blurHomeFeed();
      blurSideBar();
      blurPosts();
    } 
  });
  

  function hideLogo() {
    hideAllFound({
      queryArgs: {
        cssQuery: "div._1TuTH",
        settingLogic: "tum-remove-logo",
      }
    })
  }
  
  function blurHomeFeed() {
    blurAllFound({
      queryArgs: {
        cssQuery: `main>div.cfpPU`,
        settingLogic: "tum-blur-dash-feed"
      }
    })
  }
  
  
  function blurSideBar() {
    blurAllFound({
      queryArgs: {
        cssQuery: `div._1oyP0`,
        settingLogic: "tum-blur-dash-sidebar"
      },
      blurArgs: {
        blur: true,
        showText: "ON_HOVER",
        textSize: "SMALL"
      }
    })
  }


  function blurPosts() {
    rebootCssQueryObserve({
      cssQuery: "article",
      settingLogic: null,
      undoCb: () => null,
      cb: ({domNode}) => {
        const items = Array.from(domNode.querySelectorAll(`:scope > div:not(._3wjj2)`)) as HTMLElement[];

        addSettingEffect({
          settingLogic: { all: [
            { not: "tum-disable" },
            "tum-blur-posts"
          ]},
          effect: () => {
            blurTogether({
              domNodes: items,
              blurArgs: {
                blur: true,
                showText: "ON_HOVER",
                textSize: "SMALL"
              }
            })
          },
          undoEffect: () => {
            blurTogether({
              domNodes: items,
              blurArgs: { blur: false }
            })
          }
        })
      }
    });
  }


  function removeTrendingNavButton() {
    hideAllFound({
      transformDomNode: (domNode) => domNode.parentElement as HTMLElement,
      queryArgs: {
        cssQuery: `a[href="https://www.tumblr.com/explore/trending"]`,
        settingLogic: "tum-remove-trending-nav",
      }
    })
  }
}

