import { TIME_COUNT_SETTING_PREFIX, SettingId } from "../../middleware/SettingsId";
import { millisecToDisplayTime } from "../../middleware/msToDisplayTime";

const SECONDS = 1000;
const MINUTES = 60 * SECONDS;
const ACTIVE_SITE_SETTING_ID = "sm-active-site";



function div(
  className: string, 
  props?: { 
    ref?: (ref: HTMLDivElement) => void,
    innards?: HTMLElement[]
  }
) {
  const out = document.createElement("div");
  out.className = className;
  if (props) {
    props.innards && props.innards.forEach((child) => out.append(child));
    props.ref && props.ref(out);
  }
  return out;
}


export class Timer {
  public static interactionEvents = ["mousedown", "mousemove", "wheel", "keydown", "touchstart"]; 

  protected nodes = undefined as any as {
    root: HTMLElement;
    detailPanel: HTMLElement;
    timer: {
      currentSite: HTMLElement;
      allSites: HTMLElement;
    }
  };

  protected state = {
    enabled: false,
    mostRecentInteraction: Date.now(),
    sessionStart: undefined as number | undefined,
    sessionEnd: undefined as number | undefined,
    totalTime: 0,
    totalTimeAll: 0,
    synced: false,
    allowTimerUpdate: true,
  }

  protected storageId = `${TIME_COUNT_SETTING_PREFIX}${location.hostname}`;
  protected sessionStartId = `sm-session-start-${location.hostname}`;
  protected sessionEndId = `sm-session-end-${location.hostname}`;

  constructor() {
    this.enable();
  }

  public get domNode(): HTMLElement {
    if (!this.nodes) {
      this.nodes = { timer: {} } as any;

      const lastStaticPosition = { left: 0, bottom: 0 };

      const currentPosition = {
        left: 5,
        bottom: 5
      }

      const currentPositionIsStatic = () => {
        lastStaticPosition.left = currentPosition.left;
        lastStaticPosition.bottom = currentPosition.bottom;
      }

      currentPositionIsStatic();

      const moveToCurrentPosition = () => {
        this.nodes.root.style.left = `${currentPosition.left}px`;
        this.nodes.root.style.bottom = `${currentPosition.bottom}px`;
      }

      this.nodes.root = div("Timer", { innards: [
        this.nodes.detailPanel = div("DetailsPanel", { ref: (ref) => {
          const ul = document.createElement("ul");
          ref.append(ul);

          ul.innerHTML = `
            <li>View the "Timer Tab" in Sober Media for more complete info</li>
            <li>Click and drag to move timer</li>
          `;
          const closeMe = document.createElement("li");
          closeMe.classList.add("onPush");
          closeMe.innerText = `Click here to hide timer for this session`;
          closeMe.addEventListener("click", () => {
            this.nodes.root.style.display = "none";
          });
          ul.append(closeMe);

        }}),
        div("Counts", { 
          ref: (ref) => {
            let grabStart: undefined | { x: number, y: number };
            const moveListener = (ev: MouseEvent) => {
              if (!grabStart) { return; }
              const grabMoveLeft = ev.clientX - grabStart.x;
              const grabMoveBottom = grabStart.y - ev.clientY;
              currentPosition.left = lastStaticPosition.left + grabMoveLeft;
              currentPosition.bottom = lastStaticPosition.bottom + grabMoveBottom;
              console.log(grabMoveLeft, grabMoveBottom);
              moveToCurrentPosition();
            }
            ref.addEventListener("mousedown", (ev) => {
              grabStart = { x: ev.clientX, y: ev.clientY };
              document.body.addEventListener("mousemove", moveListener);
            });

            document.body.addEventListener("mouseup", (ev) => {
              grabStart = undefined;
              document.body.removeEventListener("mousemove", moveListener);
              currentPositionIsStatic();
            });
          },
          innards: [
          div("CurrentSite", { innards: [
            (() => {
              const img = document.createElement("img");
              img.src = `${location.origin}/favicon.ico`;
              return img;
            })(),
            this.nodes.timer.currentSite = document.createElement("span")
          ]}),
          this.nodes.timer.allSites = div("AllSites"),
        ]})
      ]});

      moveToCurrentPosition();
    }
    return this.nodes.root;
  }


  public addDomNodeToBody() {
    document.body.prepend(this.domNode);
  }


  protected updateAllowInterval: any;
  public enable() {
    if (this.state.enabled) { return; }
    this.state.enabled = true;

    if (this.nodes) {
      this.nodes.root.style.display = "";
    }
    
    const allowTimerUpdate = () => {
      this.state.allowTimerUpdate = true;
    }

    this.updateAllowInterval = setInterval(allowTimerUpdate, 5 * SECONDS);

    Timer.interactionEvents.forEach((evType) => {
      window.addEventListener(evType, this.interactionLiveRef)
    });

    this.assertVideoFinder();

    const allTimesId: SettingId = "sm-time-count-for-all";

    chrome.storage.sync.get([
      this.storageId, 
      allTimesId, 
      this.sessionStartId,
      this.sessionEndId
    ], (resp) => {
      this.state.synced = true;
      if (!resp) { return; }
      this.state.totalTime += resp[this.storageId] || 0;
      this.state.totalTimeAll = resp[allTimesId] || 0;
      this.state.sessionStart = resp[this.sessionStartId] || Date.now();
      this.state.sessionEnd = resp[this.sessionEndId] || undefined;

      allowTimerUpdate();
      this.updateTimer();
    });

    chrome.storage.onChanged.addListener((changes) => {
      const changeAll = changes[allTimesId];
      if (changeAll) {
        this.state.totalTimeAll = changeAll.newValue;
      }
      const changeSite = changes[this.storageId];
      if (changeSite && changeSite.newValue === 0) {
        this.state.totalTime = 0;
      }
      const activeSite = changes[ACTIVE_SITE_SETTING_ID];
      if (activeSite && activeSite !== location.hostname) {
        this.state.sessionStart = undefined;
      }
    })
  }

  protected updateTimer() {
    if (this.state.allowTimerUpdate === false) { return; }
    this.state.allowTimerUpdate = false;
    if (this.state.synced) {
      chrome.storage.sync.get(this.storageId, (obj) => {
        const storedValue = obj[this.storageId];
        if (storedValue > this.state.totalTime) {
          this.state.totalTime = storedValue;
          return;
        }

        chrome.storage.sync.set({
          [this.storageId]: this.state.totalTime,
          [ACTIVE_SITE_SETTING_ID]: location.hostname
        });
      })
    }
    if (!this.nodes) { return; }
    this.nodes.timer.currentSite.textContent = millisecToDisplayTime(this.state.totalTime);
    this.nodes.timer.allSites.textContent = `all: ${millisecToDisplayTime(Math.max(this.state.totalTimeAll, this.state.totalTime))}`;
  }

  public disable() {
    if (this.state.enabled === false) { return; }
    this.state.enabled = false;

    if (this.nodes) {
      this.nodes.root.style.display = "none";
    }
    if (this.updateAllowInterval) {
      clearInterval(this.updateAllowInterval);
      this.updateAllowInterval = undefined;
    }
    Timer.interactionEvents.forEach((evType) => {
      window.removeEventListener(evType, this.interactionLiveRef)
    });
  }

  protected videoElFinder?: MutationObserver;
  protected videoEls: Array<HTMLVideoElement>[] = [];
  protected assertVideoFinder() {
    if (this.videoElFinder) { return; }
    const modVideoEl = (videoEl: HTMLVideoElement) => {
      videoEl.addEventListener("timeupdate", (ev) => {
        if (document.hasFocus() && this.state.enabled) {
          this.interactionLive();
        }
      });
    }

    document.querySelectorAll("video").forEach(modVideoEl);

    this.videoElFinder = new MutationObserver((mutList) => {
      for(const mut of mutList) {
        if (mut.type !== 'childList') { return; }
        mut.addedNodes.forEach(modVideoEl as any);
      }
    });
    this.videoElFinder.observe(document.body, {
      childList: true,
      subtree: true,
    })
  }


  protected interactionTimeout: number | undefined;
  protected interactionLiveRef = this.interactionLive.bind(this);
  public interactionLive() {
    const timerState = this.state;
    const { sessionEnd } = timerState;
    this.nodes && this.nodes.root.classList.remove("--inactive");
  
    const lastInteraction = timerState.mostRecentInteraction;
    timerState.mostRecentInteraction = Date.now();

    if (!timerState.sessionStart) {
      timerState.sessionStart = Date.now();
      timerState.sessionEnd = undefined;
    }
  
    if (sessionEnd) {
      const timeSinceSession = Date.now() - sessionEnd;
      const sessionLength = sessionEnd - timerState.sessionStart;
      if (timeSinceSession > Math.min(5 * MINUTES, sessionLength)) {
        timerState.sessionStart = Date.now();
        chrome.storage.sync.set({[this.sessionStartId]: timerState.sessionStart });
      } else {
        timerState.totalTime += timeSinceSession;
        timerState.allowTimerUpdate = true;
      }
      timerState.sessionEnd = undefined;
      chrome.storage.sync.set({[this.sessionEndId]: timerState.sessionEnd });
  
    } else {
      timerState.totalTime += (timerState.mostRecentInteraction - lastInteraction);
    }
  
    if (this.interactionTimeout) {
      window.clearTimeout(this.interactionTimeout);
    }
    this.updateTimer();
  
    this.interactionTimeout = window.setTimeout(() => {
      this.interactionTimeout = undefined;
      timerState.sessionEnd = timerState.mostRecentInteraction;
      chrome.storage.sync.set({[this.sessionEndId]: timerState.sessionEnd });
      this.nodes && this.nodes.root.classList.add("--inactive");
    }, 5 * SECONDS)
  }
}