import { SettingId } from "../../middleware/SettingsId";
import { addSettingEffect, ISettingLogic } from "./Utils";


let defaultLogic: ISettingLogic | undefined;
export function setDefaultQueryLogic(logic: ISettingLogic) {
  defaultLogic = logic;
}



export interface IObserveCssQueryEvent {
  domNode: HTMLElement;
  querySetup: IQuerySetup;
}

export interface IObserveCssQueryArgs {
  root?: HTMLElement;
  cssQuery: string;
  settingLogic: null | ISettingLogic;
  // flipSettingVal?: boolean;
  reboot?: false;
  limit?: number;
  cb: (ev: IObserveCssQueryEvent) => void;
  undoCb: (ev: IObserveCssQueryEvent) => void;
}

export interface IQuerySetup {
  cb: (ev: IObserveCssQueryEvent) => void;
  undoCb: (ev: IObserveCssQueryEvent) => void;
  foundElements: HTMLElement[];
  limit?: number;
  settingLogic: null | ISettingLogic;
  disabled: boolean;
  // flipSettingVal?: boolean;
  removeQuery: () => void;
}

export interface IObserverSetup {
  observer: MutationObserver,
  cssQueries: Map<string, IQuerySetup>
}

const existingObservers = new Map<Element, IObserverSetup>();
export function rebootCssQueryObserve(args: IObserveCssQueryArgs) {
  const root: HTMLElement = args.root = args.root || document as any;

  if (args.settingLogic && defaultLogic) {
    args.settingLogic = { all: [ args.settingLogic, defaultLogic ]};
  }

  let observerSetup = existingObservers.get(root)
  if (observerSetup) {
    const querySetup = observerSetup.cssQueries.get(args.cssQuery);
    if (querySetup) {
      if (args.reboot === false) { return; }
      if (querySetup.disabled) { return; }
      querySetup.foundElements.forEach((domNode) => {
        runQuerySetupOnElement(querySetup, domNode);
      });
      return;
    } 

    observerSetup.cssQueries.set(args.cssQuery, initQuerySetup(args));
    return;
  }

  const cssQueries = new Map<string, IQuerySetup>();
  cssQueries.set(args.cssQuery, initQuerySetup(args));

  const mutObserver = new MutationObserver((mutList) => {
    for(const mut of mutList) {
      if (mut.type !== 'childList') { return; }
      
      const cssQueryEntries = Array.from(cssQueries.entries());
      cssQueryEntries.forEach(([cssQuery, querySetup]) => {
        if (querySetup.disabled) { return; }

        for (const child of mut.addedNodes) {
          if (child instanceof HTMLElement) {
            let matches: HTMLElement[] = [];
            if (child.matches(cssQuery)) {
              matches.push(child);
            }
            matches = matches.concat(Array.from(child.querySelectorAll(cssQuery)));

            for (const match of matches) {
              addAddElementToQuerySetup(querySetup, match);
              if (querySetup.disabled) { return; }
            }
          }
        }
      });
    } 
  });

  mutObserver.observe(root, {
    childList: true,
    subtree: true,
  });

  existingObservers.set(root, {
    observer: mutObserver,
    cssQueries,
  });
}


function initQuerySetup(args: IObserveCssQueryArgs) {
  const out: IQuerySetup = {
    cb: args.cb,
    undoCb: args.undoCb,
    settingLogic: args.settingLogic,
    foundElements: [],
    disabled: false,
    // flipSettingVal: args.flipSettingVal,
    removeQuery: () => {
      out.disabled = true;
    }
  };
  const currentMatches = (args.root || document).querySelectorAll<HTMLElement>(args.cssQuery);
  for (const match of currentMatches) {
    addAddElementToQuerySetup(out, match);
  }
  return out;
}


function addAddElementToQuerySetup(querySetup: IQuerySetup, element: HTMLElement) {
  if (querySetup.limit && querySetup.limit <= querySetup.foundElements.length) { 
    querySetup.disabled = true;
    return; 
  }
  if (querySetup.foundElements.includes(element)) { return; }
  querySetup.foundElements.push(element);
  runQuerySetupOnElement(querySetup, element);
}

async function runQuerySetupOnElement(querySetup: IQuerySetup, element: HTMLElement) {
  if (!querySetup.settingLogic) {
    querySetup.cb({
      domNode: element,
      querySetup,
    });
    return;
  }
  addSettingEffect({
    settingLogic: querySetup.settingLogic,
    effect: () => querySetup.cb({
      domNode: element,
      querySetup,
    }),
    undoEffect: () => querySetup.undoCb({
      domNode: element,
      querySetup,
    }),
  });
}


// function querySetupAtLimit(querySetup: IQuerySetup) {
//   return querySetup.limit && querySetup.limit <= querySetup.foundElements.length;
// }

