import { ContentScript } from "../../middleware/ContentScriptEnum";
import { SettingId } from "../../middleware/SettingsId";



const locationChangeEventType = "sober-media-location-change";

export function observeUrlChanges(cb: (loc: Location) => any) {
  assertLocationChangeObserver();
  window.addEventListener(locationChangeEventType, () => cb(window.location));
  cb(window.location);
}

function assertLocationChangeObserver() {
  const state = window as any as { soberMediaLocationWatchSetup: any };
  if (state.soberMediaLocationWatchSetup) { return; }
  state.soberMediaLocationWatchSetup = true;

  let lastHref = location.href;
  const checkForLocationChanged = () => {
    requestAnimationFrame(() => {
      const currentHref = location.href;
      if (currentHref !== lastHref) {
        // cancelMutObservers();
        lastHref = currentHref;
        window.dispatchEvent(new Event(locationChangeEventType));
      } 
    });
  }

  ["popstate", "click", "keydown", "keyup", "touchstart", "touchend", "mousedown", "mouseup"].forEach((eventType) => {
    window.addEventListener(eventType, checkForLocationChanged)
  });

  document.addEventListener("DOMContentLoaded", checkForLocationChanged);

  setTimeout(checkForLocationChanged, 250);
}





export function setupSingleContentScriptAssert(scriptCheck: ContentScript) {
  const wind = window as any;
  wind.soberMedia = wind.soberMedia || {};
  if (wind.soberMedia[scriptCheck]) { return false; }

  wind.soberMedia[scriptCheck] = true;
  chrome.runtime.onMessage.addListener((request, sender, sendResponse) => {
    if (request && request.checkForContentScript === scriptCheck) {
      sendResponse(true);
    }
  });
  return true;
}






export function getSetting(id: SettingId) {
  return new Promise((resolve) => {
    chrome.storage.sync.get([id], (result) => {
      resolve(result[id]);
    })
  })
}

export function setSetting(id: SettingId, value: string | boolean | number) {
  return new Promise((resolve) => {
    console.log("Setting ", id, value);
    chrome.storage.sync.set({[id]: value}, () => {
      resolve();
    })
  })
}



export interface ISettingEffect {
  settingLogic: ISettingLogic
  effect: () => void;
  undoEffect: () => void;
  currentState?: boolean;
  updateLogic?: (newLog: ISettingLogic | boolean) => void;
};

const settingEffects = new Array<ISettingEffect>();
const settingState: Partial<Record<SettingId, boolean>> = {};

export function addSettingEffect(args: ISettingEffect): Required<ISettingEffect> {
  setupStorageObserver();

  const effectItem: Required<ISettingEffect> = {
    ...args,
    currentState: false,
    updateLogic: (newLog) => {
      effectItem.settingLogic = newLog;
      updateSettingEffect(effectItem);
    }
  }
  settingEffects.push(effectItem);
  updateSettingEffect(effectItem);
  return effectItem;
}

let _storageObserverSetup = false;
function setupStorageObserver() {
  if (_storageObserverSetup) { return; }
  _storageObserverSetup = true;

  chrome.storage.sync.get(null, (allSettings) => {
    Object.entries(allSettings).forEach(([key, value]) => {
      settingState[key as SettingId] = value;
    });
    settingEffects.forEach(updateSettingEffect);
  })
  chrome.storage.onChanged.addListener((changes) => {
    Object.entries(changes).forEach(([settingId, change]) => {
      if (change.newValue === change.oldValue) { return; }
      settingState[settingId as SettingId] = change.newValue;
    });
    settingEffects.forEach(updateSettingEffect);
  })
}

function updateSettingEffect(effect: ISettingEffect) {
  const cleanState = resolveSettingLogic(effect.settingLogic);
  if (cleanState === effect.currentState) { return; }

  effect.currentState = cleanState;

  if (effect.currentState) {
    effect.effect();
  } else {
    effect.undoEffect();
  }
}



export type ISettingLogic = boolean | SettingId 
  | { all: ISettingLogic | ISettingLogic[] } 
  | { any: ISettingLogic | ISettingLogic[] }
  | { not: SettingId };

export function resolveSettingLogic(logic: ISettingLogic): boolean {
  if (typeof logic === "string") {
    return !!settingState[logic];
  } else if (typeof logic === "boolean") {
    return logic;
  } else if ("not" in logic) {
    return !settingState[logic.not];
  }
  //@ts-ignore
  const objValue: ISettingLogic | ISettingLogic[] = logic["all"] || logic["any"];
  if (Array.isArray(objValue) === false) {
    return resolveSettingLogic(objValue as ISettingLogic);
  }

  const arr = objValue as ISettingLogic[];
  if ("all" in logic) {
    return arr.every(resolveSettingLogic)
  }

  return arr.some(resolveSettingLogic);
}


export function findParent(ptr: HTMLElement, cssQuery: string) {
  ptr = ptr.parentElement as any;
  while (ptr && ptr.matches(cssQuery) === false) {
    ptr = ptr.parentElement as any;
  }
  return ptr;
}

