import { addSettingEffect, getSetting, setSetting } from "./Utils";


export interface ITrustCoinProps {
  name: string;
  settingId: string;
}

export function renderTrustCoin(props: ITrustCoinProps) {
  const trustCoin = document.createElement("div");
  trustCoin.classList.add("TrustCoin");
  trustCoin.title = `Add/Remove Trust for ${props.name}`;
  trustCoin.addEventListener("click", (ev) => {
    ev.stopImmediatePropagation();
    ev.preventDefault();
    getSetting(props.settingId as any).then((value) => {
      setSetting(props.settingId as any, !value);
    });
    return false;
  })
  addSettingEffect({
    settingLogic: props.settingId as any,
    effect: () => {
      trustCoin.classList.add("--trusted");
    },
    undoEffect: () => {
      trustCoin.classList.remove("--trusted");
    }
  })

  return trustCoin;
}