import { rebootCssQueryObserve, IObserveCssQueryArgs } from "./CssQueryObserve";

export interface IBlurEvent { 
  removeEventListeners: () => void;
}


export interface ISetBlurArgs {
  blur: boolean;
  unblurOnClick?: boolean;
  showText?: boolean | "ON_HOVER";
  blurStrength?: "NORMAL" | "LOW" | "VERY_LOW";
  textSize?: "NORMAL" | "SMALL" | "VERY_SMALL";
  onUnblur?: (ev: IBlurEvent) => void;
}

const unblurOnClickNodes = new Map<HTMLElement, void>();

export function setElementBlur(domNode: HTMLElement, args: ISetBlurArgs) {
  if (args.blur === domNode.classList.contains("--blurred")) { return; }

  domNode.classList.toggle("--blurred", args.blur);

  if (args.blur) {
    domNode.classList.toggle("hasUnblurText", args.showText !== false);

    if (args.unblurOnClick !== false) {
      if (unblurOnClickNodes.has(domNode)) { return; }
      unblurOnClickNodes.set(domNode);
  
      const listener = (ev: MouseEvent) => {
        if (domNode.classList.contains("--blurred") === false) { return; }
        ev.stopImmediatePropagation();
        ev.preventDefault();
        domNode.classList.remove("--blurred");
        unblurOnClickNodes.delete(domNode);
        args.onUnblur && args.onUnblur({
          removeEventListeners: () => {
            domNode.removeEventListener("click", listener);
          }
        });
        return false;
      };
      domNode.addEventListener("click", listener);
    }

    if (args.showText === "ON_HOVER") {
      domNode.classList.add("hideTextUntilHover");
    }

    switch(args.blurStrength) {
      case "LOW": domNode.classList.add("lowPowerBlur"); break;
      case "VERY_LOW": domNode.classList.add("veryLowPowerBlur"); break;
    } 

    switch(args.textSize) {
      case "SMALL": domNode.classList.add("smallText"); break;
      case "VERY_SMALL": domNode.classList.add("verySmallText"); break;
    } 
    
  }
}




export interface IBlurTogetherArgs {
  domNodes: HTMLElement[];
  blurArgs?: ISetBlurArgs;
}


export function blurTogether(args: IBlurTogetherArgs) {
  const { domNodes } = args;
  const listener = (ev: MouseEvent) => {
    ev.stopImmediatePropagation();
    ev.preventDefault();
    
    for (const domNode of domNodes) {
      domNode.removeEventListener("click", listener)
      setElementBlur(domNode, { blur: false });
    }

    if (args.blurArgs && args.blurArgs.onUnblur) {
      args.blurArgs.onUnblur({ removeEventListeners: () => null});
    }
  }

  domNodes.forEach((domNode) => {
    setElementBlur(domNode, { unblurOnClick: false, showText: false, blur: true, ...args.blurArgs});
    domNode.addEventListener("click", listener);
  });
}









export interface IBlurAllFoundArgs {
  permaUnblur?: boolean;
  checkIgnore?: (domNode: HTMLElement) => boolean;
  queryArgs: Omit<IObserveCssQueryArgs, "cb" | "undoCb">;
  blurArgs?: ISetBlurArgs;
}

const foundDomNodes = new Map<string, HTMLElement[]>();
export function blurAllFound(args: IBlurAllFoundArgs) {
  rebootCssQueryObserve({
    ...args.queryArgs,
    cb: ({domNode, querySetup}) => {
      if (args.checkIgnore && args.checkIgnore(domNode)) {
        return;
      }
      setElementBlur(domNode, {
        blur: true,
        onUnblur: (ev) => {
          if (args.permaUnblur) { 
            ev.removeEventListeners(); 
            querySetup.removeQuery();
          }
        },
        ...args.blurArgs
      });
    },
    undoCb: ({domNode}) => {
      setElementBlur(domNode, { blur: false });
    },
  })
}



export interface IHideAllFoundArgs {
  transformDomNode?: (domNode: HTMLElement) => HTMLElement;
  queryArgs: Omit<IObserveCssQueryArgs, "cb" | "undoCb">;
}

export function hideAllFound(args: IHideAllFoundArgs) {
  rebootCssQueryObserve({
    ...args.queryArgs,
    cb: ({domNode}) => {
      domNode = args.transformDomNode ? args.transformDomNode(domNode) : domNode;
      domNode.style.display = "none";
      domNode.style.visibility = "hidden";
    },
    undoCb: ({domNode}) => {
      domNode = args.transformDomNode ? args.transformDomNode(domNode) : domNode;
      domNode.style.display = domNode.style.visibility = "";
    },
  })
}



export function blurChildrenAsGroup(args: {
  checkBlurChild?: (child: Element) => boolean;
  queryArgs: Omit<IObserveCssQueryArgs, "cb" | "undoCb">;
  blurArgs?: ISetBlurArgs;
}) {
  const groupings = new Map<HTMLElement, HTMLElement[]>();

  rebootCssQueryObserve({
    ...args.queryArgs,
    cb: ({domNode}) => {
      let group: HTMLElement[] = groupings.get(domNode) as any;
      if (group) {
        blurGroup();
        return;
      }

      groupings.set(domNode, group = []);
      observeChildren(domNode, (child) => {
        if (args.checkBlurChild && args.checkBlurChild(child) === false) { return; }
        group.push(child);
        blurGroup();
      })

      function blurGroup() {
        blurTogether({
          domNodes: group
        });
      }
    },
    undoCb: ({domNode}) => {
      const group: HTMLElement[] = groupings.get(domNode) as any;
      group && blurTogether({ domNodes: group, blurArgs: { blur: false }});
    },
  });
}


export function observeChildren(
  root: HTMLElement,
  cb: (domNode: HTMLElement) => void
) {
  const mutObserver = new MutationObserver((mutList) => {
    for(const mut of mutList) {
      if (mut.type !== 'childList') { return; }
      
      for (const child of mut.addedNodes) {
        if (child instanceof HTMLElement) {
          cb(child);
        }
      }
    }  
  });

  mutObserver.observe(root, {
    childList: true,
  });

  for (const child of root.childNodes) {
    if (child instanceof HTMLElement) {
      cb(child);
    }
  }

  return mutObserver;
}