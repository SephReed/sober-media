import { setupSingleContentScriptAssert, findParent, addSettingEffect, ISettingLogic, resolveSettingLogic } from "./Utils";
import { ContentScript } from "../../middleware/ContentScriptEnum";
import { setElementBlur } from "./SimpleMutations";
import { rebootCssQueryObserve } from "./CssQueryObserve";
import { BLUR_LINK_SETTING_PREFIX, NUM_LINKS_BLURRED_SETTING_PREFIX, SettingId } from "../../middleware/SettingsId";

if (setupSingleContentScriptAssert(ContentScript.BLUR_LINKS)) {
  const blurCounts = new Map<string, {
    synced: boolean;
    count: number;
  }>();

  const blurredItems = new Map<HTMLElement, void>();

  rebootCssQueryObserve({
    settingLogic: null,
    cssQuery: "a",
    cb: ({domNode}) => {
      chrome.storage.sync.get(null, (items) => {
        const blurLinks = Object.keys(items).filter((key) => {
          return key.startsWith(BLUR_LINK_SETTING_PREFIX);
        });

        const anchorEl = domNode as HTMLAnchorElement;
        const blurReasons = blurLinks.filter((settingId) => anchorEl.href.includes(settingId.replace(BLUR_LINK_SETTING_PREFIX, "")));
        if (!blurReasons.length) { return; }
        
        let blurTarget: HTMLElement = anchorEl;
        if (location.host.includes("duckduckgo")) {
          blurTarget = findParent(anchorEl, ".result") || blurTarget;
        } else if (location.host.includes("google")) {
          blurTarget = findParent(anchorEl, ".rc") || blurTarget;
        }

        if (blurredItems.has(blurTarget)) {
          return;
        }
        blurredItems.set(blurTarget);

        blurReasons.sort((a, b) => b.length - a.length);
        let linkCounted = false;

        addSettingEffect({
          settingLogic: { all: [
            { not: "sm-link-blurring-disabled"},
            { any: blurReasons as SettingId[] }
          ]},
          effect: () => {
            setElementBlur(blurTarget, { blur: true, showText: false, blurStrength: "LOW" });
            if (!linkCounted) {
              linkBlurred(blurReasons[0].replace(BLUR_LINK_SETTING_PREFIX, ""));
              linkCounted = true;
            }
          },
          undoEffect: () => setElementBlur(blurTarget, { blur: false })
        })
      })
    },
    undoCb: () => null
  })


  function linkBlurred(link: string) {
    const settingId = `${NUM_LINKS_BLURRED_SETTING_PREFIX}${link}`;
    const needsSync = (blurCounts.has(link) === false);
    if (needsSync) {
      blurCounts.set(link, {
        synced: false,
        count: 0,
      });

      chrome.storage.sync.get(settingId, (prop) => {
        const linkBlurState = blurCounts.get(link);
        if (!linkBlurState) { return; }
        linkBlurState.synced = true;

        const value = prop && prop[settingId];
        if (typeof value === "number") {
          linkBlurState.count += value;
        }

        pushChange();
      })
    }
    (blurCounts.get(link) as any).count++;
    pushChange()
    

    function pushChange() {
      const countProps = blurCounts.get(link);
      countProps && chrome.storage.sync.set({
       [settingId]: countProps.count
      })
    }
  }
}