import { ContentScript } from "../../middleware/ContentScriptEnum";
import { setupSingleContentScriptAssert, addSettingEffect, ISettingEffect, ISettingLogic } from "./Utils";
import { findPotentialTimerReasons } from "../../middleware/FindTimerReason";
import { ADD_TIMER_SETTING_PREFIX, SettingId } from "../../middleware/SettingsId";
import { Timer } from "./Timer";

if (setupSingleContentScriptAssert(ContentScript.ADD_TIMER)) {  
  let runTimerEffect: Required<ISettingEffect>;
  let showOverlayEffect: Required<ISettingEffect>;
  let timer: Timer;

  updateEffect();

  chrome.storage.onChanged.addListener((changes) => {
    const hasTimerEnableChange = Object.keys(changes).some((settingId) => {
      return settingId.startsWith(ADD_TIMER_SETTING_PREFIX) && changes[settingId].newValue !== false;
    });
    if (hasTimerEnableChange) {
      updateEffect();
    }
  })
  
  function updateEffect() {
    findPotentialTimerReasons(location.href).then((reasons) => {
      if (!reasons) { return; }
  
      const runTimerLogic: ISettingLogic = { all: [
        { not: "sm-timer-disabled" },
        { any: reasons },
      ]};
      const addOverlayLogic: ISettingLogic = { all: [
        { not: "sm-timer-disabled" },
        { any: reasons },
        "sm-timer-show-overlay",
      ]};

      if (runTimerEffect) {
        runTimerEffect.updateLogic(runTimerLogic);
        showOverlayEffect.updateLogic(addOverlayLogic);
        return;
      }
      runTimerEffect = addSettingEffect({
        settingLogic: runTimerLogic,
        effect: () => {
          timer = timer || new Timer();
          timer.enable();
        },
        undoEffect: () => {
          timer && timer.disable();
        },
      });

      showOverlayEffect = addSettingEffect({
        settingLogic: addOverlayLogic,
        effect: () => timer && timer.addDomNodeToBody(),
        undoEffect: () => timer && timer.domNode.remove(),
      })
    });
    
  }
}