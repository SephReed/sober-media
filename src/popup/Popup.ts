import "./popup.scss";

import { onDomReady, setInnards, byId, div, Innards, bold, br, Source, Sourcify } from "helium-ui";
import { renderTabListPane } from "./components/TabListPane/TabListPane";
import { renderBooleanSetting } from "./components/BooleanSetting/BooleanSetting";
import { SM } from "./state/State";
import { renderFacebookSettings, renderTwitterSettings, renderHackerNewsSettings, renderRedditSettings, renderYoutubeSettings, renderPinterestSettings, renderTumblrSettings } from "./pages/KnownSitesSettings/KnownSitesSettings";
import { renderAboutPage } from "./pages/About/AboutPage";
import { renderTimePage } from "./pages/Timer/TimerPage";
import { renderFlaggedSitesPage } from "./pages/FlaggedSites/FlaggedSitesPage";
import { tweener } from "helium-ui-tweener";



const app = renderApp();

onDomReady(() => {
  setInnards(byId("root"), app);
})


function renderApp() {
  return div("SoberMediaPopup", [
    renderTabListPane(),

    div("ContentPane", tweener({
      type: "crossfade-on-top",
      disableResize: true,
    },() => {
      switch(SM.path) {
        case "about": return renderAboutPage();
        case "timer": return renderTimePage();
        case "flagged-sites": return renderFlaggedSitesPage();
        case "facebook": return renderFacebookSettings();
        case "hacker-news": return renderHackerNewsSettings();
        case "pinterest": return renderPinterestSettings();
        case "reddit": return renderRedditSettings();
        case "tumblr": return renderTumblrSettings();
        case "twitter": return renderTwitterSettings();
        case "youtube": return renderYoutubeSettings();
      }
    })),
  ])
}



// function renderBooleanSetting(props: {
//   label: Innards;
//   settingId: string;
// }) {
//   const state = new Source(false);
//   return div("BooleanSetting", {
//     ddxClass: () => state.get() ? "--enabled" : "",
//     onPush: () => state.set(!state.get())
//   },[
//     div("Switch", [
//       div("SwitchThumb")
//     ]),
//     div("Label", [props.label]),
//   ]);
// }

// let changeColor = document.getElementById('changeColor') as HTMLInputElement;

// chrome.storage.sync.get('color', function(data) {
//   changeColor.style.backgroundColor = data.color;
//   changeColor.setAttribute('value', data.color);
// });

// changeColor.onclick = function(element) {
//   let color = (element.target as HTMLInputElement).value;
//   chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
//     chrome.tabs.executeScript(
//       tabs[0].id as number,
//       {code: 'document.body.style.backgroundColor = "' + color + '";'});
//   });
// };