import { Source, noDeps } from "helium-ui";

const currentUrl = new Source<URL>();

export function getCurrentUrl() {
  updateUrl();
  return currentUrl.get();
}

function updateUrl() {
  chrome.tabs.query({ active: true, currentWindow: true }, (tabs) => {
    const tab = tabs[0];
    if (!tab || !tab.url) { return; }
    const newUrl = new URL(tab.url);
    if (currentUrl.get() && newUrl.href === currentUrl.get().href) {
      return;
    }
    currentUrl.set(newUrl)
  });
}

export function getHostMatchListForUrl(): null | string[] {
  updateUrl();
  
  const url = currentUrl.get();
  if (!url) { return null; }
  
  const hostChunks = url.host.split(".").filter((it) => it !== "www");
  const hosts: string[] = [];
  for (let i = 0; i < hostChunks.length -1; i++) {
    hosts.push(hostChunks.slice(i).join("."));
  }
  
  let href = hosts[0];
  hosts.reverse();
  
  const pathChunks = url.pathname.split("/").filter((it) => !!it);
  pathChunks.forEach((chunk) => {
    href += `/${chunk}`;
    hosts.push(href);
  })

  return hosts;
}