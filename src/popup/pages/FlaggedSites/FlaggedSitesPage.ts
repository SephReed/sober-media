import { div, br, Source, noDeps, bold, table, tr, td, el } from "helium-ui";
import { renderBooleanSetting, renderDeletableBooleanSetting, renderDeleteBoolean } from "../../components/BooleanSetting/BooleanSetting";
import { SM } from "../../state/State";
import { BLUR_LINK_SETTING_PREFIX, NUM_LINKS_BLURRED_SETTING_PREFIX } from "../../../middleware/SettingsId";
import { getHostMatchListForUrl } from "../../Util";


// const currentUrl = new Source<URL>();
const PREFIX = BLUR_LINK_SETTING_PREFIX;

export function renderFlaggedSitesPage() {
  // chrome.tabs.query({ active: true, currentWindow: true }, (tabs) => {
  //   const tab = tabs[0];
  //   if (!tab || !tab.url) { return; }
  //   currentUrl.set(new URL(tab.url))
  // });

  return div("FlaggedSitesPage", [
    div("PageTitle", "Site Flagging"),
    br(2),
    bold(["General"]),
    renderBooleanSetting({
      settingId: "sm-link-blurring-disabled",
      label: "Disable All Link Blurring",
      type: "RED",
    }),
    div("DisablableSection", {
      ddxClass: () => SM.settings["sm-link-blurring-disabled"] ? `--disableSection` : "",
    }, [
      br(2),
      bold(["Flag This Site"]),
      div("BlurUrls", () => {
        const hosts = getHostMatchListForUrl();
        if (!hosts) { return "Loading..."; }
        
        return hosts.map((host) => 
          renderBooleanSetting({
            label: `Blur Links to ${host}`,
            settingId: PREFIX + host as any
          })
        );
      }),
      br(3),
      div("PageTitle", "Flagged Site List"),
      div("center", "(list of sites for which you would like blurring to help you avoid links towards)"),
      br(),
      // div("FlagSiteList", () => {
      //   const flagSettingIds = Array.from(Object.keys(SM.settings)).filter((key) => key.startsWith(PREFIX));
  
      //   if (!flagSettingIds.length) {
      //     return div("center", "No sites are currently flagged");
      //   }
  
      //   return flagSettingIds.map((flagSettingId) => renderDeletableBooleanSetting({
      //     settingId: flagSettingId as any,
      //     label: flagSettingId.replace(PREFIX, ""),
      //   }));
      // }),

      div("FlagSiteList", () => {
        const flagSettingIds = Array.from(Object.keys(SM.settings)).filter((key) => key.startsWith(PREFIX));
  
        if (!flagSettingIds.length) {
          return div("center", "No sites are currently flagged");
        }
  
        return [
          div("GridRow", [
            "Remove",
            "Enable",
            "Site",
            "Blur Count",
          ].map((name) => bold([name]))),
          ...flagSettingIds.map((flagSettingId) => {
            const host = flagSettingId.replace(PREFIX, "");
            return div("GridRow", [
              renderDeleteBoolean(flagSettingId as any),
              renderBooleanSetting({
                settingId: flagSettingId as any,
                label: null,
              }),
              div([host]),
              div([String(SM.settings.get(`${NUM_LINKS_BLURRED_SETTING_PREFIX}${host}` as any) || 0)]),
            ])
          })
        ]
        // return table([
        //   tr([
        //     "Remove",
        //     "Enable",
        //     "Site",
        //     "Blur Count",
        //   ].map((name) => el("th", [name]))),
        //   ...flagSettingIds.map((flagSettingId) => {
        //     const host = flagSettingId.replace(PREFIX, "");
        //     return tr([
        //       renderDeleteBoolean(flagSettingId as any),
        //       renderBooleanSetting({
        //         settingId: flagSettingId as any,
        //         label: null,
        //       }),
        //       host,
        //       SM.settings.get(`${NUM_LINKS_BLURRED_SETTING_PREFIX}${host}` as any) || 0,
        //     ])
        //   })
        // ]);
      })
    ])
  ])
}