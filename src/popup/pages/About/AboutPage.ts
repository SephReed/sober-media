import "./aboutPage.scss";


import { div, br, img, bold, italic, a, span, noDeps } from "helium-ui";
import { renderBooleanSetting } from "../../components/BooleanSetting/BooleanSetting";
import { renderDropdown } from "../../components/DropDown/DropDown";
import { SM } from "../../state/State";
import { FB_TRUST_SETTING_PREFIX, TWIT_TRUST_SETTING_PREFIX, BLUR_LINK_SETTING_PREFIX, NUM_LINKS_BLURRED_SETTING_PREFIX, ADD_TIMER_SETTING_PREFIX, SettingId, TIME_COUNT_SETTING_PREFIX } from "../../../middleware/SettingsId";
import { millisecToDisplayTime } from "../../../middleware/msToDisplayTime";


export function renderAboutPage() {
  return div("AboutPage", [
    div("FlagSite", {
      onPush: () => SM.path = "flagged-sites",
    },[
      img({src: "/images/red_flag256.png"}),
      italic(["flag current site"]),
    ]),

    div("TotalTime", {
      onPush: () => SM.path = "timer",
    },[
      img({src: "/images/timer256.png"}),
      italic(() => {
        const totalTime = SM.settings.get("sm-time-count-for-all") as unknown as number;
        return totalTime 
          ? `time today: ${millisecToDisplayTime(totalTime)}`
          : `time site usage`
      }),
    ]),

    div("Header", [
      br(),
      img("Logo", {src: "/images/sober_media256.png"}),
      br(),
      div("PageTitle", "Sober Media"),
      italic([`designed to help you fend off the slow creep of social media addiction`]),
    ]),

    br(2),

    bold(["Add / Remove Tabs"]),
    div([
      renderBooleanSetting({
        label: "Facebook",
        settingId: "sm-show-tab-fb"
      }),
      renderBooleanSetting({
        label: "Hacker News",
        settingId: "sm-show-tab-hn"
      }),
      renderBooleanSetting({
        label: "Pinterest",
        settingId: "sm-show-tab-pin"
      }),
      renderBooleanSetting({
        label: "Reddit",
        settingId: "sm-show-tab-redd"
      }),
      renderBooleanSetting({
        label: "Tumblr",
        settingId: "sm-show-tab-tumblr"
      }),
      renderBooleanSetting({
        label: "Twitter",
        settingId: "sm-show-tab-twit"
      }),
      renderBooleanSetting({
        label: "Youtube",
        settingId: "sm-show-tab-yt"
      }),
    ]),

    br(2),

    bold(["More Info"]),

    renderDropdown({
      label: "What is media sobriety?", 
      content: [
        br(),
        `The ability for social media to manipulate us has strayed far beyond advertisement, and into the morally dubious lands of changing our personalities, ethics, and world views.  These changes are very slow and subtle, but don't be fooled: third parties are paying top dollar to claim a piece of our minds.`,
        br(2),
        `This is done primarily through targeted "feeds", which have been designed using the most advanced technology money can buy.  They exist specifically to grab our attention as quickly as possible, for long as possible, using unbelievably powerful AI backed up with every psychology trick in the book.  Nobody is immune to this, not even the designers.`,
        br(2),
        `This is where Sober Media comes in: by blurring out various social media "hooks" and giving you the choice whether or not to pay them your attention, a thin shield of consent is put between your mind and potential manipulations.`,
        br(2),
        `If you have not seen the movie "The Social Dilemma", we'd rate it 5/5 for doing an amazing job of illuminating this subject. :)`,
        br(2),
      ]
    }),
    br(0.5),

    renderDropdown({
      label: "How to block ads.", 
      content: [
        br(),
        `If you'd like to cut advertisements out of your diet entirely many "Ad-Blockers" exist.  Here's a few we recommend:`,
        br(1.5),

        a({ href: "https://ublockorigin.com/"}, "uBlock Origin"),
        ` - Fairly simple, blocks trackers, has some neat advanced features.  Works well out of the box.`,
        br(0.5),

        a({ href: "https://www.ghostery.com/"}, "Ghostery"),
        ` - Super simple and has a cute icon.  If you're not tech savvy, start here.`,
        br(0.5),

        a({ href: "https://privacybadger.org/#How-is-Privacy-Badger-different-from-Disconnect%2c-Adblock-Plus%2c-Ghostery%2c-and-other-blocking-extensions"}, "Privacy Badger"),
        ` - Advanced, made by the EFF (Electronic Frontier Foundation), built to sniff out sneaky trackers (not just known ones).`,
        br(0.5),

        a({ href: "https://adblockplus.org/"}, "Ad Block Plus"),
        ` - One of the first ad blockers ever made.  A bit dated, but works well enough.`,
        
        br(2),
      ]
    }),

    br(0.5),
    renderDropdown({
      label: "What about TikTok and Instagram?  Mobile Apps?", 
      content: [
        br(),
        `Unfortunately, TikTok and Instagram are mobile only experiences.  As such, they free to use any form of tracking or manipulation they desire.  This is also true of the mobile app versions of every social media platform.`,
        br(2),
        `Whenever possible, it is highly recommended you sequester your "social media time" to web interfaces.  It will give you the option (using extensions) to block push notifications, advertisements, trackers (spyware), and nefarious content.`
      ]
    }),

    br(0.5),
    renderDropdown({
      label: "Ideas?  Contributions?  Dev?", 
      content: [
        br(),
        `If you'd like to share input for future direction of this extension, please submit an issue at the `,
        a({href: `https://gitlab.com/SephReed/sober-media/-/issues`}, "Sober Media Issues Page"),
        `.  Or -- if that's too much work -- you can try `,
        a({href: `https://twitter.com/intent/tweet?hashtags=SoberMedia&text=@SephReed%20I%20have%20thoughts%20about`}, "getting at me on twitter"),
        `.  Or do both and cross-link them.`,
        br(2),
        `If -- out of some random kindness -- you would like to give me money, I have a `,
        a({href: `https://www.paypal.com/cgi-bin/webscr?cmd=_donations&business=J2XGTAHLKE6PY&item_name=Just+felt+like+being+nice&currency_code=USD`}, "paypal donation link"),
        ` as well as a `,
        a({href: `https://www.patreon.com/seph_reed`}, "patreon."),
        br(2),
        `Lastly, if you're a dev and would like to work together on basically whatever, use either of the contact links above.  And if you're in the process of development, you can `,
        span({
          onPush: () => {
            const keepObj: Record<string, any> = {};
            noDeps(() => {
              Object.keys(SM.settings).filter((key) => {
                if (key.startsWith(FB_TRUST_SETTING_PREFIX)) { return true; }
                if (key.startsWith(TWIT_TRUST_SETTING_PREFIX)) { return true; }
                if (key.startsWith(BLUR_LINK_SETTING_PREFIX)) { return true; }
                // if (key.startsWith(NUM_LINKS_BLURRED_SETTING_PREFIX)) { return true; }
                if (key.startsWith(ADD_TIMER_SETTING_PREFIX)) { return true; }
                // if (key.startsWith(TIME_COUNT_SETTING_PREFIX)) { return true; }
              }).forEach((settingId) => {
                keepObj[settingId] = SM.settings[settingId as SettingId];
              });
            });
            console.log(keepObj);
            chrome.storage.sync.clear(() => {
              chrome.storage.sync.set(keepObj);
            });
          },
          innards: "restore all defaults"
        }),
        ` or `,
        span({
          onPush: () => chrome.storage.sync.clear(),
          innards: "hard reset the extension."
        }),
        
      ]
    }),
  ]);
}