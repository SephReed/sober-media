import { div, bold, br, Innards, span, italic, img, inputTextbox } from "helium-ui";
import { renderBooleanSetting, renderDeletableBooleanSetting } from "../../components/BooleanSetting/BooleanSetting";
import { SettingId, FB_TRUST_SETTING_PREFIX, TWIT_TRUST_SETTING_PREFIX, ADD_TIMER_SETTING_PREFIX, TIME_COUNT_SETTING_PREFIX } from "../../../middleware/SettingsId";
import { SM } from "../../state/State";
import { millisecToDisplayTime } from "../../../middleware/msToDisplayTime";


export function renderFacebookSettings() {
  return div("SettingsPage", [
    div("PageTitle", "Facebook Settings"),
    br(),
    renderCommonSettings({
      siteName: "Facebook",
      addTimer: "add-timer-www.facebook.com",
      disable: "fb-disable",
      hideLogo: "fb-remove-logo",
      trustTokens: "fb-trust-tokens",
    }),
    renderDisablableSection("fb-disable", [
      br(),
      bold(["Main Feed"]),
      renderBooleanSetting({
        label: "Blur Entire Feed",
        settingId: "fb-blur-home-feed"
      }),
      renderBooleanSetting({
        label: "Blur Stories",
        settingId: "fb-blur-stories"
      }),
      renderBooleanSetting({
        label: "Blur Untrusted Friends Posts",
        settingId: "fb-blur-untrusted"
      }),
      renderBooleanSetting({
        label: "Blur Comments",
        settingId: "fb-blur-post-comments"
      }),
      br(),
      bold(["Misc"]),
      renderBooleanSetting({
        label: "Blur Popularity Stats (likes, views, comment count, etc)",
        settingId: "fb-blur-popularity-stats",
      }),
      renderBooleanSetting({
        label: "Blur Photo Comments",
        settingId: "fb-blur-photo-comments"
      }),
      renderBooleanSetting({
        label: `Blur "Marketplace" Feed`,
        settingId: "fb-blur-marketplace-feed",
      }),
      renderBooleanSetting({
        label: `Blur "Watch" Feed`,
        settingId: "fb-blur-watch-feed",
      }),
      renderBooleanSetting({
        label: `Blur "Gaming" Feed`,
        settingId: "fb-blur-gaming-feed",
      }),
      br(),
      bold(["Topbar"]),
      renderBooleanSetting({
        label: "Blur Notifications",
        settingId: "fb-blur-notifications"
      }),
      renderBooleanSetting({
        label: `Blur "Gaming" Tab`,
        settingId: "fb-blur-tab-gaming"
      }),
      renderBooleanSetting({
        label: `Blur "Watch" Tab`,
        settingId: "fb-blur-tab-watch"
      }),
      renderBooleanSetting({
        label: `Blur "Groups" Tab`,
        settingId: "fb-blur-tab-groups"
      }),
      renderDisablableSection({ not: "fb-trust-tokens" }, [
        br(3),
        div("PageTitle", [span("TrustToken"), "Trust Tokens", span("TrustToken")]),
        div("center", italic([`list of users who you trust enough to automatically unblur their posts`])),
        br(),
        div("TrustTokenList", () => {
          const fbTrustTokens = Object.keys(SM.settings).filter((key) => key.startsWith(FB_TRUST_SETTING_PREFIX));
          if (!fbTrustTokens.length) {
            return div("center", "Currently, none of your friends have trust tokens");
          }
          return fbTrustTokens.map((settingId) => renderDeletableBooleanSetting({
            settingId: settingId as SettingId,
            label: settingId.replace(FB_TRUST_SETTING_PREFIX, ""),
          }))
        })
      ])
    ])
  ]);
}


export function renderTwitterSettings() {
  return div("SettingsPage", [
    div("PageTitle", "Twitter Settings"),
    renderCommonSettings({
      siteName: "Twitter",
      addTimer: "add-timer-twitter.com",
      disable: "twit-disable",
      hideLogo: "twit-remove-logo",
      trustTokens: "twit-trust-tokens",
    }),
    renderDisablableSection("twit-disable", [
      br(),
      bold(["Tweets"]),
      renderBooleanSetting({
        label: "Blur Home Screen Feed",
        settingId: "twit-blur-home-screen-feed"
      }),
      renderBooleanSetting({
        label: "Blur Untrusted Tweeters' Content",
        settingId: "twit-blur-tweet"
      }),
      br(),
      bold(["Follow More"]),
      renderBooleanSetting({
        label: `Blur "Trending Now"`,
        settingId: "twit-blur-trending-now"
      }),
      renderBooleanSetting({
        label: `Blur "Who to follow"`,
        settingId: "twit-blur-who-to-follow"
      }),
      renderBooleanSetting({
        label: `Blur "Relevant People"`,
        settingId: "twit-blur-relevant-people"
      }),
      br(3),
      renderDisablableSection({ not: "twit-trust-tokens" }, [
        div("PageTitle", [span("TrustToken"), "Trust Tokens", span("TrustToken")]),
        div("center", italic([`list of users who you trust enough to automatically unblur their posts`])),
        br(),
        div("TrustTokenList", () => {
          const fbTrustTokens = Object.keys(SM.settings).filter((key) => key.startsWith(TWIT_TRUST_SETTING_PREFIX));
          if (!fbTrustTokens.length) {
            return div("center", "Currently, no twitter handles have trust tokens");
          }
          return fbTrustTokens.map((settingId) => renderDeletableBooleanSetting({
            settingId: settingId as SettingId,
            label: settingId.replace(TWIT_TRUST_SETTING_PREFIX, ""),
          }))
        })
      ]),
    ]),
  ]);
}


export function renderYoutubeSettings() {
  return div("SettingsPage", [
    div("PageTitle", "Youtube Settings"),
    renderCommonSettings({
      siteName: "Youtube",
      addTimer: "add-timer-www.youtube.com",
      disable: "ytd-disable",
      hideLogo: "ytd-remove-logo"
    }),
    renderDisablableSection("ytd-disable", [
      br(),
      bold(["Removal"]),
      renderBooleanSetting({
        label: `Remove "Trending" Navigation Button`,
        settingId: "ytd-no-trending"
      }),
      br(),
      bold(["Blurring"]),
      renderBooleanSetting({
        label: "Blur Home Screen Feed",
        settingId: "ytd-blur-home-screen-feed"
      }),
      renderBooleanSetting({
        label: "Blur Recommendations Column Beside Video",
        settingId: "ytd-blur-recommendations"
      }),
      renderBooleanSetting({
        label: "Blur Comments Below Video",
        settingId: "ytd-blur-comments"
      }),
    ]),
  ]);
}


export function renderHackerNewsSettings() {
  const customRegexId: SettingId = "hn-custom-regex";
  let regexInput: HTMLInputElement;

  return div("SettingsPage", [
    div("PageTitle", "Hacker News Settings"),
    renderCommonSettings({
      siteName: "Hacker News",
      addTimer: "add-timer-news.ycombinator.com",
      disable: "hn-disable",
      hideLogo: "hn-remove-logo"
    }),
    renderDisablableSection("hn-disable", [
      br(),
      bold(["Post Presets"]),
      renderBooleanSetting({
        label: `Blur /apple/i`,
        settingId: "hn-blur-apple"
      }),
      renderBooleanSetting({
        label: `Blur /facebook/i`,
        settingId: "hn-blur-fb"
      }),
      renderBooleanSetting({
        label: `Blur /google/i`,
        settingId: "hn-blur-google"
      }),
      renderBooleanSetting({
        label: `Blur /microsoft/i`,
        settingId: "hn-blur-ms"
      }),
      renderBooleanSetting({
        label: `Blur /mozilla/i`,
        settingId: "hn-blur-moz"
      }),
      renderBooleanSetting({
        label: `Blur /twitter/i`,
        settingId: "hn-blur-twit"
      }),
      br(),
      bold(["Custom RegExp"]),
      br(),
      regexInput = inputTextbox({
        onInput: (value) => {
          if (value.startsWith("/") === false) {
            value = `/${value}`;
          } 
          if (/\/[ig]*$/.test(value) === false) {
            value += "/i"
          }
          regexInput.value = value;

          chrome.storage.sync.set({[customRegexId]: value});
        },
        ref: (ref) => {
          chrome.storage.sync.get(customRegexId, (obj) => {
            const value = obj[customRegexId] || "";
            (ref as HTMLInputElement).value = value;
          })
        }
      })
    ]),
  ]);
}


export function renderRedditSettings() {
  return div("SettingsPage", [
    div("PageTitle", "Reddit Settings"),
    renderCommonSettings({
      siteName: "Reddit",
      addTimer: "add-timer-www.reddit.com",
      disable: "redd-disable",
      hideLogo: "redd-remove-logo"
    }),
    renderDisablableSection("redd-disable", [
      br(),
      bold(["Home Feed"]),
      renderBooleanSetting({
        label: `Blur Home Feed`,
        settingId: "redd-blur-home-feed"
      }),
      renderBooleanSetting({
        label: `Blur Post's Content on Home Feed`,
        settingId: "redd-blur-home-posts"
      }),

      br(),
      bold(["Home Sidebar"]),

      renderBooleanSetting({
        label: `Remove Entire Sidebar from Home`,
        settingId: "redd-remove-home-sidebar"
      }),

      renderBooleanSetting({
        label: `Blur Sidebar Subreddit Suggestions`,
        settingId: "redd-blur-sub-suggs"
      }),

      renderBooleanSetting({
        label: `Blur "Trending Communities"`,
        settingId: "redd-blur-trending-communities"
      }),
    ]),
  ]);
}



export function renderPinterestSettings() {
  return div("SettingsPage", [
    div("PageTitle", "Pinterest Settings"),
    renderCommonSettings({
      siteName: "Pinterest",
      addTimer: "add-timer-www.pinterest.com",
      disable: "pin-disable",
      hideLogo: "pin-remove-logo"
    }),
    renderDisablableSection("pin-disable", [
      br(),
      bold(["Feeds"]),
      renderBooleanSetting({
        label: `Blur Home Feed`,
        settingId: "pin-blur-home-feed"
      }),
      renderBooleanSetting({
        label: `Blur Today Feed`,
        settingId: "pin-blur-today-feed"
      }),
      renderBooleanSetting({
        label: `Blur "More like this" Feed`,
        settingId: "pin-blur-more-like-feed"
      }),
    ]),
  ]);
}


export function renderTumblrSettings() {
  return div("SettingsPage", [
    div("PageTitle", "Tumblr Settings"),
    renderCommonSettings({
      siteName: "Tumblr",
      addTimer: "add-timer-www.tumblr.com",
      disable: "tum-disable",
      hideLogo: "tum-remove-logo"
    }),
    renderDisablableSection("tum-disable", [
      br(),
      bold(["Blurring"]),
      renderBooleanSetting({
        label: `Blur Dashboard Feed`,
        settingId: "tum-blur-dash-feed"
      }),
      renderBooleanSetting({
        label: `Blur Posts Content`,
        settingId: "tum-blur-posts"
      }),
      renderBooleanSetting({
        label: `Blur Dashboard Sidebar`,
        settingId: "tum-blur-dash-sidebar"
      }),
      br(2),
      bold(["Remove"]),
      renderBooleanSetting({
        label: `Remove "Trending" Nav Item`,
        settingId: "tum-remove-trending-nav"
      }),
    ]),
  ]);
}







function renderCommonSettings(args: {
  siteName: string;
  disable: SettingId;
  addTimer: SettingId;
  hideLogo?: SettingId;
  trustTokens?: SettingId;
}) {
  const timeToday = () => {
    const displayTime =SM.settings.get(args.addTimer.replace(ADD_TIMER_SETTING_PREFIX, TIME_COUNT_SETTING_PREFIX) as any);
    return displayTime && `(time today: ${millisecToDisplayTime(displayTime)})`;
  }
  return div("CommonSettings", [
    bold(["Common Settings"]),

    renderBooleanSetting({
      label: div("TimerSetting", [
        img({src: "/images/timer256.png"}), 
        `Enable Timer on ${args.siteName} `,
        () => timeToday()
      ]),
      settingId: args.addTimer,
    }),
    renderBooleanSetting({
      label: `Disable Modifications on ${args.siteName}`,
      settingId: args.disable,
      type: "RED"
    }),
    args.trustTokens && renderDisablableSection(args.disable, [
      renderBooleanSetting({
        label: [span("TrustToken"), `Enable Trust Tokens on ${args.siteName} Posts`],
        settingId: args.trustTokens
      }),
    ]),
    args.hideLogo && renderDisablableSection(args.disable, [
      renderBooleanSetting({
        label: `Hide ${args.siteName} Logo`,
        settingId: args.hideLogo
      }),
    ])
  ])
}



function renderDisablableSection(logic: SettingId | { not: SettingId }, innards: Innards) {
  const settingId = typeof logic === "string" ? logic : logic.not;
  
  return div("DisablableSection", {
    innards: innards,
    ddxClass: () => {
      let value = SM.settings[settingId];
      if (typeof logic === "object") { value = !value; }
      return value ? "--disableSection" : "";
    }
  })
}