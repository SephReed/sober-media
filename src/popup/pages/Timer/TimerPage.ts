import { div, br, bold, italic } from "helium-ui";
import { renderBooleanSetting, renderDeletableBooleanSetting, renderDeleteBoolean } from "../../components/BooleanSetting/BooleanSetting";
import { SM } from "../../state/State";
import { ADD_TIMER_SETTING_PREFIX, NUM_LINKS_BLURRED_SETTING_PREFIX, TIME_COUNT_SETTING_PREFIX } from "../../../middleware/SettingsId";
import { getHostMatchListForUrl, getCurrentUrl } from "../../Util";
import { millisecToDisplayTime } from "../../../middleware/msToDisplayTime";

const PREFIX = ADD_TIMER_SETTING_PREFIX;

export function renderTimePage() {
  return div("TimerPage", [
    div("PageTitle", "Timer & Settings"),
    br(2),
    div("TimeSpent center", [
      italic(["total time today:"]),
      div("Count", () => millisecToDisplayTime(SM.settings.get("sm-time-count-for-all") as unknown as number || 0)),
    ]),
    br(),
    bold(["General"]),
    renderBooleanSetting({
      settingId: "sm-timer-disabled",
      label: "Disable Timer on All Sites",
      type: "RED",
    }),

    div("DisablableSection", {
      ddxClass: () => SM.settings["sm-timer-disabled"] ? `--disableSection` : "",
    }, [
      renderBooleanSetting({
        settingId: "sm-timer-show-overlay",
        label: "Show Timer Overlay",
      }),
      br(2),
      bold(["Time This Site"]),
      div("TimeUrls", () => {
        const url = getCurrentUrl();
        if (!url) { return "Loading..."; }
        
        const host = url.hostname;
        return renderBooleanSetting({
          label: `Run Timer on ${host}`,
          settingId: PREFIX + host as any
        })
      }),
      br(3),
      div("PageTitle", "Timed Pages"),
      br(),
      div("TimedSiteList", () => {
        const timedSiteIds = Array.from(Object.keys(SM.settings)).filter((key) => key.startsWith(PREFIX));

        if (!timedSiteIds.length) {
          return div("center", "No sites are currently timed");
        }

        const lineItems = timedSiteIds.map((setId) => {
          const host = setId.replace(PREFIX, "");
          const totalTime = SM.settings.get(`${TIME_COUNT_SETTING_PREFIX}${host}` as any) || 0;
          return {
            totalTime,
            domNode: div("GridRow", [
              renderDeleteBoolean(setId as any),
              renderBooleanSetting({
                settingId: setId as any,
                label: null,
              }),
              div([host]),
              div([
                totalTime ? millisecToDisplayTime(totalTime) : "-"
              ]),
            ])
          }
        })

        return [
          div("GridRow", [
            "Remove",
            "Enable",
            "Site",
            "Time Count",
          ].map((name) => bold([name]))),
          ...lineItems.sort((a, b) => b.totalTime - a.totalTime).map((it) => it.domNode)
        ]
      })
    ]),
  ])
}