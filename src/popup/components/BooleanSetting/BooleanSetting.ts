import "./booleanSetting.scss";

import { Innards, Source, div } from "helium-ui";
import { SettingId } from "../../../middleware/SettingsId";
import { SM } from "../../state/State";

export interface IBooleanSettingProps {
  label: Innards;
  settingId: SettingId;
  type?: "GREEN" | "RED"
}

export function renderBooleanSetting(props: IBooleanSettingProps) {
  const {settingId} = props;

  return div("BooleanSetting", {
    class: props.type === "RED" ? "redOnEnable" : "",
    ddxClass: () => SM.settings[settingId] ? "--enabled" : "",
    onPush: () => {
      SM.settings[settingId] = !SM.settings[settingId];
    }
  },[
    div("Switch", [
      div("SwitchThumb")
    ]),
    props.label !== null ? div("Label", [props.label]) : null,
  ]);
}



export function renderDeletableBooleanSetting(props: IBooleanSettingProps) {
  return div("DeletableBooleanSetting", {
    ddxClass: () => props.settingId in SM.settings ? "" : "--removed"
  },[
    renderDeleteBoolean(props.settingId),
    renderBooleanSetting(props),
  ])
}

export function renderDeleteBoolean(settingId: SettingId) {
  return div("DeleteSetting", {
    title: "Remove Setting",
    onPush: () => delete SM.settings[settingId],
    innards: "X",
  });
}