import "./tabListPane.scss";


import { div, img } from "helium-ui";
import { PathId } from "../../state/PathId";
import { SM } from "../../state/State";
import { SettingId } from "../../../middleware/SettingsId";


const ArgsForTabs: Array<{
  path: PathId;
  backgroundColor: string;
  tooltip: string;
  imgSrc: string;
  shouldDisplaySettingId?: SettingId,
}> = [
  { path: "about",
    backgroundColor: "#FFE1AD",
    tooltip: "About Sober Media",
    imgSrc: "/images/sober_media256.png"
  },

  { path: "timer",
    backgroundColor: "#292929",
    tooltip: "Timer Settings",
    imgSrc: "/images/timer256.png"
  },

  { path: "flagged-sites",
    backgroundColor: "#ffe600",
    tooltip: "Flagged Websites",
    imgSrc: "/images/red_flag256.png"
  },

  { path: "facebook",
    backgroundColor: "#3b5998",
    tooltip: "Facebook settings",
    // imgSrc: "https://blog-assets.hootsuite.com/wp-content/uploads/2018/09/f-ogo_RGB_HEX-58.png",
    imgSrc: "https://facebook.com/favicon.ico",
    shouldDisplaySettingId: "sm-show-tab-fb",
  },

  { path: "hacker-news",
    backgroundColor: "#ff6502",
    tooltip: "Hacker News settings",
    imgSrc: "https://news.ycombinator.com/favicon.ico",
    shouldDisplaySettingId: "sm-show-tab-hn",
  },

  { path: "pinterest",
    backgroundColor: "#e60024",
    tooltip: "Pinterest settings",
    imgSrc: "https://www.pinterest.com/favicon.ico",
    shouldDisplaySettingId: "sm-show-tab-pin",
  },

  { path: "reddit",
    backgroundColor: "#ff4400",
    tooltip: "Reddit settings",
    imgSrc: "https://www.reddit.com/favicon.ico",
    shouldDisplaySettingId: "sm-show-tab-redd",
  },

  { path: "tumblr",
    backgroundColor: "#001935",
    tooltip: "Tumblr settings",
    imgSrc: "https://assets.tumblr.com/images/favicons/favicon.ico",
    shouldDisplaySettingId: "sm-show-tab-tumblr",
  },

  { path: "twitter",
    backgroundColor: "#1ea0f1",
    tooltip: "Twitter settings",
    // imgSrc: "https://twitter.com/favicon.ico",
    imgSrc: "https://mk0hootsuiteblof6bud.kinstacdn.com/wp-content/uploads/2018/09/Twitter_Logo_WhiteOnBlue-310x310.png",
    shouldDisplaySettingId: "sm-show-tab-twit",
  },

  { path: "youtube",
    backgroundColor: "#b31217",
    tooltip: "Youtube settings",
    imgSrc: "https://www.youtube.com/favicon.ico",
    shouldDisplaySettingId: "sm-show-tab-yt",
  },

  
]



export function renderTabListPane() {
  let lastColor: string;
  return div("TabListPane", [
    div("TabList", [
      ...ArgsForTabs.map((args) => {
        const settId = args.shouldDisplaySettingId as SettingId;
        return () => (settId && !SM.settings[settId]) ? null : div("TabThumb", {
          onPush: () => SM.path = args.path,
          ddxClass: () => SM.path === args.path ? "--selected" : "",
          style: `background-color: ${args.backgroundColor}`,
          title: args.tooltip,
          innards: [
            img({ src: args.imgSrc }),
          ]
        })
      }),
      // div("TabThumb filler")
    ]),
    div("TabPaneBorder", {
      ddx: (ref) => {
        const tabArgs = ArgsForTabs.find((it) => it.path === SM.path);
        if (!tabArgs) { return; }
        ref.style.backgroundColor = tabArgs.backgroundColor;
        // if (lastColor) {
        //   ref.style.backgroundImage = `radial-gradient(${tabArgs.backgroundColor}, ${lastColor})`;
        // } else {
        // }
        // lastColor = tabArgs.backgroundColor;
      }
    })
  ]);
}