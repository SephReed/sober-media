import "./dropDown.scss";

import { Innards, div, Source } from "helium-ui";
import { tweener } from "helium-ui-tweener";

export function renderDropdown(props: {
  label: Innards,
  content: Innards,
}) {
  const showContent = new Source(false);
  const out = div("Dropdown", {
    ddxClass: () => showContent.get() ? "--showContent" : "",
  },[
    div("DropdownLabel", {
      onPush: () => showContent.set(!showContent.get()),
      innards: props.label,
    }),
    div("DropdownContent", tweener({
      type: "none",
    }, () => showContent.get() && props.content))
  ]);
  return out; 
}