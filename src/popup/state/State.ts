import { Sourcify, derive } from "helium-ui";
import { PathId } from "./PathId";
import { SettingId, BLUR_LINK_SETTING_PREFIX } from "../../middleware/SettingsId";


export const SM = Sourcify<{
  path: PathId;
  settings: Record<SettingId, boolean>;
}>({
  path: "" as any,
  settings: {
    "sm-initialized": false,

    "fb-disable": false,
    "fb-remove-logo": true,
    "fb-trust-tokens": true,
    "fb-blur-stories": true,
    "fb-blur-home-feed": true,
    "fb-blur-marketplace-feed": true,
    "fb-blur-watch-feed": true,
    "fb-blur-gaming-feed": true,
    "fb-blur-untrusted": true,
    "fb-blur-post-comments": true,
    "fb-blur-notifications": true,
    "fb-blur-tab-watch": true,
    "fb-blur-tab-gaming": true,
    "fb-blur-tab-market": true,
    "fb-blur-tab-groups": true,
    "fb-blur-photo-comments": true, 
    "fb-blur-popularity-stats": true,

    "twit-disable": false,
    "twit-remove-logo": true,
    "twit-trust-tokens": true,
    "twit-blur-home-screen-feed": true,
    "twit-blur-tweet": true,
    "twit-blur-trending-now": true,
    "twit-blur-who-to-follow": true,
    "twit-blur-relevant-people": true,

    "ytd-disable": false,
    "ytd-remove-logo": true,
    "ytd-no-trending": true,
    "ytd-blur-home-screen-feed": true,
    "ytd-blur-recommendations": true,
    "ytd-blur-comments": true,

    "hn-disable": false,
    "hn-remove-logo": false,
    "hn-blur-apple": true,
    "hn-blur-fb": true,
    "hn-blur-google": true,
    "hn-blur-ms": true,
    "hn-blur-moz": true,
    "hn-blur-twit": true,
    "hn-custom-regex": "/buzzfeed/i" as any,

    "redd-disable": false,
    "redd-remove-logo": true,
    "redd-blur-home-feed": true,
    "redd-blur-home-posts": true,
    "redd-remove-home-sidebar": true,
    "redd-blur-sub-suggs": true,
    "redd-blur-trending-communities": true,

    "pin-disable": false,
    "pin-remove-logo": true,
    "pin-blur-home-feed": true,
    "pin-blur-today-feed": true,
    "pin-blur-more-like-feed": true,
    
    "tum-disable": false,
    "tum-remove-logo": true,
    "tum-blur-dash-feed": true,
    "tum-blur-posts": true,
    "tum-blur-dash-sidebar": true,
    "tum-remove-trending-nav": true,

    "sm-show-tab-fb": true,
    "sm-show-tab-hn": false,
    "sm-show-tab-pin": false,
    "sm-show-tab-redd": false,
    "sm-show-tab-tumblr": false,
    "sm-show-tab-twit": true,
    "sm-show-tab-yt": true,

    "sm-timer-disabled": false,
    "sm-link-blurring-disabled": false,
    "sm-timer-show-overlay": true,

    "add-timer-www.facebook.com": true,
    "add-timer-twitter.com": true,
    "add-timer-www.youtube.com": true,
    "add-timer-news.ycombinator.com": true,
    "add-timer-www.reddit.com": true,
    "add-timer-www.pinterest.com": true,
    "add-timer-www.tumblr.com": true,

    "sm-time-count-for-all": 0 as any,
  }
});

let showTab: SettingId;
chrome.tabs.query({ active: true, currentWindow: true }, (tabs) => {
  const tab = tabs[0];
  if (!tab || !tab.url) { 
    SM.path = "about";
    return; 
  }

  const url = new URL(tab.url);
  switch (url.hostname) {
    case "www.facebook.com": SM.path = "facebook"; showTab = "sm-show-tab-fb"; break;
    case "news.ycombinator.com": SM.path = "hacker-news"; showTab = "sm-show-tab-hn"; break;
    case "www.pinterest.com": SM.path = "pinterest"; showTab = "sm-show-tab-pin"; break;
    case "www.reddit.com": SM.path = "reddit"; showTab = "sm-show-tab-redd"; break;
    case "www.tumblr.com": SM.path = "tumblr"; showTab = "sm-show-tab-tumblr"; break;
    case "twitter.com": SM.path = "twitter"; showTab = "sm-show-tab-twit"; break;
    case "www.youtube.com": SM.path = "youtube"; showTab = "sm-show-tab-yt"; break;
    default: SM.path = "about"; break;
  }
})


chrome.storage.sync.get(null, (items) => {
  if (showTab) {
    items[showTab] = true;
  }
  if (!items["sm-initialized"]) {
    items[`${BLUR_LINK_SETTING_PREFIX}buzzfeed.com`] = true;
    items[`${BLUR_LINK_SETTING_PREFIX}buzzfeednews.com`] = true;
    items["sm-initialized"] = true;
  }
  SM.settings.upsert(items);

  let lastKeys = Array.from(Object.keys(SM.settings));
  derive(() => {
    const allItems = SM.settings.toObject();
    const currentKeys = Array.from(Object.keys(SM.settings));
    chrome.storage.sync.set(allItems);

    const removedKeys = lastKeys.filter((key) => !currentKeys.includes(key));
    lastKeys = currentKeys;
    console.log("Removed Keys", lastKeys, currentKeys, removedKeys);
    removedKeys.forEach((key) => chrome.storage.sync.remove(key));
  })
});


chrome.storage.onChanged.addListener((changes, namespace) => {
  for (const _key in changes) {
    const key = _key as SettingId;
    SM.settings[key] = changes[key].newValue;
  }
});
