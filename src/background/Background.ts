import { ContentScript } from "../middleware/ContentScriptEnum";
import { ADD_TIMER_SETTING_PREFIX, SettingId, TIME_COUNT_SETTING_PREFIX } from "../middleware/SettingsId";
import { findTimerReason } from "../middleware/FindTimerReason";

// manifest.json
// "commands": {
//   "Ctrl+M": {
//       "suggested_key": {
//           "default": "Ctrl+M",
//           "mac": "Command+M"
//       },
//       "description": "Ctrl+M."
//   }
// },

chrome.commands.onCommand.addListener((shortcut) => {
  if(shortcut.includes("+M")) {
    chrome.runtime.reload();
  }
});

const injectArgs: Array<{
  hostname: string;
  js: string;
  contentScriptId: ContentScript;
}> = [
  { hostname: "www.youtube.com",
    js: "content_scripts/youtube.js",
    contentScriptId: ContentScript.YOUTUBE,
  },
  { hostname: "www.facebook.com",
    js: "content_scripts/facebook.js",
    contentScriptId: ContentScript.FACEBOOK,
  },
  { hostname: "www.reddit.com",
    js: "content_scripts/reddit.js",
    contentScriptId: ContentScript.REDDIT,
  },
  { hostname: "www.pinterest.com",
    js: "content_scripts/pinterest.js",
    contentScriptId: ContentScript.PINTEREST,
  },
  { hostname: "twitter.com",
    js: "content_scripts/twitter.js",
    contentScriptId: ContentScript.TWITTER,
  },
  { hostname: "www.tumblr.com",
    js: "content_scripts/tumblr.js",
    contentScriptId: ContentScript.TUMBLR,
  },
  { hostname: "news.ycombinator.com",
    js: "content_scripts/hacker-news.js",
    contentScriptId: ContentScript.HACKER_NEWS,
  },
];

const disableBlurLinkSettingId: SettingId = "sm-link-blurring-disabled";
const disableTimerSettingId: SettingId = "sm-timer-disabled";

chrome.runtime.onInstalled.addListener(function() {
  setupTimeTotalUpdater();

  chrome.tabs.onUpdated.addListener((tabId, changeInfo, tab) => {
    if (changeInfo.status === "loading" && tab.url) {
      const url = new URL(tab.url);
      console.log(url.hostname);

      chrome.storage.sync.get(disableBlurLinkSettingId, (obj) => {
        if (obj[disableBlurLinkSettingId] !== true) {
          addBlurLinks(tab);
        }
      });

      chrome.storage.sync.get(disableTimerSettingId, (obj) => {
        if (obj[disableTimerSettingId] !== true) {
          tryAddTimer(tab);
        }
      });


      injectArgs.forEach((inject) => {
        if (url.hostname.endsWith(inject.hostname)) {
          chrome.tabs.sendMessage(tabId, {checkForContentScript: inject.contentScriptId}, (resp) => {
            if (resp !== true) {
              // addScriptToTab(tabId, inject.js);
              chrome.tabs.executeScript(tabId, { file: inject.js });
              chrome.tabs.insertCSS(tabId, { file: "sober-media.css"});
            }
          });
        }
      })
    }
  });

  chrome.storage.onChanged.addListener((changes) => {
    if (changes[disableTimerSettingId] && changes[disableTimerSettingId].newValue === false) {
      chrome.tabs.query({}, (tabs) => {     
        tabs.forEach((tab) => tryAddTimer(tab))
      });
    }
    
    if (changes[disableBlurLinkSettingId] && changes[disableBlurLinkSettingId].newValue === false) {
      chrome.tabs.query({}, (tabs) => {     
        tabs.forEach((tab) => addBlurLinks(tab));
      });
    }

    const timerAdded = Object.keys(changes).find((settingId) => {
      return settingId.startsWith(ADD_TIMER_SETTING_PREFIX);
    });
    if (timerAdded) {
      chrome.storage.sync.get(disableTimerSettingId, (obj) => {
        if (obj[disableTimerSettingId] !== true) {
          chrome.tabs.query({}, (tabs) => {     
            tabs.forEach((tab) => tryAddTimer(tab))
          });
        }
      })
    }
  })
});



function tryAddTimer(tab: chrome.tabs.Tab) {
  findTimerReason(tab.url).then((reason) => {
    if (!reason || !tab.id) { return; }
    const tabId = tab.id;

    chrome.tabs.sendMessage(tabId, {checkForContentScript: ContentScript.ADD_TIMER}, (resp) => {
      if (resp !== true) {
        chrome.tabs.executeScript(tabId, { file: "content_scripts/interaction-timer.js" });
        chrome.tabs.insertCSS(tabId, { file: "timer.css"});
      }
    });
  })
}


function addBlurLinks(tab: chrome.tabs.Tab) {
  if (!tab.id) { return; }
  const tabId = tab.id;
  chrome.tabs.sendMessage(tabId, {checkForContentScript: ContentScript.BLUR_LINKS}, (resp) => {
    if (resp !== true) {
      chrome.tabs.executeScript(tabId, { file: "content_scripts/blur-links.js" });
      chrome.tabs.insertCSS(tabId, { file: "sober-media.css"});
    }
  });
}



function setupTimeTotalUpdater() {
  const allTimes: Record<string, number> = {};
  const allId: SettingId = "sm-time-count-for-all";

  const sumAllTimes = () => {
    let sum = 0;
    Object.entries(allTimes).forEach(([key, value]) => {
      if (key !== allId) {
        sum += value;
      }
    });
    chrome.storage.sync.set({[allId]: sum});
  }

  chrome.storage.sync.get(null, (stor) => {
    Object.entries(stor).forEach(([storId, value]) => {
      if (storId.startsWith(TIME_COUNT_SETTING_PREFIX) === false) { return; }
      allTimes[storId] = typeof value === "number" ? value: 0;
    });
    sumAllTimes();
  });

  chrome.storage.onChanged.addListener((changes) => {
    let timeAdded = false;
    Object.entries(changes).forEach(([storId, change]) => {
      if (storId.startsWith(TIME_COUNT_SETTING_PREFIX) === false) { return; }
      timeAdded = true;
      const value = change.newValue;
      allTimes[storId] = typeof value === "number" ? value: 0;
    });

    if (timeAdded) {
      sumAllTimes();
      checkForDayChange();
    }
  })
}


function checkForDayChange() {
  const dayRestartId = "sm-next-day-restart";

  chrome.storage.sync.get(dayRestartId, (stor) => {
    const updateResetTime = () => {
      const nextChange = new Date();
      // nextChange.setSeconds(nextChange.getSeconds() + 15);
      nextChange.setDate(nextChange.getDate() + 1);
      nextChange.setHours(4);
      chrome.storage.sync.set({[dayRestartId]: nextChange.toISOString()});
    }

    if (!stor || !stor[dayRestartId]) {
      updateResetTime();
      return;
    }
    const restartTime = new Date(stor[dayRestartId]);
    if (restartTime.getTime() <= Date.now()) {
      chrome.storage.sync.get(null, (stor) => {
        const update = {} as any;
        Object.entries(stor).forEach(([storId, value]) => {
          if (storId.startsWith(TIME_COUNT_SETTING_PREFIX) === false) { return; }
          update[storId] = 0;
        });
        chrome.storage.sync.set(update);
        updateResetTime();
      });
    }
  });
}



// const BATCH_TIMEOUT = 1500; //ms
// const addScriptBatchers = new Map<number, Map<string, void>>();
// function addScriptToTab(tabId: number, file: string) {
//   let batch = addScriptBatchers.get(tabId);
//   if (batch && batch.has(file)) {
//     return;
//   } else if (!batch) {
//     addScriptBatchers.set(tabId, batch = new Map());
//   }

//   batch.set(file);
//   console.log("executing script", file);
//   chrome.tabs.executeScript(tabId, { file });
//   setTimeout(() => batch && batch.delete(file), BATCH_TIMEOUT);
// }


