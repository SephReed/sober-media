export function millisecToDisplayTime(ms: number) {
  const ds = ms / 1000;
  const seconds = ds % 60;
  const minutes = Math.floor(ds / 60) % 60;
  const hours = Math.floor(ds / 3600);

  let secondsStr = String(seconds);
  if (seconds < 10) { secondsStr = "0" + secondsStr; }

  let minutesStr = String(minutes);
  if (hours && minutes < 10) { minutesStr = "0" + minutesStr; }

  let out = ``;
  if (hours) {
    out += hours + ":";
  }
  out += `${minutesStr}:${secondsStr.slice(0, 2)}`

  return out;
}