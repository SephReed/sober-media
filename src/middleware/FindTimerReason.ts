import { ADD_TIMER_SETTING_PREFIX, SettingId } from "./SettingsId";
import { url } from "inspector";

export async function findTimerReason(href?: string): Promise<SettingId | undefined> {
  if (!href) { return; }
  return new Promise((resolve) => {
    chrome.storage.sync.get(null, (settings) => {
      const addTimerReason = Object.keys(settings).find((settingId) => {
        if (settingId.startsWith(ADD_TIMER_SETTING_PREFIX) === false) { return false; }
        if (settings[settingId] !== true) { return false; }
        const host = settingId.replace(ADD_TIMER_SETTING_PREFIX, "");
        return href.includes(host);
      });
      resolve(addTimerReason as any);
    });
  });
}

export async function findPotentialTimerReasons(href?: string): Promise<SettingId[] | undefined> {
  if (!href) { return; }
  return new Promise((resolve) => {
    chrome.storage.sync.get(null, (settings) => {
      const addTimerReason = Object.keys(settings).filter((settingId) => {
        if (settingId.startsWith(ADD_TIMER_SETTING_PREFIX) === false) { return false; }
        const host = settingId.replace(ADD_TIMER_SETTING_PREFIX, "");
        return href.includes(host);
      });
      resolve(addTimerReason as any);
    });
  });
}