export const SETTING_STORAGE_ID = "BOOLEAN_SETTINGS";
export const BLUR_LINK_SETTING_PREFIX = `blur-link-`;
export const NUM_LINKS_BLURRED_SETTING_PREFIX = `num-links-blurred`;
export const ADD_TIMER_SETTING_PREFIX = `add-timer-`;

export const TIME_COUNT_SETTING_PREFIX = `sm-time-count-for-`;

export const FB_TRUST_SETTING_PREFIX = `fb-grant-trust-`;
export const TWIT_TRUST_SETTING_PREFIX = `twit-grant-trust-`;


export const SettingIdList = [
  "fb-disable",
  "fb-remove-logo",
  "fb-trust-tokens",
  "fb-blur-home-feed",
  "fb-blur-watch-feed",
  "fb-blur-marketplace-feed",
  "fb-blur-gaming-feed",
  "fb-blur-stories",
  "fb-blur-untrusted",
  "fb-blur-post-comments",
  "fb-blur-photo-comments",
  "fb-blur-notifications",
  "fb-blur-tab-gaming",
  "fb-blur-tab-watch",
  "fb-blur-tab-groups",
  "fb-blur-tab-market",
  "fb-blur-popularity-stats",

  "twit-disable",
  "twit-remove-logo",
  "twit-trust-tokens",
  "twit-blur-home-screen-feed",
  "twit-blur-tweet",
  "twit-blur-trending-now",
  "twit-blur-who-to-follow",
  "twit-blur-relevant-people",

  "ytd-disable",
  "ytd-remove-logo",
  "ytd-no-trending",
  "ytd-blur-home-screen-feed",
  "ytd-blur-recommendations",
  "ytd-blur-comments",

  "hn-disable",
  "hn-remove-logo",
  "hn-blur-apple",
  "hn-blur-fb",
  "hn-blur-google",
  "hn-blur-ms",
  "hn-blur-moz",
  "hn-blur-twit",
  "hn-custom-regex",

  "redd-disable",
  "redd-remove-logo",
  "redd-blur-home-feed",
  "redd-blur-home-posts",
  "redd-remove-home-sidebar",
  "redd-blur-sub-suggs",
  "redd-blur-trending-communities",

  "pin-disable",
  "pin-remove-logo",
  "pin-blur-home-feed",
  "pin-blur-today-feed",
  "pin-blur-more-like-feed",
  
  "tum-disable",
  "tum-remove-logo",
  "tum-blur-dash-feed",
  "tum-blur-posts",
  "tum-blur-dash-sidebar",
  "tum-remove-trending-nav",

  "sm-show-tab-fb",
  "sm-show-tab-hn",
  "sm-show-tab-pin",
  "sm-show-tab-redd",
  "sm-show-tab-tumblr",
  "sm-show-tab-twit",
  "sm-show-tab-yt",

  "sm-initialized",
  "sm-timer-disabled",
  "sm-link-blurring-disabled",
  "sm-timer-show-overlay",

  "add-timer-www.facebook.com",
  "add-timer-twitter.com",
  "add-timer-www.youtube.com",
  "add-timer-news.ycombinator.com",
  "add-timer-www.reddit.com",
  "add-timer-www.pinterest.com",
  "add-timer-www.tumblr.com",

  "sm-time-count-for-all"
] as const;


export type SettingId = typeof SettingIdList[number];