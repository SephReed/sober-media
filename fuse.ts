import { fusebox } from 'fuse-box';



const popupBundler = fusebox({
  entry: 'src/popup/Popup.ts',
  target: "browser",
  sourceMap: false,
  cache: false,
  webIndex: {
    template: 'src/popup/Popup.html',
    distFileName: "popup.html"
  },
});

popupBundler.runDev({
  bundles: {
    distRoot: "./extension/popup",
    app: "popup.js"
  }
});


const bundles: Array<{
  entry: string;
  root: string;
  fileName: string;
}> = [
  { entry: 'src/background/Background.ts',
    root: "./extension/background",
    fileName: "background.js",
  },

  { entry: 'src/content_scripts/sites/YouTube.content.ts',
    root: "./extension/content_scripts",
    fileName: "youtube.js",
  },

  { entry: 'src/content_scripts/sites/Facebook.content.ts',
    root: "./extension/content_scripts",
    fileName: "facebook.js",
  },

  { entry: 'src/content_scripts/sites/Twitter.content.ts',
    root: "./extension/content_scripts",
    fileName: "twitter.js",
  },

  { entry: 'src/content_scripts/sites/HackerNews.content.ts',
    root: "./extension/content_scripts",
    fileName: "hacker-news.js",
  },

  { entry: 'src/content_scripts/sites/Reddit.content.ts',
    root: "./extension/content_scripts",
    fileName: "reddit.js",
  },

  { entry: 'src/content_scripts/sites/Pinterest.content.ts',
    root: "./extension/content_scripts",
    fileName: "pinterest.js",
  },

  { entry: 'src/content_scripts/sites/Tumblr.content.ts',
    root: "./extension/content_scripts",
    fileName: "tumblr.js",
  },

  { entry: 'src/content_scripts/common/BlurLinks.content.ts',
    root: "./extension/content_scripts",
    fileName: "blur-links.js",
  },

  { entry: 'src/content_scripts/common/InteractionTimer.content.ts',
    root: "./extension/content_scripts",
    fileName: "interaction-timer.js",
  },
] 



bundles.forEach((bundle) => {
  const bundler = fusebox({
    entry: bundle.entry,
    target: "browser",
    sourceMap: false,
    cache: false,
  });

  bundler.runDev({
    uglify: false,
    bundles: {
      distRoot: bundle.root,
      app: bundle.fileName
    }
  });
})




