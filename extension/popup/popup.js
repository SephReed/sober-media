(function() {
  var f = window.__fuse = window.__fuse || {};
  var modules = f.modules = f.modules || {}; f.dt = function (x) { return x && x.__esModule ? x : { "default": x }; };

  f.bundle = function(collection, fn) {
    for (var num in collection) {
      modules[num] = collection[num];
    }
    fn ? fn() : void 0;
  };
  f.c = {};
  f.r = function(id) {
    var cached = f.c[id];
    if (cached) return cached.m.exports;
    var module = modules[id];
    if (!module) {
      
      throw new Error('Module ' + id + ' was not found');
    }
    cached = f.c[id] = {};
    cached.exports = {};
    cached.m = { exports: cached.exports };
    module(f.r, cached.exports, cached.m);
    return cached.m.exports;
  }; 
})();
__fuse.bundle({

// src/popup/Popup.ts @1
1: function(__fusereq, exports, module){
exports.__esModule = true;
__fusereq(2);
var helium_ui_1 = __fusereq(4);
var TabListPane_1 = __fusereq(5);
var State_1 = __fusereq(6);
var KnownSitesSettings_1 = __fusereq(7);
var AboutPage_1 = __fusereq(8);
var TimerPage_1 = __fusereq(9);
var FlaggedSitesPage_1 = __fusereq(10);
var helium_ui_tweener_1 = __fusereq(11);
const app = renderApp();
helium_ui_1.onDomReady(() => {
  helium_ui_1.setInnards(helium_ui_1.byId("root"), app);
});
function renderApp() {
  return helium_ui_1.div("SoberMediaPopup", [TabListPane_1.renderTabListPane(), helium_ui_1.div("ContentPane", helium_ui_tweener_1.tweener({
    type: "crossfade-on-top",
    disableResize: true
  }, () => {
    switch (State_1.SM.path) {
      case "about":
        return AboutPage_1.renderAboutPage();
      case "timer":
        return TimerPage_1.renderTimePage();
      case "flagged-sites":
        return FlaggedSitesPage_1.renderFlaggedSitesPage();
      case "facebook":
        return KnownSitesSettings_1.renderFacebookSettings();
      case "hacker-news":
        return KnownSitesSettings_1.renderHackerNewsSettings();
      case "pinterest":
        return KnownSitesSettings_1.renderPinterestSettings();
      case "reddit":
        return KnownSitesSettings_1.renderRedditSettings();
      case "tumblr":
        return KnownSitesSettings_1.renderTumblrSettings();
      case "twitter":
        return KnownSitesSettings_1.renderTwitterSettings();
      case "youtube":
        return KnownSitesSettings_1.renderYoutubeSettings();
    }
  }))]);
}

},

// src/popup/popup.scss @2
2: function(__fusereq, exports, module){
__fusereq(3)("src/popup/popup.scss","html {\n  background: #FFFAF6; }\n\nhtml, body {\n  padding: 0px;\n  margin: 0px;\n  min-height: 100vh;\n  min-width: 100%; }\n\nbody {\n  box-sizing: border-box;\n  font-family: Hind, Arial, Helvetica, sans-serif;\n  color: #222; }\n  body *, body *::before, body *::after {\n    box-sizing: inherit; }\n\n.onPush {\n  cursor: pointer; }\n\n.center {\n  text-align: center; }\n\na {\n  text-decoration: none;\n  color: #006c80; }\n  a:hover {\n    text-decoration: underline; }\n\n.SoberMediaPopup {\n  width: 500px;\n  min-height: 325px;\n  max-height: 600px;\n  overflow: auto;\n  display: flex;\n  flex-direction: column; }\n  .SoberMediaPopup > .ContentPane {\n    flex: 1;\n    overflow: auto; }\n  .SoberMediaPopup .SettingsPage, .SoberMediaPopup .AboutPage, .SoberMediaPopup .TimerPage, .SoberMediaPopup .FlaggedSitesPage {\n    padding: 1em;\n    margin-bottom: 1em; }\n    .SoberMediaPopup .SettingsPage .BooleanSetting, .SoberMediaPopup .AboutPage .BooleanSetting, .SoberMediaPopup .TimerPage .BooleanSetting, .SoberMediaPopup .FlaggedSitesPage .BooleanSetting {\n      margin-bottom: 0.25em; }\n    .SoberMediaPopup .SettingsPage .DisablableSection.--disableSection, .SoberMediaPopup .AboutPage .DisablableSection.--disableSection, .SoberMediaPopup .TimerPage .DisablableSection.--disableSection, .SoberMediaPopup .FlaggedSitesPage .DisablableSection.--disableSection {\n      pointer-events: none;\n      opacity: 0.25; }\n    .SoberMediaPopup .SettingsPage .TimerSetting > img, .SoberMediaPopup .AboutPage .TimerSetting > img, .SoberMediaPopup .TimerPage .TimerSetting > img, .SoberMediaPopup .FlaggedSitesPage .TimerSetting > img {\n      height: 1.5em;\n      margin-right: 0.25em;\n      vertical-align: sub; }\n    .SoberMediaPopup .SettingsPage .TrustToken, .SoberMediaPopup .AboutPage .TrustToken, .SoberMediaPopup .TimerPage .TrustToken, .SoberMediaPopup .FlaggedSitesPage .TrustToken {\n      display: inline-block;\n      position: relative;\n      height: 1.5em;\n      width: 1.5em;\n      border-radius: 5em;\n      border: 2px solid goldenrod;\n      vertical-align: sub;\n      margin: 0px 0.25em; }\n      .SoberMediaPopup .SettingsPage .TrustToken::before, .SoberMediaPopup .AboutPage .TrustToken::before, .SoberMediaPopup .TimerPage .TrustToken::before, .SoberMediaPopup .FlaggedSitesPage .TrustToken::before {\n        display: block;\n        position: absolute;\n        content: \"\";\n        top: 0px;\n        left: 0px;\n        right: 0px;\n        bottom: 0px;\n        background-size: 90%;\n        background-position: center center;\n        background-repeat: no-repeat;\n        background-image: url(\"data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg' height='210' width='210'><polygon points='100,10 40,198 190,78 10,78 160,198' style='fill:goldenrod;'/></svg>\"); }\n  .SoberMediaPopup .PageTitle {\n    text-align: center;\n    text-transform: uppercase;\n    font-size: 0.85em;\n    font-weight: bold; }\n  .SoberMediaPopup .FlaggedSitesPage .FlagSiteList,\n  .SoberMediaPopup .TimerPage .TimedSiteList {\n    display: grid;\n    grid-template-columns: auto auto 1fr auto; }\n    .SoberMediaPopup .FlaggedSitesPage .FlagSiteList .Switch,\n    .SoberMediaPopup .TimerPage .TimedSiteList .Switch {\n      margin: 0px; }\n  .SoberMediaPopup .TimerPage .TimeSpent > .Count {\n    font-size: 3em; }\n  .SoberMediaPopup .GridRow {\n    display: contents; }\n    .SoberMediaPopup .GridRow > * {\n      margin: auto;\n      padding: 0px 0.5em; }\n\ntable {\n  width: 100%; }\n  table th {\n    font-weight: bold; }\n")
},

// node_modules/fuse-box/modules/fuse-box-css/index.js @3
3: function(__fusereq, exports, module){
var cssHandler = function (__filename, contents) {
  var styleId = __filename.replace(/[\.\/]+/g, '-');
  if (styleId.charAt(0) === '-') styleId = styleId.substring(1);
  var exists = document.getElementById(styleId);
  if (!exists) {
    var s = document.createElement(contents ? 'style' : 'link');
    s.id = styleId;
    s.type = 'text/css';
    if (contents) {
      s.innerHTML = contents;
    } else {
      s.rel = 'stylesheet';
      s.href = __filename;
    }
    document.getElementsByTagName('head')[0].appendChild(s);
  } else {
    if (contents) exists.innerHTML = contents;
  }
};
module.exports = cssHandler;

},

// node_modules/helium-ui/dist/index.js @4
4: function(__fusereq, exports, module){
"use strict";
var __createBinding = this && this.__createBinding || (Object.create ? function (o, m, k, k2) {
  if (k2 === undefined) k2 = k;
  Object.defineProperty(o, k2, {
    enumerable: true,
    get: function () {
      return m[k];
    }
  });
} : function (o, m, k, k2) {
  if (k2 === undefined) k2 = k;
  o[k2] = m[k];
});
var __exportStar = this && this.__exportStar || (function (m, exports) {
  for (var p in m) if (p !== "default" && !exports.hasOwnProperty(p)) __createBinding(exports, m, p);
});
Object.defineProperty(exports, "__esModule", {
  value: true
});
__exportStar(__fusereq(12), exports);
__exportStar(__fusereq(13), exports);
__exportStar(__fusereq(14), exports);
__exportStar(__fusereq(15), exports);
__exportStar(__fusereq(16), exports);
__exportStar(__fusereq(17), exports);

},

// src/popup/components/TabListPane/TabListPane.ts @5
5: function(__fusereq, exports, module){
exports.__esModule = true;
__fusereq(18);
var helium_ui_1 = __fusereq(4);
var State_1 = __fusereq(6);
const ArgsForTabs = [{
  path: "about",
  backgroundColor: "#FFE1AD",
  tooltip: "About Sober Media",
  imgSrc: "/images/sober_media256.png"
}, {
  path: "timer",
  backgroundColor: "#292929",
  tooltip: "Timer Settings",
  imgSrc: "/images/timer256.png"
}, {
  path: "flagged-sites",
  backgroundColor: "#ffe600",
  tooltip: "Flagged Websites",
  imgSrc: "/images/red_flag256.png"
}, {
  path: "facebook",
  backgroundColor: "#3b5998",
  tooltip: "Facebook settings",
  imgSrc: "https://facebook.com/favicon.ico",
  shouldDisplaySettingId: "sm-show-tab-fb"
}, {
  path: "hacker-news",
  backgroundColor: "#ff6502",
  tooltip: "Hacker News settings",
  imgSrc: "https://news.ycombinator.com/favicon.ico",
  shouldDisplaySettingId: "sm-show-tab-hn"
}, {
  path: "pinterest",
  backgroundColor: "#e60024",
  tooltip: "Pinterest settings",
  imgSrc: "https://www.pinterest.com/favicon.ico",
  shouldDisplaySettingId: "sm-show-tab-pin"
}, {
  path: "reddit",
  backgroundColor: "#ff4400",
  tooltip: "Reddit settings",
  imgSrc: "https://www.reddit.com/favicon.ico",
  shouldDisplaySettingId: "sm-show-tab-redd"
}, {
  path: "tumblr",
  backgroundColor: "#001935",
  tooltip: "Tumblr settings",
  imgSrc: "https://assets.tumblr.com/images/favicons/favicon.ico",
  shouldDisplaySettingId: "sm-show-tab-tumblr"
}, {
  path: "twitter",
  backgroundColor: "#1ea0f1",
  tooltip: "Twitter settings",
  imgSrc: "https://mk0hootsuiteblof6bud.kinstacdn.com/wp-content/uploads/2018/09/Twitter_Logo_WhiteOnBlue-310x310.png",
  shouldDisplaySettingId: "sm-show-tab-twit"
}, {
  path: "youtube",
  backgroundColor: "#b31217",
  tooltip: "Youtube settings",
  imgSrc: "https://www.youtube.com/favicon.ico",
  shouldDisplaySettingId: "sm-show-tab-yt"
}];
function renderTabListPane() {
  let lastColor;
  return helium_ui_1.div("TabListPane", [helium_ui_1.div("TabList", [...ArgsForTabs.map(args => {
    const settId = args.shouldDisplaySettingId;
    return () => settId && !State_1.SM.settings[settId] ? null : helium_ui_1.div("TabThumb", {
      onPush: () => State_1.SM.path = args.path,
      ddxClass: () => State_1.SM.path === args.path ? "--selected" : "",
      style: `background-color: ${args.backgroundColor}`,
      title: args.tooltip,
      innards: [helium_ui_1.img({
        src: args.imgSrc
      })]
    });
  })]), helium_ui_1.div("TabPaneBorder", {
    ddx: ref => {
      const tabArgs = ArgsForTabs.find(it => it.path === State_1.SM.path);
      if (!tabArgs) {
        return;
      }
      ref.style.backgroundColor = tabArgs.backgroundColor;
    }
  })]);
}
exports.renderTabListPane = renderTabListPane;

},

// src/popup/state/State.ts @6
6: function(__fusereq, exports, module){
exports.__esModule = true;
var helium_ui_1 = __fusereq(4);
var SettingsId_1 = __fusereq(22);
exports.SM = helium_ui_1.Sourcify({
  path: "",
  settings: {
    "sm-initialized": false,
    "fb-disable": false,
    "fb-remove-logo": true,
    "fb-trust-tokens": true,
    "fb-blur-stories": true,
    "fb-blur-home-feed": true,
    "fb-blur-marketplace-feed": true,
    "fb-blur-watch-feed": true,
    "fb-blur-gaming-feed": true,
    "fb-blur-untrusted": true,
    "fb-blur-post-comments": true,
    "fb-blur-notifications": true,
    "fb-blur-tab-watch": true,
    "fb-blur-tab-gaming": true,
    "fb-blur-tab-market": true,
    "fb-blur-tab-groups": true,
    "fb-blur-photo-comments": true,
    "fb-blur-popularity-stats": true,
    "twit-disable": false,
    "twit-remove-logo": true,
    "twit-trust-tokens": true,
    "twit-blur-home-screen-feed": true,
    "twit-blur-tweet": true,
    "twit-blur-trending-now": true,
    "twit-blur-who-to-follow": true,
    "twit-blur-relevant-people": true,
    "ytd-disable": false,
    "ytd-remove-logo": true,
    "ytd-no-trending": true,
    "ytd-blur-home-screen-feed": true,
    "ytd-blur-recommendations": true,
    "ytd-blur-comments": true,
    "hn-disable": false,
    "hn-remove-logo": false,
    "hn-blur-apple": true,
    "hn-blur-fb": true,
    "hn-blur-google": true,
    "hn-blur-ms": true,
    "hn-blur-moz": true,
    "hn-blur-twit": true,
    "hn-custom-regex": "/buzzfeed/i",
    "redd-disable": false,
    "redd-remove-logo": true,
    "redd-blur-home-feed": true,
    "redd-blur-home-posts": true,
    "redd-remove-home-sidebar": true,
    "redd-blur-sub-suggs": true,
    "redd-blur-trending-communities": true,
    "pin-disable": false,
    "pin-remove-logo": true,
    "pin-blur-home-feed": true,
    "pin-blur-today-feed": true,
    "pin-blur-more-like-feed": true,
    "tum-disable": false,
    "tum-remove-logo": true,
    "tum-blur-dash-feed": true,
    "tum-blur-posts": true,
    "tum-blur-dash-sidebar": true,
    "tum-remove-trending-nav": true,
    "sm-show-tab-fb": true,
    "sm-show-tab-hn": false,
    "sm-show-tab-pin": false,
    "sm-show-tab-redd": false,
    "sm-show-tab-tumblr": false,
    "sm-show-tab-twit": true,
    "sm-show-tab-yt": true,
    "sm-timer-disabled": false,
    "sm-link-blurring-disabled": false,
    "sm-timer-show-overlay": true,
    "add-timer-www.facebook.com": true,
    "add-timer-twitter.com": true,
    "add-timer-www.youtube.com": true,
    "add-timer-news.ycombinator.com": true,
    "add-timer-www.reddit.com": true,
    "add-timer-www.pinterest.com": true,
    "add-timer-www.tumblr.com": true,
    "sm-time-count-for-all": 0
  }
});
let showTab;
chrome.tabs.query({
  active: true,
  currentWindow: true
}, tabs => {
  const tab = tabs[0];
  if (!tab || !tab.url) {
    exports.SM.path = "about";
    return;
  }
  const url = new URL(tab.url);
  switch (url.hostname) {
    case "www.facebook.com":
      exports.SM.path = "facebook";
      showTab = "sm-show-tab-fb";
      break;
    case "news.ycombinator.com":
      exports.SM.path = "hacker-news";
      showTab = "sm-show-tab-hn";
      break;
    case "www.pinterest.com":
      exports.SM.path = "pinterest";
      showTab = "sm-show-tab-pin";
      break;
    case "www.reddit.com":
      exports.SM.path = "reddit";
      showTab = "sm-show-tab-redd";
      break;
    case "www.tumblr.com":
      exports.SM.path = "tumblr";
      showTab = "sm-show-tab-tumblr";
      break;
    case "twitter.com":
      exports.SM.path = "twitter";
      showTab = "sm-show-tab-twit";
      break;
    case "www.youtube.com":
      exports.SM.path = "youtube";
      showTab = "sm-show-tab-yt";
      break;
    default:
      exports.SM.path = "about";
      break;
  }
});
chrome.storage.sync.get(null, items => {
  if (showTab) {
    items[showTab] = true;
  }
  if (!items["sm-initialized"]) {
    items[`${SettingsId_1.BLUR_LINK_SETTING_PREFIX}buzzfeed.com`] = true;
    items[`${SettingsId_1.BLUR_LINK_SETTING_PREFIX}buzzfeednews.com`] = true;
    items["sm-initialized"] = true;
  }
  exports.SM.settings.upsert(items);
  let lastKeys = Array.from(Object.keys(exports.SM.settings));
  helium_ui_1.derive(() => {
    const allItems = exports.SM.settings.toObject();
    const currentKeys = Array.from(Object.keys(exports.SM.settings));
    chrome.storage.sync.set(allItems);
    const removedKeys = lastKeys.filter(key => !currentKeys.includes(key));
    lastKeys = currentKeys;
    console.log("Removed Keys", lastKeys, currentKeys, removedKeys);
    removedKeys.forEach(key => chrome.storage.sync.remove(key));
  });
});
chrome.storage.onChanged.addListener((changes, namespace) => {
  for (const _key in changes) {
    const key = _key;
    exports.SM.settings[key] = changes[key].newValue;
  }
});

},

// src/popup/pages/KnownSitesSettings/KnownSitesSettings.ts @7
7: function(__fusereq, exports, module){
exports.__esModule = true;
var helium_ui_1 = __fusereq(4);
var BooleanSetting_1 = __fusereq(20);
var SettingsId_1 = __fusereq(22);
var State_1 = __fusereq(6);
var msToDisplayTime_1 = __fusereq(23);
function renderFacebookSettings() {
  return helium_ui_1.div("SettingsPage", [helium_ui_1.div("PageTitle", "Facebook Settings"), helium_ui_1.br(), renderCommonSettings({
    siteName: "Facebook",
    addTimer: "add-timer-www.facebook.com",
    disable: "fb-disable",
    hideLogo: "fb-remove-logo",
    trustTokens: "fb-trust-tokens"
  }), renderDisablableSection("fb-disable", [helium_ui_1.br(), helium_ui_1.bold(["Main Feed"]), BooleanSetting_1.renderBooleanSetting({
    label: "Blur Entire Feed",
    settingId: "fb-blur-home-feed"
  }), BooleanSetting_1.renderBooleanSetting({
    label: "Blur Stories",
    settingId: "fb-blur-stories"
  }), BooleanSetting_1.renderBooleanSetting({
    label: "Blur Untrusted Friends Posts",
    settingId: "fb-blur-untrusted"
  }), BooleanSetting_1.renderBooleanSetting({
    label: "Blur Comments",
    settingId: "fb-blur-post-comments"
  }), helium_ui_1.br(), helium_ui_1.bold(["Misc"]), BooleanSetting_1.renderBooleanSetting({
    label: "Blur Popularity Stats (likes, views, comment count, etc)",
    settingId: "fb-blur-popularity-stats"
  }), BooleanSetting_1.renderBooleanSetting({
    label: "Blur Photo Comments",
    settingId: "fb-blur-photo-comments"
  }), BooleanSetting_1.renderBooleanSetting({
    label: `Blur "Marketplace" Feed`,
    settingId: "fb-blur-marketplace-feed"
  }), BooleanSetting_1.renderBooleanSetting({
    label: `Blur "Watch" Feed`,
    settingId: "fb-blur-watch-feed"
  }), BooleanSetting_1.renderBooleanSetting({
    label: `Blur "Gaming" Feed`,
    settingId: "fb-blur-gaming-feed"
  }), helium_ui_1.br(), helium_ui_1.bold(["Topbar"]), BooleanSetting_1.renderBooleanSetting({
    label: "Blur Notifications",
    settingId: "fb-blur-notifications"
  }), BooleanSetting_1.renderBooleanSetting({
    label: `Blur "Gaming" Tab`,
    settingId: "fb-blur-tab-gaming"
  }), BooleanSetting_1.renderBooleanSetting({
    label: `Blur "Watch" Tab`,
    settingId: "fb-blur-tab-watch"
  }), BooleanSetting_1.renderBooleanSetting({
    label: `Blur "Groups" Tab`,
    settingId: "fb-blur-tab-groups"
  }), renderDisablableSection({
    not: "fb-trust-tokens"
  }, [helium_ui_1.br(3), helium_ui_1.div("PageTitle", [helium_ui_1.span("TrustToken"), "Trust Tokens", helium_ui_1.span("TrustToken")]), helium_ui_1.div("center", helium_ui_1.italic([`list of users who you trust enough to automatically unblur their posts`])), helium_ui_1.br(), helium_ui_1.div("TrustTokenList", () => {
    const fbTrustTokens = Object.keys(State_1.SM.settings).filter(key => key.startsWith(SettingsId_1.FB_TRUST_SETTING_PREFIX));
    if (!fbTrustTokens.length) {
      return helium_ui_1.div("center", "Currently, none of your friends have trust tokens");
    }
    return fbTrustTokens.map(settingId => BooleanSetting_1.renderDeletableBooleanSetting({
      settingId: settingId,
      label: settingId.replace(SettingsId_1.FB_TRUST_SETTING_PREFIX, "")
    }));
  })])])]);
}
exports.renderFacebookSettings = renderFacebookSettings;
function renderTwitterSettings() {
  return helium_ui_1.div("SettingsPage", [helium_ui_1.div("PageTitle", "Twitter Settings"), renderCommonSettings({
    siteName: "Twitter",
    addTimer: "add-timer-twitter.com",
    disable: "twit-disable",
    hideLogo: "twit-remove-logo",
    trustTokens: "twit-trust-tokens"
  }), renderDisablableSection("twit-disable", [helium_ui_1.br(), helium_ui_1.bold(["Tweets"]), BooleanSetting_1.renderBooleanSetting({
    label: "Blur Home Screen Feed",
    settingId: "twit-blur-home-screen-feed"
  }), BooleanSetting_1.renderBooleanSetting({
    label: "Blur Untrusted Tweeters' Content",
    settingId: "twit-blur-tweet"
  }), helium_ui_1.br(), helium_ui_1.bold(["Follow More"]), BooleanSetting_1.renderBooleanSetting({
    label: `Blur "Trending Now"`,
    settingId: "twit-blur-trending-now"
  }), BooleanSetting_1.renderBooleanSetting({
    label: `Blur "Who to follow"`,
    settingId: "twit-blur-who-to-follow"
  }), BooleanSetting_1.renderBooleanSetting({
    label: `Blur "Relevant People"`,
    settingId: "twit-blur-relevant-people"
  }), helium_ui_1.br(3), renderDisablableSection({
    not: "twit-trust-tokens"
  }, [helium_ui_1.div("PageTitle", [helium_ui_1.span("TrustToken"), "Trust Tokens", helium_ui_1.span("TrustToken")]), helium_ui_1.div("center", helium_ui_1.italic([`list of users who you trust enough to automatically unblur their posts`])), helium_ui_1.br(), helium_ui_1.div("TrustTokenList", () => {
    const fbTrustTokens = Object.keys(State_1.SM.settings).filter(key => key.startsWith(SettingsId_1.TWIT_TRUST_SETTING_PREFIX));
    if (!fbTrustTokens.length) {
      return helium_ui_1.div("center", "Currently, no twitter handles have trust tokens");
    }
    return fbTrustTokens.map(settingId => BooleanSetting_1.renderDeletableBooleanSetting({
      settingId: settingId,
      label: settingId.replace(SettingsId_1.TWIT_TRUST_SETTING_PREFIX, "")
    }));
  })])])]);
}
exports.renderTwitterSettings = renderTwitterSettings;
function renderYoutubeSettings() {
  return helium_ui_1.div("SettingsPage", [helium_ui_1.div("PageTitle", "Youtube Settings"), renderCommonSettings({
    siteName: "Youtube",
    addTimer: "add-timer-www.youtube.com",
    disable: "ytd-disable",
    hideLogo: "ytd-remove-logo"
  }), renderDisablableSection("ytd-disable", [helium_ui_1.br(), helium_ui_1.bold(["Removal"]), BooleanSetting_1.renderBooleanSetting({
    label: `Remove "Trending" Navigation Button`,
    settingId: "ytd-no-trending"
  }), helium_ui_1.br(), helium_ui_1.bold(["Blurring"]), BooleanSetting_1.renderBooleanSetting({
    label: "Blur Home Screen Feed",
    settingId: "ytd-blur-home-screen-feed"
  }), BooleanSetting_1.renderBooleanSetting({
    label: "Blur Recommendations Column Beside Video",
    settingId: "ytd-blur-recommendations"
  }), BooleanSetting_1.renderBooleanSetting({
    label: "Blur Comments Below Video",
    settingId: "ytd-blur-comments"
  })])]);
}
exports.renderYoutubeSettings = renderYoutubeSettings;
function renderHackerNewsSettings() {
  const customRegexId = "hn-custom-regex";
  let regexInput;
  return helium_ui_1.div("SettingsPage", [helium_ui_1.div("PageTitle", "Hacker News Settings"), renderCommonSettings({
    siteName: "Hacker News",
    addTimer: "add-timer-news.ycombinator.com",
    disable: "hn-disable",
    hideLogo: "hn-remove-logo"
  }), renderDisablableSection("hn-disable", [helium_ui_1.br(), helium_ui_1.bold(["Post Presets"]), BooleanSetting_1.renderBooleanSetting({
    label: `Blur /apple/i`,
    settingId: "hn-blur-apple"
  }), BooleanSetting_1.renderBooleanSetting({
    label: `Blur /facebook/i`,
    settingId: "hn-blur-fb"
  }), BooleanSetting_1.renderBooleanSetting({
    label: `Blur /google/i`,
    settingId: "hn-blur-google"
  }), BooleanSetting_1.renderBooleanSetting({
    label: `Blur /microsoft/i`,
    settingId: "hn-blur-ms"
  }), BooleanSetting_1.renderBooleanSetting({
    label: `Blur /mozilla/i`,
    settingId: "hn-blur-moz"
  }), BooleanSetting_1.renderBooleanSetting({
    label: `Blur /twitter/i`,
    settingId: "hn-blur-twit"
  }), helium_ui_1.br(), helium_ui_1.bold(["Custom RegExp"]), helium_ui_1.br(), regexInput = helium_ui_1.inputTextbox({
    onInput: value => {
      if (value.startsWith("/") === false) {
        value = `/${value}`;
      }
      if ((/\/[ig]*$/).test(value) === false) {
        value += "/i";
      }
      regexInput.value = value;
      chrome.storage.sync.set({
        [customRegexId]: value
      });
    },
    ref: ref => {
      chrome.storage.sync.get(customRegexId, obj => {
        const value = obj[customRegexId] || "";
        ref.value = value;
      });
    }
  })])]);
}
exports.renderHackerNewsSettings = renderHackerNewsSettings;
function renderRedditSettings() {
  return helium_ui_1.div("SettingsPage", [helium_ui_1.div("PageTitle", "Reddit Settings"), renderCommonSettings({
    siteName: "Reddit",
    addTimer: "add-timer-www.reddit.com",
    disable: "redd-disable",
    hideLogo: "redd-remove-logo"
  }), renderDisablableSection("redd-disable", [helium_ui_1.br(), helium_ui_1.bold(["Home Feed"]), BooleanSetting_1.renderBooleanSetting({
    label: `Blur Home Feed`,
    settingId: "redd-blur-home-feed"
  }), BooleanSetting_1.renderBooleanSetting({
    label: `Blur Post's Content on Home Feed`,
    settingId: "redd-blur-home-posts"
  }), helium_ui_1.br(), helium_ui_1.bold(["Home Sidebar"]), BooleanSetting_1.renderBooleanSetting({
    label: `Remove Entire Sidebar from Home`,
    settingId: "redd-remove-home-sidebar"
  }), BooleanSetting_1.renderBooleanSetting({
    label: `Blur Sidebar Subreddit Suggestions`,
    settingId: "redd-blur-sub-suggs"
  }), BooleanSetting_1.renderBooleanSetting({
    label: `Blur "Trending Communities"`,
    settingId: "redd-blur-trending-communities"
  })])]);
}
exports.renderRedditSettings = renderRedditSettings;
function renderPinterestSettings() {
  return helium_ui_1.div("SettingsPage", [helium_ui_1.div("PageTitle", "Pinterest Settings"), renderCommonSettings({
    siteName: "Pinterest",
    addTimer: "add-timer-www.pinterest.com",
    disable: "pin-disable",
    hideLogo: "pin-remove-logo"
  }), renderDisablableSection("pin-disable", [helium_ui_1.br(), helium_ui_1.bold(["Feeds"]), BooleanSetting_1.renderBooleanSetting({
    label: `Blur Home Feed`,
    settingId: "pin-blur-home-feed"
  }), BooleanSetting_1.renderBooleanSetting({
    label: `Blur Today Feed`,
    settingId: "pin-blur-today-feed"
  }), BooleanSetting_1.renderBooleanSetting({
    label: `Blur "More like this" Feed`,
    settingId: "pin-blur-more-like-feed"
  })])]);
}
exports.renderPinterestSettings = renderPinterestSettings;
function renderTumblrSettings() {
  return helium_ui_1.div("SettingsPage", [helium_ui_1.div("PageTitle", "Tumblr Settings"), renderCommonSettings({
    siteName: "Tumblr",
    addTimer: "add-timer-www.tumblr.com",
    disable: "tum-disable",
    hideLogo: "tum-remove-logo"
  }), renderDisablableSection("tum-disable", [helium_ui_1.br(), helium_ui_1.bold(["Blurring"]), BooleanSetting_1.renderBooleanSetting({
    label: `Blur Dashboard Feed`,
    settingId: "tum-blur-dash-feed"
  }), BooleanSetting_1.renderBooleanSetting({
    label: `Blur Posts Content`,
    settingId: "tum-blur-posts"
  }), BooleanSetting_1.renderBooleanSetting({
    label: `Blur Dashboard Sidebar`,
    settingId: "tum-blur-dash-sidebar"
  }), helium_ui_1.br(2), helium_ui_1.bold(["Remove"]), BooleanSetting_1.renderBooleanSetting({
    label: `Remove "Trending" Nav Item`,
    settingId: "tum-remove-trending-nav"
  })])]);
}
exports.renderTumblrSettings = renderTumblrSettings;
function renderCommonSettings(args) {
  const timeToday = () => {
    const displayTime = State_1.SM.settings.get(args.addTimer.replace(SettingsId_1.ADD_TIMER_SETTING_PREFIX, SettingsId_1.TIME_COUNT_SETTING_PREFIX));
    return displayTime && `(time today: ${msToDisplayTime_1.millisecToDisplayTime(displayTime)})`;
  };
  return helium_ui_1.div("CommonSettings", [helium_ui_1.bold(["Common Settings"]), BooleanSetting_1.renderBooleanSetting({
    label: helium_ui_1.div("TimerSetting", [helium_ui_1.img({
      src: "/images/timer256.png"
    }), `Enable Timer on ${args.siteName} `, () => timeToday()]),
    settingId: args.addTimer
  }), BooleanSetting_1.renderBooleanSetting({
    label: `Disable Modifications on ${args.siteName}`,
    settingId: args.disable,
    type: "RED"
  }), args.trustTokens && renderDisablableSection(args.disable, [BooleanSetting_1.renderBooleanSetting({
    label: [helium_ui_1.span("TrustToken"), `Enable Trust Tokens on ${args.siteName} Posts`],
    settingId: args.trustTokens
  })]), args.hideLogo && renderDisablableSection(args.disable, [BooleanSetting_1.renderBooleanSetting({
    label: `Hide ${args.siteName} Logo`,
    settingId: args.hideLogo
  })])]);
}
function renderDisablableSection(logic, innards) {
  const settingId = typeof logic === "string" ? logic : logic.not;
  return helium_ui_1.div("DisablableSection", {
    innards: innards,
    ddxClass: () => {
      let value = State_1.SM.settings[settingId];
      if (typeof logic === "object") {
        value = !value;
      }
      return value ? "--disableSection" : "";
    }
  });
}

},

// src/popup/pages/About/AboutPage.ts @8
8: function(__fusereq, exports, module){
exports.__esModule = true;
__fusereq(19);
var helium_ui_1 = __fusereq(4);
var BooleanSetting_1 = __fusereq(20);
var DropDown_1 = __fusereq(21);
var State_1 = __fusereq(6);
var SettingsId_1 = __fusereq(22);
var msToDisplayTime_1 = __fusereq(23);
function renderAboutPage() {
  return helium_ui_1.div("AboutPage", [helium_ui_1.div("FlagSite", {
    onPush: () => State_1.SM.path = "flagged-sites"
  }, [helium_ui_1.img({
    src: "/images/red_flag256.png"
  }), helium_ui_1.italic(["flag current site"])]), helium_ui_1.div("TotalTime", {
    onPush: () => State_1.SM.path = "timer"
  }, [helium_ui_1.img({
    src: "/images/timer256.png"
  }), helium_ui_1.italic(() => {
    const totalTime = State_1.SM.settings.get("sm-time-count-for-all");
    return totalTime ? `time today: ${msToDisplayTime_1.millisecToDisplayTime(totalTime)}` : `time site usage`;
  })]), helium_ui_1.div("Header", [helium_ui_1.br(), helium_ui_1.img("Logo", {
    src: "/images/sober_media256.png"
  }), helium_ui_1.br(), helium_ui_1.div("PageTitle", "Sober Media"), helium_ui_1.italic([`designed to help you fend off the slow creep of social media addiction`])]), helium_ui_1.br(2), helium_ui_1.bold(["Add / Remove Tabs"]), helium_ui_1.div([BooleanSetting_1.renderBooleanSetting({
    label: "Facebook",
    settingId: "sm-show-tab-fb"
  }), BooleanSetting_1.renderBooleanSetting({
    label: "Hacker News",
    settingId: "sm-show-tab-hn"
  }), BooleanSetting_1.renderBooleanSetting({
    label: "Pinterest",
    settingId: "sm-show-tab-pin"
  }), BooleanSetting_1.renderBooleanSetting({
    label: "Reddit",
    settingId: "sm-show-tab-redd"
  }), BooleanSetting_1.renderBooleanSetting({
    label: "Tumblr",
    settingId: "sm-show-tab-tumblr"
  }), BooleanSetting_1.renderBooleanSetting({
    label: "Twitter",
    settingId: "sm-show-tab-twit"
  }), BooleanSetting_1.renderBooleanSetting({
    label: "Youtube",
    settingId: "sm-show-tab-yt"
  })]), helium_ui_1.br(2), helium_ui_1.bold(["More Info"]), DropDown_1.renderDropdown({
    label: "What is media sobriety?",
    content: [helium_ui_1.br(), `The ability for social media to manipulate us has strayed far beyond advertisement, and into the morally dubious lands of changing our personalities, ethics, and world views.  These changes are very slow and subtle, but don't be fooled: third parties are paying top dollar to claim a piece of our minds.`, helium_ui_1.br(2), `This is done primarily through targeted "feeds", which have been designed using the most advanced technology money can buy.  They exist specifically to grab our attention as quickly as possible, for long as possible, using unbelievably powerful AI backed up with every psychology trick in the book.  Nobody is immune to this, not even the designers.`, helium_ui_1.br(2), `This is where Sober Media comes in: by blurring out various social media "hooks" and giving you the choice whether or not to pay them your attention, a thin shield of consent is put between your mind and potential manipulations.`, helium_ui_1.br(2), `If you have not seen the movie "The Social Dilemma", we'd rate it 5/5 for doing an amazing job of illuminating this subject. :)`, helium_ui_1.br(2)]
  }), helium_ui_1.br(0.5), DropDown_1.renderDropdown({
    label: "How to block ads.",
    content: [helium_ui_1.br(), `If you'd like to cut advertisements out of your diet entirely many "Ad-Blockers" exist.  Here's a few we recommend:`, helium_ui_1.br(1.5), helium_ui_1.a({
      href: "https://ublockorigin.com/"
    }, "uBlock Origin"), ` - Fairly simple, blocks trackers, has some neat advanced features.  Works well out of the box.`, helium_ui_1.br(0.5), helium_ui_1.a({
      href: "https://www.ghostery.com/"
    }, "Ghostery"), ` - Super simple and has a cute icon.  If you're not tech savvy, start here.`, helium_ui_1.br(0.5), helium_ui_1.a({
      href: "https://privacybadger.org/#How-is-Privacy-Badger-different-from-Disconnect%2c-Adblock-Plus%2c-Ghostery%2c-and-other-blocking-extensions"
    }, "Privacy Badger"), ` - Advanced, made by the EFF (Electronic Frontier Foundation), built to sniff out sneaky trackers (not just known ones).`, helium_ui_1.br(0.5), helium_ui_1.a({
      href: "https://adblockplus.org/"
    }, "Ad Block Plus"), ` - One of the first ad blockers ever made.  A bit dated, but works well enough.`, helium_ui_1.br(2)]
  }), helium_ui_1.br(0.5), DropDown_1.renderDropdown({
    label: "What about TikTok and Instagram?  Mobile Apps?",
    content: [helium_ui_1.br(), `Unfortunately, TikTok and Instagram are mobile only experiences.  As such, they free to use any form of tracking or manipulation they desire.  This is also true of the mobile app versions of every social media platform.`, helium_ui_1.br(2), `Whenever possible, it is highly recommended you sequester your "social media time" to web interfaces.  It will give you the option (using extensions) to block push notifications, advertisements, trackers (spyware), and nefarious content.`]
  }), helium_ui_1.br(0.5), DropDown_1.renderDropdown({
    label: "Ideas?  Contributions?  Dev?",
    content: [helium_ui_1.br(), `If you'd like to share input for future direction of this extension, please submit an issue at the `, helium_ui_1.a({
      href: `https://gitlab.com/SephReed/sober-media/-/issues`
    }, "Sober Media Issues Page"), `.  Or -- if that's too much work -- you can try `, helium_ui_1.a({
      href: `https://twitter.com/intent/tweet?hashtags=SoberMedia&text=@SephReed%20I%20have%20thoughts%20about`
    }, "getting at me on twitter"), `.  Or do both and cross-link them.`, helium_ui_1.br(2), `If -- out of some random kindness -- you would like to give me money, I have a `, helium_ui_1.a({
      href: `https://www.paypal.com/cgi-bin/webscr?cmd=_donations&business=J2XGTAHLKE6PY&item_name=Just+felt+like+being+nice&currency_code=USD`
    }, "paypal donation link"), ` as well as a `, helium_ui_1.a({
      href: `https://www.patreon.com/seph_reed`
    }, "patreon."), helium_ui_1.br(2), `Lastly, if you're a dev and would like to work together on basically whatever, use either of the contact links above.  And if you're in the process of development, you can `, helium_ui_1.span({
      onPush: () => {
        const keepObj = {};
        helium_ui_1.noDeps(() => {
          Object.keys(State_1.SM.settings).filter(key => {
            if (key.startsWith(SettingsId_1.FB_TRUST_SETTING_PREFIX)) {
              return true;
            }
            if (key.startsWith(SettingsId_1.TWIT_TRUST_SETTING_PREFIX)) {
              return true;
            }
            if (key.startsWith(SettingsId_1.BLUR_LINK_SETTING_PREFIX)) {
              return true;
            }
            if (key.startsWith(SettingsId_1.ADD_TIMER_SETTING_PREFIX)) {
              return true;
            }
          }).forEach(settingId => {
            keepObj[settingId] = State_1.SM.settings[settingId];
          });
        });
        console.log(keepObj);
        chrome.storage.sync.clear(() => {
          chrome.storage.sync.set(keepObj);
        });
      },
      innards: "restore all defaults"
    }), ` or `, helium_ui_1.span({
      onPush: () => chrome.storage.sync.clear(),
      innards: "hard reset the extension."
    })]
  })]);
}
exports.renderAboutPage = renderAboutPage;

},

// src/popup/pages/Timer/TimerPage.ts @9
9: function(__fusereq, exports, module){
exports.__esModule = true;
var helium_ui_1 = __fusereq(4);
var BooleanSetting_1 = __fusereq(20);
var State_1 = __fusereq(6);
var SettingsId_1 = __fusereq(22);
var Util_1 = __fusereq(24);
var msToDisplayTime_1 = __fusereq(23);
const PREFIX = SettingsId_1.ADD_TIMER_SETTING_PREFIX;
function renderTimePage() {
  return helium_ui_1.div("TimerPage", [helium_ui_1.div("PageTitle", "Timer & Settings"), helium_ui_1.br(2), helium_ui_1.div("TimeSpent center", [helium_ui_1.italic(["total time today:"]), helium_ui_1.div("Count", () => msToDisplayTime_1.millisecToDisplayTime(State_1.SM.settings.get("sm-time-count-for-all") || 0))]), helium_ui_1.br(), helium_ui_1.bold(["General"]), BooleanSetting_1.renderBooleanSetting({
    settingId: "sm-timer-disabled",
    label: "Disable Timer on All Sites",
    type: "RED"
  }), helium_ui_1.div("DisablableSection", {
    ddxClass: () => State_1.SM.settings["sm-timer-disabled"] ? `--disableSection` : ""
  }, [BooleanSetting_1.renderBooleanSetting({
    settingId: "sm-timer-show-overlay",
    label: "Show Timer Overlay"
  }), helium_ui_1.br(2), helium_ui_1.bold(["Time This Site"]), helium_ui_1.div("TimeUrls", () => {
    const url = Util_1.getCurrentUrl();
    if (!url) {
      return "Loading...";
    }
    const host = url.hostname;
    return BooleanSetting_1.renderBooleanSetting({
      label: `Run Timer on ${host}`,
      settingId: PREFIX + host
    });
  }), helium_ui_1.br(3), helium_ui_1.div("PageTitle", "Timed Pages"), helium_ui_1.br(), helium_ui_1.div("TimedSiteList", () => {
    const timedSiteIds = Array.from(Object.keys(State_1.SM.settings)).filter(key => key.startsWith(PREFIX));
    if (!timedSiteIds.length) {
      return helium_ui_1.div("center", "No sites are currently timed");
    }
    const lineItems = timedSiteIds.map(setId => {
      const host = setId.replace(PREFIX, "");
      const totalTime = State_1.SM.settings.get(`${SettingsId_1.TIME_COUNT_SETTING_PREFIX}${host}`) || 0;
      return {
        totalTime,
        domNode: helium_ui_1.div("GridRow", [BooleanSetting_1.renderDeleteBoolean(setId), BooleanSetting_1.renderBooleanSetting({
          settingId: setId,
          label: null
        }), helium_ui_1.div([host]), helium_ui_1.div([totalTime ? msToDisplayTime_1.millisecToDisplayTime(totalTime) : "-"])])
      };
    });
    return [helium_ui_1.div("GridRow", ["Remove", "Enable", "Site", "Time Count"].map(name => helium_ui_1.bold([name]))), ...lineItems.sort((a, b) => b.totalTime - a.totalTime).map(it => it.domNode)];
  })])]);
}
exports.renderTimePage = renderTimePage;

},

// src/popup/pages/FlaggedSites/FlaggedSitesPage.ts @10
10: function(__fusereq, exports, module){
exports.__esModule = true;
var helium_ui_1 = __fusereq(4);
var BooleanSetting_1 = __fusereq(20);
var State_1 = __fusereq(6);
var SettingsId_1 = __fusereq(22);
var Util_1 = __fusereq(24);
const PREFIX = SettingsId_1.BLUR_LINK_SETTING_PREFIX;
function renderFlaggedSitesPage() {
  return helium_ui_1.div("FlaggedSitesPage", [helium_ui_1.div("PageTitle", "Site Flagging"), helium_ui_1.br(2), helium_ui_1.bold(["General"]), BooleanSetting_1.renderBooleanSetting({
    settingId: "sm-link-blurring-disabled",
    label: "Disable All Link Blurring",
    type: "RED"
  }), helium_ui_1.div("DisablableSection", {
    ddxClass: () => State_1.SM.settings["sm-link-blurring-disabled"] ? `--disableSection` : ""
  }, [helium_ui_1.br(2), helium_ui_1.bold(["Flag This Site"]), helium_ui_1.div("BlurUrls", () => {
    const hosts = Util_1.getHostMatchListForUrl();
    if (!hosts) {
      return "Loading...";
    }
    return hosts.map(host => BooleanSetting_1.renderBooleanSetting({
      label: `Blur Links to ${host}`,
      settingId: PREFIX + host
    }));
  }), helium_ui_1.br(3), helium_ui_1.div("PageTitle", "Flagged Site List"), helium_ui_1.div("center", "(list of sites for which you would like blurring to help you avoid links towards)"), helium_ui_1.br(), helium_ui_1.div("FlagSiteList", () => {
    const flagSettingIds = Array.from(Object.keys(State_1.SM.settings)).filter(key => key.startsWith(PREFIX));
    if (!flagSettingIds.length) {
      return helium_ui_1.div("center", "No sites are currently flagged");
    }
    return [helium_ui_1.div("GridRow", ["Remove", "Enable", "Site", "Blur Count"].map(name => helium_ui_1.bold([name]))), ...flagSettingIds.map(flagSettingId => {
      const host = flagSettingId.replace(PREFIX, "");
      return helium_ui_1.div("GridRow", [BooleanSetting_1.renderDeleteBoolean(flagSettingId), BooleanSetting_1.renderBooleanSetting({
        settingId: flagSettingId,
        label: null
      }), helium_ui_1.div([host]), helium_ui_1.div([String(State_1.SM.settings.get(`${SettingsId_1.NUM_LINKS_BLURRED_SETTING_PREFIX}${host}`) || 0)])]);
    })];
  })])]);
}
exports.renderFlaggedSitesPage = renderFlaggedSitesPage;

},

// node_modules/helium-ui-tweener/src/Tween.ts @11
11: function(__fusereq, exports, module){
exports.__esModule = true;
__fusereq(25);
var helium_ui_1 = __fusereq(26);
exports.tweener = tweener;
exports.tweener = tweener;
function tweener(propsOrDdx, maybeDDx) {
  let props;
  let ddx;
  if (typeof propsOrDdx === "function") {
    ddx = propsOrDdx;
    props = {};
  } else {
    props = propsOrDdx;
    ddx = maybeDDx;
  }
  let ms = props.ms !== undefined ? props.ms : 150;
  let resizeMs = props.resizeMs || 250;
  let type = props.type || "crossfade-on-top";
  if (type === "none") {
    type = undefined;
  }
  let skipFirst = props.skipFirst !== false;
  let tweenSizing = props.disableResize !== true;
  let currentView;
  let currentContent;
  return helium_ui_1.div("Tweener", {
    ddx: parent => {
      const newView = ddx();
      const lastContent = currentContent;
      if (newView === currentView) {
        return;
      }
      currentView = newView;
      const newContent = helium_ui_1.div("Tweener-Content", newView);
      if (!lastContent && skipFirst) {
        return helium_ui_1.setInnards(parent, currentContent = newContent);
      }
      const doSizeTween = newContent && tweenSizing;
      lastContent.style.width = `${lastContent.clientWidth}px`;
      lastContent.style.height = `${lastContent.clientHeight}px`;
      helium_ui_1.setInnards(parent, newContent);
      const targetSize = {
        width: newContent.clientWidth,
        height: newContent.clientHeight
      };
      helium_ui_1.setInnards(parent, lastContent);
      newContent.style.width = `${targetSize.width}px`;
      newContent.style.height = `${targetSize.height}px`;
      let newAnimationComplete = false;
      let oldAnimationComplete = false;
      let sizeTweenComplete = !doSizeTween ? true : false;
      const toBeRemoved = [];
      function removeIfReady(task) {
        task && toBeRemoved.push(task);
        if (newAnimationComplete && oldAnimationComplete && sizeTweenComplete) {
          toBeRemoved.forEach(({item, classes}) => {
            classes.forEach(classnName => item.classList.remove(classnName));
          });
          parent.style.transitionDuration = "";
          newContent.style.width = parent.style.width = "";
          newContent.style.height = parent.style.height = "";
        }
      }
      if (type) {
        [lastContent, newContent].filter(it => !!it).forEach(content => {
          content.style.animationDuration = `${ms}ms`;
          content.classList.remove("--tween_out");
          content.classList.remove("--tween_in");
          const className = `--tween-anim_${type}`;
          const isNewContent = content === newContent;
          const inOutName = isNewContent ? "--tween_in" : "--tween_out";
          const listener = ev => {
            if (ev.animationName.includes(type)) {
              content.removeEventListener("animationend", listener);
              if (!isNewContent) {
                oldAnimationComplete = true;
                content.remove();
              } else {
                newAnimationComplete = true;
              }
              removeIfReady({
                item: content,
                classes: [className, inOutName]
              });
            }
          };
          content.addEventListener("animationend", listener);
          content.classList.add(inOutName);
          content.classList.add(className);
        });
      }
      if (doSizeTween) {
        parent.style.width = `${parent.clientWidth}px`;
        parent.style.height = `${parent.clientHeight}px`;
      }
      if (type) {
        helium_ui_1.setInnards(parent, [lastContent, newContent]);
      } else {
        helium_ui_1.setInnards(parent, newContent);
      }
      currentContent = newContent;
      currentContent.style.minWidth = currentContent.style.minHeight = "";
      if (!doSizeTween) {
        if (type) {
          parent.style.width = `${newContent.clientWidth}px`;
          parent.style.height = `${newContent.clientHeight}px`;
          const animListener = () => {
            parent.removeEventListener("animationend", animListener);
            parent.style.width = parent.style.height = "";
          };
          parent.addEventListener("animationend", animListener);
        }
      } else {
        const transitionListener = () => {
          parent.removeEventListener("transitionend", transitionListener);
          sizeTweenComplete = true;
          removeIfReady();
          parent.style.width = parent.style.height = "";
        };
        parent.addEventListener("transitionend", transitionListener);
        parent.style.transitionDuration = `${resizeMs}ms`;
        requestAnimationFrame(() => requestAnimationFrame(() => {
          const {width, height} = targetSize;
          parent.style.width = `${width + 1}px`;
          parent.style.height = `${height + 1}px`;
        }));
      }
    }
  });
}
exports.tweener = tweener;

},

// node_modules/helium-ui/dist/El-Tool/index.js @12
12: function(__fusereq, exports, module){
"use strict";
var __createBinding = this && this.__createBinding || (Object.create ? function (o, m, k, k2) {
  if (k2 === undefined) k2 = k;
  Object.defineProperty(o, k2, {
    enumerable: true,
    get: function () {
      return m[k];
    }
  });
} : function (o, m, k, k2) {
  if (k2 === undefined) k2 = k;
  o[k2] = m[k];
});
var __exportStar = this && this.__exportStar || (function (m, exports) {
  for (var p in m) if (p !== "default" && !exports.hasOwnProperty(p)) __createBinding(exports, m, p);
});
Object.defineProperty(exports, "__esModule", {
  value: true
});
__exportStar(__fusereq(31), exports);
__exportStar(__fusereq(32), exports);
__exportStar(__fusereq(33), exports);

},

// node_modules/helium-ui/dist/Derivations/index.js @13
13: function(__fusereq, exports, module){
"use strict";
var __createBinding = this && this.__createBinding || (Object.create ? function (o, m, k, k2) {
  if (k2 === undefined) k2 = k;
  Object.defineProperty(o, k2, {
    enumerable: true,
    get: function () {
      return m[k];
    }
  });
} : function (o, m, k, k2) {
  if (k2 === undefined) k2 = k;
  o[k2] = m[k];
});
var __exportStar = this && this.__exportStar || (function (m, exports) {
  for (var p in m) if (p !== "default" && !exports.hasOwnProperty(p)) __createBinding(exports, m, p);
});
Object.defineProperty(exports, "__esModule", {
  value: true
});
__exportStar(__fusereq(34), exports);
__exportStar(__fusereq(35), exports);

},

// node_modules/helium-ui/dist/Sourcify/index.js @14
14: function(__fusereq, exports, module){
"use strict";
var __createBinding = this && this.__createBinding || (Object.create ? function (o, m, k, k2) {
  if (k2 === undefined) k2 = k;
  Object.defineProperty(o, k2, {
    enumerable: true,
    get: function () {
      return m[k];
    }
  });
} : function (o, m, k, k2) {
  if (k2 === undefined) k2 = k;
  o[k2] = m[k];
});
var __exportStar = this && this.__exportStar || (function (m, exports) {
  for (var p in m) if (p !== "default" && !exports.hasOwnProperty(p)) __createBinding(exports, m, p);
});
Object.defineProperty(exports, "__esModule", {
  value: true
});
__exportStar(__fusereq(27), exports);
__exportStar(__fusereq(28), exports);
__exportStar(__fusereq(29), exports);
__exportStar(__fusereq(30), exports);

},

// node_modules/helium-ui/dist/HeliumBase.js @15
15: function(__fusereq, exports, module){
"use strict";
Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.changeFreezeState = exports.isHeliumHazed = exports.heliumHaze = void 0;
function heliumHaze(rookie) {
  rookie._helium = rookie._helium || ({});
  return rookie;
}
exports.heliumHaze = heliumHaze;
function isHeliumHazed(checkMe) {
  return checkMe._helium;
}
exports.isHeliumHazed = isHeliumHazed;
function changeFreezeState(hazed, shouldBeFrozen) {
  const helium = hazed._helium;
  if (helium.frozen === shouldBeFrozen) {
    return;
  }
  helium.frozen = shouldBeFrozen;
  const fns = shouldBeFrozen ? helium.freeze : helium.unfreeze;
  if (fns) {
    fns.forEach(fn => fn());
  }
}
exports.changeFreezeState = changeFreezeState;

},

// node_modules/helium-ui/dist/Augments/index.js @16
16: function(__fusereq, exports, module){
"use strict";
var __createBinding = this && this.__createBinding || (Object.create ? function (o, m, k, k2) {
  if (k2 === undefined) k2 = k;
  Object.defineProperty(o, k2, {
    enumerable: true,
    get: function () {
      return m[k];
    }
  });
} : function (o, m, k, k2) {
  if (k2 === undefined) k2 = k;
  o[k2] = m[k];
});
var __exportStar = this && this.__exportStar || (function (m, exports) {
  for (var p in m) if (p !== "default" && !exports.hasOwnProperty(p)) __createBinding(exports, m, p);
});
Object.defineProperty(exports, "__esModule", {
  value: true
});
__exportStar(__fusereq(36), exports);
__exportStar(__fusereq(37), exports);

},

// node_modules/helium-sdx/dist/index.js @17
17: function(__fusereq, exports, module){
"use strict";
function __export(m) {
  for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", {
  value: true
});
__export(__fusereq(38));
__export(__fusereq(39));
__export(__fusereq(40));

},

// src/popup/components/TabListPane/tabListPane.scss @18
18: function(__fusereq, exports, module){
__fusereq(3)("src/popup/components/TabListPane/tabListPane.scss","div.TabListPane > .TabList {\n  display: flex;\n  justify-content: flex-start;\n  background-color: #444; }\n  div.TabListPane > .TabList > .TabThumb {\n    flex: 1;\n    max-width: 5em;\n    border-bottom: 1px solid rgba(0, 0, 0, 0.5);\n    opacity: 0.9;\n    text-align: center; }\n    div.TabListPane > .TabList > .TabThumb.--selected {\n      border-color: transparent;\n      opacity: 1; }\n    div.TabListPane > .TabList > .TabThumb > img {\n      width: 2em; }\n\ndiv.TabListPane > .TabPaneBorder {\n  height: 0.66em;\n  transition: background-color ease-out 0.1s; }\n")
},

// src/popup/pages/About/aboutPage.scss @19
19: function(__fusereq, exports, module){
__fusereq(3)("src/popup/pages/About/aboutPage.scss","div.AboutPage {\n  position: relative; }\n  div.AboutPage > .TotalTime, div.AboutPage > .FlagSite {\n    position: absolute;\n    display: flex;\n    align-items: center;\n    top: 0.5em;\n    border-radius: 0.5em;\n    padding: 0.25em;\n    padding-right: 0.5em;\n    box-shadow: 0px 0px 5px rgba(0, 0, 0, 0.1); }\n    div.AboutPage > .TotalTime.FlagSite, div.AboutPage > .FlagSite.FlagSite {\n      right: 0.5em;\n      border: 1px solid #dace40;\n      background: #fce536; }\n    div.AboutPage > .TotalTime.TotalTime, div.AboutPage > .FlagSite.TotalTime {\n      left: 0.5em;\n      border: 1px solid black;\n      background: #292929;\n      color: white; }\n    div.AboutPage > .TotalTime > img, div.AboutPage > .FlagSite > img {\n      max-height: 1.25em; }\n      div.AboutPage > .TotalTime > img:first-child, div.AboutPage > .FlagSite > img:first-child {\n        margin-right: 0.5em; }\n      div.AboutPage > .TotalTime > img:last-child, div.AboutPage > .FlagSite > img:last-child {\n        margin-left: 0.25em; }\n  div.AboutPage > .Header {\n    text-align: center; }\n    div.AboutPage > .Header > .Logo {\n      max-width: 15em; }\n")
},

// src/popup/components/BooleanSetting/BooleanSetting.ts @20
20: function(__fusereq, exports, module){
exports.__esModule = true;
__fusereq(42);
var helium_ui_1 = __fusereq(4);
var State_1 = __fusereq(6);
function renderBooleanSetting(props) {
  const {settingId} = props;
  return helium_ui_1.div("BooleanSetting", {
    class: props.type === "RED" ? "redOnEnable" : "",
    ddxClass: () => State_1.SM.settings[settingId] ? "--enabled" : "",
    onPush: () => {
      State_1.SM.settings[settingId] = !State_1.SM.settings[settingId];
    }
  }, [helium_ui_1.div("Switch", [helium_ui_1.div("SwitchThumb")]), props.label !== null ? helium_ui_1.div("Label", [props.label]) : null]);
}
exports.renderBooleanSetting = renderBooleanSetting;
function renderDeletableBooleanSetting(props) {
  return helium_ui_1.div("DeletableBooleanSetting", {
    ddxClass: () => (props.settingId in State_1.SM.settings) ? "" : "--removed"
  }, [renderDeleteBoolean(props.settingId), renderBooleanSetting(props)]);
}
exports.renderDeletableBooleanSetting = renderDeletableBooleanSetting;
function renderDeleteBoolean(settingId) {
  return helium_ui_1.div("DeleteSetting", {
    title: "Remove Setting",
    onPush: () => delete State_1.SM.settings[settingId],
    innards: "X"
  });
}
exports.renderDeleteBoolean = renderDeleteBoolean;

},

// src/popup/components/DropDown/DropDown.ts @21
21: function(__fusereq, exports, module){
exports.__esModule = true;
__fusereq(41);
var helium_ui_1 = __fusereq(4);
var helium_ui_tweener_1 = __fusereq(11);
function renderDropdown(props) {
  const showContent = new helium_ui_1.Source(false);
  const out = helium_ui_1.div("Dropdown", {
    ddxClass: () => showContent.get() ? "--showContent" : ""
  }, [helium_ui_1.div("DropdownLabel", {
    onPush: () => showContent.set(!showContent.get()),
    innards: props.label
  }), helium_ui_1.div("DropdownContent", helium_ui_tweener_1.tweener({
    type: "none"
  }, () => showContent.get() && props.content))]);
  return out;
}
exports.renderDropdown = renderDropdown;

},

// src/middleware/SettingsId.ts @22
22: function(__fusereq, exports, module){
exports.__esModule = true;
exports.SETTING_STORAGE_ID = "BOOLEAN_SETTINGS";
exports.BLUR_LINK_SETTING_PREFIX = `blur-link-`;
exports.NUM_LINKS_BLURRED_SETTING_PREFIX = `num-links-blurred`;
exports.ADD_TIMER_SETTING_PREFIX = `add-timer-`;
exports.TIME_COUNT_SETTING_PREFIX = `sm-time-count-for-`;
exports.FB_TRUST_SETTING_PREFIX = `fb-grant-trust-`;
exports.TWIT_TRUST_SETTING_PREFIX = `twit-grant-trust-`;
exports.SettingIdList = ["fb-disable", "fb-remove-logo", "fb-trust-tokens", "fb-blur-home-feed", "fb-blur-watch-feed", "fb-blur-marketplace-feed", "fb-blur-gaming-feed", "fb-blur-stories", "fb-blur-untrusted", "fb-blur-post-comments", "fb-blur-photo-comments", "fb-blur-notifications", "fb-blur-tab-gaming", "fb-blur-tab-watch", "fb-blur-tab-groups", "fb-blur-tab-market", "fb-blur-popularity-stats", "twit-disable", "twit-remove-logo", "twit-trust-tokens", "twit-blur-home-screen-feed", "twit-blur-tweet", "twit-blur-trending-now", "twit-blur-who-to-follow", "twit-blur-relevant-people", "ytd-disable", "ytd-remove-logo", "ytd-no-trending", "ytd-blur-home-screen-feed", "ytd-blur-recommendations", "ytd-blur-comments", "hn-disable", "hn-remove-logo", "hn-blur-apple", "hn-blur-fb", "hn-blur-google", "hn-blur-ms", "hn-blur-moz", "hn-blur-twit", "hn-custom-regex", "redd-disable", "redd-remove-logo", "redd-blur-home-feed", "redd-blur-home-posts", "redd-remove-home-sidebar", "redd-blur-sub-suggs", "redd-blur-trending-communities", "pin-disable", "pin-remove-logo", "pin-blur-home-feed", "pin-blur-today-feed", "pin-blur-more-like-feed", "tum-disable", "tum-remove-logo", "tum-blur-dash-feed", "tum-blur-posts", "tum-blur-dash-sidebar", "tum-remove-trending-nav", "sm-show-tab-fb", "sm-show-tab-hn", "sm-show-tab-pin", "sm-show-tab-redd", "sm-show-tab-tumblr", "sm-show-tab-twit", "sm-show-tab-yt", "sm-initialized", "sm-timer-disabled", "sm-link-blurring-disabled", "sm-timer-show-overlay", "add-timer-www.facebook.com", "add-timer-twitter.com", "add-timer-www.youtube.com", "add-timer-news.ycombinator.com", "add-timer-www.reddit.com", "add-timer-www.pinterest.com", "add-timer-www.tumblr.com", "sm-time-count-for-all"];

},

// src/middleware/msToDisplayTime.ts @23
23: function(__fusereq, exports, module){
function millisecToDisplayTime(ms) {
  const ds = ms / 1000;
  const seconds = ds % 60;
  const minutes = Math.floor(ds / 60) % 60;
  const hours = Math.floor(ds / 3600);
  let secondsStr = String(seconds);
  if (seconds < 10) {
    secondsStr = "0" + secondsStr;
  }
  let minutesStr = String(minutes);
  if (hours && minutes < 10) {
    minutesStr = "0" + minutesStr;
  }
  let out = ``;
  if (hours) {
    out += hours + ":";
  }
  out += `${minutesStr}:${secondsStr.slice(0, 2)}`;
  return out;
}
exports.millisecToDisplayTime = millisecToDisplayTime;

},

// src/popup/Util.ts @24
24: function(__fusereq, exports, module){
exports.__esModule = true;
var helium_ui_1 = __fusereq(4);
const currentUrl = new helium_ui_1.Source();
function getCurrentUrl() {
  updateUrl();
  return currentUrl.get();
}
exports.getCurrentUrl = getCurrentUrl;
function updateUrl() {
  chrome.tabs.query({
    active: true,
    currentWindow: true
  }, tabs => {
    const tab = tabs[0];
    if (!tab || !tab.url) {
      return;
    }
    const newUrl = new URL(tab.url);
    if (currentUrl.get() && newUrl.href === currentUrl.get().href) {
      return;
    }
    currentUrl.set(newUrl);
  });
}
function getHostMatchListForUrl() {
  updateUrl();
  const url = currentUrl.get();
  if (!url) {
    return null;
  }
  const hostChunks = url.host.split(".").filter(it => it !== "www");
  const hosts = [];
  for (let i = 0; i < hostChunks.length - 1; i++) {
    hosts.push(hostChunks.slice(i).join("."));
  }
  let href = hosts[0];
  hosts.reverse();
  const pathChunks = url.pathname.split("/").filter(it => !!it);
  pathChunks.forEach(chunk => {
    href += `/${chunk}`;
    hosts.push(href);
  });
  return hosts;
}
exports.getHostMatchListForUrl = getHostMatchListForUrl;

},

// node_modules/helium-ui-tweener/src/tween.scss @25
25: function(__fusereq, exports, module){
__fusereq(3)("node_modules/helium-ui-tweener/src/tween.scss",".Tweener {\n  position: relative;\n  overflow: hidden; }\n  .Tweener > .--tween_in {\n    animation-timing-function: ease-out; }\n  .Tweener > .--tween_out {\n    animation-timing-function: ease-in; }\n  .Tweener > .--tween-anim_fade-on-top.--tween_in {\n    position: absolute;\n    top: 0px;\n    z-index: 1;\n    animation-name: fade-on-top-in; }\n\n@keyframes fade-on-top-in {\n  0% {\n    opacity: 0; }\n  50% {\n    opacity: 0; }\n  100% {\n    opacity: 1; } }\n  .Tweener > .--tween-anim_fade-on-top.--tween_out {\n    animation-name: fade-on-top-out; }\n\n@keyframes fade-on-top-out {\n  0% {\n    opacity: 1; }\n  50% {\n    opacity: 0; }\n  100% {\n    opacity: 0; } }\n  .Tweener > .--tween-anim_crossfade-on-top.--tween_in {\n    position: absolute;\n    top: 0px;\n    z-index: 1;\n    animation-name: crossfade-on-top-in; }\n\n@keyframes crossfade-on-top-in {\n  0% {\n    opacity: 0; }\n  33% {\n    opacity: 0; }\n  100% {\n    opacity: 1; } }\n  .Tweener > .--tween-anim_crossfade-on-top.--tween_out {\n    animation-name: crossfade-on-top-out; }\n\n@keyframes crossfade-on-top-out {\n  0% {\n    opacity: 1; }\n  66% {\n    opacity: 0; }\n  100% {\n    opacity: 0; } }\n  .Tweener > .--tween-anim_shuffle-bottom.--tween_in {\n    position: absolute;\n    z-index: 1;\n    animation-name: shuffle-bottom-in;\n    bottom: 0px;\n    width: 100%; }\n\n@keyframes shuffle-bottom-in {\n  0% {\n    transform: translateY(100%); }\n  100% {\n    transform: translateY(0%); } }\n  .Tweener > .--tween-anim_shuffle-bottom.--tween_out {\n    animation-name: shuffle-bottom-out; }\n\n@keyframes shuffle-bottom-out {\n  0% {\n    transform: translateY(0%); }\n  100% {\n    transform: translateY(100%); } }\n")
},

// node_modules/helium-ui-tweener/node_modules/helium-ui/dist/index.js @26
26: function(__fusereq, exports, module){
"use strict";
var __createBinding = this && this.__createBinding || (Object.create ? function (o, m, k, k2) {
  if (k2 === undefined) k2 = k;
  Object.defineProperty(o, k2, {
    enumerable: true,
    get: function () {
      return m[k];
    }
  });
} : function (o, m, k, k2) {
  if (k2 === undefined) k2 = k;
  o[k2] = m[k];
});
var __exportStar = this && this.__exportStar || (function (m, exports) {
  for (var p in m) if (p !== "default" && !exports.hasOwnProperty(p)) __createBinding(exports, m, p);
});
Object.defineProperty(exports, "__esModule", {
  value: true
});
__exportStar(__fusereq(43), exports);
__exportStar(__fusereq(44), exports);
__exportStar(__fusereq(45), exports);
__exportStar(__fusereq(46), exports);
__exportStar(__fusereq(47), exports);
__exportStar(__fusereq(17), exports);

},

// node_modules/helium-ui/dist/Sourcify/SourceProxyObject.js @27
27: function(__fusereq, exports, module){
"use strict";
Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.ASSERT_SHALLOW_TYPE_MATCH = exports.SourceProxyObject = void 0;
const SourceMap_1 = __fusereq(49);
const helium_sdx_1 = __fusereq(17);
class SourceProxyObject extends helium_sdx_1.SourceProxyObject {
  constructor(target) {
    super(target, new SourceMap_1.SourceMap());
  }
  getSrcMap() {
    return this.srcMap;
  }
  renderMap(renderFn) {
    this.refreshAll();
    return this.getSrcMap().renderMap(renderFn);
  }
  renderMapKeyed(renderFn) {
    this.refreshAll();
    return this.getSrcMap().renderMapKeyed(renderFn);
  }
}
exports.SourceProxyObject = SourceProxyObject;
;
exports.ASSERT_SHALLOW_TYPE_MATCH = {};

},

// node_modules/helium-ui/dist/Sourcify/SourceProxyArray.js @28
28: function(__fusereq, exports, module){
"use strict";
Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.ASSERT_SHALLOW_TYPE_MATCH_ARRAY = exports.SourceProxyArray = void 0;
const SourceArray_1 = __fusereq(48);
const helium_sdx_1 = __fusereq(17);
class SourceProxyArray extends helium_sdx_1.SourceProxyArray {
  constructor(target) {
    super(target, new SourceArray_1.SourceArray());
  }
  getSrcArray() {
    return this.srcArray;
  }
  renderMap(renderFn) {
    this.refreshAll();
    return this.getSrcArray().renderMap(renderFn);
  }
  renderMapKeyed(renderFn) {
    this.refreshAll();
    return this.getSrcArray().renderMapKeyed(renderFn);
  }
}
exports.SourceProxyArray = SourceProxyArray;
exports.ASSERT_SHALLOW_TYPE_MATCH_ARRAY = {};

},

// node_modules/helium-ui/dist/Sourcify/SourceProxyShallowTypes.js @29
29: function(__fusereq, exports, module){
"use strict";
Object.defineProperty(exports, "__esModule", {
  value: true
});
function __() {
  ;
  const b = {};
  function testFn(testMe) {
    return null;
  }
  const out = b.a[3];
  const la = b.a.overwrite([]);
  const ba = b.a.sort();
  ba.renderMap(() => null);
  la.renderMap(() => null);
  const a = b.a.filter(() => true);
  const c = a[0];
  const d = b.get("a");
}

},

// node_modules/helium-ui/dist/Sourcify/SourceProxyUtilities.js @30
30: function(__fusereq, exports, module){
"use strict";
Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.renderMapKeyed = exports.renderMap = exports.isProxied = exports.Unsourcify = exports.Sourcify = void 0;
const El_Tool_1 = __fusereq(31);
const helium_sdx_1 = __fusereq(17);
let SourcificationManagerModded = false;
function Sourcify(target) {
  if (!SourcificationManagerModded) {
    const {SourceProxyArray} = __fusereq(28);
    const {SourceProxyObject} = __fusereq(27);
    helium_sdx_1.SourcificationManager.createSourceProxyArray = item => new SourceProxyArray(item);
    helium_sdx_1.SourcificationManager.createSourceProxyObject = item => new SourceProxyObject(item);
    SourcificationManagerModded = true;
  }
  return helium_sdx_1.Sourcify(target);
}
exports.Sourcify = Sourcify;
function Unsourcify(target) {
  return helium_sdx_1.Unsourcify(target);
}
exports.Unsourcify = Unsourcify;
function isProxied(checkMe) {
  return helium_sdx_1.isProxied(checkMe);
}
exports.isProxied = isProxied;
function renderMap(obj, renderFn) {
  return _anyRMap(false, obj, renderFn);
}
exports.renderMap = renderMap;
function renderMapKeyed(obj, renderFn) {
  return _anyRMap(true, obj, renderFn);
}
exports.renderMapKeyed = renderMapKeyed;
function _anyRMap(withKey, obj, renderFn) {
  if (isProxied(obj)) {
    if (withKey) {
      return obj.rkmap(renderFn);
    }
    return obj.rmap(renderFn);
  } else if (obj && typeof obj === "object") {
    const out = [];
    let renders;
    if (Array.isArray(obj)) {
      renders = obj.map((value, index) => renderFn(value, index));
    } else {
      renders = Object.entries(obj).map(([key, value]) => renderFn(value, key));
    }
    renders.forEach(render => {
      const nodesList = El_Tool_1.convertInnardsToElements(render);
      if (!nodesList || !nodesList.length) {
        return;
      }
      nodesList.forEach(node => out.push(node));
    });
    return out;
  }
}

},

// node_modules/helium-ui/dist/El-Tool/El-Tool.js @31
31: function(__fusereq, exports, module){
"use strict";
Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.convertSingleInnardToNodes = exports.convertInnardsToRenders = exports.convertInnardsToElements = exports.append = exports.setInnards = exports.applyProps = exports.standardizeInput = exports.el = exports.isProps = exports.isRawHTML = exports.isTextNode = exports.isHTMLElement = exports.isNode = exports.onTouch = exports.onToucherEnterExit = exports.onToucherMove = exports.setStyle = exports.removeChildren = exports.ddxClass = exports.haveClass = exports.addClass = exports.classSplice = exports.onDomReady = exports.on = exports.byId = void 0;
const Derivations_1 = __fusereq(13);
const HeliumBase_1 = __fusereq(15);
function byId(id) {
  return document.getElementById(id);
}
exports.byId = byId;
function on(eventName, ref, cb) {
  ref.addEventListener(eventName, cb);
}
exports.on = on;
function onDomReady(cb) {
  if (domIsReady) {
    cb();
    return;
  }
  window.addEventListener("DOMContentLoaded", cb);
}
exports.onDomReady = onDomReady;
let domIsReady = false;
typeof window !== "undefined" && onDomReady(() => domIsReady = true);
function classSplice(element, removeClasses, addClasses) {
  if (removeClasses) {
    const remList = Array.isArray(removeClasses) ? removeClasses : removeClasses.split(/\s+/g);
    remList.filter(it => !!it).forEach(className => element.classList.remove(className));
  }
  if (addClasses) {
    const addList = Array.isArray(addClasses) ? addClasses : addClasses.split(/\s+/g);
    addList.filter(it => !!it).forEach(className => element.classList.add(className));
  }
  return element;
}
exports.classSplice = classSplice;
function addClass(addClasses, element) {
  return classSplice(element, null, addClasses);
}
exports.addClass = addClass;
function haveClass(element, className, addNotRemove) {
  return addNotRemove ? classSplice(element, null, className) : classSplice(element, className, undefined);
}
exports.haveClass = haveClass;
function ddxClass(target, ddx) {
  let lastClassList;
  Derivations_1.anchorDeriverToDomNode(target, () => {
    const removeUs = lastClassList;
    classSplice(target, removeUs, lastClassList = ddx());
  }, "props.ddxClass").run();
}
exports.ddxClass = ddxClass;
function removeChildren(parent) {
  let child = parent.firstChild;
  while (!!child) {
    child.remove();
    child = parent.firstChild;
  }
  return parent;
}
exports.removeChildren = removeChildren;
function setStyle(root, style) {
  if (typeof style === "string") {
    root.style.cssText = style;
  } else {
    Object.keys(style).forEach(styleName => root.style[styleName] = style[styleName]);
  }
  return root;
}
exports.setStyle = setStyle;
function onToucherMove(root, cb) {
  const getPosFromClientXY = (x, y) => {
    const clientRect = root.getBoundingClientRect();
    return {
      left: x - clientRect.left,
      top: y - clientRect.top
    };
  };
  root.addEventListener("mousemove", ev => {
    const toucherMoveEvent = ev;
    toucherMoveEvent.positions = [getPosFromClientXY(ev.clientX, ev.clientY)];
    cb(toucherMoveEvent);
  });
  root.addEventListener("touchmove", ev => {
    const toucherMoveEvent = ev;
    const clientRect = root.getBoundingClientRect();
    toucherMoveEvent.positions = Array.from(ev.touches).map(touch => getPosFromClientXY(touch.clientX, touch.clientY));
    cb(toucherMoveEvent);
    ev.preventDefault();
  });
}
exports.onToucherMove = onToucherMove;
function onToucherEnterExit(root, cb) {
  const modEvent = (ev, isEnterEvent) => {
    ev.isEnterEvent = isEnterEvent;
    return ev;
  };
  root.addEventListener("mouseenter", ev => cb(modEvent(ev, true)));
  root.addEventListener("mouseleave", ev => cb(modEvent(ev, false)));
  root.addEventListener("touchstart", ev => {
    cb(modEvent(ev, true));
    ev.preventDefault();
  });
  root.addEventListener("touchend", ev => {
    cb(modEvent(ev, false));
    ev.preventDefault();
  });
}
exports.onToucherEnterExit = onToucherEnterExit;
function onTouch(root, cb) {
  root.addEventListener("mouseover", event => {
    if (event.buttons == 1 || event.buttons == 3) {
      cb(event);
    }
  });
}
exports.onTouch = onTouch;
function isNode(checkMe) {
  return checkMe instanceof Node;
}
exports.isNode = isNode;
function isHTMLElement(checkMe) {
  return ("nodeName" in checkMe);
}
exports.isHTMLElement = isHTMLElement;
function isTextNode(checkMe) {
  return isNode(checkMe) && checkMe.nodeType === checkMe.TEXT_NODE;
}
exports.isTextNode = isTextNode;
function isRawHTML(checkMe) {
  return checkMe && typeof checkMe === "object" && checkMe.hasOwnProperty("hackableHTML");
}
exports.isRawHTML = isRawHTML;
function isProps(checkMe) {
  return checkMe && typeof checkMe === "object" && !Array.isArray(checkMe) && !isNode(checkMe) && !isRawHTML(checkMe);
}
exports.isProps = isProps;
function el(tagName, propsOrInnards, innards) {
  const input = standardizeInput(null, propsOrInnards, innards);
  return __createEl(tagName, input.props, input.innards);
}
exports.el = el;
function standardizeInput(nameOrPropsOrInnards, propsOrInnards, maybeInnards, isInnardsRazor) {
  let innards = maybeInnards;
  let props;
  let name;
  if (nameOrPropsOrInnards) {
    if (typeof nameOrPropsOrInnards === "string") {
      name = nameOrPropsOrInnards;
    } else {
      if (maybeInnards !== undefined) {
        throw new Error("Can not define first and third argument if first is not a string");
      }
      maybeInnards = propsOrInnards;
      propsOrInnards = nameOrPropsOrInnards;
    }
  }
  if (propsOrInnards) {
    const isInnards = isInnardsRazor ? isInnardsRazor(propsOrInnards) : !isProps(propsOrInnards);
    if (isInnards) {
      if (maybeInnards !== undefined) {
        throw new Error("Can not define third argument if second is innards");
      }
      innards = propsOrInnards;
    } else {
      props = propsOrInnards;
      innards = maybeInnards;
    }
  }
  props = props || ({});
  if (name) {
    props.class = props.class || "";
    props.class += (props.class ? " " : "") + name;
  }
  return {
    props,
    innards
  };
}
exports.standardizeInput = standardizeInput;
function applyProps(props, out, innardsCb) {
  if (props) {
    if (props.on) {
      const eventListeners = props.on || ({});
      Object.keys(eventListeners).forEach(eventType => out.addEventListener(eventType, eventListeners[eventType]));
    }
    if (props.onClick) {
      out.addEventListener("click", props.onClick);
    }
    if (props.onKeyDown) {
      out.addEventListener("keydown", props.onKeyDown);
    }
    if (props.onMouseDown) {
      out.addEventListener("mousedown", props.onMouseDown);
    }
    if (props.onPush) {
      out.classList.add("onPush");
      out.addEventListener("click", props.onPush);
    }
    if (props.onTouch) {
      onTouch(out, props.onTouch);
    }
    if (props.onToucherMove) {
      onToucherMove(out, props.onToucherMove);
    }
    if (props.onToucherEnterExit) {
      onToucherEnterExit(out, props.onToucherEnterExit);
    }
    if (props.onHoverChange) {
      out.addEventListener("mouseover", ev => props.onHoverChange(true, ev));
      out.addEventListener("mouseout", ev => props.onHoverChange(false, ev));
    }
    if (props.title) {
      out.title = props.title;
    }
    if (props.attr) {
      const attrs = props.attr || ({});
      Object.keys(attrs).forEach(attr => out.setAttribute(attr, attrs[attr]));
    }
    if (props.this) {
      const thisProps = props.this || ({});
      Object.keys(thisProps).forEach(prop => out[prop] = thisProps[prop]);
    }
    if (props.class) {
      addClass(props.class, out);
    }
    if (props.id) {
      out.id = props.id;
    }
    if (props.style) {
      setStyle(out, props.style);
    }
    if (props.innards !== undefined) {
      append(out, props.innards);
    }
  }
  if (innardsCb !== undefined) {
    innardsCb(out);
  }
  if (props) {
    if (props.ref) {
      props.ref(out);
    }
    if (props.ddxClass) {
      ddxClass(out, props.ddxClass);
    }
    if (props.ddx) {
      const keep = {};
      Derivations_1.anchorDeriverToDomNode(out, () => props.ddx(out, keep), "props.ddx").run();
    }
  }
  return out;
}
exports.applyProps = applyProps;
function __createEl(tagName, props, innards) {
  const out = document.createElement(tagName);
  applyProps(props, out, () => {
    if (innards !== undefined) {
      append(out, innards);
    }
  });
  if (HeliumBase_1.isHeliumHazed(out)) {
    out._helium.frozen = true;
  }
  return out;
}
let innardsWarningGiven;
function setInnards(root, innards) {
  if (root === document.body && !window._heliumNoWarnSetInnardsDocBody) {
    console.warn([`Suggestion: avoid using setInnards() on document body, and instead use append().`, `If you do not, anything added to the page outside of your root render is unsafe.  To disable this warning,`, `set window._heliumNoWarnSetInnardsDocBody = true`].join(" "));
  }
  removeChildren(root);
  append(root, innards);
}
exports.setInnards = setInnards;
function append(root, innards, before) {
  const addUs = convertInnardsToElements(innards);
  if (before) {
    addUs.forEach(node => {
      root.insertBefore(node, before);
    });
  } else {
    addUs.forEach(node => {
      root.appendChild(node);
    });
  }
  return root;
}
exports.append = append;
function convertInnardsToElements(innards) {
  const renders = convertInnardsToRenders(innards);
  const out = [];
  renders.forEach(render => {
    if (Array.isArray(render)) {
      render.forEach(node => out.push(node));
    } else {
      out.push(render);
    }
  });
  return out;
}
exports.convertInnardsToElements = convertInnardsToElements;
function convertInnardsToRenders(innards) {
  if (innards === undefined || innards === null) {
    return [];
  }
  if (typeof innards === "function") {
    const refresher = new Derivations_1.RerenderReplacer(innards);
    return refresher.getRender();
  }
  let childList = Array.isArray(innards) ? innards : [innards];
  return childList.flat().map(child => convertSingleInnardToNodes(child));
}
exports.convertInnardsToRenders = convertInnardsToRenders;
function convertSingleInnardToNodes(child) {
  if (typeof child === "function") {
    const replacer = new Derivations_1.RerenderReplacer(child);
    return replacer.getRender();
  } else {
    let out = [];
    if (child === undefined || child === null) {
      return [];
    }
    if (typeof child === "number") {
      child = child + "";
    }
    if (isRawHTML(child)) {
      const template = document.createElement('div');
      template.innerHTML = child.hackableHTML;
      template.getRootNode();
      Array.from(template.childNodes).forEach(child => out.push(child));
    } else if (typeof child === "string") {
      out.push(new Text(child));
    } else if (child) {
      if (Array.isArray(child)) {
        out = out.concat(convertSingleInnardToNodes(child));
      } else {
        out.push(child);
      }
    }
    return out;
  }
}
exports.convertSingleInnardToNodes = convertSingleInnardToNodes;

},

// node_modules/helium-ui/dist/El-Tool/Factories.js @32
32: function(__fusereq, exports, module){
"use strict";
Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.h6 = exports.h5 = exports.h4 = exports.h3 = exports.h2 = exports.h1 = exports.br = exports.hr = exports.bold = exports.italic = exports.inputTextbox = exports.select = exports.button = exports.img = exports.td = exports.tr = exports.table = exports.li = exports.ol = exports.ul = exports.pre = exports.span = exports.div = exports._simpleFactory = void 0;
const El_Tool_1 = __fusereq(31);
const helium_sdx_1 = __fusereq(17);
function _simpleFactory(tagName, [arg1, arg2, arg3]) {
  const {props, innards} = El_Tool_1.standardizeInput(arg1, arg2, arg3);
  return El_Tool_1.el(tagName, props, innards);
}
exports._simpleFactory = _simpleFactory;
exports.div = function (...args) {
  return _simpleFactory("div", args);
};
exports.span = function (...args) {
  return _simpleFactory("span", args);
};
exports.pre = function (...args) {
  return _simpleFactory("pre", args);
};
exports.ul = function (...args) {
  return _simpleFactory("ul", args);
};
exports.ol = function (...args) {
  return _simpleFactory("ol", args);
};
exports.li = function (...args) {
  return _simpleFactory("li", args);
};
function table(namePropChild, propChild, child) {
  const {props, innards} = El_Tool_1.standardizeInput(namePropChild, propChild, child);
  const rows = El_Tool_1.convertInnardsToRenders(innards);
  return El_Tool_1.el("table", props, rows.map(row => toTr(row)));
  function toTr(row) {
    if (Array.isArray(row) && row.length === 1) {
      row = row.pop();
    }
    if (El_Tool_1.isHTMLElement(row) && (row.tagName === "TR" || row.nodeName === "#comment")) {
      return row;
    }
    if (typeof row === "function") {
      return () => toTr(row());
    }
    return tr(row);
  }
}
exports.table = table;
function tr(namePropChild, propChild, child) {
  const {props, innards} = El_Tool_1.standardizeInput(namePropChild, propChild, child);
  const cols = El_Tool_1.convertInnardsToRenders(innards);
  return El_Tool_1.el("tr", props, cols.map(data => toTd(data)));
  function toTd(data) {
    if (Array.isArray(data) && data.length === 1) {
      data = data.pop();
    }
    if (El_Tool_1.isHTMLElement(data) && (data.tagName === "TD" || data.nodeName === "#comment")) {
      return data;
    }
    if (typeof data === "function") {
      return () => toTd(data());
    }
    return El_Tool_1.el("td", data);
  }
}
exports.tr = tr;
function td(...[classOrPropOrChild, propOrChild, child]) {
  return _simpleFactory("td", [classOrPropOrChild, propOrChild, child]);
}
exports.td = td;
function img(...[classOrPropOrChild, propOrChild, child]) {
  const {props, innards} = El_Tool_1.standardizeInput(classOrPropOrChild, propOrChild, child);
  props.attr = props.attr || ({});
  props.attr.src = props.src;
  return El_Tool_1.el("img", props, innards);
}
exports.img = img;
function button(...[classOrPropOrChild, propOrChild, child]) {
  return _simpleFactory("button", [classOrPropOrChild, propOrChild, child]);
}
exports.button = button;
function select(classNameOrProps, propsOrInnards, maybeInnards) {
  const {props, innards} = El_Tool_1.standardizeInput(classNameOrProps, propsOrInnards, maybeInnards, a => Array.isArray(a));
  props.on = props.on || ({});
  let currentVal;
  if (props.onInput) {
    const {onInput} = props;
    const onInputListener = props.on.input;
    props.on.input = ev => {
      const newVal = out.value;
      if (newVal !== currentVal) {
        currentVal = newVal;
        onInput(newVal);
      }
      return !onInputListener || onInputListener(ev);
    };
  }
  const out = El_Tool_1.el("select", props, innards.map(innard => {
    let option;
    if (innard instanceof HTMLElement) {
      option = innard;
    } else if (typeof innard === "function") {
      option = El_Tool_1.el("option", {
        ddx: ref => {
          ref.innerText = String(innard());
          if (ref.selected) {}
        }
      });
    } else {
      option = El_Tool_1.el("option", String(innard));
    }
    if (option.selected) {
      currentVal = option.value;
    }
    return option;
  }));
  return out;
}
exports.select = select;
function inputTextbox(classNameOrProps, propsOrInnards, maybeInnards) {
  const {props, innards} = El_Tool_1.standardizeInput(classNameOrProps, propsOrInnards, maybeInnards, a => a instanceof helium_sdx_1.Source);
  props.on = props.on || ({});
  if (innards instanceof helium_sdx_1.Source) {
    const onInputArg = props.onInput;
    props.onInput = value => {
      innards.set(value);
      onInputArg && onInputArg(value);
    };
  }
  if (props.onInput) {
    const {onInput} = props;
    const onInputListener = props.on.input;
    let oldVal = innards;
    props.on.input = ev => {
      const newVal = out.value;
      if (newVal !== oldVal) {
        oldVal = newVal;
        onInput(newVal);
      }
      return !onInputListener || onInputListener(ev);
    };
  }
  if (props.onValueEntered) {
    const onChange = props.on.change;
    const {onValueEntered} = props;
    props.on.change = ev => {
      onValueEntered(ev.target.value);
      return !onChange || onChange(ev);
    };
  }
  const out = El_Tool_1.el("input", props);
  out.type = props.type || "text";
  if (innards) {
    if (typeof innards === "string") {
      out.value = innards;
    } else if (innards instanceof helium_sdx_1.Source) {
      helium_sdx_1.derive(() => out.value = innards.get());
    } else {
      helium_sdx_1.derive(() => out.value = innards());
    }
  }
  return out;
}
exports.inputTextbox = inputTextbox;
exports.italic = function (...args) {
  return _simpleFactory("i", args);
};
exports.bold = function (...args) {
  return _simpleFactory("b", args);
};
exports.hr = function (...args) {
  return _simpleFactory("hr", args);
};
function br(size, props = {}) {
  if (!size) {
    return El_Tool_1.el("br", props);
  }
  props.style = {
    display: "block",
    height: `${size}em`
  };
  return El_Tool_1.el("em-br", props);
}
exports.br = br;
exports.h1 = function (...args) {
  return _simpleFactory("h1", args);
};
exports.h2 = function (...args) {
  return _simpleFactory("h2", args);
};
exports.h3 = function (...args) {
  return _simpleFactory("h3", args);
};
exports.h4 = function (...args) {
  return _simpleFactory("h4", args);
};
exports.h5 = function (...args) {
  return _simpleFactory("h5", args);
};
exports.h6 = function (...args) {
  return _simpleFactory("h6", args);
};

},

// node_modules/helium-ui/dist/El-Tool/Navigation.js @33
33: function(__fusereq, exports, module){
"use strict";
Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.replaceUrl = exports.followUrl = exports.observeUrl = exports.onUrlChange = exports.UrlState = exports._UrlState = exports.NavigationEvent = exports.extLink = exports.a = exports.anchor = void 0;
const El_Tool_1 = __fusereq(31);
const Derivations_1 = __fusereq(13);
const helium_sdx_1 = __fusereq(17);
const Sourcify_1 = __fusereq(14);
function anchor(...[classOrProp, propOrChild, child]) {
  const {props, innards} = El_Tool_1.standardizeInput(classOrProp, propOrChild, child);
  props.attr = props.attr || ({});
  const {href} = props;
  if (!href) {
    props.attr.href = "#";
  } else if (typeof href === "string") {
    props.attr.href = href;
  }
  if (props.target) {
    props.attr.target = props.target;
  }
  const ref = El_Tool_1.el("a", props, innards);
  if (typeof href === "function") {
    Derivations_1.anchorDeriverToDomNode(ref, () => {
      ref.href = href();
    });
  }
  if (props.allowTextSelect) {
    El_Tool_1.setStyle(ref, {
      "-webkit-user-drag": "none"
    });
    let selectedText = "";
    El_Tool_1.on("mousedown", ref, ev => selectedText = window.getSelection().toString());
    El_Tool_1.on("click", ref, ev => {
      if (selectedText !== window.getSelection().toString()) {
        ev.preventDefault();
      }
    });
  }
  El_Tool_1.on("click", ref, ev => {
    if (ref.href) {
      if (props.disableHref) {
        return ev.preventDefault();
      }
      if (ev.metaKey || ref.target === "_blank") {
        window.open(ref.href, '_blank');
        return ev.preventDefault();
      } else if (exports.UrlState._updateUrl({
        urlPart: ref.href,
        mode: "push",
        linkElementClickEvent: true
      }) !== "PASS_THROUGH") {
        return ev.preventDefault();
      }
    }
  });
  return ref;
}
exports.anchor = anchor;
function a(...args) {
  console.warn(`helium function "a" has been deprecated.  Please use "anchor" instead.`);
  return anchor(...args);
}
exports.a = a;
function extLink(...[classOrProp, propOrChild, child]) {
  const {props, innards} = El_Tool_1.standardizeInput(classOrProp, propOrChild, child);
  props.target = "_blank";
  return a(props, innards);
}
exports.extLink = extLink;
class NavigationEvent extends Event {
  constructor(name, props, newUrl) {
    super(name, props);
    this.pageRefreshCancelled = false;
    this.hashFocusCancelled = false;
    this.preventPageRefresh = () => {
      this.pageRefreshCancelled = true;
    };
    this.preventHashFocus = () => {
      this.hashFocusCancelled = true;
    };
    this.newUrl = NavigationEvent.createFullUrl(newUrl);
  }
  static createFullUrl(urlPart) {
    try {
      return new URL(urlPart);
    } catch (err) {
      return new URL(urlPart, location.origin);
    }
  }
}
exports.NavigationEvent = NavigationEvent;
function dispatchNavEvent(name, newUrl) {
  const ev = new NavigationEvent(name, {
    cancelable: true
  }, newUrl);
  return window.dispatchEvent(ev) && ev;
}
function dispatchUrlUpdate(firstEvent, newUrl) {
  return dispatchNavEvent(firstEvent, newUrl) && dispatchNavEvent("locationchange", newUrl);
}
class _UrlState {
  constructor() {
    this.state = Sourcify_1.Sourcify({
      url: new URL(location.href),
      urlParams: {},
      hash: ""
    });
    this.initListeners();
  }
  getUrl() {
    return this.state.url;
  }
  get href() {
    return this.getUrl().href;
  }
  get host() {
    return this.getUrl().host;
  }
  get hostname() {
    return this.getUrl().hostname;
  }
  get pathname() {
    return this.getUrl().pathname;
  }
  get hash() {
    return this.state.hash;
  }
  get urlParams() {
    return this.state.urlParams;
  }
  followUrl(newUrl) {
    return this._updateUrl({
      urlPart: newUrl,
      mode: "push"
    });
  }
  replaceUrl(newUrl) {
    return this._updateUrl({
      urlPart: newUrl,
      mode: "replace"
    });
  }
  onUrlChange(cb) {
    window.addEventListener("locationchange", cb);
  }
  observeUrl(cb) {
    this.onUrlChange(cb);
    const ev = new NavigationEvent(name, {
      cancelable: true
    }, window.location.href);
    cb(ev);
  }
  _updateUrl(args) {
    const evResponse = dispatchUrlUpdate("followurl", args.urlPart);
    if (!evResponse) {
      return "BLOCKED";
    }
    if (evResponse.pageRefreshCancelled) {
      if (evResponse.newUrl.href !== location.href) {
        if (location.host !== evResponse.newUrl.host) {
          window.location.href = evResponse.newUrl.href;
          return;
        } else {
          if (args.mode === "push") {
            history.pushState(null, "", args.urlPart);
          } else if (args.mode === "replace") {
            history.replaceState(null, "", args.urlPart);
          }
          this.state.set("url", new URL(args.urlPart));
        }
      }
      return "CAPTURED";
    }
    if (args.linkElementClickEvent) {
      return "PASS_THROUGH";
    } else {
      window.location.href = NavigationEvent.createFullUrl(args.urlPart).href;
    }
  }
  initListeners() {
    window.addEventListener('popstate', ev => {
      dispatchNavEvent("locationchange", location.href);
    });
    setTimeout(() => {
      El_Tool_1.onDomReady(() => {
        setTimeout(() => {
          const hash = location.hash;
          const target = El_Tool_1.byId(hash.slice(1));
          if (!!target) {
            target.scrollIntoView({
              behavior: "auto"
            });
          }
        }, 200);
      });
    }, 0);
    let lastHash;
    this.onUrlChange(ev => {
      if (ev.hashFocusCancelled !== true && ev.newUrl.hash !== lastHash) {
        lastHash = ev.newUrl.hash;
        requestAnimationFrame(() => requestAnimationFrame(() => {
          const target = El_Tool_1.byId(lastHash.slice(1));
          if (!!target) {
            target.scrollIntoView({
              behavior: "auto"
            });
          }
        }));
      }
    });
    helium_sdx_1.derive({
      anchor: "NO_ANCHOR",
      fn: () => {
        const url = this.getUrl();
        const urlParams = new URLSearchParams(url.search);
        urlParams.forEach((value, key) => {
          this.state.urlParams[key] = value;
        });
        this.state.hash = url.hash;
      }
    });
  }
}
exports._UrlState = _UrlState;
exports.UrlState = new _UrlState();
exports.onUrlChange = exports.UrlState.onUrlChange.bind(exports.UrlState);
exports.observeUrl = exports.UrlState.observeUrl.bind(exports.UrlState);
exports.followUrl = exports.UrlState.followUrl.bind(exports.UrlState);
exports.replaceUrl = exports.UrlState.replaceUrl.bind(exports.UrlState);

},

// node_modules/helium-ui/dist/Derivations/Derivers/index.js @34
34: function(__fusereq, exports, module){
"use strict";
var __createBinding = this && this.__createBinding || (Object.create ? function (o, m, k, k2) {
  if (k2 === undefined) k2 = k;
  Object.defineProperty(o, k2, {
    enumerable: true,
    get: function () {
      return m[k];
    }
  });
} : function (o, m, k, k2) {
  if (k2 === undefined) k2 = k;
  o[k2] = m[k];
});
var __exportStar = this && this.__exportStar || (function (m, exports) {
  for (var p in m) if (p !== "default" && !exports.hasOwnProperty(p)) __createBinding(exports, m, p);
});
Object.defineProperty(exports, "__esModule", {
  value: true
});
__exportStar(__fusereq(50), exports);
__exportStar(__fusereq(51), exports);
__exportStar(__fusereq(52), exports);

},

// node_modules/helium-ui/dist/Derivations/Sources/index.js @35
35: function(__fusereq, exports, module){
"use strict";
var __createBinding = this && this.__createBinding || (Object.create ? function (o, m, k, k2) {
  if (k2 === undefined) k2 = k;
  Object.defineProperty(o, k2, {
    enumerable: true,
    get: function () {
      return m[k];
    }
  });
} : function (o, m, k, k2) {
  if (k2 === undefined) k2 = k;
  o[k2] = m[k];
});
var __exportStar = this && this.__exportStar || (function (m, exports) {
  for (var p in m) if (p !== "default" && !exports.hasOwnProperty(p)) __createBinding(exports, m, p);
});
Object.defineProperty(exports, "__esModule", {
  value: true
});
__exportStar(__fusereq(53), exports);
__exportStar(__fusereq(48), exports);
__exportStar(__fusereq(49), exports);

},

// node_modules/helium-ui/dist/Augments/OnIntersect.js @36
36: function(__fusereq, exports, module){
"use strict";
Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.onIntersect = exports.addIntersectionObserver = void 0;
const Factories_1 = __fusereq(32);
const El_Tool_1 = __fusereq(31);
let styleEl;
function assertStyling() {
  if (styleEl) {
    return;
  }
  styleEl = El_Tool_1.el("style");
  styleEl.innerHTML = `
    .hintRelative {
      position: relative;
    }

    .CoverCheckerPositioner {
      position: absolute;
      top: 0px;
      left: 0px;
      right: 0px;
      bottom: 0px;
      pointer-events: none;
      visibility: hidden;
    }
    
    .CoverChecker {
      position: sticky;
      height: 100vh;
      width: 100vw;
      top: 0px; 
    }
  `;
  document.head.prepend(styleEl);
}
function addIntersectionObserver(nodes, init, cb) {
  let thresholds;
  const isObject = init => {
    return init && typeof init === "object" && !Array.isArray(init);
  };
  if (isObject(init)) {
    thresholds = init.threshold;
  }
  if (!Array.isArray(thresholds)) {
    thresholds = [thresholds];
  }
  let startLength = thresholds.length;
  const normalThresholds = thresholds.map(it => {
    switch (it) {
      case "contained":
      case "maxFill":
        return 1;
      case "any":
        return 0;
    }
    return it;
  }).filter(it => typeof it === "number");
  const needsCoverMod = startLength > normalThresholds.length;
  if (!Array.isArray(nodes)) {
    nodes = [nodes];
  }
  if (needsCoverMod) {
    assertStyling();
    const checkUs = nodes.map(node => {
      let checkMe;
      const addMe = Factories_1.div("CoverCheckerPositioner", checkMe = Factories_1.div("CoverChecker"));
      node.classList.add("hintRelative");
      node.prepend(addMe);
      return checkMe;
    });
    const watcher = new IntersectionObserver(cb, {
      threshold: 1
    });
    checkUs.forEach(node => watcher.observe(node));
  }
  if (normalThresholds.length === 0) {
    return;
  }
  const options = isObject(init) ? init : {};
  options.threshold = normalThresholds;
  const watcher = new IntersectionObserver(cb, options);
  setTimeout(() => {
    nodes.forEach(node => watcher.observe(node));
  }, 0);
}
exports.addIntersectionObserver = addIntersectionObserver;
function onIntersect(init, cb) {
  return ref => addIntersectionObserver(ref, init, (entries, observer) => {
    cb(ref, entries[0], observer);
  });
}
exports.onIntersect = onIntersect;

},

// node_modules/helium-ui/dist/Augments/BlockEvent.js @37
37: function(__fusereq, exports, module){
"use strict";
Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.blockEvent = void 0;
function blockEvent(eventName) {
  return ref => ref.addEventListener(eventName, ev => {
    ev.preventDefault();
    return false;
  });
}
exports.blockEvent = blockEvent;

},

// node_modules/helium-sdx/dist/Derivations/index.js @38
38: function(__fusereq, exports, module){
"use strict";
function __export(m) {
  for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", {
  value: true
});
__export(__fusereq(54));
__export(__fusereq(55));
__export(__fusereq(56));
__export(__fusereq(57));
__export(__fusereq(58));

},

// node_modules/helium-sdx/dist/Sourcify/index.js @39
39: function(__fusereq, exports, module){
"use strict";
function __export(m) {
  for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", {
  value: true
});
__export(__fusereq(59));
__export(__fusereq(60));
__export(__fusereq(61));
__export(__fusereq(62));
__export(__fusereq(63));

},

// node_modules/helium-sdx/dist/AnotherLogger.js @40
40: function(__fusereq, exports, module){
"use strict";
Object.defineProperty(exports, "__esModule", {
  value: true
});
const logPrepends = {
  freeze: "❄️",
  unfreeze: "🔥",
  dirty: "💩",
  sourceUpdated: "📦",
  treeMutations: "🌲",
  scopes: "🔎",
  sourceRequest: "🧲",
  debugDeriver: "🐞"
};
const showLogTypes = {
  freeze: false,
  unfreeze: false,
  dirty: false,
  sourceUpdated: false,
  treeMutations: false,
  scopes: false,
  sourceRequest: false,
  debugDeriver: false
};
function showLoggingType(type, shouldShow = true) {
  showLogTypes[type] = shouldShow;
}
exports.showLoggingType = showLoggingType;
function showMiscLoggingType(type, shouldShow = true) {
  showLogTypes[type] = shouldShow;
}
exports.showMiscLoggingType = showMiscLoggingType;
function aLog(...args) {
  args = args.map(arg => arg && typeof arg === "object" && arg._static || arg);
  if (args.length <= 1) {
    return console.log(args[0]);
  }
  const type = args[0];
  const shouldShow = showLogTypes[type];
  if (shouldShow === false) {
    return;
  }
  const prepend = logPrepends[type];
  if (prepend) {
    args.shift();
    if (typeof args[0] === "string") {
      args[0] = `${prepend} ${args[0]}`;
    } else {
      args.unshift(prepend);
    }
  }
  console.log(...args);
}
exports.aLog = aLog;
if (typeof window !== "undefined") {
  window.aLog = aLog;
}

},

// src/popup/components/DropDown/dropDown.scss @41
41: function(__fusereq, exports, module){
__fusereq(3)("src/popup/components/DropDown/dropDown.scss","div.Dropdown.--showContent > .DropdownLabel::before {\n  transform: rotate(90deg); }\n\ndiv.Dropdown > .DropdownLabel::before {\n  display: inline-block;\n  content: \"\\25B6\";\n  transition: transform ease-out 0.2s;\n  margin-right: 0.5em; }\n")
},

// src/popup/components/BooleanSetting/booleanSetting.scss @42
42: function(__fusereq, exports, module){
__fusereq(3)("src/popup/components/BooleanSetting/booleanSetting.scss","div.BooleanSetting {\n  display: flex;\n  align-items: center; }\n  div.BooleanSetting.--enabled > .Switch {\n    background-color: #b8fad9; }\n    div.BooleanSetting.--enabled > .Switch > .SwitchThumb {\n      left: 100%;\n      transform: translateX(-100%); }\n  div.BooleanSetting.--enabled.redOnEnable > .Switch {\n    background-color: #fab8b8; }\n  div.BooleanSetting > .Switch {\n    flex-shrink: 0;\n    position: relative;\n    background-color: #FAFAFA;\n    box-shadow: inset 0px 0px 10px rgba(0, 0, 0, 0.2);\n    border: 1px solid #DDD;\n    padding: 0.125em;\n    width: 2.5em;\n    border-radius: 2em;\n    margin-right: 1em;\n    transition: background-color 0.2s ease-out; }\n    div.BooleanSetting > .Switch > .SwitchThumb {\n      background: white;\n      box-shadow: 0px 0px 3px rgba(0, 0, 0, 0.4);\n      position: relative;\n      left: 0px;\n      height: 1em;\n      width: 1em;\n      border-radius: 2em;\n      transition: all 0.2s ease-out; }\n\ndiv.DeletableBooleanSetting {\n  display: flex;\n  align-items: center; }\n  div.DeletableBooleanSetting.--removed {\n    display: none; }\n\ndiv.DeleteSetting {\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  background-color: #fab8b8;\n  color: white;\n  margin-right: 1em;\n  border-radius: 2em;\n  border: 1px solid rgba(150, 0, 0, 0.2);\n  height: 1.5em;\n  width: 1.5em; }\n  div.DeleteSetting:hover {\n    background-color: #c04040; }\n")
},

// node_modules/helium-ui-tweener/node_modules/helium-ui/dist/El-Tool/index.js @43
43: function(__fusereq, exports, module){
"use strict";
var __createBinding = this && this.__createBinding || (Object.create ? function (o, m, k, k2) {
  if (k2 === undefined) k2 = k;
  Object.defineProperty(o, k2, {
    enumerable: true,
    get: function () {
      return m[k];
    }
  });
} : function (o, m, k, k2) {
  if (k2 === undefined) k2 = k;
  o[k2] = m[k];
});
var __exportStar = this && this.__exportStar || (function (m, exports) {
  for (var p in m) if (p !== "default" && !exports.hasOwnProperty(p)) __createBinding(exports, m, p);
});
Object.defineProperty(exports, "__esModule", {
  value: true
});
__exportStar(__fusereq(70), exports);
__exportStar(__fusereq(71), exports);
__exportStar(__fusereq(72), exports);

},

// node_modules/helium-ui-tweener/node_modules/helium-ui/dist/Derivations/index.js @44
44: function(__fusereq, exports, module){
"use strict";
var __createBinding = this && this.__createBinding || (Object.create ? function (o, m, k, k2) {
  if (k2 === undefined) k2 = k;
  Object.defineProperty(o, k2, {
    enumerable: true,
    get: function () {
      return m[k];
    }
  });
} : function (o, m, k, k2) {
  if (k2 === undefined) k2 = k;
  o[k2] = m[k];
});
var __exportStar = this && this.__exportStar || (function (m, exports) {
  for (var p in m) if (p !== "default" && !exports.hasOwnProperty(p)) __createBinding(exports, m, p);
});
Object.defineProperty(exports, "__esModule", {
  value: true
});
__exportStar(__fusereq(64), exports);
__exportStar(__fusereq(65), exports);

},

// node_modules/helium-ui-tweener/node_modules/helium-ui/dist/Sourcify/index.js @45
45: function(__fusereq, exports, module){
"use strict";
var __createBinding = this && this.__createBinding || (Object.create ? function (o, m, k, k2) {
  if (k2 === undefined) k2 = k;
  Object.defineProperty(o, k2, {
    enumerable: true,
    get: function () {
      return m[k];
    }
  });
} : function (o, m, k, k2) {
  if (k2 === undefined) k2 = k;
  o[k2] = m[k];
});
var __exportStar = this && this.__exportStar || (function (m, exports) {
  for (var p in m) if (p !== "default" && !exports.hasOwnProperty(p)) __createBinding(exports, m, p);
});
Object.defineProperty(exports, "__esModule", {
  value: true
});
__exportStar(__fusereq(66), exports);
__exportStar(__fusereq(67), exports);
__exportStar(__fusereq(68), exports);
__exportStar(__fusereq(69), exports);

},

// node_modules/helium-ui-tweener/node_modules/helium-ui/dist/HeliumBase.js @46
46: function(__fusereq, exports, module){
"use strict";
Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.changeFreezeState = exports.isHeliumHazed = exports.heliumHaze = void 0;
function heliumHaze(rookie) {
  rookie._helium = rookie._helium || ({});
  return rookie;
}
exports.heliumHaze = heliumHaze;
function isHeliumHazed(checkMe) {
  return checkMe._helium;
}
exports.isHeliumHazed = isHeliumHazed;
function changeFreezeState(hazed, shouldBeFrozen) {
  const helium = hazed._helium;
  if (helium.frozen === shouldBeFrozen) {
    return;
  }
  helium.frozen = shouldBeFrozen;
  const fns = shouldBeFrozen ? helium.freeze : helium.unfreeze;
  if (fns) {
    fns.forEach(fn => fn());
  }
}
exports.changeFreezeState = changeFreezeState;

},

// node_modules/helium-ui-tweener/node_modules/helium-ui/dist/Augments/index.js @47
47: function(__fusereq, exports, module){
"use strict";
var __createBinding = this && this.__createBinding || (Object.create ? function (o, m, k, k2) {
  if (k2 === undefined) k2 = k;
  Object.defineProperty(o, k2, {
    enumerable: true,
    get: function () {
      return m[k];
    }
  });
} : function (o, m, k, k2) {
  if (k2 === undefined) k2 = k;
  o[k2] = m[k];
});
var __exportStar = this && this.__exportStar || (function (m, exports) {
  for (var p in m) if (p !== "default" && !exports.hasOwnProperty(p)) __createBinding(exports, m, p);
});
Object.defineProperty(exports, "__esModule", {
  value: true
});
__exportStar(__fusereq(73), exports);
__exportStar(__fusereq(74), exports);

},

// node_modules/helium-ui/dist/Derivations/Sources/SourceArray.js @48
48: function(__fusereq, exports, module){
"use strict";
Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.SourceArray = void 0;
const RerenderReplacer_1 = __fusereq(50);
const helium_sdx_1 = __fusereq(17);
class SourceArray extends helium_sdx_1.SourceArray {
  renderMap(renderFn) {
    return this._mapReplacers(false, renderFn);
  }
  renderMapKeyed(renderFn) {
    return this._mapReplacers(true, renderFn);
  }
  _mapReplacers(withIndex, renderFn) {
    const obsToRerenderMap = new Map();
    const listReplacer = new RerenderReplacer_1.RerenderReplacer(() => {
      this.orderMatters();
      const out = [];
      const length = this.length;
      for (let i = 0; i < length; i++) {
        const observable = withIndex ? this._getIndexObserver(i) : this.getSource(i);
        if (obsToRerenderMap.has(observable) === false) {
          obsToRerenderMap.set(observable, new RerenderReplacer_1.RerenderReplacer(() => {
            const value = observable.get();
            if (withIndex) {
              return renderFn(this.noDeletedConstant(value.get()), i);
            } else {
              return renderFn(this.noDeletedConstant(value));
            }
          }));
        }
        const nodes = obsToRerenderMap.get(observable).getRender();
        nodes.forEach(node => out.push(node));
      }
      return out;
    });
    return listReplacer.getRender();
  }
}
exports.SourceArray = SourceArray;

},

// node_modules/helium-ui/dist/Derivations/Sources/SourceMap.js @49
49: function(__fusereq, exports, module){
"use strict";
Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.SourceMap = void 0;
const RerenderReplacer_1 = __fusereq(50);
const helium_sdx_1 = __fusereq(17);
class SourceMap extends helium_sdx_1.SourceMap {
  renderMap(renderFn) {
    return this._mapReplacers(false, renderFn);
  }
  renderMapKeyed(renderFn) {
    return this._mapReplacers(true, renderFn);
  }
  _mapReplacers(withKey, renderFn) {
    const entryReplacers = new Map();
    const orderer = new RerenderReplacer_1.RerenderReplacer(() => {
      const out = [];
      const keys = this.keys();
      for (const key of keys) {
        if (entryReplacers.has(key) === false) {
          const source = this.getSource(key);
          entryReplacers.set(key, new RerenderReplacer_1.RerenderReplacer(() => renderFn(source.get(), withKey ? key : undefined)));
        }
        const nodeList = entryReplacers.get(key).getRender();
        for (const node of nodeList) {
          out.push(node);
        }
      }
      return out;
    });
    return orderer.getRender();
  }
}
exports.SourceMap = SourceMap;

},

// node_modules/helium-ui/dist/Derivations/Derivers/RerenderReplacer.js @50
50: function(__fusereq, exports, module){
"use strict";
Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.recycle = exports.RenderRecycler = exports.RerenderReplacer = void 0;
const El_Tool_1 = __fusereq(31);
const HeliumBase_1 = __fusereq(15);
const DomAnchoredDeriver_1 = __fusereq(51);
const helium_sdx_1 = __fusereq(17);
const REMOVE_NODE = false;
const ADD_NODE = true;
let RerenderReplacer = (() => {
  class RerenderReplacer {
    constructor(rerenderFn) {
      this.rerenderFn = rerenderFn;
      this.id = "ddx_" + RerenderReplacer.nextId++;
      this.preConstructHook();
      if (helium_sdx_1.DerivationManager.disabled()) {
        this.currentRender = El_Tool_1.convertInnardsToElements(this.rerenderFn());
      } else {
        this.placeHolder = document.createComment(this.id);
        const helium = HeliumBase_1.heliumHaze(this.placeHolder)._helium;
        helium.rerenderReplacer = this;
        helium.isReplacer = true;
        const domAnchorKit = DomAnchoredDeriver_1.anchorDeriverToDomNode(this.placeHolder, this.derive.bind(this), "RerenderReplacer");
        this.getAnchorReport = domAnchorKit.report;
        domAnchorKit.run();
      }
    }
    preConstructHook() {}
    getRender() {
      return this.currentRender;
    }
    derive() {
      const placehold = this.placeHolder;
      const newRender = El_Tool_1.convertInnardsToElements(this.rerenderFn());
      newRender.push(this.placeHolder);
      const oldRender = this.currentRender;
      this.currentRender = newRender;
      const parent = placehold.parentElement;
      if (!parent || !oldRender) {
        return;
      }
      const addRemoveMap = new Map();
      oldRender.forEach(node => addRemoveMap.set(node, REMOVE_NODE));
      newRender.forEach(node => {
        if (addRemoveMap.has(node)) {
          addRemoveMap.delete(node);
        } else {
          addRemoveMap.set(node, ADD_NODE);
        }
      });
      Array.from(addRemoveMap.entries()).forEach(([node, addRemove]) => {
        if (addRemove === REMOVE_NODE && node.parentElement) {
          node.parentElement.removeChild(node);
          if (HeliumBase_1.isHeliumHazed(node) && node._helium.isReplacer) {
            node._helium.rerenderReplacer.removeAll();
          }
        }
      });
      const currentNodes = parent.childNodes;
      let parentIndex = Array.from(currentNodes).indexOf(placehold);
      let lastNode;
      const length = newRender.length;
      for (let i = 0; i < length; i++) {
        const addMe = newRender[length - 1 - i];
        const current = currentNodes[parentIndex - i];
        if (addMe === current) {} else {
          parent.insertBefore(addMe, lastNode);
          if (addRemoveMap.get(addMe) !== ADD_NODE) {
            HeliumBase_1.heliumHaze(addMe)._helium.moveMutation = true;
          } else {
            parentIndex++;
          }
        }
        lastNode = addMe;
      }
    }
    removeAll() {
      this.currentRender.forEach(node => {
        if (node.parentElement) {
          node.parentElement.removeChild(node);
        }
      });
    }
  }
  RerenderReplacer.nextId = 0;
  return RerenderReplacer;
})();
exports.RerenderReplacer = RerenderReplacer;
;
class RenderRecycler extends RerenderReplacer {
  preConstructHook() {
    const renders = [];
    const rootRenderFn = this.rerenderFn;
    this.rerenderFn = () => {
      let cachedRender = renders.find(prevRender => {
        const sourceVals = Array.from(prevRender.sourceVals.entries());
        for (const [source, val] of sourceVals) {
          if (val !== source.peek()) {
            return false;
          }
        }
        for (const [source, val] of sourceVals) {
          source.get();
        }
        return true;
      });
      if (!cachedRender) {
        renders.push(cachedRender = {
          sourceVals: new Map(),
          render: rootRenderFn()
        });
        const {scope} = this.getAnchorReport({
          scope: true
        });
        const sources = scope.getSourceDependencies();
        sources.forEach(source => cachedRender.sourceVals.set(source, source.peek()));
      } else {
        console.log("Reusing cached render!", cachedRender);
      }
      return cachedRender.render;
    };
  }
}
exports.RenderRecycler = RenderRecycler;
function recycle(renderFn) {
  const replacer = new RenderRecycler(renderFn);
  return replacer.getRender();
}
exports.recycle = recycle;

},

// node_modules/helium-ui/dist/Derivations/Derivers/DomAnchoredDeriver.js @51
51: function(__fusereq, exports, module){
"use strict";
Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.anchorDeriverToDomNode = void 0;
const HeliumBase_1 = __fusereq(15);
const helium_sdx_1 = __fusereq(17);
function anchorDeriverToDomNode(node, ddx, type) {
  let scope;
  setupNodeAnchorMutationObserver();
  return {
    run: () => {
      const helium = HeliumBase_1.heliumHaze(node)._helium;
      const anchor = new helium_sdx_1.AnchorNode();
      const deriveFn = () => {
        const onPage = node.getRootNode() === document;
        if (!onPage) {
          HeliumBase_1.changeFreezeState(node, true);
        }
        ddx();
      };
      scope = new helium_sdx_1.DeriverScope({
        fn: deriveFn,
        batching: "singleFrame",
        anchor
      });
      (helium.unfreeze = helium.unfreeze || []).push(() => {
        anchor.setStatus(helium_sdx_1.AnchorStatus.NORMAL);
      });
      (helium.freeze = helium.freeze || []).push(() => {
        anchor.setStatus(helium_sdx_1.AnchorStatus.FROZEN);
      });
      scope.runDdx();
    },
    report: req => ({
      ...req.scope && ({
        scope
      })
    })
  };
}
exports.anchorDeriverToDomNode = anchorDeriverToDomNode;
let mutationsBuffer = [];
let bufferTimeout;
let mutationObserver;
function setupNodeAnchorMutationObserver() {
  if (typeof MutationObserver === "undefined") {
    return;
  }
  if (mutationObserver) {
    return;
  }
  mutationObserver = new MutationObserver(mutationsList => {
    mutationsList.forEach(mutation => {
      if (mutation.type !== 'childList') {
        return;
      }
      mutationsBuffer.push(mutation);
    });
    if (bufferTimeout) {
      clearTimeout(bufferTimeout);
    }
    bufferTimeout = setTimeout(() => {
      bufferTimeout = undefined;
      const oldBuffer = mutationsBuffer;
      mutationsBuffer = [];
      const haveNode = new Map();
      for (const mutation of oldBuffer) {
        mutation.removedNodes.forEach(node => haveNode.set(node, false));
        mutation.addedNodes.forEach(node => haveNode.set(node, true));
      }
      const addedNodes = [];
      const removedNodes = [];
      Array.from(haveNode.entries()).forEach(([node, shouldHave]) => {
        if (HeliumBase_1.isHeliumHazed(node) && node._helium.moveMutation) {
          return node._helium.moveMutation = false;
        }
        shouldHave ? addedNodes.push(node) : removedNodes.push(node);
      });
      helium_sdx_1.aLog("treeMutations", "Adding:", addedNodes);
      helium_sdx_1.aLog("treeMutations", "Removing:", removedNodes);
      addedNodes.forEach(node => {
        HeliumBase_1.heliumHaze(node);
        permeate(node, child => {
          if (HeliumBase_1.isHeliumHazed(child)) {
            HeliumBase_1.changeFreezeState(child, false);
          }
        });
      });
      removedNodes.forEach(node => {
        HeliumBase_1.heliumHaze(node);
        permeate(node, child => {
          if (HeliumBase_1.isHeliumHazed(child)) {
            HeliumBase_1.changeFreezeState(child, true);
          }
        });
      });
    }, 10);
  });
  var config = {
    childList: true,
    subtree: true
  };
  if (typeof window !== "undefined") {
    const tryAddObserver = () => mutationObserver.observe(document.body, config);
    if (document && document.body) {
      tryAddObserver();
    } else {
      document.addEventListener("DOMContentLoaded", () => tryAddObserver());
    }
  }
  function permeate(root, cb) {
    cb(root);
    root.childNodes.forEach(child => permeate(child, cb));
  }
}

},

// node_modules/helium-ui/dist/Derivations/Derivers/LocalStorage.js @52
52: function(__fusereq, exports, module){
"use strict";
Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.syncWithLocalStorage = void 0;
const helium_sdx_1 = __fusereq(17);
function syncWithLocalStorage(id, storeMe) {
  const init = localStorage.getItem(id);
  if (init) {
    try {
      const startingData = JSON.parse(init);
      storeMe.upsert(startingData);
    } catch (err) {
      console.warn("Could not parse locally stored data", err);
    }
  }
  helium_sdx_1.derive({
    anchor: "NO_ANCHOR",
    fn: () => {
      const toStore = storeMe.toObject();
      try {
        localStorage.setItem(id, JSON.stringify(toStore));
      } catch (err) {
        console.warn("Could not store", err);
        console.log(toStore);
      }
    }
  });
}
exports.syncWithLocalStorage = syncWithLocalStorage;

},

// node_modules/helium-ui/dist/Derivations/Sources/SourceBase.js @53
53: function(__fusereq, exports, module){
"use strict";
Object.defineProperty(exports, "__esModule", {
  value: true
});

},

// node_modules/helium-sdx/dist/Derivations/AnchorNode.js @54
54: function(__fusereq, exports, module){
"use strict";
Object.defineProperty(exports, "__esModule", {
  value: true
});
var AnchorStatus;
(function (AnchorStatus) {
  AnchorStatus["NORMAL"] = "n";
  AnchorStatus["FROZEN"] = "f";
})(AnchorStatus = exports.AnchorStatus || (exports.AnchorStatus = {}));
class AnchorNode {
  constructor() {
    this.status = AnchorStatus.NORMAL;
    this.children = [];
  }
  getStatus() {
    return this.status;
  }
  setStatus(newStat, notifyChildren = true) {
    if (newStat === this.status) {
      return false;
    }
    this.status = newStat;
    if (notifyChildren) {
      this.children.forEach(child => child.setStatus(this.status));
    }
    return true;
  }
  isFrozen() {
    return this.status === AnchorStatus.FROZEN;
  }
  statusIsNormal() {
    return this.status === AnchorStatus.NORMAL;
  }
  addChild(child) {
    this.children.push(child);
  }
}
exports.AnchorNode = AnchorNode;

},

// node_modules/helium-sdx/dist/Derivations/Derivers/index.js @55
55: function(__fusereq, exports, module){
"use strict";
function __export(m) {
  for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", {
  value: true
});
__export(__fusereq(75));

},

// node_modules/helium-sdx/dist/Derivations/DerivationManager.js @56
56: function(__fusereq, exports, module){
"use strict";
Object.defineProperty(exports, "__esModule", {
  value: true
});
const DeriverScope_1 = __fusereq(57);
const AnotherLogger_1 = __fusereq(40);
const DerivationBatcher_1 = __fusereq(80);
function derive(fnOrProps) {
  let props;
  if (typeof fnOrProps === "function") {
    props = {
      fn: fnOrProps
    };
  } else {
    props = fnOrProps;
  }
  const scope = new DeriverScope_1.DeriverScope(props);
  scope.runDdx();
  return scope;
}
exports.derive = derive;
function noDeps(cb) {
  let out;
  exports.DerivationManager._runAsDeriver(undefined, () => out = cb());
  return out;
}
exports.noDeps = noDeps;
class __DerivationManager {
  constructor() {
    this.deriverStack = [];
    this.lastSourceId = 0;
    this.sourceToDeriverMap = new Map();
    this.knownScopes = new Map();
    this.batcher = new DerivationBatcher_1.DerivationBatcher(this);
    this.completedDeriversBatch = [];
    const existing = window.DerivationManager;
    if (existing) {
      return existing;
    }
    window.DerivationManager = this;
  }
  disabled() {
    return this.lastSourceId === 0;
  }
  debugDeriver() {
    if (!this.currentDeriver) throw new Error("Current Scope is undefined");
    AnotherLogger_1.showLoggingType("debugDeriver");
    this.targetDebugDeriver = this.currentDeriver;
  }
  isDebugDeriver(deriver = this.targetDebugDeriver) {
    return this.currentDeriver === deriver;
  }
  createSourceId(append) {
    return `s${this.lastSourceId++}` + (append ? `_${append}` : "");
  }
  sourceRequested(source) {
    if (!this.currentDeriver) {
      return;
    }
    const sourceId = source.getSourceId();
    if (this.isDebugDeriver()) {
      AnotherLogger_1.aLog("debugDeriver", `GET: Source requested ${sourceId}`);
    }
    AnotherLogger_1.aLog("sourceRequest", `${this.currentDeriver.id}: Source requested ${sourceId}`);
    this.currentDeriver.addSourceDependency(source);
  }
  sourceUpdated(source, equalValue) {
    const sourceId = source.getSourceId();
    AnotherLogger_1.aLog("sourceUpdated", `Update: ${sourceId}`);
    const toBeUpdated = this.popDeriversDependentOnSource(sourceId);
    toBeUpdated && toBeUpdated.forEach(deriver => {
      if (deriver.ignoresEqualValues() && equalValue) {
        this.makeDeriverDependOnSource(deriver, sourceId);
        return;
      }
      this.batcher.addDeriverWithReason(deriver, source);
    });
  }
  _runAsDeriver(deriver, ddx) {
    let startDeriver = this.currentDeriver;
    this.deriverStack.push(this.currentDeriver = deriver);
    AnotherLogger_1.aLog("scopes", "Running Scope", deriver && deriver.id);
    if (this.isDebugDeriver()) {
      AnotherLogger_1.aLog("debugDeriver", "<Rerunning Deriver>");
    }
    ddx();
    this.deriverStack.pop();
    this.currentDeriver = startDeriver;
    if (!!deriver) {
      this.completedDeriversBatch.push(deriver);
      if (!this.currentDeriver) {
        this.completedDeriversBatch.reverse().forEach(deriver => {
          deriver.getSourceDependencyIds().forEach(sourceId => {
            this.makeDeriverDependOnSource(deriver, sourceId);
          });
        });
        this.completedDeriversBatch = [];
      }
    }
  }
  _getCurrentDeriver() {
    return this.currentDeriver;
  }
  popDeriversDependentOnSource(sourceId) {
    const deriversPerSource = this.sourceToDeriverMap.get(sourceId);
    if (!deriversPerSource) {
      return;
    }
    const out = Array.from(deriversPerSource.keys());
    this.sourceToDeriverMap.delete(sourceId);
    return out;
  }
  makeDeriverDependOnSource(deriver, sourceId) {
    if (this.sourceToDeriverMap.has(sourceId) === false) {
      this.sourceToDeriverMap.set(sourceId, new Map());
    }
    return this.sourceToDeriverMap.get(sourceId).set(deriver);
  }
  dropDeriver(deriver) {
    if (deriver.statusIsNormal()) {
      throw Error("Will not drop an status normal scope");
    }
    this.knownScopes.delete(deriver);
  }
}
exports.__DerivationManager = __DerivationManager;
exports.DerivationManager = new __DerivationManager();

},

// node_modules/helium-sdx/dist/Derivations/DeriverScope.js @57
57: function(__fusereq, exports, module){
"use strict";
Object.defineProperty(exports, "__esModule", {
  value: true
});
const DerivationManager_1 = __fusereq(56);
const AnchorNode_1 = __fusereq(54);
let SCOPE_ID = 0;
class DeriverScope extends AnchorNode_1.AnchorNode {
  constructor(props) {
    super();
    this.props = props;
    this.sourceDeps = new Map();
    this.id = String(SCOPE_ID++);
    this.creationStack = new Error().stack;
    this.dirty = false;
    this.store = {};
    props.anchor = props.anchor || DerivationManager_1.DerivationManager._getCurrentDeriver() || "NO_ANCHOR";
    props.batching = props.batching || "stack";
    if (typeof props.anchor !== "string") {
      props.anchor.addChild(this);
    }
  }
  addSourceDependency(source) {
    this.sourceDeps.set(source);
  }
  runDdx() {
    this.dropChildren();
    if (this.isFrozen()) {
      DerivationManager_1.DerivationManager.dropDeriver(this);
      return this.dirty = true;
    }
    this.dirty = false;
    try {
      DerivationManager_1.DerivationManager._runAsDeriver(this, () => {
        this.sourceDeps = new Map();
        this.props.fn({
          scope: this,
          store: this.store
        });
      });
    } catch (err) {
      console.error(err);
      console.error("FROM STACK:\n", this.creationStack);
    }
  }
  getSourceDependencies() {
    return Array.from(this.sourceDeps.keys());
  }
  getSourceDependencyIds() {
    return Array.from(this.sourceDeps.keys()).map(source => source.getSourceId());
  }
  hasDep(source) {
    return this.sourceDeps.has(source);
  }
  getDeriver() {
    return this.props.fn;
  }
  getStore() {
    return this.store;
  }
  getBatching() {
    return this.props.batching;
  }
  ignoresEqualValues() {
    return this.props.allowEqualValueUpdates !== true;
  }
  unfreeze() {
    return this.setStatus(AnchorNode_1.AnchorStatus.NORMAL);
  }
  setStatus(status) {
    const didChange = super.setStatus(status, false);
    if (didChange) {
      if (this.statusIsNormal() && this.dirty) {
        this.runDdx();
      } else {
        this.children.forEach(child => child.setStatus(this.status));
      }
    }
    return didChange;
  }
  dropChildren() {
    this.children.forEach(child => child.setStatus(AnchorNode_1.AnchorStatus.FROZEN));
    this.children = [];
  }
}
exports.DeriverScope = DeriverScope;

},

// node_modules/helium-sdx/dist/Derivations/Sources/index.js @58
58: function(__fusereq, exports, module){
"use strict";
function __export(m) {
  for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", {
  value: true
});
__export(__fusereq(76));
__export(__fusereq(77));
__export(__fusereq(78));
__export(__fusereq(79));

},

// node_modules/helium-sdx/dist/Sourcify/SourceProxyObject.js @59
59: function(__fusereq, exports, module){
"use strict";
Object.defineProperty(exports, "__esModule", {
  value: true
});
const SourceProxyBase_1 = __fusereq(61);
const SourceMap_1 = __fusereq(78);
const SourcificationManager_1 = __fusereq(62);
const SourceProxyUtilities_1 = __fusereq(63);
const Derivations_1 = __fusereq(38);
class SourceProxyObject extends SourceProxyBase_1.SourceProxyBase {
  constructor(target, srcMap = new SourceMap_1.SourceMap()) {
    super(target);
    this.srcMap = srcMap;
    this[Symbol.iterator] = () => {
      this.refreshAll();
      return this.srcMap.iterator();
    };
    this._fresh = false;
  }
  bind(key, bindTo) {
    this.assertKey(key);
    if (!bindTo) {
      const src = this.srcMap.getSource(key);
      Derivations_1.derive(() => this.set(key, src.get()));
      return src;
    }
    Derivations_1.derive(() => this.set(key, bindTo.get()));
    Derivations_1.derive(() => bindTo.set(this.get(key)));
  }
  delete(index) {
    this.srcMap.delete(index);
    return delete this.target[index];
  }
  get(key) {
    this.assertKey(key);
    return this.srcMap.get(key);
  }
  has(key) {
    this.assertKey(key);
    return this.srcMap.has(key);
  }
  keys() {
    this.refreshAll();
    return this.srcMap.keys();
  }
  _propertyList() {
    return this.keys();
  }
  overwrite(newMe) {
    const extraKeys = new Map(Array.from(Object.entries(this.target)));
    Object.keys(newMe).forEach(key => {
      this.set(key, newMe[key]);
      extraKeys.delete(key);
    });
    for (const key of extraKeys.keys()) {
      this.delete(key);
    }
    return SourcificationManager_1.HELIUM_PROXY_OUT;
  }
  peek(key) {
    this.assertKey(key);
    return this.srcMap.peek(key);
  }
  set(key, value) {
    if (SourceProxyUtilities_1.isProxied(value)) {
      this.target[key] = value.toObjectNoDeps();
      this.srcMap.set(key, value);
    } else {
      this.target[key] = value;
      this.refresh(key);
    }
  }
  size() {
    this.refreshAll();
    return this.srcMap.size();
  }
  toObject() {
    const out = {};
    this.refreshAll();
    this.srcMap.forEach((item, key) => {
      if (item && typeof item === "object" && ("toObject" in item)) {
        item = item.toObject();
      }
      out[key] = item;
    });
    return out;
  }
  upsert(items, shallow = false) {
    Object.keys(items).forEach(_key => {
      const key = _key;
      const value = items[key];
      if (!shallow) {
        const current = this.peek(key);
        if (value && typeof value === "object" && !Array.isArray(value) && current && ("upsert" in current)) {
          current.upsert(value);
          return;
        }
      }
      this.set(key, items[key]);
    });
  }
  assertKey(key) {
    if ((key in this.target) === this.srcMap.peekHas(key)) {
      return;
    }
    this.refresh(key);
  }
  refresh(key) {
    let value = this.target[key];
    if (value && typeof value === "object") {
      const peek = this.srcMap.peek(key);
      if (SourceProxyUtilities_1.isProxied(peek) && peek.toObjectNoDeps() === value) {
        return;
      }
      value = SourceProxyUtilities_1.Sourcify(value);
    }
    this.srcMap.set(key, value);
  }
  refreshAll(type) {
    if (this._fresh && !type) {
      return;
    }
    Object.keys(this.target).forEach(key => {
      if (type === "HARD") {
        this.refresh(key);
      } else {
        this.assertKey(key);
      }
    });
    this._fresh = true;
  }
}
exports.SourceProxyObject = SourceProxyObject;
;
exports.ASSERT_SHALLOW_TYPE_MATCH = {};

},

// node_modules/helium-sdx/dist/Sourcify/SourceProxyArray.js @60
60: function(__fusereq, exports, module){
"use strict";
Object.defineProperty(exports, "__esModule", {
  value: true
});
const SourceProxyBase_1 = __fusereq(61);
const SourceArray_1 = __fusereq(77);
const SourcificationManager_1 = __fusereq(62);
const SourceProxyUtilities_1 = __fusereq(63);
class SourceProxyArray extends SourceProxyBase_1.SourceProxyBase {
  constructor(target, srcArray = new SourceArray_1.SourceArray()) {
    super(target);
    this.srcArray = srcArray;
    this[Symbol.iterator] = () => {
      this.refreshAll();
      return this.srcArray.iterator();
    };
    this._fresh = false;
  }
  get length() {
    this.assertIndex(this.target.length - 1);
    return this.srcArray.length;
  }
  concat(items) {
    this.refreshAll();
    const out = this.toObject();
    items.forEach(it => out.push(SourceProxyUtilities_1.Sourcify(it)));
    return SourceProxyUtilities_1.Sourcify(out);
  }
  delete(index) {
    this.srcArray.delete(index);
    return delete this.target[index];
  }
  entries() {
    this.refreshAll();
    return this.srcArray.entries();
  }
  clear() {
    while (this.target.length) {
      this.target.pop();
    }
    this.srcArray.clear();
    return SourcificationManager_1.HELIUM_PROXY_OUT;
  }
  every(cb) {
    this.refreshAll();
    return this.srcArray.every(cb);
  }
  fill(value, start, end) {
    value = SourceProxyUtilities_1.Sourcify(value);
    end = end !== undefined ? end : this.target.length;
    for (let i = start || 0; i < end; i++) {
      this.set(i, value);
    }
    return SourcificationManager_1.HELIUM_PROXY_OUT;
  }
  filter(cb) {
    this.refreshAll();
    return SourceProxyUtilities_1.Sourcify(this.srcArray.filter(cb));
  }
  find(cb) {
    this.refreshAll();
    return this.srcArray.find(cb);
  }
  findIndex(cb) {
    this.refreshAll();
    return this.srcArray.findIndex(cb);
  }
  flat(depth) {
    this.refreshAll();
    return SourceProxyUtilities_1.Sourcify(this.srcArray.flat(depth));
  }
  forEach(cb) {
    this.refreshAll();
    return this.srcArray.forEach(cb);
  }
  forEachPart(cb) {
    this.refreshAll();
    return this.srcArray.forEachPart(cb);
  }
  get(index) {
    this.assertIndex(index);
    return this.srcArray.get(index);
  }
  getPart(index) {
    this.assertIndex(index);
    return this.srcArray.getPart(index);
  }
  has(index) {
    this.assertIndex(index);
    return this.srcArray.has(index);
  }
  indexOf(target) {
    this.refreshAll();
    return this.srcArray.indexOf(target);
  }
  includes(target, fromIndex) {
    this.refreshAll();
    return this.srcArray.includes(target, fromIndex);
  }
  join(separator) {
    this.refreshAll();
    return this.srcArray.join(separator);
  }
  keys() {
    this.refreshAll();
    return this.srcArray.keys();
  }
  _propertyList() {
    this.refreshAll();
    const out = this.srcArray.keys().map(it => String(it));
    out.push("length");
    return out;
  }
  map(cb) {
    this.refreshAll();
    return SourceProxyUtilities_1.Sourcify(this.srcArray.map(cb));
  }
  overwrite(newSelf) {
    const oldLength = this.target.length;
    for (let i = 0; i < oldLength; i++) {
      if (i < newSelf.length) {
        this.set(i, newSelf[i]);
      } else {
        this.delete(i);
      }
    }
    return SourcificationManager_1.HELIUM_PROXY_OUT;
  }
  partsSlice(start, end) {
    this.refresh(start, end !== undefined ? end : -1);
    return SourceProxyUtilities_1.Sourcify(this.srcArray.partsSlice(start, end));
  }
  peekLength() {
    return this.target.length;
  }
  peek(index) {
    this.assertIndex(index);
    return this.srcArray.peek(index);
  }
  push(value) {
    const length = this.target.length;
    this.set(length, value);
  }
  pop() {
    this.assertIndex(this.target.length - 1);
    this.target.pop();
    return this.srcArray.pop();
  }
  reduce(cb) {
    this.refreshAll();
    return this.srcArray.reduce(cb);
  }
  remove(item, count = -1) {
    this.refreshAll();
    let targetItemType = item;
    if (SourceProxyUtilities_1.isProxied(item)) {
      targetItemType = item.toObjectNoDeps();
    }
    let removeCount = 0;
    for (let i = 0; i < this.target.length && (count < 0 || removeCount < count); i++) {
      if (this.target[i] === targetItemType) {
        removeCount++;
        this.target.splice(i, 1);
        i -= 1;
      }
    }
    this.srcArray.remove(item, count);
    return SourcificationManager_1.HELIUM_PROXY_OUT;
  }
  reverse() {
    this.refreshAll();
    this.target.reverse();
    this.srcArray.reverse();
    return SourcificationManager_1.HELIUM_PROXY_OUT;
  }
  slice(start, end) {
    this.refresh(start, end !== undefined ? end : -1);
    return SourceProxyUtilities_1.Sourcify(this.srcArray.slice(start, end));
  }
  set(index, value) {
    if (SourceProxyUtilities_1.isProxied(value)) {
      this.target[index] = value.toObjectNoDeps();
      this.srcArray.set(index, value);
    } else {
      this.target[index] = value;
      this.refresh(index);
    }
  }
  shift() {
    this.assertIndex(0);
    this.target.shift();
    return this.srcArray.shift();
  }
  some(cb) {
    this.refreshAll();
    return this.srcArray.some(cb);
  }
  sort(sortFn) {
    this.refreshAll();
    this.target.sort(sortFn);
    this.srcArray.sort(sortFn);
    return SourcificationManager_1.HELIUM_PROXY_OUT;
  }
  splice(start, deleteCount, ...items) {
    this.target.splice(start, deleteCount, ...(items || []).map(it => SourceProxyUtilities_1.isProxied(it) ? it.toObjectNoDeps() : it));
    return SourceProxyUtilities_1.Sourcify(this.srcArray.splice(start, deleteCount, ...items || []));
  }
  toObject() {
    const out = [];
    this.refreshAll();
    this.srcArray.forEach((item, index) => {
      if (item && typeof item === "object" && ("toObject" in item)) {
        item = item.toObject();
      }
      out[index] = item;
    });
    return out;
  }
  unshift(item) {
    this.target.unshift(SourceProxyUtilities_1.Unsourcify(item));
    return this.srcArray.unshift(SourceProxyUtilities_1.Sourcify(item));
  }
  assertIndex(index) {
    if (index >= this.target.length) {
      return;
    }
    if (!!((index in this.target)) === !!this.srcArray.peekHas(index)) {
      return;
    }
    this.refresh(index);
  }
  refresh(start, end) {
    if (end === undefined) {
      end = start + 1;
    } else if (end === -1) {
      end = this.target.length - 1;
    }
    for (let i = start; i < end; i++) {
      let value = this.target[i];
      if (value && typeof value === "object") {
        const peek = this.srcArray.peek(i);
        if (SourceProxyUtilities_1.isProxied(peek) && peek.toObjectNoDeps() === value) {
          return;
        }
        value = SourceProxyUtilities_1.Sourcify(value);
      }
      this.srcArray.set(i, value);
    }
  }
  refreshAll(type) {
    if (this._fresh && !type) {
      return;
    }
    for (let i = 0; i < this.target.length; i++) {
      if (type === "HARD") {
        this.refresh(i);
      } else {
        this.assertIndex(i);
      }
    }
    this._fresh = true;
  }
}
exports.SourceProxyArray = SourceProxyArray;
exports.ASSERT_SHALLOW_TYPE_MATCH_ARRAY = {};

},

// node_modules/helium-sdx/dist/Sourcify/SourceProxyBase.js @61
61: function(__fusereq, exports, module){
"use strict";
Object.defineProperty(exports, "__esModule", {
  value: true
});
class SourceProxyBase {
  constructor(target) {
    this.target = target;
  }
}
exports.SourceProxyBase = SourceProxyBase;

},

// node_modules/helium-sdx/dist/Sourcify/SourcificationManager.js @62
62: function(__fusereq, exports, module){
"use strict";
Object.defineProperty(exports, "__esModule", {
  value: true
});
const SourceProxyArray_1 = __fusereq(60);
const SourceProxyObject_1 = __fusereq(59);
exports.HELIUM_PROXY_OUT = {};
class SourcificationManager {
  constructor(item) {
    this.setProxy(item);
    if (Array.isArray(item)) {
      this.type = "ARR";
      this.srcMgmt = SourcificationManager.createSourceProxyArray(item);
    } else {
      this.type == "OBJ";
      this.srcMgmt = SourcificationManager.createSourceProxyObject(item);
    }
  }
  get proxy() {
    return this._proxy;
  }
  autocorrectPropName(propName) {
    if (typeof propName === "symbol") {
      return undefined;
    }
    if (this.type === "ARR") {
      let asNum;
      if (!isNaN(asNum = parseInt(propName))) {
        return asNum;
      } else {
        return undefined;
      }
    }
    return propName;
  }
  setProxy(item) {
    this._proxy = new Proxy(item, {
      deleteProperty: (_, propName) => {
        propName = this.autocorrectPropName(propName);
        if (propName === undefined) {
          return false;
        }
        return this.srcMgmt.delete(propName);
      },
      get: (_, propName) => {
        switch (propName) {
          case "getProxyManager":
            return () => this;
          case "toObjectNoDeps":
            return () => item;
          case "pretend":
            return () => this._proxy;
        }
        const correctedName = this.autocorrectPropName(propName);
        if (correctedName !== undefined && this.srcMgmt.has(correctedName)) {
          return this.srcMgmt.get(correctedName);
        }
        if ((propName in this.srcMgmt)) {
          if (typeof this.srcMgmt[propName] === "function") {
            return (...args) => {
              const out = this.srcMgmt[propName](...args);
              if (out === exports.HELIUM_PROXY_OUT) {
                return this._proxy;
              }
              return out;
            };
          }
          return this.srcMgmt[propName];
        }
      },
      has: (_, propName) => {
        switch (propName) {
          case "getProxyManager":
          case "toObjectNoDeps":
            return true;
        }
        if ((propName in this.srcMgmt)) {
          return true;
        }
        propName = this.autocorrectPropName(propName);
        if (propName === undefined) {
          return false;
        }
        return this.srcMgmt.has(propName);
      },
      ownKeys: _ => {
        return this.srcMgmt._propertyList();
      },
      set: (_, propName, value) => {
        propName = this.autocorrectPropName(propName);
        if (propName === undefined) {
          throw new Error(`Can not modify array prototype "${propName}" of type: ${typeof propName}`);
        }
        this.srcMgmt.set(propName, value);
        return true;
      }
    });
  }
}
exports.SourcificationManager = SourcificationManager;
SourcificationManager.createSourceProxyArray = item => new SourceProxyArray_1.SourceProxyArray(item);
SourcificationManager.createSourceProxyObject = item => new SourceProxyObject_1.SourceProxyObject(item);

},

// node_modules/helium-sdx/dist/Sourcify/SourceProxyUtilities.js @63
63: function(__fusereq, exports, module){
"use strict";
Object.defineProperty(exports, "__esModule", {
  value: true
});
const SourcificationManager_1 = __fusereq(62);
function Sourcify(target) {
  if (isProxied(target)) {
    return target;
  } else if (typeof target !== "object") {
    return target;
  } else if ((/^(object|array)$/i).test(target.constructor.name) === false) {
    return target;
  }
  const mgmt = new SourcificationManager_1.SourcificationManager(target);
  return mgmt.proxy;
}
exports.Sourcify = Sourcify;
function Unsourcify(target) {
  if (isProxied(target)) {
    return target.toObjectNoDeps();
  }
  return target;
}
exports.Unsourcify = Unsourcify;
function isProxied(checkMe) {
  return checkMe && typeof checkMe === "object" && ("getProxyManager" in checkMe);
}
exports.isProxied = isProxied;

},

// node_modules/helium-ui-tweener/node_modules/helium-ui/dist/Derivations/Derivers/index.js @64
64: function(__fusereq, exports, module){
"use strict";
var __createBinding = this && this.__createBinding || (Object.create ? function (o, m, k, k2) {
  if (k2 === undefined) k2 = k;
  Object.defineProperty(o, k2, {
    enumerable: true,
    get: function () {
      return m[k];
    }
  });
} : function (o, m, k, k2) {
  if (k2 === undefined) k2 = k;
  o[k2] = m[k];
});
var __exportStar = this && this.__exportStar || (function (m, exports) {
  for (var p in m) if (p !== "default" && !exports.hasOwnProperty(p)) __createBinding(exports, m, p);
});
Object.defineProperty(exports, "__esModule", {
  value: true
});
__exportStar(__fusereq(84), exports);
__exportStar(__fusereq(85), exports);
__exportStar(__fusereq(86), exports);

},

// node_modules/helium-ui-tweener/node_modules/helium-ui/dist/Derivations/Sources/index.js @65
65: function(__fusereq, exports, module){
"use strict";
var __createBinding = this && this.__createBinding || (Object.create ? function (o, m, k, k2) {
  if (k2 === undefined) k2 = k;
  Object.defineProperty(o, k2, {
    enumerable: true,
    get: function () {
      return m[k];
    }
  });
} : function (o, m, k, k2) {
  if (k2 === undefined) k2 = k;
  o[k2] = m[k];
});
var __exportStar = this && this.__exportStar || (function (m, exports) {
  for (var p in m) if (p !== "default" && !exports.hasOwnProperty(p)) __createBinding(exports, m, p);
});
Object.defineProperty(exports, "__esModule", {
  value: true
});
__exportStar(__fusereq(81), exports);
__exportStar(__fusereq(82), exports);
__exportStar(__fusereq(83), exports);

},

// node_modules/helium-ui-tweener/node_modules/helium-ui/dist/Sourcify/SourceProxyObject.js @66
66: function(__fusereq, exports, module){
"use strict";
Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.ASSERT_SHALLOW_TYPE_MATCH = exports.SourceProxyObject = void 0;
const SourceMap_1 = __fusereq(83);
const helium_sdx_1 = __fusereq(17);
class SourceProxyObject extends helium_sdx_1.SourceProxyObject {
  constructor(target) {
    super(target, new SourceMap_1.SourceMap());
  }
  getSrcMap() {
    return this.srcMap;
  }
  renderMap(renderFn) {
    this.refreshAll();
    return this.getSrcMap().renderMap(renderFn);
  }
  renderMapKeyed(renderFn) {
    this.refreshAll();
    return this.getSrcMap().renderMapKeyed(renderFn);
  }
}
exports.SourceProxyObject = SourceProxyObject;
;
exports.ASSERT_SHALLOW_TYPE_MATCH = {};

},

// node_modules/helium-ui-tweener/node_modules/helium-ui/dist/Sourcify/SourceProxyArray.js @67
67: function(__fusereq, exports, module){
"use strict";
Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.ASSERT_SHALLOW_TYPE_MATCH_ARRAY = exports.SourceProxyArray = void 0;
const SourceArray_1 = __fusereq(82);
const helium_sdx_1 = __fusereq(17);
class SourceProxyArray extends helium_sdx_1.SourceProxyArray {
  constructor(target) {
    super(target, new SourceArray_1.SourceArray());
  }
  getSrcArray() {
    return this.srcArray;
  }
  renderMap(renderFn) {
    this.refreshAll();
    return this.getSrcArray().renderMap(renderFn);
  }
  renderMapKeyed(renderFn) {
    this.refreshAll();
    return this.getSrcArray().renderMapKeyed(renderFn);
  }
}
exports.SourceProxyArray = SourceProxyArray;
exports.ASSERT_SHALLOW_TYPE_MATCH_ARRAY = {};

},

// node_modules/helium-ui-tweener/node_modules/helium-ui/dist/Sourcify/SourceProxyShallowTypes.js @68
68: function(__fusereq, exports, module){
"use strict";
Object.defineProperty(exports, "__esModule", {
  value: true
});
function __() {
  ;
  const b = {};
  function testFn(testMe) {
    return null;
  }
  const out = b.a[3];
  const la = b.a.overwrite([]);
  const ba = b.a.sort();
  ba.renderMap(() => null);
  la.renderMap(() => null);
  const a = b.a.filter(() => true);
  const c = a[0];
  const d = b.get("a");
}

},

// node_modules/helium-ui-tweener/node_modules/helium-ui/dist/Sourcify/SourceProxyUtilities.js @69
69: function(__fusereq, exports, module){
"use strict";
Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.renderMapKeyed = exports.renderMap = exports.isProxied = exports.Unsourcify = exports.Sourcify = void 0;
const El_Tool_1 = __fusereq(70);
const helium_sdx_1 = __fusereq(17);
let SourcificationManagerModded = false;
function Sourcify(target) {
  if (!SourcificationManagerModded) {
    const {SourceProxyArray} = __fusereq(67);
    const {SourceProxyObject} = __fusereq(66);
    helium_sdx_1.SourcificationManager.createSourceProxyArray = item => new SourceProxyArray(item);
    helium_sdx_1.SourcificationManager.createSourceProxyObject = item => new SourceProxyObject(item);
    SourcificationManagerModded = true;
  }
  return helium_sdx_1.Sourcify(target);
}
exports.Sourcify = Sourcify;
function Unsourcify(target) {
  return helium_sdx_1.Unsourcify(target);
}
exports.Unsourcify = Unsourcify;
function isProxied(checkMe) {
  return helium_sdx_1.isProxied(checkMe);
}
exports.isProxied = isProxied;
function renderMap(obj, renderFn) {
  return _anyRMap(false, obj, renderFn);
}
exports.renderMap = renderMap;
function renderMapKeyed(obj, renderFn) {
  return _anyRMap(true, obj, renderFn);
}
exports.renderMapKeyed = renderMapKeyed;
function _anyRMap(withKey, obj, renderFn) {
  if (isProxied(obj)) {
    if (withKey) {
      return obj.rkmap(renderFn);
    }
    return obj.rmap(renderFn);
  } else if (obj && typeof obj === "object") {
    const out = [];
    let renders;
    if (Array.isArray(obj)) {
      renders = obj.map((value, index) => renderFn(value, index));
    } else {
      renders = Object.entries(obj).map(([key, value]) => renderFn(value, key));
    }
    renders.forEach(render => {
      const nodesList = El_Tool_1.convertInnardsToElements(render);
      if (!nodesList || !nodesList.length) {
        return;
      }
      nodesList.forEach(node => out.push(node));
    });
    return out;
  }
}

},

// node_modules/helium-ui-tweener/node_modules/helium-ui/dist/El-Tool/El-Tool.js @70
70: function(__fusereq, exports, module){
"use strict";
Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.convertSingleInnardToNodes = exports.convertInnardsToRenders = exports.convertInnardsToElements = exports.append = exports.setInnards = exports.applyProps = exports.standardizeInput = exports.el = exports.isProps = exports.isRawHTML = exports.isTextNode = exports.isHTMLElement = exports.isNode = exports.onTouch = exports.onToucherEnterExit = exports.onToucherMove = exports.setStyle = exports.removeChildren = exports.ddxClass = exports.haveClass = exports.addClass = exports.classSplice = exports.onDomReady = exports.on = exports.byId = void 0;
const Derivations_1 = __fusereq(44);
const HeliumBase_1 = __fusereq(46);
function byId(id) {
  return document.getElementById(id);
}
exports.byId = byId;
function on(eventName, ref, cb) {
  ref.addEventListener(eventName, cb);
}
exports.on = on;
function onDomReady(cb) {
  if (domIsReady) {
    cb();
    return;
  }
  window.addEventListener("DOMContentLoaded", cb);
}
exports.onDomReady = onDomReady;
let domIsReady = false;
typeof window !== "undefined" && onDomReady(() => domIsReady = true);
function classSplice(element, removeClasses, addClasses) {
  if (removeClasses) {
    const remList = Array.isArray(removeClasses) ? removeClasses : removeClasses.split(/\s+/g);
    remList.filter(it => !!it).forEach(className => element.classList.remove(className));
  }
  if (addClasses) {
    const addList = Array.isArray(addClasses) ? addClasses : addClasses.split(/\s+/g);
    addList.filter(it => !!it).forEach(className => element.classList.add(className));
  }
  return element;
}
exports.classSplice = classSplice;
function addClass(addClasses, element) {
  return classSplice(element, null, addClasses);
}
exports.addClass = addClass;
function haveClass(element, className, addNotRemove) {
  return addNotRemove ? classSplice(element, null, className) : classSplice(element, className, undefined);
}
exports.haveClass = haveClass;
function ddxClass(target, ddx) {
  let lastClassList;
  Derivations_1.anchorDeriverToDomNode(target, () => {
    const removeUs = lastClassList;
    classSplice(target, removeUs, lastClassList = ddx());
  }, "props.ddxClass").run();
}
exports.ddxClass = ddxClass;
function removeChildren(parent) {
  let child = parent.firstChild;
  while (!!child) {
    child.remove();
    child = parent.firstChild;
  }
  return parent;
}
exports.removeChildren = removeChildren;
function setStyle(root, style) {
  if (typeof style === "string") {
    root.style.cssText = style;
  } else {
    Object.keys(style).forEach(styleName => root.style[styleName] = style[styleName]);
  }
  return root;
}
exports.setStyle = setStyle;
function onToucherMove(root, cb) {
  const getPosFromClientXY = (x, y) => {
    const clientRect = root.getBoundingClientRect();
    return {
      left: x - clientRect.left,
      top: y - clientRect.top
    };
  };
  root.addEventListener("mousemove", ev => {
    const toucherMoveEvent = ev;
    toucherMoveEvent.positions = [getPosFromClientXY(ev.clientX, ev.clientY)];
    cb(toucherMoveEvent);
  });
  root.addEventListener("touchmove", ev => {
    const toucherMoveEvent = ev;
    const clientRect = root.getBoundingClientRect();
    toucherMoveEvent.positions = Array.from(ev.touches).map(touch => getPosFromClientXY(touch.clientX, touch.clientY));
    cb(toucherMoveEvent);
    ev.preventDefault();
  });
}
exports.onToucherMove = onToucherMove;
function onToucherEnterExit(root, cb) {
  const modEvent = (ev, isEnterEvent) => {
    ev.isEnterEvent = isEnterEvent;
    return ev;
  };
  root.addEventListener("mouseenter", ev => cb(modEvent(ev, true)));
  root.addEventListener("mouseleave", ev => cb(modEvent(ev, false)));
  root.addEventListener("touchstart", ev => {
    cb(modEvent(ev, true));
    ev.preventDefault();
  });
  root.addEventListener("touchend", ev => {
    cb(modEvent(ev, false));
    ev.preventDefault();
  });
}
exports.onToucherEnterExit = onToucherEnterExit;
function onTouch(root, cb) {
  root.addEventListener("mouseover", event => {
    if (event.buttons == 1 || event.buttons == 3) {
      cb(event);
    }
  });
}
exports.onTouch = onTouch;
function isNode(checkMe) {
  return checkMe instanceof Node;
}
exports.isNode = isNode;
function isHTMLElement(checkMe) {
  return ("nodeName" in checkMe);
}
exports.isHTMLElement = isHTMLElement;
function isTextNode(checkMe) {
  return isNode(checkMe) && checkMe.nodeType === checkMe.TEXT_NODE;
}
exports.isTextNode = isTextNode;
function isRawHTML(checkMe) {
  return checkMe && typeof checkMe === "object" && checkMe.hasOwnProperty("hackableHTML");
}
exports.isRawHTML = isRawHTML;
function isProps(checkMe) {
  return checkMe && typeof checkMe === "object" && !Array.isArray(checkMe) && !isNode(checkMe) && !isRawHTML(checkMe);
}
exports.isProps = isProps;
function el(tagName, propsOrInnards, innards) {
  const input = standardizeInput(null, propsOrInnards, innards);
  return __createEl(tagName, input.props, input.innards);
}
exports.el = el;
function standardizeInput(nameOrPropsOrInnards, propsOrInnards, maybeInnards, isInnardsRazor) {
  let innards = maybeInnards;
  let props;
  let name;
  if (nameOrPropsOrInnards) {
    if (typeof nameOrPropsOrInnards === "string") {
      name = nameOrPropsOrInnards;
    } else {
      if (maybeInnards !== undefined) {
        throw new Error("Can not define first and third argument if first is not a string");
      }
      maybeInnards = propsOrInnards;
      propsOrInnards = nameOrPropsOrInnards;
    }
  }
  if (propsOrInnards) {
    const isInnards = isInnardsRazor ? isInnardsRazor(propsOrInnards) : !isProps(propsOrInnards);
    if (isInnards) {
      if (maybeInnards !== undefined) {
        throw new Error("Can not define third argument if second is innards");
      }
      innards = propsOrInnards;
    } else {
      props = propsOrInnards;
      innards = maybeInnards;
    }
  }
  props = props || ({});
  if (name) {
    props.class = props.class || "";
    props.class += (props.class ? " " : "") + name;
  }
  return {
    props,
    innards
  };
}
exports.standardizeInput = standardizeInput;
function applyProps(props, out, innardsCb) {
  if (props) {
    if (props.on) {
      const eventListeners = props.on || ({});
      Object.keys(eventListeners).forEach(eventType => out.addEventListener(eventType, eventListeners[eventType]));
    }
    if (props.onClick) {
      out.addEventListener("click", props.onClick);
    }
    if (props.onKeyDown) {
      out.addEventListener("keydown", props.onKeyDown);
    }
    if (props.onMouseDown) {
      out.addEventListener("mousedown", props.onMouseDown);
    }
    if (props.onPush) {
      out.classList.add("onPush");
      out.addEventListener("click", props.onPush);
    }
    if (props.onTouch) {
      onTouch(out, props.onTouch);
    }
    if (props.onToucherMove) {
      onToucherMove(out, props.onToucherMove);
    }
    if (props.onToucherEnterExit) {
      onToucherEnterExit(out, props.onToucherEnterExit);
    }
    if (props.onHoverChange) {
      out.addEventListener("mouseover", ev => props.onHoverChange(true, ev));
      out.addEventListener("mouseout", ev => props.onHoverChange(false, ev));
    }
    if (props.title) {
      out.title = props.title;
    }
    if (props.attr) {
      const attrs = props.attr || ({});
      Object.keys(attrs).forEach(attr => out.setAttribute(attr, attrs[attr]));
    }
    if (props.this) {
      const thisProps = props.this || ({});
      Object.keys(thisProps).forEach(prop => out[prop] = thisProps[prop]);
    }
    if (props.class) {
      addClass(props.class, out);
    }
    if (props.id) {
      out.id = props.id;
    }
    if (props.style) {
      setStyle(out, props.style);
    }
    if (props.innards !== undefined) {
      append(out, props.innards);
    }
  }
  if (innardsCb !== undefined) {
    innardsCb(out);
  }
  if (props) {
    if (props.ref) {
      props.ref(out);
    }
    if (props.ddxClass) {
      ddxClass(out, props.ddxClass);
    }
    if (props.ddx) {
      const keep = {};
      Derivations_1.anchorDeriverToDomNode(out, () => props.ddx(out, keep), "props.ddx").run();
    }
  }
  return out;
}
exports.applyProps = applyProps;
function __createEl(tagName, props, innards) {
  const out = document.createElement(tagName);
  applyProps(props, out, () => {
    if (innards !== undefined) {
      append(out, innards);
    }
  });
  if (HeliumBase_1.isHeliumHazed(out)) {
    out._helium.frozen = true;
  }
  return out;
}
let innardsWarningGiven;
function setInnards(root, innards) {
  if (root === document.body && !window._heliumNoWarnSetInnardsDocBody) {
    console.warn([`Suggestion: avoid using setInnards() on document body, and instead use append().`, `If you do not, anything added to the page outside of your root render is unsafe.  To disable this warning,`, `set window._heliumNoWarnSetInnardsDocBody = true`].join(" "));
  }
  removeChildren(root);
  append(root, innards);
}
exports.setInnards = setInnards;
function append(root, innards, before) {
  const addUs = convertInnardsToElements(innards);
  if (before) {
    addUs.forEach(node => {
      root.insertBefore(node, before);
    });
  } else {
    addUs.forEach(node => {
      root.appendChild(node);
    });
  }
  return root;
}
exports.append = append;
function convertInnardsToElements(innards) {
  const renders = convertInnardsToRenders(innards);
  const out = [];
  renders.forEach(render => {
    if (Array.isArray(render)) {
      render.forEach(node => out.push(node));
    } else {
      out.push(render);
    }
  });
  return out;
}
exports.convertInnardsToElements = convertInnardsToElements;
function convertInnardsToRenders(innards) {
  if (innards === undefined || innards === null) {
    return [];
  }
  if (typeof innards === "function") {
    const refresher = new Derivations_1.RerenderReplacer(innards);
    return refresher.getRender();
  }
  let childList = Array.isArray(innards) ? innards : [innards];
  return childList.flat().map(child => convertSingleInnardToNodes(child));
}
exports.convertInnardsToRenders = convertInnardsToRenders;
function convertSingleInnardToNodes(child) {
  if (typeof child === "function") {
    const replacer = new Derivations_1.RerenderReplacer(child);
    return replacer.getRender();
  } else {
    let out = [];
    if (child === undefined || child === null) {
      return [];
    }
    if (typeof child === "number") {
      child = child + "";
    }
    if (isRawHTML(child)) {
      const template = document.createElement('div');
      template.innerHTML = child.hackableHTML;
      template.getRootNode();
      Array.from(template.childNodes).forEach(child => out.push(child));
    } else if (typeof child === "string") {
      out.push(new Text(child));
    } else if (child) {
      if (Array.isArray(child)) {
        out = out.concat(convertSingleInnardToNodes(child));
      } else {
        out.push(child);
      }
    }
    return out;
  }
}
exports.convertSingleInnardToNodes = convertSingleInnardToNodes;

},

// node_modules/helium-ui-tweener/node_modules/helium-ui/dist/El-Tool/Factories.js @71
71: function(__fusereq, exports, module){
"use strict";
Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.h6 = exports.h5 = exports.h4 = exports.h3 = exports.h2 = exports.h1 = exports.br = exports.hr = exports.bold = exports.italic = exports.inputTextbox = exports.select = exports.button = exports.img = exports.td = exports.tr = exports.table = exports.li = exports.ol = exports.ul = exports.pre = exports.span = exports.div = exports._simpleFactory = void 0;
const El_Tool_1 = __fusereq(70);
const helium_sdx_1 = __fusereq(17);
function _simpleFactory(tagName, [arg1, arg2, arg3]) {
  const {props, innards} = El_Tool_1.standardizeInput(arg1, arg2, arg3);
  return El_Tool_1.el(tagName, props, innards);
}
exports._simpleFactory = _simpleFactory;
exports.div = function (...args) {
  return _simpleFactory("div", args);
};
exports.span = function (...args) {
  return _simpleFactory("span", args);
};
exports.pre = function (...args) {
  return _simpleFactory("pre", args);
};
exports.ul = function (...args) {
  return _simpleFactory("ul", args);
};
exports.ol = function (...args) {
  return _simpleFactory("ol", args);
};
exports.li = function (...args) {
  return _simpleFactory("li", args);
};
function table(namePropChild, propChild, child) {
  const {props, innards} = El_Tool_1.standardizeInput(namePropChild, propChild, child);
  const rows = El_Tool_1.convertInnardsToRenders(innards);
  return El_Tool_1.el("table", props, rows.map(row => toTr(row)));
  function toTr(row) {
    if (Array.isArray(row) && row.length === 1) {
      row = row.pop();
    }
    if (El_Tool_1.isHTMLElement(row) && (row.tagName === "TR" || row.nodeName === "#comment")) {
      return row;
    }
    if (typeof row === "function") {
      return () => toTr(row());
    }
    return tr(row);
  }
}
exports.table = table;
function tr(namePropChild, propChild, child) {
  const {props, innards} = El_Tool_1.standardizeInput(namePropChild, propChild, child);
  const cols = El_Tool_1.convertInnardsToRenders(innards);
  return El_Tool_1.el("tr", props, cols.map(data => toTd(data)));
  function toTd(data) {
    if (Array.isArray(data) && data.length === 1) {
      data = data.pop();
    }
    if (El_Tool_1.isHTMLElement(data) && (data.tagName === "TD" || data.nodeName === "#comment")) {
      return data;
    }
    if (typeof data === "function") {
      return () => toTd(data());
    }
    return El_Tool_1.el("td", data);
  }
}
exports.tr = tr;
function td(...[classOrPropOrChild, propOrChild, child]) {
  return _simpleFactory("td", [classOrPropOrChild, propOrChild, child]);
}
exports.td = td;
function img(...[classOrPropOrChild, propOrChild, child]) {
  const {props, innards} = El_Tool_1.standardizeInput(classOrPropOrChild, propOrChild, child);
  props.attr = props.attr || ({});
  props.attr.src = props.src;
  return El_Tool_1.el("img", props, innards);
}
exports.img = img;
function button(...[classOrPropOrChild, propOrChild, child]) {
  return _simpleFactory("button", [classOrPropOrChild, propOrChild, child]);
}
exports.button = button;
function select(classNameOrProps, propsOrInnards, maybeInnards) {
  const {props, innards} = El_Tool_1.standardizeInput(classNameOrProps, propsOrInnards, maybeInnards, a => Array.isArray(a));
  props.on = props.on || ({});
  let currentVal;
  if (props.onInput) {
    const {onInput} = props;
    const onInputListener = props.on.input;
    props.on.input = ev => {
      const newVal = out.value;
      if (newVal !== currentVal) {
        currentVal = newVal;
        onInput(newVal);
      }
      return !onInputListener || onInputListener(ev);
    };
  }
  const out = El_Tool_1.el("select", props, innards.map(innard => {
    let option;
    if (innard instanceof HTMLElement) {
      option = innard;
    } else if (typeof innard === "function") {
      option = El_Tool_1.el("option", {
        ddx: ref => {
          ref.innerText = String(innard());
          if (ref.selected) {}
        }
      });
    } else {
      option = El_Tool_1.el("option", String(innard));
    }
    if (option.selected) {
      currentVal = option.value;
    }
    return option;
  }));
  return out;
}
exports.select = select;
function inputTextbox(classNameOrProps, propsOrInnards, maybeInnards) {
  const {props, innards} = El_Tool_1.standardizeInput(classNameOrProps, propsOrInnards, maybeInnards, a => a instanceof helium_sdx_1.Source);
  props.on = props.on || ({});
  if (innards instanceof helium_sdx_1.Source) {
    const onInputArg = props.onInput;
    props.onInput = value => {
      innards.set(value);
      onInputArg && onInputArg(value);
    };
  }
  if (props.onInput) {
    const {onInput} = props;
    const onInputListener = props.on.input;
    let oldVal = innards;
    props.on.input = ev => {
      const newVal = out.value;
      if (newVal !== oldVal) {
        oldVal = newVal;
        onInput(newVal);
      }
      return !onInputListener || onInputListener(ev);
    };
  }
  if (props.onValueEntered) {
    const onChange = props.on.change;
    const {onValueEntered} = props;
    props.on.change = ev => {
      onValueEntered(ev.target.value);
      return !onChange || onChange(ev);
    };
  }
  const out = El_Tool_1.el("input", props);
  out.type = props.type || "text";
  if (innards) {
    if (typeof innards === "string") {
      out.value = innards;
    } else if (innards instanceof helium_sdx_1.Source) {
      helium_sdx_1.derive(() => out.value = innards.get());
    } else {
      helium_sdx_1.derive(() => out.value = innards());
    }
  }
  return out;
}
exports.inputTextbox = inputTextbox;
exports.italic = function (...args) {
  return _simpleFactory("i", args);
};
exports.bold = function (...args) {
  return _simpleFactory("b", args);
};
exports.hr = function (...args) {
  return _simpleFactory("hr", args);
};
function br(size, props = {}) {
  if (!size) {
    return El_Tool_1.el("br", props);
  }
  props.style = {
    display: "block",
    height: `${size}em`
  };
  return El_Tool_1.el("em-br", props);
}
exports.br = br;
exports.h1 = function (...args) {
  return _simpleFactory("h1", args);
};
exports.h2 = function (...args) {
  return _simpleFactory("h2", args);
};
exports.h3 = function (...args) {
  return _simpleFactory("h3", args);
};
exports.h4 = function (...args) {
  return _simpleFactory("h4", args);
};
exports.h5 = function (...args) {
  return _simpleFactory("h5", args);
};
exports.h6 = function (...args) {
  return _simpleFactory("h6", args);
};

},

// node_modules/helium-ui-tweener/node_modules/helium-ui/dist/El-Tool/Navigation.js @72
72: function(__fusereq, exports, module){
"use strict";
Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.replaceUrl = exports.followUrl = exports.observeUrl = exports.onUrlChange = exports.UrlState = exports._UrlState = exports.NavigationEvent = exports.extLink = exports.a = exports.anchor = void 0;
const El_Tool_1 = __fusereq(70);
const Derivations_1 = __fusereq(44);
const helium_sdx_1 = __fusereq(17);
const Sourcify_1 = __fusereq(45);
function anchor(...[classOrProp, propOrChild, child]) {
  const {props, innards} = El_Tool_1.standardizeInput(classOrProp, propOrChild, child);
  props.attr = props.attr || ({});
  const {href} = props;
  if (!href) {
    props.attr.href = "#";
  } else if (typeof href === "string") {
    props.attr.href = href;
  }
  if (props.target) {
    props.attr.target = props.target;
  }
  const ref = El_Tool_1.el("a", props, innards);
  if (typeof href === "function") {
    Derivations_1.anchorDeriverToDomNode(ref, () => {
      ref.href = href();
    });
  }
  if (props.allowTextSelect) {
    El_Tool_1.setStyle(ref, {
      "-webkit-user-drag": "none"
    });
    let selectedText = "";
    El_Tool_1.on("mousedown", ref, ev => selectedText = window.getSelection().toString());
    El_Tool_1.on("click", ref, ev => {
      if (selectedText !== window.getSelection().toString()) {
        ev.preventDefault();
      }
    });
  }
  El_Tool_1.on("click", ref, ev => {
    if (ref.href) {
      if (props.disableHref) {
        return ev.preventDefault();
      }
      if (ev.metaKey || ref.target === "_blank") {
        window.open(ref.href, '_blank');
        return ev.preventDefault();
      } else if (exports.UrlState._updateUrl({
        urlPart: ref.href,
        mode: "push",
        linkElementClickEvent: true
      }) !== "PASS_THROUGH") {
        return ev.preventDefault();
      }
    }
  });
  return ref;
}
exports.anchor = anchor;
function a(...args) {
  console.warn(`helium function "a" has been deprecated.  Please use "anchor" instead.`);
  return anchor(...args);
}
exports.a = a;
function extLink(...[classOrProp, propOrChild, child]) {
  const {props, innards} = El_Tool_1.standardizeInput(classOrProp, propOrChild, child);
  props.target = "_blank";
  return a(props, innards);
}
exports.extLink = extLink;
class NavigationEvent extends Event {
  constructor(name, props, newUrl) {
    super(name, props);
    this.pageRefreshCancelled = false;
    this.hashFocusCancelled = false;
    this.preventPageRefresh = () => {
      this.pageRefreshCancelled = true;
    };
    this.preventHashFocus = () => {
      this.hashFocusCancelled = true;
    };
    this.newUrl = NavigationEvent.createFullUrl(newUrl);
  }
  static createFullUrl(urlPart) {
    try {
      return new URL(urlPart);
    } catch (err) {
      return new URL(urlPart, location.origin);
    }
  }
}
exports.NavigationEvent = NavigationEvent;
function dispatchNavEvent(name, newUrl) {
  const ev = new NavigationEvent(name, {
    cancelable: true
  }, newUrl);
  return window.dispatchEvent(ev) && ev;
}
function dispatchUrlUpdate(firstEvent, newUrl) {
  return dispatchNavEvent(firstEvent, newUrl) && dispatchNavEvent("locationchange", newUrl);
}
class _UrlState {
  constructor() {
    this.state = Sourcify_1.Sourcify({
      url: new URL(location.href),
      urlParams: {},
      hash: ""
    });
    this.initListeners();
  }
  getUrl() {
    return this.state.url;
  }
  get href() {
    return this.getUrl().href;
  }
  get host() {
    return this.getUrl().host;
  }
  get hostname() {
    return this.getUrl().hostname;
  }
  get pathname() {
    return this.getUrl().pathname;
  }
  get hash() {
    return this.state.hash;
  }
  get urlParams() {
    return this.state.urlParams;
  }
  followUrl(newUrl) {
    return this._updateUrl({
      urlPart: newUrl,
      mode: "push"
    });
  }
  replaceUrl(newUrl) {
    return this._updateUrl({
      urlPart: newUrl,
      mode: "replace"
    });
  }
  onUrlChange(cb) {
    window.addEventListener("locationchange", cb);
  }
  observeUrl(cb) {
    this.onUrlChange(cb);
    const ev = new NavigationEvent(name, {
      cancelable: true
    }, window.location.href);
    cb(ev);
  }
  _updateUrl(args) {
    const evResponse = dispatchUrlUpdate("followurl", args.urlPart);
    if (!evResponse) {
      return "BLOCKED";
    }
    if (evResponse.pageRefreshCancelled) {
      if (evResponse.newUrl.href !== location.href) {
        if (location.host !== evResponse.newUrl.host) {
          window.location.href = evResponse.newUrl.href;
          return;
        } else {
          if (args.mode === "push") {
            history.pushState(null, "", args.urlPart);
          } else if (args.mode === "replace") {
            history.replaceState(null, "", args.urlPart);
          }
          this.state.set("url", new URL(args.urlPart));
        }
      }
      return "CAPTURED";
    }
    if (args.linkElementClickEvent) {
      return "PASS_THROUGH";
    } else {
      window.location.href = NavigationEvent.createFullUrl(args.urlPart).href;
    }
  }
  initListeners() {
    window.addEventListener('popstate', ev => {
      dispatchNavEvent("locationchange", location.href);
    });
    setTimeout(() => {
      El_Tool_1.onDomReady(() => {
        setTimeout(() => {
          const hash = location.hash;
          const target = El_Tool_1.byId(hash.slice(1));
          if (!!target) {
            target.scrollIntoView({
              behavior: "auto"
            });
          }
        }, 200);
      });
    }, 0);
    let lastHash;
    this.onUrlChange(ev => {
      if (ev.hashFocusCancelled !== true && ev.newUrl.hash !== lastHash) {
        lastHash = ev.newUrl.hash;
        requestAnimationFrame(() => requestAnimationFrame(() => {
          const target = El_Tool_1.byId(lastHash.slice(1));
          if (!!target) {
            target.scrollIntoView({
              behavior: "auto"
            });
          }
        }));
      }
    });
    helium_sdx_1.derive({
      anchor: "NO_ANCHOR",
      fn: () => {
        const url = this.getUrl();
        const urlParams = new URLSearchParams(url.search);
        urlParams.forEach((value, key) => {
          this.state.urlParams[key] = value;
        });
        this.state.hash = url.hash;
      }
    });
  }
}
exports._UrlState = _UrlState;
exports.UrlState = new _UrlState();
exports.onUrlChange = exports.UrlState.onUrlChange.bind(exports.UrlState);
exports.observeUrl = exports.UrlState.observeUrl.bind(exports.UrlState);
exports.followUrl = exports.UrlState.followUrl.bind(exports.UrlState);
exports.replaceUrl = exports.UrlState.replaceUrl.bind(exports.UrlState);

},

// node_modules/helium-ui-tweener/node_modules/helium-ui/dist/Augments/OnIntersect.js @73
73: function(__fusereq, exports, module){
"use strict";
Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.onIntersect = exports.addIntersectionObserver = void 0;
const Factories_1 = __fusereq(71);
const El_Tool_1 = __fusereq(70);
let styleEl;
function assertStyling() {
  if (styleEl) {
    return;
  }
  styleEl = El_Tool_1.el("style");
  styleEl.innerHTML = `
    .hintRelative {
      position: relative;
    }

    .CoverCheckerPositioner {
      position: absolute;
      top: 0px;
      left: 0px;
      right: 0px;
      bottom: 0px;
      pointer-events: none;
      visibility: hidden;
    }
    
    .CoverChecker {
      position: sticky;
      height: 100vh;
      width: 100vw;
      top: 0px; 
    }
  `;
  document.head.prepend(styleEl);
}
function addIntersectionObserver(nodes, init, cb) {
  let thresholds;
  const isObject = init => {
    return init && typeof init === "object" && !Array.isArray(init);
  };
  if (isObject(init)) {
    thresholds = init.threshold;
  }
  if (!Array.isArray(thresholds)) {
    thresholds = [thresholds];
  }
  let startLength = thresholds.length;
  const normalThresholds = thresholds.map(it => {
    switch (it) {
      case "contained":
      case "maxFill":
        return 1;
      case "any":
        return 0;
    }
    return it;
  }).filter(it => typeof it === "number");
  const needsCoverMod = startLength > normalThresholds.length;
  if (!Array.isArray(nodes)) {
    nodes = [nodes];
  }
  if (needsCoverMod) {
    assertStyling();
    const checkUs = nodes.map(node => {
      let checkMe;
      const addMe = Factories_1.div("CoverCheckerPositioner", checkMe = Factories_1.div("CoverChecker"));
      node.classList.add("hintRelative");
      node.prepend(addMe);
      return checkMe;
    });
    const watcher = new IntersectionObserver(cb, {
      threshold: 1
    });
    checkUs.forEach(node => watcher.observe(node));
  }
  if (normalThresholds.length === 0) {
    return;
  }
  const options = isObject(init) ? init : {};
  options.threshold = normalThresholds;
  const watcher = new IntersectionObserver(cb, options);
  setTimeout(() => {
    nodes.forEach(node => watcher.observe(node));
  }, 0);
}
exports.addIntersectionObserver = addIntersectionObserver;
function onIntersect(init, cb) {
  return ref => addIntersectionObserver(ref, init, (entries, observer) => {
    cb(ref, entries[0], observer);
  });
}
exports.onIntersect = onIntersect;

},

// node_modules/helium-ui-tweener/node_modules/helium-ui/dist/Augments/BlockEvent.js @74
74: function(__fusereq, exports, module){
"use strict";
Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.blockEvent = void 0;
function blockEvent(eventName) {
  return ref => ref.addEventListener(eventName, ev => {
    ev.preventDefault();
    return false;
  });
}
exports.blockEvent = blockEvent;

},

// node_modules/helium-sdx/dist/Derivations/Derivers/DeriverSwitch.js @75
75: function(__fusereq, exports, module){
"use strict";
Object.defineProperty(exports, "__esModule", {
  value: true
});
const DerivationManager_1 = __fusereq(56);
function deriverSwitch(args) {
  const {watch, responses} = args;
  const noWay = responses.find(it => !(("match" in it) || ("test" in it)));
  if (!!noWay) {
    console.error(noWay);
    throw new Error(`Response has no ability to match item.  Perhaps use "DEFAULT_RESPONSE" as match field.`);
  }
  responses.sort((a, b) => {
    const aIsDefault = a.test === "DEFAULT_RESPONSE";
    const bIsDefault = b.test === "DEFAULT_RESPONSE";
    if (aIsDefault && !bIsDefault) {
      return 1;
    }
    if (!aIsDefault && bIsDefault) {
      return -1;
    }
    return 0;
  });
  let lastResponse;
  DerivationManager_1.derive(() => {
    let value;
    if (typeof watch === "function") {
      value = watch();
    } else {
      value = watch.get();
    }
    DerivationManager_1.noDeps(async () => {
      for (const resp of responses) {
        const {match, test} = resp;
        let tryMe;
        if (test === "DEFAULT_RESPONSE") {
          tryMe = resp;
        } else if (value === match) {
          tryMe = resp;
        } else if (test instanceof RegExp) {
          if (test.test(String(value))) {
            tryMe = resp;
          }
        } else if (typeof test === "function") {
          if (await test(value)) {
            tryMe = resp;
          }
        }
        if (!!tryMe) {
          if (tryMe === lastResponse && !args.retriggerSameResponse) {
            return;
          }
          const attempt = await tryMe.action(value);
          if (attempt === "NO_SWITCH_NEEDED") {
            return;
          }
          if (attempt !== "TRY_NEXT_ACTION") {
            return lastResponse = resp;
          }
        }
      }
    });
  });
}
exports.deriverSwitch = deriverSwitch;

},

// node_modules/helium-sdx/dist/Derivations/Sources/Source.js @76
76: function(__fusereq, exports, module){
"use strict";
Object.defineProperty(exports, "__esModule", {
  value: true
});
const DerivationManager_1 = __fusereq(56);
function source(value) {
  return new Source(value);
}
exports.source = source;
class Source {
  constructor(value, args = {}) {
    this.value = value;
    this.args = args;
    this.sourceId = DerivationManager_1.DerivationManager.createSourceId(args.idAppend);
  }
  get() {
    DerivationManager_1.DerivationManager.sourceRequested(this);
    return this.value;
  }
  peek() {
    return this.value;
  }
  getSourceId() {
    return this.sourceId;
  }
  set(value, forceUpdates = false) {
    const equalValue = !forceUpdates && this.value === value;
    this.value = value;
    this.dispatchUpdateNotifications(equalValue);
  }
  bindTo(bindTo) {
    DerivationManager_1.derive(() => this.set(bindTo.get()));
    DerivationManager_1.derive(() => bindTo.set(this.get()));
  }
  dispatchUpdateNotifications(equalValue = false) {
    DerivationManager_1.DerivationManager.sourceUpdated(this, equalValue);
  }
}
exports.Source = Source;

},

// node_modules/helium-sdx/dist/Derivations/Sources/SourceArray.js @77
77: function(__fusereq, exports, module){
"use strict";
Object.defineProperty(exports, "__esModule", {
  value: true
});
const Source_1 = __fusereq(76);
const SourceMap_1 = __fusereq(78);
const DerivationManager_1 = __fusereq(56);
class SourceArrayBase {
  constructor(array) {
    this.DELETED = {
      DELETED: true
    };
    if (array) {
      this._array = array.map(value => new Source_1.Source(value, {
        idAppend: "aval"
      }));
      this._length = new Source_1.Source(array.length, {
        idAppend: "alen"
      });
    } else {
      this._array = [];
      this._length = new Source_1.Source(0, {
        idAppend: "alen"
      });
    }
  }
  get length() {
    return this._length.get();
  }
  delete(index) {
    this.getSource(index).set(this.DELETED);
  }
  forEachPart(cb) {
    for (let i = 0; i < this.length; i++) {
      cb(this.getPart(i), i);
    }
  }
  getPart(index) {
    return this.noDeletedConstant(this.getSource(index).get());
  }
  noDeletedConstant(out) {
    return out === this.DELETED ? undefined : out;
  }
  getSource(index, noInit = false) {
    if (!this._array[index] && !noInit) {
      this._array[index] = new Source_1.Source(this.DELETED, {
        idAppend: "aval"
      });
    }
    return this._array[index];
  }
  peek(index) {
    if (typeof index === "number" && this._array[index]) {
      return this.noDeletedConstant(this._array[index].peek());
    }
  }
  peekHas(index) {
    return index < this.peekLength() && this._array[index] && this._array[index].peek() !== this.DELETED;
  }
  peekLength() {
    return this._length.peek();
  }
  pop() {
    const index = this.peekLength() - 1;
    const value = this._array.pop().get();
    this._length.set(index);
    this.syncIndex(index);
    return this.noDeletedConstant(value);
  }
  push(value) {
    const index = this.peekLength();
    this.set(index, value);
    this._length.set(index + 1);
    this.syncIndex(index);
  }
  remove(item, limit = -1) {
    let count = 0;
    const length = this._length.peek();
    for (let i = 0; i < length - count; i++) {
      if (limit > -1 && count >= limit) {
        break;
      }
      if (this.peek(i) === item) {
        this._array.splice(i, 1);
        i--;
        count++;
      }
    }
    this._length.set(length - count);
    this.onArrangementUpdate();
  }
  set(index, value, forceUpdates) {
    if (!this._array[index]) {
      this._array[index] = new Source_1.Source(value, {
        idAppend: "aval"
      });
    } else {
      this._array[index].set(value, forceUpdates);
    }
    if (index + 1 > this.peekLength()) {
      this._length.set(index + 1);
      this.syncIndex(index);
    }
  }
  shift() {
    const length = this.peekLength();
    if (!length) {
      return undefined;
    }
    const out = this._array.shift();
    this._length.set(length - 1);
    this.onArrangementUpdate();
    return this.noDeletedConstant(out.get());
  }
  partsSlice(start, end) {
    return this._array.slice(start, end).map(it => this.noDeletedConstant(it.get()));
  }
  toObjectNoDeps() {
    return this._array.map(item => item.peek());
  }
  unshift(value) {
    this._array.unshift(new Source_1.Source(value, {
      idAppend: "aval"
    }));
    this._length.set(this.peekLength() + 1);
    this.onArrangementUpdate();
  }
  onArrangementUpdate() {}
  syncIndex(index) {}
}
exports.SourceArrayBase = SourceArrayBase;
class SourceArray extends SourceArrayBase {
  constructor() {
    super(...arguments);
    this.lastUpdate = new Source_1.Source(undefined, {
      idAppend: "aupt"
    });
    this.anchorDeriver = DerivationManager_1.DerivationManager._getCurrentDeriver();
  }
  lastIndexOf() {
    throw new Error("TODO");
  }
  reduceRight() {
    throw new Error("TODO");
  }
  flatMap() {
    throw new Error("TODO");
  }
  concat(items) {
    return this.toObject().concat(items);
  }
  copyWithin(target, start, end) {
    const length = this.peekLength();
    end = end !== undefined ? end : length;
    let copyIndex = start || 0;
    for (let i = target || 0; i < length && copyIndex < end; (i++, copyIndex++)) {
      this.set(i, this.peek(copyIndex));
    }
    return this;
  }
  every(cb) {
    const length = this.peekLength();
    for (let i = 0; i < length; i++) {
      if (cb(this.getPart(i)) !== true) {
        return false;
      }
    }
    return true;
  }
  clear() {
    while (this._array.length) {
      this._array.pop();
      this.syncIndex(this._array.length);
    }
    this._length.set(0);
    return this;
  }
  entries() {
    const out = [];
    this.forEach((item, index) => out.push([index, item]));
    return out;
  }
  fill(value, start, end) {
    const length = this.peekLength();
    for (let i = start || 0; i < end || length; i++) {
      this.set(i, value);
    }
  }
  filter(cb) {
    const out = [];
    this.forEach(item => {
      if (cb(item)) {
        out.push(item);
      }
    });
    return out;
  }
  find(cb) {
    const index = this.findIndex(cb);
    return index === -1 ? undefined : this.getPart(index);
  }
  findIndex(cb) {
    this.orderMatters();
    const length = this.peekLength();
    for (let i = 0; i < length; i++) {
      const value = this.getPart(i);
      if (cb(value)) {
        return i;
      }
    }
    return -1;
  }
  flat(depth) {
    this.orderMatters();
    return this.toObject().flat(depth);
  }
  forEach(cb) {
    this.orderMatters();
    return this.forEachPart(cb);
  }
  has(index) {
    const indexSrc = this._getIndexObserver(index).get();
    const value = indexSrc.get();
    return value !== this.DELETED;
  }
  iterator() {
    const self = this;
    void this.length;
    return iterate();
    function* iterate() {
      for (let i = 0; i < self.length; i++) {
        yield self.get(i);
      }
    }
  }
  get(index) {
    return this.noDeletedConstant(this._getIndexObserver(index).get().get());
  }
  includes(item, fromIndex) {
    if (fromIndex !== undefined) {
      throw new Error("TODO: implement fromIndex");
    }
    return this.indexOf(item) !== -1;
  }
  indexOf(item) {
    this.orderMatters();
    const length = this.peekLength();
    for (let i = 0; i < length; i++) {
      if (this.getPart(i) === item) {
        return i;
      }
    }
    this.lengthMatters();
    return -1;
  }
  join(separator) {
    this.orderMatters();
    this.lengthMatters();
    return this._array.map(it => it.get()).join(separator);
  }
  keys() {
    const out = [];
    const length = this.length;
    for (let i = 0; i < length; i++) {
      let item = this._getIndexObserver(i);
      if (item.get() && item.get().get() !== this.DELETED) {
        out.push(i);
      }
    }
    return out;
  }
  map(cb) {
    const out = [];
    this.forEach(item => out.push(cb(item)));
    return out;
  }
  reduce(cb) {
    let out;
    this.forEach((item, index) => {
      if (index === 0) {
        return out = item;
      }
      out = cb(out, item);
    });
    return out;
  }
  reverse() {
    const activeValues = this._array.splice(0, this._length.peek());
    this._array.splice(0, 0, ...activeValues.reverse());
    this.onArrangementUpdate();
    return this;
  }
  slice(start, end) {
    const length = this.peekLength();
    const out = [];
    for (let i = start; end !== undefined && i < end && i < length; i++) {
      out.push(this.get(i));
    }
    return out;
  }
  some(cb) {
    return this.findIndex(cb) !== -1;
  }
  sort(cb) {
    cb = cb || ((l, r) => {
      if (l < r) {
        return -1;
      }
      if (l > r) {
        return 1;
      }
      return 0;
    });
    const activeValues = this._array.splice(0, this._length.peek());
    this._array.splice(0, 0, ...activeValues.sort((a, b) => cb(a.peek(), b.peek())));
    this.onArrangementUpdate();
    return this;
  }
  splice(start, deleteCount, ...items) {
    if (start < 0) {
      throw new Error("TODO");
    }
    const length = this._length.peek();
    if (deleteCount === undefined) {
      deleteCount = length - start;
    }
    start = Math.min(start, length);
    const numDeleted = Math.min(length - start, deleteCount);
    const numAdded = items.length;
    const out = this._array.splice(start, deleteCount, ...items.map(value => new Source_1.Source(value)));
    this._length.set(Math.max(start, length) + (numAdded - numDeleted));
    this.onArrangementUpdate();
    return out.map(it => it.peek());
  }
  toObject() {
    this.orderMatters();
    const out = [];
    this.forEach(item => out.push(item));
    return out;
  }
  values() {
    return this.toObject();
  }
  onArrangementUpdate() {
    this.lastUpdate.set(Date.now());
    if (this.indexMap) {
      DerivationManager_1.noDeps(() => Array.from(this.indexMap._static().keys()).forEach(key => this.indexMap.set(key, this.getSource(key))));
    }
  }
  syncIndex(index) {
    const indexedSrc = this.indexMap && this.indexMap.peek(this._array.length);
    if (indexedSrc) {
      if (this.peekLength() <= index) {
        indexedSrc.set(this.DELETED);
      } else {
        indexedSrc.set(this.peek(index));
      }
    }
  }
  _getIndexObserver(index) {
    if (!this.indexMap) {
      this.indexMap = new SourceMap_1.SourceMapBase(null, {
        id: "ai"
      });
      DerivationManager_1.derive({
        fn: () => {
          this.orderMatters();
          Array.from(this.indexMap._static().keys()).forEach(key => this.indexMap.set(key, this.getSource(key)));
        },
        anchor: this.anchorDeriver
      });
    }
    if (!this.indexMap._static().has(index)) {
      DerivationManager_1.noDeps(() => this.indexMap.set(index, this.getSource(index)));
    }
    return this.indexMap.getSource(index);
  }
  orderMatters() {
    void this.lastUpdate.get();
  }
  lengthMatters() {
    void this.length;
  }
}
exports.SourceArray = SourceArray;

},

// node_modules/helium-sdx/dist/Derivations/Sources/SourceMap.js @78
78: function(__fusereq, exports, module){
"use strict";
Object.defineProperty(exports, "__esModule", {
  value: true
});
const Source_1 = __fusereq(76);
const DerivationManager_1 = __fusereq(56);
class SourceMapBase {
  constructor(entries, args) {
    this.args = args;
    this._map = new Map();
    if (entries) {
      throw new Error("TODO");
    }
  }
  getSource(key) {
    return this.getSetSource({
      key
    });
  }
  get(key) {
    return this.getSource(key).get();
  }
  set(key, value, forceUpdates) {
    this.getSetSource({
      key,
      value
    });
  }
  _static() {
    return this._map;
  }
  peek(key) {
    if (this._map.has(key)) {
      return this._map.get(key).peek();
    }
  }
  getSetSource(args) {
    const key = args.key;
    const setNotGet = ("value" in args);
    const keyDoesNotExist = this._map.has(key) === false;
    if (keyDoesNotExist) {
      let idAppend = "mval";
      if (this.args && this.args.id) {
        idAppend = `${this.args.id}_${idAppend}`;
      }
      if (typeof key === "string" || typeof key === "number") {
        idAppend += `_${key}`;
      }
      this._map.set(key, new Source_1.Source(args.value, {
        idAppend
      }));
      if (setNotGet) {
        return;
      }
    } else if (setNotGet) {
      this._map.get(key).set(args.value);
      return;
    }
    return this._map.get(key);
  }
}
exports.SourceMapBase = SourceMapBase;
class SourceMap extends SourceMapBase {
  constructor() {
    super(...arguments);
    this._keys = new Source_1.Source([], {
      idAppend: "mapKeys"
    });
    this._has = new SourceMapBase(null, {
      id: "has"
    });
    this._size = new Source_1.Source(0);
  }
  forEach(cb) {
    this.keys().forEach(key => cb(this.get(key), key));
  }
  entries() {
    throw new Error("TODO");
  }
  values() {
    throw new Error("TODO");
  }
  iterator() {
    const self = this;
    return iterate();
    function* iterate() {
      for (const it of self.keys()) {
        yield self.get(it);
      }
    }
  }
  toObject() {
    const out = {};
    this.forEach((value, key) => {
      if (typeof key === "string" || typeof key === "number") {
        out[key] = value;
      }
    });
    return out;
  }
  size() {
    return this._size.get();
  }
  set(key, value, forceUpdates) {
    let keysChanged = false;
    if (!this.peekHas(key)) {
      keysChanged = true;
    }
    this._has.set(key, true);
    super.set(key, value, forceUpdates);
    if (keysChanged) {
      this.updateKeys();
    }
  }
  delete(key) {
    if (!this.peekHas(key)) {
      return;
    }
    this._has.set(key, false);
    super.set(key, undefined);
    this.updateKeys();
  }
  clear() {
    this.keys().forEach(key => this.delete(key));
  }
  getnit(key, value) {
    if (this.peekHas(key)) {
      return this.get(key);
    }
    this.set(key, value);
    return value;
  }
  has(key) {
    return !!this._has.get(key);
  }
  keys() {
    return this._keys.get();
  }
  isEmpty() {
    return !this._keys.get().length;
  }
  updateKeys() {
    DerivationManager_1.noDeps(() => {
      const keys = Array.from(this._map.keys()).filter(key => this.peekHas(key));
      this._keys.set(keys);
      this._size.set(keys.length);
    });
  }
  peekHas(key) {
    return !!this._has.peek(key);
  }
}
exports.SourceMap = SourceMap;

},

// node_modules/helium-sdx/dist/Derivations/Sources/DdxSource.js @79
79: function(__fusereq, exports, module){
"use strict";
Object.defineProperty(exports, "__esModule", {
  value: true
});
const Source_1 = __fusereq(76);
const DerivationManager_1 = __fusereq(56);
class DdxSource {
  constructor(ddx) {
    this.source = new Source_1.Source();
    DerivationManager_1.derive(() => {
      this.source.set(ddx());
    });
  }
  get() {
    return this.source.get();
  }
  peek() {
    return this.source.peek();
  }
  getSource() {
    return this.source;
  }
}
exports.DdxSource = DdxSource;

},

// node_modules/helium-sdx/dist/Derivations/DerivationBatcher.js @80
80: function(__fusereq, exports, module){
"use strict";
Object.defineProperty(exports, "__esModule", {
  value: true
});
const AnotherLogger_1 = __fusereq(40);
class DerivationBatcher {
  constructor(manager) {
    this.manager = manager;
    this.typeToBatchListMap = new Map();
    this.batchTimeouts = new Map();
  }
  addDeriverWithReason(deriver, source) {
    const reasons = this.getDeriverReasons(deriver, true);
    reasons.set(source);
    if (this.manager.isDebugDeriver(deriver)) {
      AnotherLogger_1.aLog("debugDeriver", "DDX: Source dependency updating: ", source.getSourceId());
    }
    this.assertBatchRunning(deriver);
  }
  getDeriverReasons(deriver, force = false) {
    const batchType = deriver.getBatching();
    let batchList = this.typeToBatchListMap.get(batchType);
    if (!batchList) {
      if (!force) {
        return;
      }
      this.typeToBatchListMap.set(batchType, batchList = new Map());
    }
    let reasons = batchList.get(deriver);
    if (!reasons) {
      if (!force) {
        return;
      }
      batchList.set(deriver, reasons = new Map());
    }
    return reasons;
  }
  assertBatchRunning(deriver) {
    const batchType = deriver.getBatching();
    if (batchType === "none") {
      return deriver.runDdx();
    }
    if (this.batchTimeouts.has(batchType)) {
      return;
    }
    const start = timeout => this.batchTimeouts.set(batchType, timeout);
    const end = (() => {
      this.batchTimeouts.delete(batchType);
      this.runBatch(batchType);
    }).bind(this);
    if (typeof batchType === "number") {
      return start(setTimeout(end, batchType));
    }
    switch (batchType) {
      case "singleFrame":
        start(requestAnimationFrame(end));
        return;
      case "doubleFrame":
        start(requestAnimationFrame(() => requestAnimationFrame(end)));
        return;
      case "stack":
        start(setTimeout(end, 0));
        return;
      case "oneSec":
        start(setTimeout(end, 1000));
        return;
      case "fiveSec":
        start(setTimeout(end, 5 * 1000));
        return;
      case "tenSec":
        start(setTimeout(end, 10 * 1000));
        return;
      case "twentySec":
        start(setTimeout(end, 20 * 1000));
        return;
      default:
        throw new Error(`Unknown batchType ${batchType}`);
    }
  }
  runBatch(batchType) {
    AnotherLogger_1.aLog("sourceUpdated", "<Start Source Update Batch>");
    let batchList = this.typeToBatchListMap.get(batchType);
    if (!batchList) {
      return;
    }
    const deriverList = Array.from(batchList.entries()).filter(([deriver, reasons]) => {
      for (const source of reasons.keys()) {
        if (deriver.hasDep(source)) {
          return true;
        }
      }
    }).map(([deriver, reasons]) => {
      if (this.manager.isDebugDeriver(deriver)) {
        AnotherLogger_1.aLog("debugDeriver", `DDX: Deriving for change in source(s): ${Array.from(reasons.keys()).join(", ")}`);
      }
      return deriver;
    });
    batchList.clear();
    deriverList.forEach(deriver => {
      deriver.dropChildren();
    });
    deriverList.forEach(deriver => {
      deriver.runDdx();
    });
  }
}
exports.DerivationBatcher = DerivationBatcher;

},

// node_modules/helium-ui-tweener/node_modules/helium-ui/dist/Derivations/Sources/SourceBase.js @81
81: function(__fusereq, exports, module){
"use strict";
Object.defineProperty(exports, "__esModule", {
  value: true
});

},

// node_modules/helium-ui-tweener/node_modules/helium-ui/dist/Derivations/Sources/SourceArray.js @82
82: function(__fusereq, exports, module){
"use strict";
Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.SourceArray = void 0;
const RerenderReplacer_1 = __fusereq(84);
const helium_sdx_1 = __fusereq(17);
class SourceArray extends helium_sdx_1.SourceArray {
  renderMap(renderFn) {
    return this._mapReplacers(false, renderFn);
  }
  renderMapKeyed(renderFn) {
    return this._mapReplacers(true, renderFn);
  }
  _mapReplacers(withIndex, renderFn) {
    const obsToRerenderMap = new Map();
    const listReplacer = new RerenderReplacer_1.RerenderReplacer(() => {
      this.orderMatters();
      const out = [];
      const length = this.length;
      for (let i = 0; i < length; i++) {
        const observable = withIndex ? this._getIndexObserver(i) : this.getSource(i);
        if (obsToRerenderMap.has(observable) === false) {
          obsToRerenderMap.set(observable, new RerenderReplacer_1.RerenderReplacer(() => {
            const value = observable.get();
            if (withIndex) {
              return renderFn(this.noDeletedConstant(value.get()), i);
            } else {
              return renderFn(this.noDeletedConstant(value));
            }
          }));
        }
        const nodes = obsToRerenderMap.get(observable).getRender();
        nodes.forEach(node => out.push(node));
      }
      return out;
    });
    return listReplacer.getRender();
  }
}
exports.SourceArray = SourceArray;

},

// node_modules/helium-ui-tweener/node_modules/helium-ui/dist/Derivations/Sources/SourceMap.js @83
83: function(__fusereq, exports, module){
"use strict";
Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.SourceMap = void 0;
const RerenderReplacer_1 = __fusereq(84);
const helium_sdx_1 = __fusereq(17);
class SourceMap extends helium_sdx_1.SourceMap {
  renderMap(renderFn) {
    return this._mapReplacers(false, renderFn);
  }
  renderMapKeyed(renderFn) {
    return this._mapReplacers(true, renderFn);
  }
  _mapReplacers(withKey, renderFn) {
    const entryReplacers = new Map();
    const orderer = new RerenderReplacer_1.RerenderReplacer(() => {
      const out = [];
      const keys = this.keys();
      for (const key of keys) {
        if (entryReplacers.has(key) === false) {
          const source = this.getSource(key);
          entryReplacers.set(key, new RerenderReplacer_1.RerenderReplacer(() => renderFn(source.get(), withKey ? key : undefined)));
        }
        const nodeList = entryReplacers.get(key).getRender();
        for (const node of nodeList) {
          out.push(node);
        }
      }
      return out;
    });
    return orderer.getRender();
  }
}
exports.SourceMap = SourceMap;

},

// node_modules/helium-ui-tweener/node_modules/helium-ui/dist/Derivations/Derivers/RerenderReplacer.js @84
84: function(__fusereq, exports, module){
"use strict";
Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.recycle = exports.RenderRecycler = exports.RerenderReplacer = void 0;
const El_Tool_1 = __fusereq(70);
const HeliumBase_1 = __fusereq(46);
const DomAnchoredDeriver_1 = __fusereq(85);
const helium_sdx_1 = __fusereq(17);
const REMOVE_NODE = false;
const ADD_NODE = true;
let RerenderReplacer = (() => {
  class RerenderReplacer {
    constructor(rerenderFn) {
      this.rerenderFn = rerenderFn;
      this.id = "ddx_" + RerenderReplacer.nextId++;
      this.preConstructHook();
      if (helium_sdx_1.DerivationManager.disabled()) {
        this.currentRender = El_Tool_1.convertInnardsToElements(this.rerenderFn());
      } else {
        this.placeHolder = document.createComment(this.id);
        const helium = HeliumBase_1.heliumHaze(this.placeHolder)._helium;
        helium.rerenderReplacer = this;
        helium.isReplacer = true;
        const domAnchorKit = DomAnchoredDeriver_1.anchorDeriverToDomNode(this.placeHolder, this.derive.bind(this), "RerenderReplacer");
        this.getAnchorReport = domAnchorKit.report;
        domAnchorKit.run();
      }
    }
    preConstructHook() {}
    getRender() {
      return this.currentRender;
    }
    derive() {
      const placehold = this.placeHolder;
      const newRender = El_Tool_1.convertInnardsToElements(this.rerenderFn());
      newRender.push(this.placeHolder);
      const oldRender = this.currentRender;
      this.currentRender = newRender;
      const parent = placehold.parentElement;
      if (!parent || !oldRender) {
        return;
      }
      const addRemoveMap = new Map();
      oldRender.forEach(node => addRemoveMap.set(node, REMOVE_NODE));
      newRender.forEach(node => {
        if (addRemoveMap.has(node)) {
          addRemoveMap.delete(node);
        } else {
          addRemoveMap.set(node, ADD_NODE);
        }
      });
      Array.from(addRemoveMap.entries()).forEach(([node, addRemove]) => {
        if (addRemove === REMOVE_NODE && node.parentElement) {
          node.parentElement.removeChild(node);
          if (HeliumBase_1.isHeliumHazed(node) && node._helium.isReplacer) {
            node._helium.rerenderReplacer.removeAll();
          }
        }
      });
      const currentNodes = parent.childNodes;
      let parentIndex = Array.from(currentNodes).indexOf(placehold);
      let lastNode;
      const length = newRender.length;
      for (let i = 0; i < length; i++) {
        const addMe = newRender[length - 1 - i];
        const current = currentNodes[parentIndex - i];
        if (addMe === current) {} else {
          parent.insertBefore(addMe, lastNode);
          if (addRemoveMap.get(addMe) !== ADD_NODE) {
            HeliumBase_1.heliumHaze(addMe)._helium.moveMutation = true;
          } else {
            parentIndex++;
          }
        }
        lastNode = addMe;
      }
    }
    removeAll() {
      this.currentRender.forEach(node => {
        if (node.parentElement) {
          node.parentElement.removeChild(node);
        }
      });
    }
  }
  RerenderReplacer.nextId = 0;
  return RerenderReplacer;
})();
exports.RerenderReplacer = RerenderReplacer;
;
class RenderRecycler extends RerenderReplacer {
  preConstructHook() {
    const renders = [];
    const rootRenderFn = this.rerenderFn;
    this.rerenderFn = () => {
      let cachedRender = renders.find(prevRender => {
        const sourceVals = Array.from(prevRender.sourceVals.entries());
        for (const [source, val] of sourceVals) {
          if (val !== source.peek()) {
            return false;
          }
        }
        for (const [source, val] of sourceVals) {
          source.get();
        }
        return true;
      });
      if (!cachedRender) {
        renders.push(cachedRender = {
          sourceVals: new Map(),
          render: rootRenderFn()
        });
        const {scope} = this.getAnchorReport({
          scope: true
        });
        const sources = scope.getSourceDependencies();
        sources.forEach(source => cachedRender.sourceVals.set(source, source.peek()));
      } else {
        console.log("Reusing cached render!", cachedRender);
      }
      return cachedRender.render;
    };
  }
}
exports.RenderRecycler = RenderRecycler;
function recycle(renderFn) {
  const replacer = new RenderRecycler(renderFn);
  return replacer.getRender();
}
exports.recycle = recycle;

},

// node_modules/helium-ui-tweener/node_modules/helium-ui/dist/Derivations/Derivers/DomAnchoredDeriver.js @85
85: function(__fusereq, exports, module){
"use strict";
Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.anchorDeriverToDomNode = void 0;
const HeliumBase_1 = __fusereq(46);
const helium_sdx_1 = __fusereq(17);
function anchorDeriverToDomNode(node, ddx, type) {
  let scope;
  setupNodeAnchorMutationObserver();
  return {
    run: () => {
      const helium = HeliumBase_1.heliumHaze(node)._helium;
      const anchor = new helium_sdx_1.AnchorNode();
      const deriveFn = () => {
        const onPage = node.getRootNode() === document;
        if (!onPage) {
          HeliumBase_1.changeFreezeState(node, true);
        }
        ddx();
      };
      scope = new helium_sdx_1.DeriverScope({
        fn: deriveFn,
        batching: "singleFrame",
        anchor
      });
      (helium.unfreeze = helium.unfreeze || []).push(() => {
        anchor.setStatus(helium_sdx_1.AnchorStatus.NORMAL);
      });
      (helium.freeze = helium.freeze || []).push(() => {
        anchor.setStatus(helium_sdx_1.AnchorStatus.FROZEN);
      });
      scope.runDdx();
    },
    report: req => ({
      ...req.scope && ({
        scope
      })
    })
  };
}
exports.anchorDeriverToDomNode = anchorDeriverToDomNode;
let mutationsBuffer = [];
let bufferTimeout;
let mutationObserver;
function setupNodeAnchorMutationObserver() {
  if (typeof MutationObserver === "undefined") {
    return;
  }
  if (mutationObserver) {
    return;
  }
  mutationObserver = new MutationObserver(mutationsList => {
    mutationsList.forEach(mutation => {
      if (mutation.type !== 'childList') {
        return;
      }
      mutationsBuffer.push(mutation);
    });
    if (bufferTimeout) {
      clearTimeout(bufferTimeout);
    }
    bufferTimeout = setTimeout(() => {
      bufferTimeout = undefined;
      const oldBuffer = mutationsBuffer;
      mutationsBuffer = [];
      const haveNode = new Map();
      for (const mutation of oldBuffer) {
        mutation.removedNodes.forEach(node => haveNode.set(node, false));
        mutation.addedNodes.forEach(node => haveNode.set(node, true));
      }
      const addedNodes = [];
      const removedNodes = [];
      Array.from(haveNode.entries()).forEach(([node, shouldHave]) => {
        if (HeliumBase_1.isHeliumHazed(node) && node._helium.moveMutation) {
          return node._helium.moveMutation = false;
        }
        shouldHave ? addedNodes.push(node) : removedNodes.push(node);
      });
      helium_sdx_1.aLog("treeMutations", "Adding:", addedNodes);
      helium_sdx_1.aLog("treeMutations", "Removing:", removedNodes);
      addedNodes.forEach(node => {
        HeliumBase_1.heliumHaze(node);
        permeate(node, child => {
          if (HeliumBase_1.isHeliumHazed(child)) {
            HeliumBase_1.changeFreezeState(child, false);
          }
        });
      });
      removedNodes.forEach(node => {
        HeliumBase_1.heliumHaze(node);
        permeate(node, child => {
          if (HeliumBase_1.isHeliumHazed(child)) {
            HeliumBase_1.changeFreezeState(child, true);
          }
        });
      });
    }, 10);
  });
  var config = {
    childList: true,
    subtree: true
  };
  if (typeof window !== "undefined") {
    const tryAddObserver = () => mutationObserver.observe(document.body, config);
    if (document && document.body) {
      tryAddObserver();
    } else {
      document.addEventListener("DOMContentLoaded", () => tryAddObserver());
    }
  }
  function permeate(root, cb) {
    cb(root);
    root.childNodes.forEach(child => permeate(child, cb));
  }
}

},

// node_modules/helium-ui-tweener/node_modules/helium-ui/dist/Derivations/Derivers/LocalStorage.js @86
86: function(__fusereq, exports, module){
"use strict";
Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.syncWithLocalStorage = void 0;
const helium_sdx_1 = __fusereq(17);
function syncWithLocalStorage(id, storeMe) {
  const init = localStorage.getItem(id);
  if (init) {
    try {
      const startingData = JSON.parse(init);
      storeMe.upsert(startingData);
    } catch (err) {
      console.warn("Could not parse locally stored data", err);
    }
  }
  helium_sdx_1.derive({
    anchor: "NO_ANCHOR",
    fn: () => {
      const toStore = storeMe.toObject();
      try {
        localStorage.setItem(id, JSON.stringify(toStore));
      } catch (err) {
        console.warn("Could not store", err);
        console.log(toStore);
      }
    }
  });
}
exports.syncWithLocalStorage = syncWithLocalStorage;

}
}, function(){
__fuse.r(1)
})