(function() {
  var f = window.__fuse = window.__fuse || {};
  var modules = f.modules = f.modules || {}; f.dt = function (x) { return x && x.__esModule ? x : { "default": x }; };

  f.bundle = function(collection, fn) {
    for (var num in collection) {
      modules[num] = collection[num];
    }
    fn ? fn() : void 0;
  };
  f.c = {};
  f.r = function(id) {
    var cached = f.c[id];
    if (cached) return cached.m.exports;
    var module = modules[id];
    if (!module) {
      
      throw new Error('Module ' + id + ' was not found');
    }
    cached = f.c[id] = {};
    cached.exports = {};
    cached.m = { exports: cached.exports };
    module(f.r, cached.exports, cached.m);
    return cached.m.exports;
  }; 
})();
__fuse.bundle({

// src/content_scripts/common/InteractionTimer.content.ts @1
1: function(__fusereq, exports, module){
exports.__esModule = true;
var ContentScriptEnum_1 = __fusereq(2);
var Utils_1 = __fusereq(3);
var FindTimerReason_1 = __fusereq(4);
var SettingsId_1 = __fusereq(5);
var Timer_1 = __fusereq(6);
if (Utils_1.setupSingleContentScriptAssert(ContentScriptEnum_1.ContentScript.ADD_TIMER)) {
  let runTimerEffect;
  let showOverlayEffect;
  let timer;
  updateEffect();
  chrome.storage.onChanged.addListener(changes => {
    const hasTimerEnableChange = Object.keys(changes).some(settingId => {
      return settingId.startsWith(SettingsId_1.ADD_TIMER_SETTING_PREFIX) && changes[settingId].newValue !== false;
    });
    if (hasTimerEnableChange) {
      updateEffect();
    }
  });
  function updateEffect() {
    FindTimerReason_1.findPotentialTimerReasons(location.href).then(reasons => {
      if (!reasons) {
        return;
      }
      const runTimerLogic = {
        all: [{
          not: "sm-timer-disabled"
        }, {
          any: reasons
        }]
      };
      const addOverlayLogic = {
        all: [{
          not: "sm-timer-disabled"
        }, {
          any: reasons
        }, "sm-timer-show-overlay"]
      };
      if (runTimerEffect) {
        runTimerEffect.updateLogic(runTimerLogic);
        showOverlayEffect.updateLogic(addOverlayLogic);
        return;
      }
      runTimerEffect = Utils_1.addSettingEffect({
        settingLogic: runTimerLogic,
        effect: () => {
          timer = timer || new Timer_1.Timer();
          timer.enable();
        },
        undoEffect: () => {
          timer && timer.disable();
        }
      });
      showOverlayEffect = Utils_1.addSettingEffect({
        settingLogic: addOverlayLogic,
        effect: () => timer && timer.addDomNodeToBody(),
        undoEffect: () => timer && timer.domNode.remove()
      });
    });
  }
}

},

// src/middleware/ContentScriptEnum.ts @2
2: function(__fusereq, exports, module){
var ContentScript;
(function (ContentScript) {
  ContentScript["YOUTUBE"] = "YOUTUBE_CONTENT_SCRIPT";
  ContentScript["FACEBOOK"] = "FACEBOOK_CONTENT_SCRIPT";
  ContentScript["TWITTER"] = "TWITTER_CONTENT_SCRIPT";
  ContentScript["REDDIT"] = "REDDIT_CONTENT_SCRIPT";
  ContentScript["HACKER_NEWS"] = "HACKER_NEWS_CONTENT_SCRIPT";
  ContentScript["TUMBLR"] = "TUMBLR_CONTENT_SCRIPT";
  ContentScript["PINTEREST"] = "PINTEREST_CONTENT_SCRIPT";
  ContentScript["BLUR_LINKS"] = "BLUR_FLAGGED_LINKS_CONTENT_SCRIPT";
  ContentScript["ADD_TIMER"] = "ADD_TIMER_CONTENT_SCRIPT";
})(ContentScript || (ContentScript = {}))
exports.ContentScript = ContentScript;

},

// src/content_scripts/common/Utils.ts @3
3: function(__fusereq, exports, module){
const locationChangeEventType = "sober-media-location-change";
function observeUrlChanges(cb) {
  assertLocationChangeObserver();
  window.addEventListener(locationChangeEventType, () => cb(window.location));
  cb(window.location);
}
exports.observeUrlChanges = observeUrlChanges;
function assertLocationChangeObserver() {
  const state = window;
  if (state.soberMediaLocationWatchSetup) {
    return;
  }
  state.soberMediaLocationWatchSetup = true;
  let lastHref = location.href;
  const checkForLocationChanged = () => {
    requestAnimationFrame(() => {
      const currentHref = location.href;
      if (currentHref !== lastHref) {
        lastHref = currentHref;
        window.dispatchEvent(new Event(locationChangeEventType));
      }
    });
  };
  ["popstate", "click", "keydown", "keyup", "touchstart", "touchend", "mousedown", "mouseup"].forEach(eventType => {
    window.addEventListener(eventType, checkForLocationChanged);
  });
  document.addEventListener("DOMContentLoaded", checkForLocationChanged);
  setTimeout(checkForLocationChanged, 250);
}
function setupSingleContentScriptAssert(scriptCheck) {
  const wind = window;
  wind.soberMedia = wind.soberMedia || ({});
  if (wind.soberMedia[scriptCheck]) {
    return false;
  }
  wind.soberMedia[scriptCheck] = true;
  chrome.runtime.onMessage.addListener((request, sender, sendResponse) => {
    if (request && request.checkForContentScript === scriptCheck) {
      sendResponse(true);
    }
  });
  return true;
}
exports.setupSingleContentScriptAssert = setupSingleContentScriptAssert;
function getSetting(id) {
  return new Promise(resolve => {
    chrome.storage.sync.get([id], result => {
      resolve(result[id]);
    });
  });
}
exports.getSetting = getSetting;
function setSetting(id, value) {
  return new Promise(resolve => {
    console.log("Setting ", id, value);
    chrome.storage.sync.set({
      [id]: value
    }, () => {
      resolve();
    });
  });
}
exports.setSetting = setSetting;
;
const settingEffects = new Array();
const settingState = {};
function addSettingEffect(args) {
  setupStorageObserver();
  const effectItem = {
    ...args,
    currentState: false,
    updateLogic: newLog => {
      effectItem.settingLogic = newLog;
      updateSettingEffect(effectItem);
    }
  };
  settingEffects.push(effectItem);
  updateSettingEffect(effectItem);
  return effectItem;
}
exports.addSettingEffect = addSettingEffect;
let _storageObserverSetup = false;
function setupStorageObserver() {
  if (_storageObserverSetup) {
    return;
  }
  _storageObserverSetup = true;
  chrome.storage.sync.get(null, allSettings => {
    Object.entries(allSettings).forEach(([key, value]) => {
      settingState[key] = value;
    });
    settingEffects.forEach(updateSettingEffect);
  });
  chrome.storage.onChanged.addListener(changes => {
    Object.entries(changes).forEach(([settingId, change]) => {
      if (change.newValue === change.oldValue) {
        return;
      }
      settingState[settingId] = change.newValue;
    });
    settingEffects.forEach(updateSettingEffect);
  });
}
function updateSettingEffect(effect) {
  const cleanState = resolveSettingLogic(effect.settingLogic);
  if (cleanState === effect.currentState) {
    return;
  }
  effect.currentState = cleanState;
  if (effect.currentState) {
    effect.effect();
  } else {
    effect.undoEffect();
  }
}
function resolveSettingLogic(logic) {
  if (typeof logic === "string") {
    return !!settingState[logic];
  } else if (typeof logic === "boolean") {
    return logic;
  } else if (("not" in logic)) {
    return !settingState[logic.not];
  }
  const objValue = logic["all"] || logic["any"];
  if (Array.isArray(objValue) === false) {
    return resolveSettingLogic(objValue);
  }
  const arr = objValue;
  if (("all" in logic)) {
    return arr.every(resolveSettingLogic);
  }
  return arr.some(resolveSettingLogic);
}
exports.resolveSettingLogic = resolveSettingLogic;
function findParent(ptr, cssQuery) {
  ptr = ptr.parentElement;
  while (ptr && ptr.matches(cssQuery) === false) {
    ptr = ptr.parentElement;
  }
  return ptr;
}
exports.findParent = findParent;

},

// src/middleware/FindTimerReason.ts @4
4: function(__fusereq, exports, module){
exports.__esModule = true;
var SettingsId_1 = __fusereq(5);
async function findTimerReason(href) {
  if (!href) {
    return;
  }
  return new Promise(resolve => {
    chrome.storage.sync.get(null, settings => {
      const addTimerReason = Object.keys(settings).find(settingId => {
        if (settingId.startsWith(SettingsId_1.ADD_TIMER_SETTING_PREFIX) === false) {
          return false;
        }
        if (settings[settingId] !== true) {
          return false;
        }
        const host = settingId.replace(SettingsId_1.ADD_TIMER_SETTING_PREFIX, "");
        return href.includes(host);
      });
      resolve(addTimerReason);
    });
  });
}
exports.findTimerReason = findTimerReason;
async function findPotentialTimerReasons(href) {
  if (!href) {
    return;
  }
  return new Promise(resolve => {
    chrome.storage.sync.get(null, settings => {
      const addTimerReason = Object.keys(settings).filter(settingId => {
        if (settingId.startsWith(SettingsId_1.ADD_TIMER_SETTING_PREFIX) === false) {
          return false;
        }
        const host = settingId.replace(SettingsId_1.ADD_TIMER_SETTING_PREFIX, "");
        return href.includes(host);
      });
      resolve(addTimerReason);
    });
  });
}
exports.findPotentialTimerReasons = findPotentialTimerReasons;

},

// src/middleware/SettingsId.ts @5
5: function(__fusereq, exports, module){
exports.__esModule = true;
exports.SETTING_STORAGE_ID = "BOOLEAN_SETTINGS";
exports.BLUR_LINK_SETTING_PREFIX = `blur-link-`;
exports.NUM_LINKS_BLURRED_SETTING_PREFIX = `num-links-blurred`;
exports.ADD_TIMER_SETTING_PREFIX = `add-timer-`;
exports.TIME_COUNT_SETTING_PREFIX = `sm-time-count-for-`;
exports.FB_TRUST_SETTING_PREFIX = `fb-grant-trust-`;
exports.TWIT_TRUST_SETTING_PREFIX = `twit-grant-trust-`;
exports.SettingIdList = ["fb-disable", "fb-remove-logo", "fb-trust-tokens", "fb-blur-home-feed", "fb-blur-watch-feed", "fb-blur-marketplace-feed", "fb-blur-gaming-feed", "fb-blur-stories", "fb-blur-untrusted", "fb-blur-post-comments", "fb-blur-photo-comments", "fb-blur-notifications", "fb-blur-tab-gaming", "fb-blur-tab-watch", "fb-blur-tab-groups", "fb-blur-tab-market", "fb-blur-popularity-stats", "twit-disable", "twit-remove-logo", "twit-trust-tokens", "twit-blur-home-screen-feed", "twit-blur-tweet", "twit-blur-trending-now", "twit-blur-who-to-follow", "twit-blur-relevant-people", "ytd-disable", "ytd-remove-logo", "ytd-no-trending", "ytd-blur-home-screen-feed", "ytd-blur-recommendations", "ytd-blur-comments", "hn-disable", "hn-remove-logo", "hn-blur-apple", "hn-blur-fb", "hn-blur-google", "hn-blur-ms", "hn-blur-moz", "hn-blur-twit", "hn-custom-regex", "redd-disable", "redd-remove-logo", "redd-blur-home-feed", "redd-blur-home-posts", "redd-remove-home-sidebar", "redd-blur-sub-suggs", "redd-blur-trending-communities", "pin-disable", "pin-remove-logo", "pin-blur-home-feed", "pin-blur-today-feed", "pin-blur-more-like-feed", "tum-disable", "tum-remove-logo", "tum-blur-dash-feed", "tum-blur-posts", "tum-blur-dash-sidebar", "tum-remove-trending-nav", "sm-show-tab-fb", "sm-show-tab-hn", "sm-show-tab-pin", "sm-show-tab-redd", "sm-show-tab-tumblr", "sm-show-tab-twit", "sm-show-tab-yt", "sm-initialized", "sm-timer-disabled", "sm-link-blurring-disabled", "sm-timer-show-overlay", "add-timer-www.facebook.com", "add-timer-twitter.com", "add-timer-www.youtube.com", "add-timer-news.ycombinator.com", "add-timer-www.reddit.com", "add-timer-www.pinterest.com", "add-timer-www.tumblr.com", "sm-time-count-for-all"];

},

// src/content_scripts/common/Timer.ts @6
6: function(__fusereq, exports, module){
exports.__esModule = true;
var SettingsId_1 = __fusereq(5);
var msToDisplayTime_1 = __fusereq(7);
const SECONDS = 1000;
const MINUTES = 60 * SECONDS;
const ACTIVE_SITE_SETTING_ID = "sm-active-site";
function div(className, props) {
  const out = document.createElement("div");
  out.className = className;
  if (props) {
    props.innards && props.innards.forEach(child => out.append(child));
    props.ref && props.ref(out);
  }
  return out;
}
class Timer {
  constructor() {
    this.nodes = undefined;
    this.state = {
      enabled: false,
      mostRecentInteraction: Date.now(),
      sessionStart: undefined,
      sessionEnd: undefined,
      totalTime: 0,
      totalTimeAll: 0,
      synced: false,
      allowTimerUpdate: true
    };
    this.storageId = `${SettingsId_1.TIME_COUNT_SETTING_PREFIX}${location.hostname}`;
    this.sessionStartId = `sm-session-start-${location.hostname}`;
    this.sessionEndId = `sm-session-end-${location.hostname}`;
    this.videoEls = [];
    this.interactionLiveRef = this.interactionLive.bind(this);
    this.enable();
  }
  get domNode() {
    if (!this.nodes) {
      this.nodes = {
        timer: {}
      };
      const lastStaticPosition = {
        left: 0,
        bottom: 0
      };
      const currentPosition = {
        left: 5,
        bottom: 5
      };
      const currentPositionIsStatic = () => {
        lastStaticPosition.left = currentPosition.left;
        lastStaticPosition.bottom = currentPosition.bottom;
      };
      currentPositionIsStatic();
      const moveToCurrentPosition = () => {
        this.nodes.root.style.left = `${currentPosition.left}px`;
        this.nodes.root.style.bottom = `${currentPosition.bottom}px`;
      };
      this.nodes.root = div("Timer", {
        innards: [this.nodes.detailPanel = div("DetailsPanel", {
          ref: ref => {
            const ul = document.createElement("ul");
            ref.append(ul);
            ul.innerHTML = `
            <li>View the "Timer Tab" in Sober Media for more complete info</li>
            <li>Click and drag to move timer</li>
          `;
            const closeMe = document.createElement("li");
            closeMe.classList.add("onPush");
            closeMe.innerText = `Click here to hide timer for this session`;
            closeMe.addEventListener("click", () => {
              this.nodes.root.style.display = "none";
            });
            ul.append(closeMe);
          }
        }), div("Counts", {
          ref: ref => {
            let grabStart;
            const moveListener = ev => {
              if (!grabStart) {
                return;
              }
              const grabMoveLeft = ev.clientX - grabStart.x;
              const grabMoveBottom = grabStart.y - ev.clientY;
              currentPosition.left = lastStaticPosition.left + grabMoveLeft;
              currentPosition.bottom = lastStaticPosition.bottom + grabMoveBottom;
              console.log(grabMoveLeft, grabMoveBottom);
              moveToCurrentPosition();
            };
            ref.addEventListener("mousedown", ev => {
              grabStart = {
                x: ev.clientX,
                y: ev.clientY
              };
              document.body.addEventListener("mousemove", moveListener);
            });
            document.body.addEventListener("mouseup", ev => {
              grabStart = undefined;
              document.body.removeEventListener("mousemove", moveListener);
              currentPositionIsStatic();
            });
          },
          innards: [div("CurrentSite", {
            innards: [(() => {
              const img = document.createElement("img");
              img.src = `${location.origin}/favicon.ico`;
              return img;
            })(), this.nodes.timer.currentSite = document.createElement("span")]
          }), this.nodes.timer.allSites = div("AllSites")]
        })]
      });
      moveToCurrentPosition();
    }
    return this.nodes.root;
  }
  addDomNodeToBody() {
    document.body.prepend(this.domNode);
  }
  enable() {
    if (this.state.enabled) {
      return;
    }
    this.state.enabled = true;
    if (this.nodes) {
      this.nodes.root.style.display = "";
    }
    const allowTimerUpdate = () => {
      this.state.allowTimerUpdate = true;
    };
    this.updateAllowInterval = setInterval(allowTimerUpdate, 5 * SECONDS);
    Timer.interactionEvents.forEach(evType => {
      window.addEventListener(evType, this.interactionLiveRef);
    });
    this.assertVideoFinder();
    const allTimesId = "sm-time-count-for-all";
    chrome.storage.sync.get([this.storageId, allTimesId, this.sessionStartId, this.sessionEndId], resp => {
      this.state.synced = true;
      if (!resp) {
        return;
      }
      this.state.totalTime += resp[this.storageId] || 0;
      this.state.totalTimeAll = resp[allTimesId] || 0;
      this.state.sessionStart = resp[this.sessionStartId] || Date.now();
      this.state.sessionEnd = resp[this.sessionEndId] || undefined;
      allowTimerUpdate();
      this.updateTimer();
    });
    chrome.storage.onChanged.addListener(changes => {
      const changeAll = changes[allTimesId];
      if (changeAll) {
        this.state.totalTimeAll = changeAll.newValue;
      }
      const changeSite = changes[this.storageId];
      if (changeSite && changeSite.newValue === 0) {
        this.state.totalTime = 0;
      }
      const activeSite = changes[ACTIVE_SITE_SETTING_ID];
      if (activeSite && activeSite !== location.hostname) {
        this.state.sessionStart = undefined;
      }
    });
  }
  updateTimer() {
    if (this.state.allowTimerUpdate === false) {
      return;
    }
    this.state.allowTimerUpdate = false;
    if (this.state.synced) {
      chrome.storage.sync.get(this.storageId, obj => {
        const storedValue = obj[this.storageId];
        if (storedValue > this.state.totalTime) {
          this.state.totalTime = storedValue;
          return;
        }
        chrome.storage.sync.set({
          [this.storageId]: this.state.totalTime,
          [ACTIVE_SITE_SETTING_ID]: location.hostname
        });
      });
    }
    if (!this.nodes) {
      return;
    }
    this.nodes.timer.currentSite.textContent = msToDisplayTime_1.millisecToDisplayTime(this.state.totalTime);
    this.nodes.timer.allSites.textContent = `all: ${msToDisplayTime_1.millisecToDisplayTime(Math.max(this.state.totalTimeAll, this.state.totalTime))}`;
  }
  disable() {
    if (this.state.enabled === false) {
      return;
    }
    this.state.enabled = false;
    if (this.nodes) {
      this.nodes.root.style.display = "none";
    }
    if (this.updateAllowInterval) {
      clearInterval(this.updateAllowInterval);
      this.updateAllowInterval = undefined;
    }
    Timer.interactionEvents.forEach(evType => {
      window.removeEventListener(evType, this.interactionLiveRef);
    });
  }
  assertVideoFinder() {
    if (this.videoElFinder) {
      return;
    }
    const modVideoEl = videoEl => {
      videoEl.addEventListener("timeupdate", ev => {
        if (document.hasFocus() && this.state.enabled) {
          this.interactionLive();
        }
      });
    };
    document.querySelectorAll("video").forEach(modVideoEl);
    this.videoElFinder = new MutationObserver(mutList => {
      for (const mut of mutList) {
        if (mut.type !== 'childList') {
          return;
        }
        mut.addedNodes.forEach(modVideoEl);
      }
    });
    this.videoElFinder.observe(document.body, {
      childList: true,
      subtree: true
    });
  }
  interactionLive() {
    const timerState = this.state;
    const {sessionEnd} = timerState;
    this.nodes && this.nodes.root.classList.remove("--inactive");
    const lastInteraction = timerState.mostRecentInteraction;
    timerState.mostRecentInteraction = Date.now();
    if (!timerState.sessionStart) {
      timerState.sessionStart = Date.now();
      timerState.sessionEnd = undefined;
    }
    if (sessionEnd) {
      const timeSinceSession = Date.now() - sessionEnd;
      const sessionLength = sessionEnd - timerState.sessionStart;
      if (timeSinceSession > Math.min(5 * MINUTES, sessionLength)) {
        timerState.sessionStart = Date.now();
        chrome.storage.sync.set({
          [this.sessionStartId]: timerState.sessionStart
        });
      } else {
        timerState.totalTime += timeSinceSession;
        timerState.allowTimerUpdate = true;
      }
      timerState.sessionEnd = undefined;
      chrome.storage.sync.set({
        [this.sessionEndId]: timerState.sessionEnd
      });
    } else {
      timerState.totalTime += timerState.mostRecentInteraction - lastInteraction;
    }
    if (this.interactionTimeout) {
      window.clearTimeout(this.interactionTimeout);
    }
    this.updateTimer();
    this.interactionTimeout = window.setTimeout(() => {
      this.interactionTimeout = undefined;
      timerState.sessionEnd = timerState.mostRecentInteraction;
      chrome.storage.sync.set({
        [this.sessionEndId]: timerState.sessionEnd
      });
      this.nodes && this.nodes.root.classList.add("--inactive");
    }, 5 * SECONDS);
  }
}
Timer.interactionEvents = ["mousedown", "mousemove", "wheel", "keydown", "touchstart"];
exports.Timer = Timer;

},

// src/middleware/msToDisplayTime.ts @7
7: function(__fusereq, exports, module){
function millisecToDisplayTime(ms) {
  const ds = ms / 1000;
  const seconds = ds % 60;
  const minutes = Math.floor(ds / 60) % 60;
  const hours = Math.floor(ds / 3600);
  let secondsStr = String(seconds);
  if (seconds < 10) {
    secondsStr = "0" + secondsStr;
  }
  let minutesStr = String(minutes);
  if (hours && minutes < 10) {
    minutesStr = "0" + minutesStr;
  }
  let out = ``;
  if (hours) {
    out += hours + ":";
  }
  out += `${minutesStr}:${secondsStr.slice(0, 2)}`;
  return out;
}
exports.millisecToDisplayTime = millisecToDisplayTime;

}
}, function(){
__fuse.r(1)
})