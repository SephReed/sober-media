(function() {
  var f = window.__fuse = window.__fuse || {};
  var modules = f.modules = f.modules || {}; f.dt = function (x) { return x && x.__esModule ? x : { "default": x }; };

  f.bundle = function(collection, fn) {
    for (var num in collection) {
      modules[num] = collection[num];
    }
    fn ? fn() : void 0;
  };
  f.c = {};
  f.r = function(id) {
    var cached = f.c[id];
    if (cached) return cached.m.exports;
    var module = modules[id];
    if (!module) {
      
      throw new Error('Module ' + id + ' was not found');
    }
    cached = f.c[id] = {};
    cached.exports = {};
    cached.m = { exports: cached.exports };
    module(f.r, cached.exports, cached.m);
    return cached.m.exports;
  }; 
})();
__fuse.bundle({

// src/content_scripts/sites/Twitter.content.ts @1
1: function(__fusereq, exports, module){
exports.__esModule = true;
var Utils_1 = __fusereq(2);
var ContentScriptEnum_1 = __fusereq(3);
var SimpleMutations_1 = __fusereq(4);
var CssQueryObserve_1 = __fusereq(5);
var SettingsId_1 = __fusereq(6);
var TrustCoin_1 = __fusereq(7);
if (Utils_1.setupSingleContentScriptAssert(ContentScriptEnum_1.ContentScript.TWITTER)) {
  CssQueryObserve_1.setDefaultQueryLogic({
    not: `twit-disable`
  });
  const knownTweets = new Map();
  Utils_1.observeUrlChanges(async loc => {
    if (loc.host.endsWith("twitter.com") === false) {
      return;
    }
    blurTrendingNow();
    blurRelevantPeople();
    blurWhoToFollow();
    removeLogo();
    if (loc.pathname === "/home") {
      blurFeed();
      blurTweets();
    }
  });
  function blurTrendingNow() {
    SimpleMutations_1.blurChildrenAsGroup({
      queryArgs: {
        cssQuery: `div[aria-label="Timeline: Trending now"] > div`,
        settingLogic: "twit-blur-trending-now"
      },
      checkBlurChild: child => child.textContent !== "What’s happening"
    });
  }
  function blurWhoToFollow() {
    SimpleMutations_1.blurChildrenAsGroup({
      queryArgs: {
        cssQuery: `aside[aria-label="Who to follow"]`,
        settingLogic: "twit-blur-who-to-follow"
      },
      checkBlurChild: child => child.textContent !== "Who to follow"
    });
  }
  function blurRelevantPeople() {
    SimpleMutations_1.blurAllFound({
      queryArgs: {
        cssQuery: `aside[aria-label="Relevant people"]`,
        settingLogic: "twit-blur-relevant-people"
      },
      blurArgs: {
        blur: true,
        showText: "ON_HOVER"
      }
    });
  }
  function blurFeed() {
    SimpleMutations_1.blurAllFound({
      permaUnblur: true,
      queryArgs: {
        cssQuery: `div[aria-label="Timeline: Your Home Timeline"]`,
        settingLogic: "twit-blur-home-screen-feed"
      }
    });
  }
  function blurTweets() {
    CssQueryObserve_1.rebootCssQueryObserve({
      cssQuery: `div[aria-label="Timeline: Your Home Timeline"] div[data-testid="tweet"] > .css-1dbjc4n.r-1iusvr4.r-16y2uox.r-1777fci.r-1mi0q7o > div:nth-child(2)`,
      reboot: false,
      settingLogic: null,
      cb: ({domNode}) => {
        const textNode = domNode.querySelector(`.css-901oao`);
        const tweetText = textNode && textNode.textContent && textNode.textContent.slice(0, 60);
        if (tweetText && knownTweets.get(tweetText) === false) {
          return;
        }
        const prev = domNode.previousElementSibling;
        const link = prev.querySelector("a");
        const handle = link && link.innerText.match(/@.+/);
        if (!link || !handle || !handle[0]) {
          addBlur({
            all: [{
              not: "twit-disable"
            }, "twit-blur-tweet"]
          });
          return;
        }
        const trustSettingId = `${SettingsId_1.TWIT_TRUST_SETTING_PREFIX}${handle}`;
        let trustCoin;
        Utils_1.addSettingEffect({
          settingLogic: {
            all: [{
              not: "twit-disable"
            }, "twit-blur-tweet", "twit-trust-tokens"]
          },
          effect: () => {
            if (!trustCoin) {
              trustCoin = TrustCoin_1.renderTrustCoin({
                settingId: trustSettingId,
                name: handle[0]
              });
              link.parentElement.style.flexDirection = "row";
            }
            link.parentElement.append(trustCoin);
          },
          undoEffect: () => trustCoin && trustCoin.remove()
        });
        addBlur({
          all: [{
            not: "twit-disable"
          }, "twit-blur-tweet", {
            all: ["twit-trust-tokens", {
              not: trustSettingId
            }]
          }]
        });
        function addBlur(settingLogic) {
          Utils_1.addSettingEffect({
            settingLogic,
            effect: () => SimpleMutations_1.setElementBlur(domNode, {
              blur: true,
              showText: false,
              onUnblur: ev => {
                ev.removeEventListeners();
                tweetText && knownTweets.set(tweetText, false);
              }
            }),
            undoEffect: () => SimpleMutations_1.setElementBlur(domNode, {
              blur: false
            })
          });
        }
      },
      undoCb: () => {}
    });
  }
  function removeLogo() {
    SimpleMutations_1.hideAllFound({
      queryArgs: {
        cssQuery: `a[href="/home"][aria-label="Twitter"]`,
        settingLogic: "twit-remove-logo"
      }
    });
  }
}

},

// src/content_scripts/common/Utils.ts @2
2: function(__fusereq, exports, module){
const locationChangeEventType = "sober-media-location-change";
function observeUrlChanges(cb) {
  assertLocationChangeObserver();
  window.addEventListener(locationChangeEventType, () => cb(window.location));
  cb(window.location);
}
exports.observeUrlChanges = observeUrlChanges;
function assertLocationChangeObserver() {
  const state = window;
  if (state.soberMediaLocationWatchSetup) {
    return;
  }
  state.soberMediaLocationWatchSetup = true;
  let lastHref = location.href;
  const checkForLocationChanged = () => {
    requestAnimationFrame(() => {
      const currentHref = location.href;
      if (currentHref !== lastHref) {
        lastHref = currentHref;
        window.dispatchEvent(new Event(locationChangeEventType));
      }
    });
  };
  ["popstate", "click", "keydown", "keyup", "touchstart", "touchend", "mousedown", "mouseup"].forEach(eventType => {
    window.addEventListener(eventType, checkForLocationChanged);
  });
  document.addEventListener("DOMContentLoaded", checkForLocationChanged);
  setTimeout(checkForLocationChanged, 250);
}
function setupSingleContentScriptAssert(scriptCheck) {
  const wind = window;
  wind.soberMedia = wind.soberMedia || ({});
  if (wind.soberMedia[scriptCheck]) {
    return false;
  }
  wind.soberMedia[scriptCheck] = true;
  chrome.runtime.onMessage.addListener((request, sender, sendResponse) => {
    if (request && request.checkForContentScript === scriptCheck) {
      sendResponse(true);
    }
  });
  return true;
}
exports.setupSingleContentScriptAssert = setupSingleContentScriptAssert;
function getSetting(id) {
  return new Promise(resolve => {
    chrome.storage.sync.get([id], result => {
      resolve(result[id]);
    });
  });
}
exports.getSetting = getSetting;
function setSetting(id, value) {
  return new Promise(resolve => {
    console.log("Setting ", id, value);
    chrome.storage.sync.set({
      [id]: value
    }, () => {
      resolve();
    });
  });
}
exports.setSetting = setSetting;
;
const settingEffects = new Array();
const settingState = {};
function addSettingEffect(args) {
  setupStorageObserver();
  const effectItem = {
    ...args,
    currentState: false,
    updateLogic: newLog => {
      effectItem.settingLogic = newLog;
      updateSettingEffect(effectItem);
    }
  };
  settingEffects.push(effectItem);
  updateSettingEffect(effectItem);
  return effectItem;
}
exports.addSettingEffect = addSettingEffect;
let _storageObserverSetup = false;
function setupStorageObserver() {
  if (_storageObserverSetup) {
    return;
  }
  _storageObserverSetup = true;
  chrome.storage.sync.get(null, allSettings => {
    Object.entries(allSettings).forEach(([key, value]) => {
      settingState[key] = value;
    });
    settingEffects.forEach(updateSettingEffect);
  });
  chrome.storage.onChanged.addListener(changes => {
    Object.entries(changes).forEach(([settingId, change]) => {
      if (change.newValue === change.oldValue) {
        return;
      }
      settingState[settingId] = change.newValue;
    });
    settingEffects.forEach(updateSettingEffect);
  });
}
function updateSettingEffect(effect) {
  const cleanState = resolveSettingLogic(effect.settingLogic);
  if (cleanState === effect.currentState) {
    return;
  }
  effect.currentState = cleanState;
  if (effect.currentState) {
    effect.effect();
  } else {
    effect.undoEffect();
  }
}
function resolveSettingLogic(logic) {
  if (typeof logic === "string") {
    return !!settingState[logic];
  } else if (typeof logic === "boolean") {
    return logic;
  } else if (("not" in logic)) {
    return !settingState[logic.not];
  }
  const objValue = logic["all"] || logic["any"];
  if (Array.isArray(objValue) === false) {
    return resolveSettingLogic(objValue);
  }
  const arr = objValue;
  if (("all" in logic)) {
    return arr.every(resolveSettingLogic);
  }
  return arr.some(resolveSettingLogic);
}
exports.resolveSettingLogic = resolveSettingLogic;
function findParent(ptr, cssQuery) {
  ptr = ptr.parentElement;
  while (ptr && ptr.matches(cssQuery) === false) {
    ptr = ptr.parentElement;
  }
  return ptr;
}
exports.findParent = findParent;

},

// src/middleware/ContentScriptEnum.ts @3
3: function(__fusereq, exports, module){
var ContentScript;
(function (ContentScript) {
  ContentScript["YOUTUBE"] = "YOUTUBE_CONTENT_SCRIPT";
  ContentScript["FACEBOOK"] = "FACEBOOK_CONTENT_SCRIPT";
  ContentScript["TWITTER"] = "TWITTER_CONTENT_SCRIPT";
  ContentScript["REDDIT"] = "REDDIT_CONTENT_SCRIPT";
  ContentScript["HACKER_NEWS"] = "HACKER_NEWS_CONTENT_SCRIPT";
  ContentScript["TUMBLR"] = "TUMBLR_CONTENT_SCRIPT";
  ContentScript["PINTEREST"] = "PINTEREST_CONTENT_SCRIPT";
  ContentScript["BLUR_LINKS"] = "BLUR_FLAGGED_LINKS_CONTENT_SCRIPT";
  ContentScript["ADD_TIMER"] = "ADD_TIMER_CONTENT_SCRIPT";
})(ContentScript || (ContentScript = {}))
exports.ContentScript = ContentScript;

},

// src/content_scripts/common/SimpleMutations.ts @4
4: function(__fusereq, exports, module){
exports.__esModule = true;
var CssQueryObserve_1 = __fusereq(5);
const unblurOnClickNodes = new Map();
function setElementBlur(domNode, args) {
  if (args.blur === domNode.classList.contains("--blurred")) {
    return;
  }
  domNode.classList.toggle("--blurred", args.blur);
  if (args.blur) {
    domNode.classList.toggle("hasUnblurText", args.showText !== false);
    if (args.unblurOnClick !== false) {
      if (unblurOnClickNodes.has(domNode)) {
        return;
      }
      unblurOnClickNodes.set(domNode);
      const listener = ev => {
        if (domNode.classList.contains("--blurred") === false) {
          return;
        }
        ev.stopImmediatePropagation();
        ev.preventDefault();
        domNode.classList.remove("--blurred");
        unblurOnClickNodes.delete(domNode);
        args.onUnblur && args.onUnblur({
          removeEventListeners: () => {
            domNode.removeEventListener("click", listener);
          }
        });
        return false;
      };
      domNode.addEventListener("click", listener);
    }
    if (args.showText === "ON_HOVER") {
      domNode.classList.add("hideTextUntilHover");
    }
    switch (args.blurStrength) {
      case "LOW":
        domNode.classList.add("lowPowerBlur");
        break;
      case "VERY_LOW":
        domNode.classList.add("veryLowPowerBlur");
        break;
    }
    switch (args.textSize) {
      case "SMALL":
        domNode.classList.add("smallText");
        break;
      case "VERY_SMALL":
        domNode.classList.add("verySmallText");
        break;
    }
  }
}
exports.setElementBlur = setElementBlur;
function blurTogether(args) {
  const {domNodes} = args;
  const listener = ev => {
    ev.stopImmediatePropagation();
    ev.preventDefault();
    for (const domNode of domNodes) {
      domNode.removeEventListener("click", listener);
      setElementBlur(domNode, {
        blur: false
      });
    }
    if (args.blurArgs && args.blurArgs.onUnblur) {
      args.blurArgs.onUnblur({
        removeEventListeners: () => null
      });
    }
  };
  domNodes.forEach(domNode => {
    setElementBlur(domNode, {
      unblurOnClick: false,
      showText: false,
      blur: true,
      ...args.blurArgs
    });
    domNode.addEventListener("click", listener);
  });
}
exports.blurTogether = blurTogether;
const foundDomNodes = new Map();
function blurAllFound(args) {
  CssQueryObserve_1.rebootCssQueryObserve({
    ...args.queryArgs,
    cb: ({domNode, querySetup}) => {
      if (args.checkIgnore && args.checkIgnore(domNode)) {
        return;
      }
      setElementBlur(domNode, {
        blur: true,
        onUnblur: ev => {
          if (args.permaUnblur) {
            ev.removeEventListeners();
            querySetup.removeQuery();
          }
        },
        ...args.blurArgs
      });
    },
    undoCb: ({domNode}) => {
      setElementBlur(domNode, {
        blur: false
      });
    }
  });
}
exports.blurAllFound = blurAllFound;
function hideAllFound(args) {
  CssQueryObserve_1.rebootCssQueryObserve({
    ...args.queryArgs,
    cb: ({domNode}) => {
      domNode = args.transformDomNode ? args.transformDomNode(domNode) : domNode;
      domNode.style.display = "none";
      domNode.style.visibility = "hidden";
    },
    undoCb: ({domNode}) => {
      domNode = args.transformDomNode ? args.transformDomNode(domNode) : domNode;
      domNode.style.display = domNode.style.visibility = "";
    }
  });
}
exports.hideAllFound = hideAllFound;
function blurChildrenAsGroup(args) {
  const groupings = new Map();
  CssQueryObserve_1.rebootCssQueryObserve({
    ...args.queryArgs,
    cb: ({domNode}) => {
      let group = groupings.get(domNode);
      if (group) {
        blurGroup();
        return;
      }
      groupings.set(domNode, group = []);
      observeChildren(domNode, child => {
        if (args.checkBlurChild && args.checkBlurChild(child) === false) {
          return;
        }
        group.push(child);
        blurGroup();
      });
      function blurGroup() {
        blurTogether({
          domNodes: group
        });
      }
    },
    undoCb: ({domNode}) => {
      const group = groupings.get(domNode);
      group && blurTogether({
        domNodes: group,
        blurArgs: {
          blur: false
        }
      });
    }
  });
}
exports.blurChildrenAsGroup = blurChildrenAsGroup;
function observeChildren(root, cb) {
  const mutObserver = new MutationObserver(mutList => {
    for (const mut of mutList) {
      if (mut.type !== 'childList') {
        return;
      }
      for (const child of mut.addedNodes) {
        if (child instanceof HTMLElement) {
          cb(child);
        }
      }
    }
  });
  mutObserver.observe(root, {
    childList: true
  });
  for (const child of root.childNodes) {
    if (child instanceof HTMLElement) {
      cb(child);
    }
  }
  return mutObserver;
}
exports.observeChildren = observeChildren;

},

// src/content_scripts/common/CssQueryObserve.ts @5
5: function(__fusereq, exports, module){
exports.__esModule = true;
var Utils_1 = __fusereq(2);
let defaultLogic;
function setDefaultQueryLogic(logic) {
  defaultLogic = logic;
}
exports.setDefaultQueryLogic = setDefaultQueryLogic;
const existingObservers = new Map();
function rebootCssQueryObserve(args) {
  const root = args.root = args.root || document;
  if (args.settingLogic && defaultLogic) {
    args.settingLogic = {
      all: [args.settingLogic, defaultLogic]
    };
  }
  let observerSetup = existingObservers.get(root);
  if (observerSetup) {
    const querySetup = observerSetup.cssQueries.get(args.cssQuery);
    if (querySetup) {
      if (args.reboot === false) {
        return;
      }
      if (querySetup.disabled) {
        return;
      }
      querySetup.foundElements.forEach(domNode => {
        runQuerySetupOnElement(querySetup, domNode);
      });
      return;
    }
    observerSetup.cssQueries.set(args.cssQuery, initQuerySetup(args));
    return;
  }
  const cssQueries = new Map();
  cssQueries.set(args.cssQuery, initQuerySetup(args));
  const mutObserver = new MutationObserver(mutList => {
    for (const mut of mutList) {
      if (mut.type !== 'childList') {
        return;
      }
      const cssQueryEntries = Array.from(cssQueries.entries());
      cssQueryEntries.forEach(([cssQuery, querySetup]) => {
        if (querySetup.disabled) {
          return;
        }
        for (const child of mut.addedNodes) {
          if (child instanceof HTMLElement) {
            let matches = [];
            if (child.matches(cssQuery)) {
              matches.push(child);
            }
            matches = matches.concat(Array.from(child.querySelectorAll(cssQuery)));
            for (const match of matches) {
              addAddElementToQuerySetup(querySetup, match);
              if (querySetup.disabled) {
                return;
              }
            }
          }
        }
      });
    }
  });
  mutObserver.observe(root, {
    childList: true,
    subtree: true
  });
  existingObservers.set(root, {
    observer: mutObserver,
    cssQueries
  });
}
exports.rebootCssQueryObserve = rebootCssQueryObserve;
function initQuerySetup(args) {
  const out = {
    cb: args.cb,
    undoCb: args.undoCb,
    settingLogic: args.settingLogic,
    foundElements: [],
    disabled: false,
    removeQuery: () => {
      out.disabled = true;
    }
  };
  const currentMatches = (args.root || document).querySelectorAll(args.cssQuery);
  for (const match of currentMatches) {
    addAddElementToQuerySetup(out, match);
  }
  return out;
}
function addAddElementToQuerySetup(querySetup, element) {
  if (querySetup.limit && querySetup.limit <= querySetup.foundElements.length) {
    querySetup.disabled = true;
    return;
  }
  if (querySetup.foundElements.includes(element)) {
    return;
  }
  querySetup.foundElements.push(element);
  runQuerySetupOnElement(querySetup, element);
}
async function runQuerySetupOnElement(querySetup, element) {
  if (!querySetup.settingLogic) {
    querySetup.cb({
      domNode: element,
      querySetup
    });
    return;
  }
  Utils_1.addSettingEffect({
    settingLogic: querySetup.settingLogic,
    effect: () => querySetup.cb({
      domNode: element,
      querySetup
    }),
    undoEffect: () => querySetup.undoCb({
      domNode: element,
      querySetup
    })
  });
}

},

// src/middleware/SettingsId.ts @6
6: function(__fusereq, exports, module){
exports.__esModule = true;
exports.SETTING_STORAGE_ID = "BOOLEAN_SETTINGS";
exports.BLUR_LINK_SETTING_PREFIX = `blur-link-`;
exports.NUM_LINKS_BLURRED_SETTING_PREFIX = `num-links-blurred`;
exports.ADD_TIMER_SETTING_PREFIX = `add-timer-`;
exports.TIME_COUNT_SETTING_PREFIX = `sm-time-count-for-`;
exports.FB_TRUST_SETTING_PREFIX = `fb-grant-trust-`;
exports.TWIT_TRUST_SETTING_PREFIX = `twit-grant-trust-`;
exports.SettingIdList = ["fb-disable", "fb-remove-logo", "fb-trust-tokens", "fb-blur-home-feed", "fb-blur-watch-feed", "fb-blur-marketplace-feed", "fb-blur-gaming-feed", "fb-blur-stories", "fb-blur-untrusted", "fb-blur-post-comments", "fb-blur-photo-comments", "fb-blur-notifications", "fb-blur-tab-gaming", "fb-blur-tab-watch", "fb-blur-tab-groups", "fb-blur-tab-market", "fb-blur-popularity-stats", "twit-disable", "twit-remove-logo", "twit-trust-tokens", "twit-blur-home-screen-feed", "twit-blur-tweet", "twit-blur-trending-now", "twit-blur-who-to-follow", "twit-blur-relevant-people", "ytd-disable", "ytd-remove-logo", "ytd-no-trending", "ytd-blur-home-screen-feed", "ytd-blur-recommendations", "ytd-blur-comments", "hn-disable", "hn-remove-logo", "hn-blur-apple", "hn-blur-fb", "hn-blur-google", "hn-blur-ms", "hn-blur-moz", "hn-blur-twit", "hn-custom-regex", "redd-disable", "redd-remove-logo", "redd-blur-home-feed", "redd-blur-home-posts", "redd-remove-home-sidebar", "redd-blur-sub-suggs", "redd-blur-trending-communities", "pin-disable", "pin-remove-logo", "pin-blur-home-feed", "pin-blur-today-feed", "pin-blur-more-like-feed", "tum-disable", "tum-remove-logo", "tum-blur-dash-feed", "tum-blur-posts", "tum-blur-dash-sidebar", "tum-remove-trending-nav", "sm-show-tab-fb", "sm-show-tab-hn", "sm-show-tab-pin", "sm-show-tab-redd", "sm-show-tab-tumblr", "sm-show-tab-twit", "sm-show-tab-yt", "sm-initialized", "sm-timer-disabled", "sm-link-blurring-disabled", "sm-timer-show-overlay", "add-timer-www.facebook.com", "add-timer-twitter.com", "add-timer-www.youtube.com", "add-timer-news.ycombinator.com", "add-timer-www.reddit.com", "add-timer-www.pinterest.com", "add-timer-www.tumblr.com", "sm-time-count-for-all"];

},

// src/content_scripts/common/TrustCoin.ts @7
7: function(__fusereq, exports, module){
exports.__esModule = true;
var Utils_1 = __fusereq(2);
function renderTrustCoin(props) {
  const trustCoin = document.createElement("div");
  trustCoin.classList.add("TrustCoin");
  trustCoin.title = `Add/Remove Trust for ${props.name}`;
  trustCoin.addEventListener("click", ev => {
    ev.stopImmediatePropagation();
    ev.preventDefault();
    Utils_1.getSetting(props.settingId).then(value => {
      Utils_1.setSetting(props.settingId, !value);
    });
    return false;
  });
  Utils_1.addSettingEffect({
    settingLogic: props.settingId,
    effect: () => {
      trustCoin.classList.add("--trusted");
    },
    undoEffect: () => {
      trustCoin.classList.remove("--trusted");
    }
  });
  return trustCoin;
}
exports.renderTrustCoin = renderTrustCoin;

}
}, function(){
__fuse.r(1)
})