(function() {
  var f = window.__fuse = window.__fuse || {};
  var modules = f.modules = f.modules || {}; f.dt = function (x) { return x && x.__esModule ? x : { "default": x }; };

  f.bundle = function(collection, fn) {
    for (var num in collection) {
      modules[num] = collection[num];
    }
    fn ? fn() : void 0;
  };
  f.c = {};
  f.r = function(id) {
    var cached = f.c[id];
    if (cached) return cached.m.exports;
    var module = modules[id];
    if (!module) {
      
      throw new Error('Module ' + id + ' was not found');
    }
    cached = f.c[id] = {};
    cached.exports = {};
    cached.m = { exports: cached.exports };
    module(f.r, cached.exports, cached.m);
    return cached.m.exports;
  }; 
})();
__fuse.bundle({

// src/content_scripts/sites/Facebook.content.ts @1
1: function(__fusereq, exports, module){
exports.__esModule = true;
var Utils_1 = __fusereq(2);
var ContentScriptEnum_1 = __fusereq(3);
var SimpleMutations_1 = __fusereq(4);
var CssQueryObserve_1 = __fusereq(5);
var TrustCoin_1 = __fusereq(6);
var SettingsId_1 = __fusereq(7);
if (Utils_1.setupSingleContentScriptAssert(ContentScriptEnum_1.ContentScript.FACEBOOK)) {
  CssQueryObserve_1.setDefaultQueryLogic({
    not: "fb-disable"
  });
  Utils_1.observeUrlChanges(async loc => {
    if (loc.host.endsWith("facebook.com") === false) {
      return;
    }
    blurNotifications();
    removeLogo();
    blurGroupsTab();
    blurWatchTab();
    blurGamingTab();
    blurMarketTab();
    blurPopularityStats();
    blurEachFeedPost();
    blurPostComments();
    if (loc.pathname === "/") {
      blurHomeFeed();
      blurStories();
    } else if (loc.pathname.includes("/photo")) {
      blurPhotoComments();
    } else if (loc.pathname.includes("/marketplace")) {
      blurMarketplaceFeed();
    } else if (loc.pathname.includes("/watch")) {
      blurWatchFeed();
    } else if (loc.pathname.includes("/gaming")) {
      blurGamingFeed();
    }
  });
  function blurHomeFeed() {
    SimpleMutations_1.blurAllFound({
      queryArgs: {
        cssQuery: `div[role="main"].rq0escxv.l9j0dhe7.du4w35lb.j83agx80.g5gj957u.pmt1y7k9.buofh1pr.hpfvmrgz.taijpn5t.gs1a9yip.owycx6da.btwxx1t3.f7vcsfb0.fjf4s8hc.b6rwyo50.oyrvap6t`,
        settingLogic: "fb-blur-home-feed"
      }
    });
  }
  function blurStories() {
    SimpleMutations_1.blurAllFound({
      queryArgs: {
        cssQuery: `div[data-pagelet="Stories"]`,
        settingLogic: "fb-blur-stories"
      }
    });
  }
  function blurEachFeedPost() {
    CssQueryObserve_1.rebootCssQueryObserve({
      cssQuery: `div[role="feed"] > div`,
      settingLogic: null,
      cb: ({domNode}) => {
        let blurredItems = [];
        if (location.pathname.includes("/search")) {
          return;
        }
        const nameEl = domNode.querySelector(".pybr56ya.dati1w0a.hv4rvrfc.n851cfcs.btwxx1t3.j83agx80.ll8tlv6m");
        if (!nameEl) {
          blurredItems.push(domNode);
          doBlur("fb-blur-untrusted");
          return;
        }
        let trustSettingId;
        const personLink = nameEl.querySelector("h4 a");
        if (personLink) {
          let trustCoin;
          Utils_1.addSettingEffect({
            settingLogic: {
              all: [{
                not: "fb-disable"
              }, "fb-trust-tokens", "fb-blur-untrusted"]
            },
            effect: () => {
              if (!trustCoin) {
                const name = personLink.textContent;
                trustSettingId = `${SettingsId_1.FB_TRUST_SETTING_PREFIX}${name}`;
                trustCoin = TrustCoin_1.renderTrustCoin({
                  settingId: trustSettingId,
                  name
                });
              }
              personLink.parentElement.appendChild(trustCoin);
            },
            undoEffect: () => {
              trustCoin && trustCoin.remove();
            }
          });
        }
        let ptr = nameEl.parentElement;
        while (ptr && (ptr = ptr.nextElementSibling) && ptr) {
          blurredItems.push(ptr);
        }
        if (trustSettingId) {
          doBlur({
            all: [{
              not: "fb-disable"
            }, "fb-blur-untrusted", {
              all: ["fb-trust-tokens", {
                not: trustSettingId
              }]
            }]
          });
        } else {
          doBlur({
            all: [{
              not: "fb-disable"
            }, "fb-blur-untrusted"]
          });
        }
        function doBlur(blurRules) {
          Utils_1.addSettingEffect({
            settingLogic: blurRules,
            effect: () => {
              blurredItems.length && SimpleMutations_1.blurTogether({
                domNodes: blurredItems,
                blurArgs: {
                  blur: true,
                  showText: true,
                  textSize: "SMALL",
                  onUnblur: () => {
                    blurredItems = [];
                  }
                }
              });
            },
            undoEffect: () => {
              SimpleMutations_1.blurTogether({
                domNodes: blurredItems,
                blurArgs: {
                  blur: false
                }
              });
            }
          });
        }
      },
      undoCb: () => {}
    });
  }
  function blurPostComments() {
    SimpleMutations_1.blurAllFound({
      queryArgs: {
        cssQuery: `div[role="feed"] > div  .cwj9ozl2.tvmbv18p > ul`,
        settingLogic: "fb-blur-post-comments"
      },
      blurArgs: {
        blur: true,
        textSize: "SMALL"
      }
    });
  }
  async function blurNotifications() {
    SimpleMutations_1.blurAllFound({
      queryArgs: {
        cssQuery: `.bp9cbjyn.j83agx80.datstx6m.taijpn5t.oi9244e8:nth-child(2)`,
        settingLogic: "fb-blur-notifications"
      },
      blurArgs: {
        blur: true,
        showText: false
      }
    });
  }
  async function removeLogo() {
    SimpleMutations_1.hideAllFound({
      queryArgs: {
        cssQuery: `.bp9cbjyn.rq0escxv.j83agx80.buofh1pr.byvelhso.dhix69tm.poy2od1o.j9ispegn.kr520xx4.ehxjyohh`,
        settingLogic: "fb-remove-logo"
      }
    });
  }
  function blurMarketTab() {
    blurTab("Marketplace", `fb-blur-tab-market`);
  }
  function blurGroupsTab() {
    blurTab("Groups", `fb-blur-tab-groups`);
  }
  function blurWatchTab() {
    blurTab("Watch", `fb-blur-tab-watch`);
  }
  function blurGamingTab() {
    blurTab("Gaming", `fb-blur-tab-gaming`);
  }
  function blurTab(ariaLabel, settingLogic) {
    SimpleMutations_1.blurAllFound({
      queryArgs: {
        cssQuery: `a[aria-label^="${ariaLabel}"]`,
        settingLogic
      },
      blurArgs: {
        blur: true,
        showText: false
      }
    });
  }
  function blurPhotoComments() {
    SimpleMutations_1.blurAllFound({
      queryArgs: {
        cssQuery: `.q5bimw55.rpm2j7zs.k7i0oixp.gvuykj2m.j83agx80.cbu4d94t.ni8dbmo4.eg9m0zos.l9j0dhe7.du4w35lb.ofs802cu.pohlnb88.dkue75c7.mb9wzai9.d8ncny3e.buofh1pr.g5gj957u.tgvbjcpo.l56l04vs.r57mb794.kh7kg01d.c3g1iek1.k4xni2cv .cwj9ozl2>ul`,
        settingLogic: `fb-blur-photo-comments`
      }
    });
  }
  function blurPopularityStats() {
    SimpleMutations_1.blurAllFound({
      queryArgs: {
        cssQuery: `.bp9cbjyn.m9osqain.j83agx80.jq4qci2q.bkfpd7mw.a3bd9o3v.kvgmc6g5.wkznzc2l.oygrvhab.dhix69tm.jktsbyx5.rz4wbd8a.osnr6wyh.a8nywdso.s1tcr66n`,
        settingLogic: `fb-blur-popularity-stats`
      },
      blurArgs: {
        blur: true,
        showText: false
      }
    });
  }
  function blurMarketplaceFeed() {
    SimpleMutations_1.blurAllFound({
      queryArgs: {
        cssQuery: `div[aria-label="Collection of Marketplace items"]`,
        settingLogic: "fb-blur-marketplace-feed"
      }
    });
  }
  function blurWatchFeed() {
    SimpleMutations_1.blurAllFound({
      queryArgs: {
        cssQuery: `#watch_feed`,
        settingLogic: "fb-blur-watch-feed"
      }
    });
  }
  function blurGamingFeed() {
    SimpleMutations_1.blurAllFound({
      queryArgs: {
        cssQuery: `div[aria-label="Gaming"][role="main"]`,
        settingLogic: "fb-blur-gaming-feed"
      }
    });
  }
}

},

// src/content_scripts/common/Utils.ts @2
2: function(__fusereq, exports, module){
const locationChangeEventType = "sober-media-location-change";
function observeUrlChanges(cb) {
  assertLocationChangeObserver();
  window.addEventListener(locationChangeEventType, () => cb(window.location));
  cb(window.location);
}
exports.observeUrlChanges = observeUrlChanges;
function assertLocationChangeObserver() {
  const state = window;
  if (state.soberMediaLocationWatchSetup) {
    return;
  }
  state.soberMediaLocationWatchSetup = true;
  let lastHref = location.href;
  const checkForLocationChanged = () => {
    requestAnimationFrame(() => {
      const currentHref = location.href;
      if (currentHref !== lastHref) {
        lastHref = currentHref;
        window.dispatchEvent(new Event(locationChangeEventType));
      }
    });
  };
  ["popstate", "click", "keydown", "keyup", "touchstart", "touchend", "mousedown", "mouseup"].forEach(eventType => {
    window.addEventListener(eventType, checkForLocationChanged);
  });
  document.addEventListener("DOMContentLoaded", checkForLocationChanged);
  setTimeout(checkForLocationChanged, 250);
}
function setupSingleContentScriptAssert(scriptCheck) {
  const wind = window;
  wind.soberMedia = wind.soberMedia || ({});
  if (wind.soberMedia[scriptCheck]) {
    return false;
  }
  wind.soberMedia[scriptCheck] = true;
  chrome.runtime.onMessage.addListener((request, sender, sendResponse) => {
    if (request && request.checkForContentScript === scriptCheck) {
      sendResponse(true);
    }
  });
  return true;
}
exports.setupSingleContentScriptAssert = setupSingleContentScriptAssert;
function getSetting(id) {
  return new Promise(resolve => {
    chrome.storage.sync.get([id], result => {
      resolve(result[id]);
    });
  });
}
exports.getSetting = getSetting;
function setSetting(id, value) {
  return new Promise(resolve => {
    console.log("Setting ", id, value);
    chrome.storage.sync.set({
      [id]: value
    }, () => {
      resolve();
    });
  });
}
exports.setSetting = setSetting;
;
const settingEffects = new Array();
const settingState = {};
function addSettingEffect(args) {
  setupStorageObserver();
  const effectItem = {
    ...args,
    currentState: false,
    updateLogic: newLog => {
      effectItem.settingLogic = newLog;
      updateSettingEffect(effectItem);
    }
  };
  settingEffects.push(effectItem);
  updateSettingEffect(effectItem);
  return effectItem;
}
exports.addSettingEffect = addSettingEffect;
let _storageObserverSetup = false;
function setupStorageObserver() {
  if (_storageObserverSetup) {
    return;
  }
  _storageObserverSetup = true;
  chrome.storage.sync.get(null, allSettings => {
    Object.entries(allSettings).forEach(([key, value]) => {
      settingState[key] = value;
    });
    settingEffects.forEach(updateSettingEffect);
  });
  chrome.storage.onChanged.addListener(changes => {
    Object.entries(changes).forEach(([settingId, change]) => {
      if (change.newValue === change.oldValue) {
        return;
      }
      settingState[settingId] = change.newValue;
    });
    settingEffects.forEach(updateSettingEffect);
  });
}
function updateSettingEffect(effect) {
  const cleanState = resolveSettingLogic(effect.settingLogic);
  if (cleanState === effect.currentState) {
    return;
  }
  effect.currentState = cleanState;
  if (effect.currentState) {
    effect.effect();
  } else {
    effect.undoEffect();
  }
}
function resolveSettingLogic(logic) {
  if (typeof logic === "string") {
    return !!settingState[logic];
  } else if (typeof logic === "boolean") {
    return logic;
  } else if (("not" in logic)) {
    return !settingState[logic.not];
  }
  const objValue = logic["all"] || logic["any"];
  if (Array.isArray(objValue) === false) {
    return resolveSettingLogic(objValue);
  }
  const arr = objValue;
  if (("all" in logic)) {
    return arr.every(resolveSettingLogic);
  }
  return arr.some(resolveSettingLogic);
}
exports.resolveSettingLogic = resolveSettingLogic;
function findParent(ptr, cssQuery) {
  ptr = ptr.parentElement;
  while (ptr && ptr.matches(cssQuery) === false) {
    ptr = ptr.parentElement;
  }
  return ptr;
}
exports.findParent = findParent;

},

// src/middleware/ContentScriptEnum.ts @3
3: function(__fusereq, exports, module){
var ContentScript;
(function (ContentScript) {
  ContentScript["YOUTUBE"] = "YOUTUBE_CONTENT_SCRIPT";
  ContentScript["FACEBOOK"] = "FACEBOOK_CONTENT_SCRIPT";
  ContentScript["TWITTER"] = "TWITTER_CONTENT_SCRIPT";
  ContentScript["REDDIT"] = "REDDIT_CONTENT_SCRIPT";
  ContentScript["HACKER_NEWS"] = "HACKER_NEWS_CONTENT_SCRIPT";
  ContentScript["TUMBLR"] = "TUMBLR_CONTENT_SCRIPT";
  ContentScript["PINTEREST"] = "PINTEREST_CONTENT_SCRIPT";
  ContentScript["BLUR_LINKS"] = "BLUR_FLAGGED_LINKS_CONTENT_SCRIPT";
  ContentScript["ADD_TIMER"] = "ADD_TIMER_CONTENT_SCRIPT";
})(ContentScript || (ContentScript = {}))
exports.ContentScript = ContentScript;

},

// src/content_scripts/common/SimpleMutations.ts @4
4: function(__fusereq, exports, module){
exports.__esModule = true;
var CssQueryObserve_1 = __fusereq(5);
const unblurOnClickNodes = new Map();
function setElementBlur(domNode, args) {
  if (args.blur === domNode.classList.contains("--blurred")) {
    return;
  }
  domNode.classList.toggle("--blurred", args.blur);
  if (args.blur) {
    domNode.classList.toggle("hasUnblurText", args.showText !== false);
    if (args.unblurOnClick !== false) {
      if (unblurOnClickNodes.has(domNode)) {
        return;
      }
      unblurOnClickNodes.set(domNode);
      const listener = ev => {
        if (domNode.classList.contains("--blurred") === false) {
          return;
        }
        ev.stopImmediatePropagation();
        ev.preventDefault();
        domNode.classList.remove("--blurred");
        unblurOnClickNodes.delete(domNode);
        args.onUnblur && args.onUnblur({
          removeEventListeners: () => {
            domNode.removeEventListener("click", listener);
          }
        });
        return false;
      };
      domNode.addEventListener("click", listener);
    }
    if (args.showText === "ON_HOVER") {
      domNode.classList.add("hideTextUntilHover");
    }
    switch (args.blurStrength) {
      case "LOW":
        domNode.classList.add("lowPowerBlur");
        break;
      case "VERY_LOW":
        domNode.classList.add("veryLowPowerBlur");
        break;
    }
    switch (args.textSize) {
      case "SMALL":
        domNode.classList.add("smallText");
        break;
      case "VERY_SMALL":
        domNode.classList.add("verySmallText");
        break;
    }
  }
}
exports.setElementBlur = setElementBlur;
function blurTogether(args) {
  const {domNodes} = args;
  const listener = ev => {
    ev.stopImmediatePropagation();
    ev.preventDefault();
    for (const domNode of domNodes) {
      domNode.removeEventListener("click", listener);
      setElementBlur(domNode, {
        blur: false
      });
    }
    if (args.blurArgs && args.blurArgs.onUnblur) {
      args.blurArgs.onUnblur({
        removeEventListeners: () => null
      });
    }
  };
  domNodes.forEach(domNode => {
    setElementBlur(domNode, {
      unblurOnClick: false,
      showText: false,
      blur: true,
      ...args.blurArgs
    });
    domNode.addEventListener("click", listener);
  });
}
exports.blurTogether = blurTogether;
const foundDomNodes = new Map();
function blurAllFound(args) {
  CssQueryObserve_1.rebootCssQueryObserve({
    ...args.queryArgs,
    cb: ({domNode, querySetup}) => {
      if (args.checkIgnore && args.checkIgnore(domNode)) {
        return;
      }
      setElementBlur(domNode, {
        blur: true,
        onUnblur: ev => {
          if (args.permaUnblur) {
            ev.removeEventListeners();
            querySetup.removeQuery();
          }
        },
        ...args.blurArgs
      });
    },
    undoCb: ({domNode}) => {
      setElementBlur(domNode, {
        blur: false
      });
    }
  });
}
exports.blurAllFound = blurAllFound;
function hideAllFound(args) {
  CssQueryObserve_1.rebootCssQueryObserve({
    ...args.queryArgs,
    cb: ({domNode}) => {
      domNode = args.transformDomNode ? args.transformDomNode(domNode) : domNode;
      domNode.style.display = "none";
      domNode.style.visibility = "hidden";
    },
    undoCb: ({domNode}) => {
      domNode = args.transformDomNode ? args.transformDomNode(domNode) : domNode;
      domNode.style.display = domNode.style.visibility = "";
    }
  });
}
exports.hideAllFound = hideAllFound;
function blurChildrenAsGroup(args) {
  const groupings = new Map();
  CssQueryObserve_1.rebootCssQueryObserve({
    ...args.queryArgs,
    cb: ({domNode}) => {
      let group = groupings.get(domNode);
      if (group) {
        blurGroup();
        return;
      }
      groupings.set(domNode, group = []);
      observeChildren(domNode, child => {
        if (args.checkBlurChild && args.checkBlurChild(child) === false) {
          return;
        }
        group.push(child);
        blurGroup();
      });
      function blurGroup() {
        blurTogether({
          domNodes: group
        });
      }
    },
    undoCb: ({domNode}) => {
      const group = groupings.get(domNode);
      group && blurTogether({
        domNodes: group,
        blurArgs: {
          blur: false
        }
      });
    }
  });
}
exports.blurChildrenAsGroup = blurChildrenAsGroup;
function observeChildren(root, cb) {
  const mutObserver = new MutationObserver(mutList => {
    for (const mut of mutList) {
      if (mut.type !== 'childList') {
        return;
      }
      for (const child of mut.addedNodes) {
        if (child instanceof HTMLElement) {
          cb(child);
        }
      }
    }
  });
  mutObserver.observe(root, {
    childList: true
  });
  for (const child of root.childNodes) {
    if (child instanceof HTMLElement) {
      cb(child);
    }
  }
  return mutObserver;
}
exports.observeChildren = observeChildren;

},

// src/content_scripts/common/CssQueryObserve.ts @5
5: function(__fusereq, exports, module){
exports.__esModule = true;
var Utils_1 = __fusereq(2);
let defaultLogic;
function setDefaultQueryLogic(logic) {
  defaultLogic = logic;
}
exports.setDefaultQueryLogic = setDefaultQueryLogic;
const existingObservers = new Map();
function rebootCssQueryObserve(args) {
  const root = args.root = args.root || document;
  if (args.settingLogic && defaultLogic) {
    args.settingLogic = {
      all: [args.settingLogic, defaultLogic]
    };
  }
  let observerSetup = existingObservers.get(root);
  if (observerSetup) {
    const querySetup = observerSetup.cssQueries.get(args.cssQuery);
    if (querySetup) {
      if (args.reboot === false) {
        return;
      }
      if (querySetup.disabled) {
        return;
      }
      querySetup.foundElements.forEach(domNode => {
        runQuerySetupOnElement(querySetup, domNode);
      });
      return;
    }
    observerSetup.cssQueries.set(args.cssQuery, initQuerySetup(args));
    return;
  }
  const cssQueries = new Map();
  cssQueries.set(args.cssQuery, initQuerySetup(args));
  const mutObserver = new MutationObserver(mutList => {
    for (const mut of mutList) {
      if (mut.type !== 'childList') {
        return;
      }
      const cssQueryEntries = Array.from(cssQueries.entries());
      cssQueryEntries.forEach(([cssQuery, querySetup]) => {
        if (querySetup.disabled) {
          return;
        }
        for (const child of mut.addedNodes) {
          if (child instanceof HTMLElement) {
            let matches = [];
            if (child.matches(cssQuery)) {
              matches.push(child);
            }
            matches = matches.concat(Array.from(child.querySelectorAll(cssQuery)));
            for (const match of matches) {
              addAddElementToQuerySetup(querySetup, match);
              if (querySetup.disabled) {
                return;
              }
            }
          }
        }
      });
    }
  });
  mutObserver.observe(root, {
    childList: true,
    subtree: true
  });
  existingObservers.set(root, {
    observer: mutObserver,
    cssQueries
  });
}
exports.rebootCssQueryObserve = rebootCssQueryObserve;
function initQuerySetup(args) {
  const out = {
    cb: args.cb,
    undoCb: args.undoCb,
    settingLogic: args.settingLogic,
    foundElements: [],
    disabled: false,
    removeQuery: () => {
      out.disabled = true;
    }
  };
  const currentMatches = (args.root || document).querySelectorAll(args.cssQuery);
  for (const match of currentMatches) {
    addAddElementToQuerySetup(out, match);
  }
  return out;
}
function addAddElementToQuerySetup(querySetup, element) {
  if (querySetup.limit && querySetup.limit <= querySetup.foundElements.length) {
    querySetup.disabled = true;
    return;
  }
  if (querySetup.foundElements.includes(element)) {
    return;
  }
  querySetup.foundElements.push(element);
  runQuerySetupOnElement(querySetup, element);
}
async function runQuerySetupOnElement(querySetup, element) {
  if (!querySetup.settingLogic) {
    querySetup.cb({
      domNode: element,
      querySetup
    });
    return;
  }
  Utils_1.addSettingEffect({
    settingLogic: querySetup.settingLogic,
    effect: () => querySetup.cb({
      domNode: element,
      querySetup
    }),
    undoEffect: () => querySetup.undoCb({
      domNode: element,
      querySetup
    })
  });
}

},

// src/content_scripts/common/TrustCoin.ts @6
6: function(__fusereq, exports, module){
exports.__esModule = true;
var Utils_1 = __fusereq(2);
function renderTrustCoin(props) {
  const trustCoin = document.createElement("div");
  trustCoin.classList.add("TrustCoin");
  trustCoin.title = `Add/Remove Trust for ${props.name}`;
  trustCoin.addEventListener("click", ev => {
    ev.stopImmediatePropagation();
    ev.preventDefault();
    Utils_1.getSetting(props.settingId).then(value => {
      Utils_1.setSetting(props.settingId, !value);
    });
    return false;
  });
  Utils_1.addSettingEffect({
    settingLogic: props.settingId,
    effect: () => {
      trustCoin.classList.add("--trusted");
    },
    undoEffect: () => {
      trustCoin.classList.remove("--trusted");
    }
  });
  return trustCoin;
}
exports.renderTrustCoin = renderTrustCoin;

},

// src/middleware/SettingsId.ts @7
7: function(__fusereq, exports, module){
exports.__esModule = true;
exports.SETTING_STORAGE_ID = "BOOLEAN_SETTINGS";
exports.BLUR_LINK_SETTING_PREFIX = `blur-link-`;
exports.NUM_LINKS_BLURRED_SETTING_PREFIX = `num-links-blurred`;
exports.ADD_TIMER_SETTING_PREFIX = `add-timer-`;
exports.TIME_COUNT_SETTING_PREFIX = `sm-time-count-for-`;
exports.FB_TRUST_SETTING_PREFIX = `fb-grant-trust-`;
exports.TWIT_TRUST_SETTING_PREFIX = `twit-grant-trust-`;
exports.SettingIdList = ["fb-disable", "fb-remove-logo", "fb-trust-tokens", "fb-blur-home-feed", "fb-blur-watch-feed", "fb-blur-marketplace-feed", "fb-blur-gaming-feed", "fb-blur-stories", "fb-blur-untrusted", "fb-blur-post-comments", "fb-blur-photo-comments", "fb-blur-notifications", "fb-blur-tab-gaming", "fb-blur-tab-watch", "fb-blur-tab-groups", "fb-blur-tab-market", "fb-blur-popularity-stats", "twit-disable", "twit-remove-logo", "twit-trust-tokens", "twit-blur-home-screen-feed", "twit-blur-tweet", "twit-blur-trending-now", "twit-blur-who-to-follow", "twit-blur-relevant-people", "ytd-disable", "ytd-remove-logo", "ytd-no-trending", "ytd-blur-home-screen-feed", "ytd-blur-recommendations", "ytd-blur-comments", "hn-disable", "hn-remove-logo", "hn-blur-apple", "hn-blur-fb", "hn-blur-google", "hn-blur-ms", "hn-blur-moz", "hn-blur-twit", "hn-custom-regex", "redd-disable", "redd-remove-logo", "redd-blur-home-feed", "redd-blur-home-posts", "redd-remove-home-sidebar", "redd-blur-sub-suggs", "redd-blur-trending-communities", "pin-disable", "pin-remove-logo", "pin-blur-home-feed", "pin-blur-today-feed", "pin-blur-more-like-feed", "tum-disable", "tum-remove-logo", "tum-blur-dash-feed", "tum-blur-posts", "tum-blur-dash-sidebar", "tum-remove-trending-nav", "sm-show-tab-fb", "sm-show-tab-hn", "sm-show-tab-pin", "sm-show-tab-redd", "sm-show-tab-tumblr", "sm-show-tab-twit", "sm-show-tab-yt", "sm-initialized", "sm-timer-disabled", "sm-link-blurring-disabled", "sm-timer-show-overlay", "add-timer-www.facebook.com", "add-timer-twitter.com", "add-timer-www.youtube.com", "add-timer-news.ycombinator.com", "add-timer-www.reddit.com", "add-timer-www.pinterest.com", "add-timer-www.tumblr.com", "sm-time-count-for-all"];

}
}, function(){
__fuse.r(1)
})