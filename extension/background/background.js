(function() {
  var f = window.__fuse = window.__fuse || {};
  var modules = f.modules = f.modules || {}; f.dt = function (x) { return x && x.__esModule ? x : { "default": x }; };

  f.bundle = function(collection, fn) {
    for (var num in collection) {
      modules[num] = collection[num];
    }
    fn ? fn() : void 0;
  };
  f.c = {};
  f.r = function(id) {
    var cached = f.c[id];
    if (cached) return cached.m.exports;
    var module = modules[id];
    if (!module) {
      
      throw new Error('Module ' + id + ' was not found');
    }
    cached = f.c[id] = {};
    cached.exports = {};
    cached.m = { exports: cached.exports };
    module(f.r, cached.exports, cached.m);
    return cached.m.exports;
  }; 
})();
__fuse.bundle({

// src/background/Background.ts @1
1: function(__fusereq, exports, module){
exports.__esModule = true;
var ContentScriptEnum_1 = __fusereq(2);
var SettingsId_1 = __fusereq(3);
var FindTimerReason_1 = __fusereq(4);
chrome.commands.onCommand.addListener(shortcut => {
  if (shortcut.includes("+M")) {
    chrome.runtime.reload();
  }
});
const injectArgs = [{
  hostname: "www.youtube.com",
  js: "content_scripts/youtube.js",
  contentScriptId: ContentScriptEnum_1.ContentScript.YOUTUBE
}, {
  hostname: "www.facebook.com",
  js: "content_scripts/facebook.js",
  contentScriptId: ContentScriptEnum_1.ContentScript.FACEBOOK
}, {
  hostname: "www.reddit.com",
  js: "content_scripts/reddit.js",
  contentScriptId: ContentScriptEnum_1.ContentScript.REDDIT
}, {
  hostname: "www.pinterest.com",
  js: "content_scripts/pinterest.js",
  contentScriptId: ContentScriptEnum_1.ContentScript.PINTEREST
}, {
  hostname: "twitter.com",
  js: "content_scripts/twitter.js",
  contentScriptId: ContentScriptEnum_1.ContentScript.TWITTER
}, {
  hostname: "www.tumblr.com",
  js: "content_scripts/tumblr.js",
  contentScriptId: ContentScriptEnum_1.ContentScript.TUMBLR
}, {
  hostname: "news.ycombinator.com",
  js: "content_scripts/hacker-news.js",
  contentScriptId: ContentScriptEnum_1.ContentScript.HACKER_NEWS
}];
const disableBlurLinkSettingId = "sm-link-blurring-disabled";
const disableTimerSettingId = "sm-timer-disabled";
chrome.runtime.onInstalled.addListener(function () {
  setupTimeTotalUpdater();
  chrome.tabs.onUpdated.addListener((tabId, changeInfo, tab) => {
    if (changeInfo.status === "loading" && tab.url) {
      const url = new URL(tab.url);
      console.log(url.hostname);
      chrome.storage.sync.get(disableBlurLinkSettingId, obj => {
        if (obj[disableBlurLinkSettingId] !== true) {
          addBlurLinks(tab);
        }
      });
      chrome.storage.sync.get(disableTimerSettingId, obj => {
        if (obj[disableTimerSettingId] !== true) {
          tryAddTimer(tab);
        }
      });
      injectArgs.forEach(inject => {
        if (url.hostname.endsWith(inject.hostname)) {
          chrome.tabs.sendMessage(tabId, {
            checkForContentScript: inject.contentScriptId
          }, resp => {
            if (resp !== true) {
              chrome.tabs.executeScript(tabId, {
                file: inject.js
              });
              chrome.tabs.insertCSS(tabId, {
                file: "sober-media.css"
              });
            }
          });
        }
      });
    }
  });
  chrome.storage.onChanged.addListener(changes => {
    if (changes[disableTimerSettingId] && changes[disableTimerSettingId].newValue === false) {
      chrome.tabs.query({}, tabs => {
        tabs.forEach(tab => tryAddTimer(tab));
      });
    }
    if (changes[disableBlurLinkSettingId] && changes[disableBlurLinkSettingId].newValue === false) {
      chrome.tabs.query({}, tabs => {
        tabs.forEach(tab => addBlurLinks(tab));
      });
    }
    const timerAdded = Object.keys(changes).find(settingId => {
      return settingId.startsWith(SettingsId_1.ADD_TIMER_SETTING_PREFIX);
    });
    if (timerAdded) {
      chrome.storage.sync.get(disableTimerSettingId, obj => {
        if (obj[disableTimerSettingId] !== true) {
          chrome.tabs.query({}, tabs => {
            tabs.forEach(tab => tryAddTimer(tab));
          });
        }
      });
    }
  });
});
function tryAddTimer(tab) {
  FindTimerReason_1.findTimerReason(tab.url).then(reason => {
    if (!reason || !tab.id) {
      return;
    }
    const tabId = tab.id;
    chrome.tabs.sendMessage(tabId, {
      checkForContentScript: ContentScriptEnum_1.ContentScript.ADD_TIMER
    }, resp => {
      if (resp !== true) {
        chrome.tabs.executeScript(tabId, {
          file: "content_scripts/interaction-timer.js"
        });
        chrome.tabs.insertCSS(tabId, {
          file: "timer.css"
        });
      }
    });
  });
}
function addBlurLinks(tab) {
  if (!tab.id) {
    return;
  }
  const tabId = tab.id;
  chrome.tabs.sendMessage(tabId, {
    checkForContentScript: ContentScriptEnum_1.ContentScript.BLUR_LINKS
  }, resp => {
    if (resp !== true) {
      chrome.tabs.executeScript(tabId, {
        file: "content_scripts/blur-links.js"
      });
      chrome.tabs.insertCSS(tabId, {
        file: "sober-media.css"
      });
    }
  });
}
function setupTimeTotalUpdater() {
  const allTimes = {};
  const allId = "sm-time-count-for-all";
  const sumAllTimes = () => {
    let sum = 0;
    Object.entries(allTimes).forEach(([key, value]) => {
      if (key !== allId) {
        sum += value;
      }
    });
    chrome.storage.sync.set({
      [allId]: sum
    });
  };
  chrome.storage.sync.get(null, stor => {
    Object.entries(stor).forEach(([storId, value]) => {
      if (storId.startsWith(SettingsId_1.TIME_COUNT_SETTING_PREFIX) === false) {
        return;
      }
      allTimes[storId] = typeof value === "number" ? value : 0;
    });
    sumAllTimes();
  });
  chrome.storage.onChanged.addListener(changes => {
    let timeAdded = false;
    Object.entries(changes).forEach(([storId, change]) => {
      if (storId.startsWith(SettingsId_1.TIME_COUNT_SETTING_PREFIX) === false) {
        return;
      }
      timeAdded = true;
      const value = change.newValue;
      allTimes[storId] = typeof value === "number" ? value : 0;
    });
    if (timeAdded) {
      sumAllTimes();
      checkForDayChange();
    }
  });
}
function checkForDayChange() {
  const dayRestartId = "sm-next-day-restart";
  chrome.storage.sync.get(dayRestartId, stor => {
    const updateResetTime = () => {
      const nextChange = new Date();
      nextChange.setDate(nextChange.getDate() + 1);
      nextChange.setHours(4);
      chrome.storage.sync.set({
        [dayRestartId]: nextChange.toISOString()
      });
    };
    if (!stor || !stor[dayRestartId]) {
      updateResetTime();
      return;
    }
    const restartTime = new Date(stor[dayRestartId]);
    if (restartTime.getTime() <= Date.now()) {
      chrome.storage.sync.get(null, stor => {
        const update = {};
        Object.entries(stor).forEach(([storId, value]) => {
          if (storId.startsWith(SettingsId_1.TIME_COUNT_SETTING_PREFIX) === false) {
            return;
          }
          update[storId] = 0;
        });
        chrome.storage.sync.set(update);
        updateResetTime();
      });
    }
  });
}

},

// src/middleware/ContentScriptEnum.ts @2
2: function(__fusereq, exports, module){
var ContentScript;
(function (ContentScript) {
  ContentScript["YOUTUBE"] = "YOUTUBE_CONTENT_SCRIPT";
  ContentScript["FACEBOOK"] = "FACEBOOK_CONTENT_SCRIPT";
  ContentScript["TWITTER"] = "TWITTER_CONTENT_SCRIPT";
  ContentScript["REDDIT"] = "REDDIT_CONTENT_SCRIPT";
  ContentScript["HACKER_NEWS"] = "HACKER_NEWS_CONTENT_SCRIPT";
  ContentScript["TUMBLR"] = "TUMBLR_CONTENT_SCRIPT";
  ContentScript["PINTEREST"] = "PINTEREST_CONTENT_SCRIPT";
  ContentScript["BLUR_LINKS"] = "BLUR_FLAGGED_LINKS_CONTENT_SCRIPT";
  ContentScript["ADD_TIMER"] = "ADD_TIMER_CONTENT_SCRIPT";
})(ContentScript || (ContentScript = {}))
exports.ContentScript = ContentScript;

},

// src/middleware/SettingsId.ts @3
3: function(__fusereq, exports, module){
exports.__esModule = true;
exports.SETTING_STORAGE_ID = "BOOLEAN_SETTINGS";
exports.BLUR_LINK_SETTING_PREFIX = `blur-link-`;
exports.NUM_LINKS_BLURRED_SETTING_PREFIX = `num-links-blurred`;
exports.ADD_TIMER_SETTING_PREFIX = `add-timer-`;
exports.TIME_COUNT_SETTING_PREFIX = `sm-time-count-for-`;
exports.FB_TRUST_SETTING_PREFIX = `fb-grant-trust-`;
exports.TWIT_TRUST_SETTING_PREFIX = `twit-grant-trust-`;
exports.SettingIdList = ["fb-disable", "fb-remove-logo", "fb-trust-tokens", "fb-blur-home-feed", "fb-blur-watch-feed", "fb-blur-marketplace-feed", "fb-blur-gaming-feed", "fb-blur-stories", "fb-blur-untrusted", "fb-blur-post-comments", "fb-blur-photo-comments", "fb-blur-notifications", "fb-blur-tab-gaming", "fb-blur-tab-watch", "fb-blur-tab-groups", "fb-blur-tab-market", "fb-blur-popularity-stats", "twit-disable", "twit-remove-logo", "twit-trust-tokens", "twit-blur-home-screen-feed", "twit-blur-tweet", "twit-blur-trending-now", "twit-blur-who-to-follow", "twit-blur-relevant-people", "ytd-disable", "ytd-remove-logo", "ytd-no-trending", "ytd-blur-home-screen-feed", "ytd-blur-recommendations", "ytd-blur-comments", "hn-disable", "hn-remove-logo", "hn-blur-apple", "hn-blur-fb", "hn-blur-google", "hn-blur-ms", "hn-blur-moz", "hn-blur-twit", "hn-custom-regex", "redd-disable", "redd-remove-logo", "redd-blur-home-feed", "redd-blur-home-posts", "redd-remove-home-sidebar", "redd-blur-sub-suggs", "redd-blur-trending-communities", "pin-disable", "pin-remove-logo", "pin-blur-home-feed", "pin-blur-today-feed", "pin-blur-more-like-feed", "tum-disable", "tum-remove-logo", "tum-blur-dash-feed", "tum-blur-posts", "tum-blur-dash-sidebar", "tum-remove-trending-nav", "sm-show-tab-fb", "sm-show-tab-hn", "sm-show-tab-pin", "sm-show-tab-redd", "sm-show-tab-tumblr", "sm-show-tab-twit", "sm-show-tab-yt", "sm-initialized", "sm-timer-disabled", "sm-link-blurring-disabled", "sm-timer-show-overlay", "add-timer-www.facebook.com", "add-timer-twitter.com", "add-timer-www.youtube.com", "add-timer-news.ycombinator.com", "add-timer-www.reddit.com", "add-timer-www.pinterest.com", "add-timer-www.tumblr.com", "sm-time-count-for-all"];

},

// src/middleware/FindTimerReason.ts @4
4: function(__fusereq, exports, module){
exports.__esModule = true;
var SettingsId_1 = __fusereq(3);
async function findTimerReason(href) {
  if (!href) {
    return;
  }
  return new Promise(resolve => {
    chrome.storage.sync.get(null, settings => {
      const addTimerReason = Object.keys(settings).find(settingId => {
        if (settingId.startsWith(SettingsId_1.ADD_TIMER_SETTING_PREFIX) === false) {
          return false;
        }
        if (settings[settingId] !== true) {
          return false;
        }
        const host = settingId.replace(SettingsId_1.ADD_TIMER_SETTING_PREFIX, "");
        return href.includes(host);
      });
      resolve(addTimerReason);
    });
  });
}
exports.findTimerReason = findTimerReason;
async function findPotentialTimerReasons(href) {
  if (!href) {
    return;
  }
  return new Promise(resolve => {
    chrome.storage.sync.get(null, settings => {
      const addTimerReason = Object.keys(settings).filter(settingId => {
        if (settingId.startsWith(SettingsId_1.ADD_TIMER_SETTING_PREFIX) === false) {
          return false;
        }
        const host = settingId.replace(SettingsId_1.ADD_TIMER_SETTING_PREFIX, "");
        return href.includes(host);
      });
      resolve(addTimerReason);
    });
  });
}
exports.findPotentialTimerReasons = findPotentialTimerReasons;

}
}, function(){
__fuse.r(1)
})